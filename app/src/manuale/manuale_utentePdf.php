<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("../inc/conn.php");
require_once("../inc/layouts.php");
require_once("../inc/func.browser_detection.php");
proteggi(1);

echo "<body>";
echo "<script src=\"".FRONT_DOCROOT."/js/scheda.js\"></script>";
echo "<script src=\"".FRONT_DOCROOT."/js/jquery/jquery.query.js\"></script>";
echo "<script src=\"".FRONT_DOCROOT."/js/jquery/jquery.min.js\"></script>";
echo "</body>";


 
$files[]="sty/filetree.css";

echo "<body> "
. " <div class=\"riga1\">";

echo openLayout1(_("Manuale utente Pdf"),$files,'popup');
echo breadcrumbs(array("HOME",_("Manuale Utente Pdf")." ". $tabellapartenza));

echo "<br>";
echo "<h1 class=\"titoloScheda\" >"."Manuale Utente"."</h1></div></div>";

echo "
  
   <div class=\"riga2\">    
      <div class=\"colonna1\">    
        <div id=\"container\" ></div>
      </div> 
    
      <div class=\"colonna2\">
        <iframe id=\"showPdf\" src=\"\" ></iframe>
      </div>   
  </div> 
      
</body>
</html>";

?>

<script type="text/javascript" >
$(document).ready( function() {

	$( '#container' ).html( '<ul class="filetree start"><li class="wait">' + 'Generating Tree...' + '<li></ul>' );
	
	getfilelist( $('#container') , 'data' );
	
	function getfilelist( cont, root ) {
	
		$( cont ).addClass( 'wait' );
			
		$.post( 'Folder_tree.php', { dir: root }, function( data ) {
	
			$( cont ).find( '.start' ).html( '' );
			$( cont ).removeClass( 'wait' ).append( data );
			if( 'Sample' == root ) 
				$( cont ).find('UL:hidden').show();
			else 
				$( cont ).find('UL:hidden').slideDown({ duration: 500, easing: null });
			
		});
	}
	
	$( '#container' ).on('click', 'LI A', function() {
		var entry = $(this).parent();
		
		if( entry.hasClass('folder') ) {
			if( entry.hasClass('collapsed') ) {
						
				entry.find('UL').remove();
				getfilelist( entry, escape( $(this).attr('rel') ));
				entry.removeClass('collapsed').addClass('expanded');
			}
			else {
				
				entry.find('UL').slideUp({ duration: 500, easing: null });
				entry.removeClass('expanded').addClass('collapsed');
			}
		} else {
			$( '#selected_file' ).text( "File:  " + $(this).attr( 'rel' ));
                        $( '#nomeFile' ).text($(this).attr( 'rel' ));
                        document.getElementById("showPdf").src = ($(this).attr( 'rel' ));
		}
	return false;
	});
	
});
</script>