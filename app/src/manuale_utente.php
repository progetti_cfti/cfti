<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo "<body>";
echo "<script src=\"js/scheda.js\"></script>";
echo "<script src=\"js/jquery/jquery.query.js\"></script>";
echo "<script src=\"js/jquery/jquery.min.js\"></script>";
echo "</body>";

require_once("./inc/conn.php");
require_once("./inc/layouts.php");
require_once("./inc/func.browser_detection.php");
 
$files[]="sty/scheda.css";


//$Scheda = new Scheda($oid);
//$nome_tab = $Scheda->table_name();


echo openLayout1(_("Manuale utente"),$files,'popup');
echo breadcrumbs(array("HOME",_("Manuale Utente")." ". $tabellapartenza));


echo "<h1 class=\"titoloScheda\" >"."Manuale Utente"."</h1>";

echo "<br>";

/*
echo "L'interfaccia è organizzata in Tabelle e Viste.
Ogni vista è organizzata in modo da mostrare solo i campi di interesse, tra tutti quelli della tabella corrispondente; la vista inoltre, mostra al posto del numero di id (identificativo univoco del record) dei record delle tabelle collegate, i campi stessi, recuperati dalla tabella collegata.";
*/
echo "<a href=\"manuale_utente.php?tabella=info_manuale\">Informazioni sul Manuale</a>";
echo "<br>";
echo "<br>";
echo "<a href=\"manuale_utente.php?tabella=viste_tabelle\">Tabelle e Viste</a>";
echo "<br>";
echo "<br>";
echo "<a href=\"manuale_utente.php?tabella=tutte_le_tabelle\">Manuale delle viste</a>";
echo "<br>";
echo "<br>";
echo "<a href=\"manuale_utente.php?tabella=vcommenti\">Manuale della vista vcommenti</a>";
echo "<br>";
echo "<br>";
echo "<a href=\"manuale_utente.php?tabella=vschedeb\">Manuale della vista vschedeb</a>";


if(isset($_GET['tabella'])){
    
    if($_GET['tabella'] == "info_manuale"){
            
            echo "<br>";
            echo "<br>";

            echo "-------------------------------------------------------------";
            
            echo "<br>";
            echo "<br>";
            
            echo "<a href=\"manuale_utente.php?azione=accesso_manuale\">Come accedere al manuale</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=consultare_manuale\">Come consultare il manuale</a>";
            echo "<br>";

        }else if($_GET['tabella'] == "viste_tabelle"){
            
            echo "<br>";
            echo "<br>";

            echo "-------------------------------------------------------------";
            
            echo "<br>";
            echo "<br>";
            echo "<b class=\"manuale\">Tabelle e Viste:</b>";
            echo "<br>";
            echo "<br>";
            
            echo "<p class=\"manuale\">L'interfaccia è organizzata in Tabelle e Viste accessibili dalla schermata del Menù principale. Una tabella è corrispondente all'archivio in questione, rappresenta l'oggetto principale del database "
            . "e contiene tutti i dati relativi alla scheda in questione. La vista , invece, è un oggetto rielaborato a partire dalla tabella corrispondente, in modo"
            . " da risultare di più facile e immediata consultazione. "
            . "Nella colonna di sinistra si trova l'elenco delle Tabelle disponibili, nella colonna di destra l'elenco "
                    . "delle Viste. Il nome delle tabelle è sempre costituito dal nome della scheda relativa, preceduta da una "
                    . "'f'. Nel nome della vista associata, invece, la 'f' viene sostituita dalla lettera 'v': ";
            echo "<br>";
            echo "<br>";
            echo "<img src=\"immagini_manuale/tabelle_viste.JPG\" alt=\"immagine\">";
            echo "<br>";
            echo "<br>";
            echo "<p class=\"manuale\">Una tabella è corrispondente all'archivio in questione, rappresenta l'oggetto principale del database "
            . "e contiene tutti i dati relativi alla scheda in questione. Le tabelle non sono ottimizzate per l'utilizzo "
                    . "dell'utente e rappresentano il dato grezzo, non sempre facilmente consultabile. In "
                    . "molte tabelle il collegamento a tabelle esterne è fatto attraverso una chiave esterna, costituita da"
                    . " un numero. Nell'esempio seguente la tabella fscheda1. Come si vede, l'ultimo campo, "
                    . "'fnloc', è un numero che identifica l'id di una località associata al record della tabella "
                    . "fscheda1 selezionato. ";
            echo "<br>";
            echo "<br>";
            echo "<img src=\"immagini_manuale/tabelle_viste1.JPG\" alt=\"immagine\">";
            echo "<br>";
            echo "<br>";
            echo "<p class=\"manuale\">La vista , invece, è un oggetto rielaborato a partire dalla tabella corrispondente, in modo"
            . " da risultare di più facile e immediata consultazione. Non sempre nella vista si trovano tutti i campi che"
                    . " ci sono nella tabella corrispondente, e spesso il loro nome è diverso per essere più leggibile."
                    . " Inoltre nella vista la chiave esterna, ovvero il numero che rappresenta l'id del record collegato al"
                    . " record selezionato della tabella in questione, è sostituita dai campi della tabella collegata di maggiore"
                    . " interesse. Nell'esempio seguente, si può vedere come il campo fnloc, rappresentante l'id "
                    . "della località collegata al record selezionato della vscheda1, è sostituito dal codice toponimo e nome"
                    . " toponimo dello stesso record. ";
            echo "<br>";
            echo "<br>";
            echo "<img src=\"immagini_manuale/tabelle_viste2.JPG\" alt=\"immagine\">";
    
        }else if($_GET['tabella'] == "tutte_le_tabelle"){
            
            echo "<br>";
            echo "<br>";

            echo "-------------------------------------------------------------";
            
            echo "<br>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=grid_view\">Grid View/Form View</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=titoli_cliccabili\">Titoli Cliccabili</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=tabelle_collegate\">Tabelle Collegate</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=nuovo_record\">Nuovo Record</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=modifica\">Modifica</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=ricerca\">Ricerca</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=import\">Import data</a>";
            echo "<br>";  
            echo "<a href=\"manuale_utente.php?azione=export\">Export data</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=edit\">Edit data</a>";
            echo "<br>";
            
    
        }else if($_GET['tabella'] == "vcommenti"){
            
            echo "<br>";
            echo "<br>";
            
            echo "-------------------------------------------------------------";
            echo "<br>";
            echo "<br>";
            echo "<p class=\"manuale\">Manuale della vista 'vcommenti'."
            . "<br><br>La vista 'vcommenti' presenta un manuale specifico perchè rispetto alle altre viste presenta"
                    . " delle funzioni aggiuntive (come ad esempio l'import dei commenti e i codici bibliografici"
                    . " cliccabili. Di seguito l'indice del manuale: ";

            echo "<br>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=grid_view_commenti\">Grid View/Form View</a>";
            echo "<br>"; 
            echo "<a href=\"manuale_utente.php?azione=titoli_cliccabili_commenti\">Titoli Cliccabili</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=nuovo_record_commenti\">Nuovo Record</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=modifica_commenti\">Modifica</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=ricerca_commenti\">Ricerca</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=importCommenti\">Import Data</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=import_commenti\">Import Testo Commento</a>";
            echo "<br>";       
            echo "<a href=\"manuale_utente.php?azione=export_commenti\">Export</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=edit\">Edit data</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=pdf_codici_biblio\">Pdf dei Codici Bibliografici</a>";
            echo "<br>";
            
    
        }else if($_GET['tabella'] == "vschedeb"){
            
            echo "<br>";
            echo "<br>";
            echo "-------------------------------------------------------------";
            echo "<br>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=grid_view\">Grid View/Form View</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=titoli_cliccabili\">Titoli Cliccabili</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=tabelle_collegate\">Tabelle Collegate</a>";
             echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=nuovo_record\">Nuovo Record</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=modifica\">Modifica</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=ricerca_commenti\">Ricerca</a>";
             echo "<br>";  
            echo "<a href=\"manuale_utente.php?azione=import_testiB\">Import Data</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=import_testiB\">Import TestiB</a>";
            echo "<br>";    
            echo "<a href=\"manuale_utente.php?azione=export\">Export data</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=edit\">Edit data</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=pdf_schedeb\">Pdf associati</a>";
            echo "<br>";
           
    
        }
}




 if(isset($_GET['azione'])){
     if($_GET['azione'] == "titoli_cliccabili_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Titoli Cliccabili:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">All'interno di ogni vista ci sono due tipi di Titoli Cliccabili. "
                    . "Il titolo principale di colore verde, se cliccato apre la pagina della tabella corrispondente alla "
                            . "vista in questione. Il titolo colorato in blu, apre invece la vista relativa al titolo cliccato."
                            . " (Vedi la sezione 'Viste e Tabelle' del Manuale).";
                    echo "<br>";
                    echo "<br>";
                    echo "<a href=\"manuale_utente.php?azione=titolo_verde_commenti\">Titolo cliccabile Verde</a>";
            echo "<br>";
            echo "<a href=\"manuale_utente.php?azione=titolo_blu_commenti\">Titolo cliccabile Blu</a>";
            
     
                }else if($_GET['azione'] == "titolo_blu_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Titolo Cliccabile Blu:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">All'interno di una qualsiasi vista, compresa la vista 'vcommenti', "
                    . "i titoli delle diverse sezioni "
                    . "colorati in blu sono cliccabili, e rimandano al record selezionato della vista relativa "
                            . "(l'esempio seguente è relativo alla vista vcommenti).";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/titoli_cliccabili.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccando sul titolo blu, si aprirà la vista relativa al titolo cliccato,"
                    . " che mostra il record selezionato. Nell'esempio cliccando sul titolo 'Categoria' si "
                            . "aprirà il record relativo alla categoria 'A0'"
                            . " nella vista delle Categorie Commento, vtabcatego:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/titoli_cliccabili2.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "titolo_blu"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Titolo Cliccabile Blu:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">All'interno di una vista, i titoli delle diverse sezioni "
                    . "colorati in blu sono cliccabili, e rimandano al record selezionato della vista relativa "
                            . "(l'esempio seguente è relativo alla vista vcommenti).";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/titoli_cliccabili.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccando sul titolo blu, si aprirà la vista relativa al titolo cliccato,"
                    . " che mostra il record selezionato. Nell'esempio cliccando sul titolo 'Categoria' si "
                            . "aprirà il record relativo alla categoria 'A0'"
                            . " nella vista delle Categorie Commento, vtabcatego:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/titoli_cliccabili2.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "titolo_verde_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Titolo Cliccabile Verde:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">In tutte le viste, compresa quella dei Commenti"
                    . " il titolo principale (di colore verde) è cliccabile"
                    . " e rimanda alla pagina della tabella corrispondente, in questo caso la 'fcommenti'. Nello specifico"
                            . " cliccando sul titolo si aprirà il record selezionato nella tabella fcommenti.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/verde1_1.JPG\" alt=\"immagine\">";   
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/verde2.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "titolo_verde"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Titolo Cliccabile Verde:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">In tutte le viste il titolo principale (di colore verde) è cliccabile"
                    . " e rimanda alla pagina della tabella corrispondente:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/verde1.JPG\" alt=\"immagine\">";   
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/verde2.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "accesso_manuale"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Accesso al Manuale:</b>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Inserire mail e password nella schermata di login:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/index.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Verrà aperta la pagina del Menù principale: ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/menu.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Per aprire il Manuale, cliccare sul tasto 'Manuale Utente', in alto a destra nel Menù Principale:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/menu1.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "consultare_manuale"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Come consultare il Manuale Utente:</b>";
                    echo "<br>";
                    echo "<p class=\"manuale\">La pagina Menù del Manuale Utente contiene un indice con le "
                    . "seguenti voci:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/manuale0.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccando sul primo titolo è possibile ottenere informazioni sul come accedere e come consultare "
                    . "il Manuale Utente:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/manuale1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "La voce 'Manuale delle viste' reindirizza al Manuale delle viste, che contiene tutte le informazioni"
                    . " per navigare e utilizzare le funzioni di tutte le viste ad eccezione delle viste 'vcommenti' e "
                            . "'vschedeb', che avendo delle funzioni particolari e differenti rispetto a tutte le altre, "
                            . "vengono trattate nelle voci seguenti dell'indice ('Manuale della vista vcommenti' e "
                            . "'Manuale della vista vschedeb') .  ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/manuale2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Da una qualsiasi pagine del Manuale Utente è possibile tornare al Manù Principale dell'Interfaccia "
                    . "cliccando su 'Home' come mostrato di seguito:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/home.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "edit"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Edit Dati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">E' possibile modificare dei record di una vista tramite file cliccando su "
                    . "'Edit data'. L'esempio seguente prende in esame la vista vscheda1:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/edit.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Dopo aver selezionato il file come mostrato nell'immagine precedente,"
                    . " si aprirà la finestra di edit, dalla quale sarà possibile modificare il separatore "
                            . "dei campi in base a quello utilizzato nel file. "
                            . "E' necessario che il file utilizzato per l'edit, abbia come prima colonna quella attarverso la quale"
                            . "verrà agganciato il record (ovvero la condizione where). Nell'esempio in questione partendo dalla "
                            . "vista vscheda1, si andranno a modificare tramite file dei record della tabella corrisondente"
                            . " fscheda1. Nell'esempio viene modificato il record con id 45884. Il file contiene proprio l'id"
                            . " come primo campo. E' necessario utilizzare un campo univoco per effettuare la modifica sui record.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/edit2.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "ricerca"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Ricerca:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Cliccare sul tasto 'Search' per entrare in modalità"
                    . " ricerca. (Nel caso dell’esempio che segue la ricerca è effettuata nella vista vscheda1)";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/ricerca_semplice.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<br>";
                    echo "<br>";
                    echo "Sono possibili diverse modalità di ricerca elencate di seguito:<br><br>"
                            ."- ricerca semplice: si effettua digitando il o i termini di ricerca all'interno del o dei campi relativi."
                            
                    . " La ricerca non è case-sensitive, cioè non risente di maiuscole o minuscole e restituirà i record che nel campo selezionato "
                            . "contengono solo ed esattamente la parola ricercata. Questo tipo di ricerca è disponibile per tutti i campi"
                            . " e per tutte le tabelle e viste. <br>"
                            ."- ricerca semplice sui testi: si effettua digitando la parola di ricerca. La ricerca restituirà non solo i record che "
                            . "contengono nel campo di ricerca la parola inserita, ma anche la parola declinata al plurale. Questo tipo di ricerca è disponibile nel solo caso dei campi 'testo commento' (per 
                                la vista vcommenti) e 'testob' (per la vista vschedeb).<br> "
                            ."- ricerca con asterischi: utilizzando la notazione con gli asterischi prima e dopo "
                            . "la parola di ricerca è possibile ricercare i record che nel campo in questione contengono "
                            . "esattamente il termine inserito all'interno degli asterischi. Esempio: *terminedacercare*. 
                                Questo tipo di ricerca è disponibile per tutti i campi e per tutte le tabelle e viste. <br>
                                - ricerca con operatore and (&): si effettua utilizzando il simbolo '&' come operatore 'and' per cercare un testo che contenga
                                sia la parola1 che la parola2. Esempio: parola1&parola2. Questo tipo di ricerca è disponibile nel solo caso dei campi 'testo commento' (per 
                                la vista vcommenti) e 'testob' (per la vista vschedeb).<br>"
                                ."- ricerca con operatore or (|): si effettua utilizzando il simbolo '|' come operatore 'or',
                                per cercare la parola1 oppure la parola2. Esempio: parola1|parola2.  Questo tipo di ricerca è disponibile nel solo caso dei campi 'testo commento' (per 
                                la vista vcommenti) e 'testob' (per la vista vschedeb).<br>".
                                "- ricerca con negazione (!): si effettua utilizzando il simbolo '!', come negazione, per cercare
                                testi che non contengono la parola di ricerca. Esempio: !parola. Questo tipo di ricerca è disponibile nel solo caso dei campi 'testo commento' (per 
                                la vista vcommenti) e 'testob' (per la vista vschedeb).<br>
                                - ricerca con distanza n (<n>): si effettua utilizzando il simbolo '<n>' per cercare 
                                due parole che hanno una distanza inferiore ad n. Esempio: parola1<3>parola2, permetterà
                                la ricerca dei testi che contengono la parola1 e parola2 ad una distanza minore di 3 caratteri. Questo tipo di ricerca è disponibile nel solo caso dei campi 'testo commento' (per 
                                la vista vcommenti) e 'testob' (per la vista vschedeb).<br><br>
                                Per avviare la ricerca cliccare 
                                ‘Start search’.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/ricerca_semplice2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Se la ricerca ha più di un risultato, sarà mostrata una tabella contenente tutti i risultati di ricerca."
                    . " Cliccando su 'Export results' è possibile esportare la tabella (vedi sezione 'Export data').";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/ricerca_semplice3.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "export_testo_commento"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Testo Commento:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">"
                    . "Dalla vista 'vcommenti' è possibile effettuare l'export del solo campo 'testo commento'. E' possibile"
                            . " effettuare l'export di tutti i testi presenti dalla schermata di default della vista 'vcommenti',"
                            . " ovvero nella modalità di visualizzazione 'Form View' (vedi sezione 'Grid View/Form View' del manuale"
                            . " utente). E' anche possibile esportare solo i testi ottenuti come risultati di una ricerca (vedi punto"
                            . " 2):"
                            . "<br><br>1. Export di tutti i testi. Dalla modalità di visualizzazione 'Form View' cliccare sul "
                            . "tasto 'Export Tutto Testi Commenti'. Seguire poi i passaggi già descritto nella sezione 'Export di tutti "
                            . "i dati'. "
                            . " ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_testoCommento.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\"><br><br>2. Export dei testi risultato di una ricerca: dopo aver effettuato"
                    . " una ricerca (vedi sezione 'Ricerca' ed aver ottenuto la tabella dei risultato cliccare sul tasto"
                            . " 'Export Commenti'";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_testoCommento2.JPG\" alt=\"immagine\">";
                    
     
                }else if($_GET['azione'] == "export_tutto_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Totale Dati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">"
                    . "Partendo dalla vista 'vcommenti' è possibile effettuare l'export di tutti i record della tabella"
                            . " 'fcommenti' cliccando su 'Export Data':";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tuttoCommenti.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">E' possibile scegliere il formato di export tra quelli mostrati di seguito:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tuttoCommenti1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">E' possibile decidere se aprire il file o scaricarlo. ";
                    echo "<p class=\"manuale\">Il file scaricato sarà disponibile nella cartella 'Download' del proprio"
                    . " pc.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tuttoCommenti2.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "export_tutto"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Totale Dati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">"
                    . "Partendo da una vista (o una tabella) cliccare su 'Export Data' per effettuare l'export di tutti i record "
                            . "della vista o della tabella selezionata: (nel caso dell’esempio che segue si effettua un export dei record"
                            . " della vista vscheda1):";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tutto.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tutto1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Il file scaricato sarà disponibile nella cartella 'Download' del proprio"
                    . " pc.";
     
                }else if($_GET['azione'] == "export_selezione_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Record Commenti Selezionati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Questa procedura consente di esportare i record ottenuti come risultato"
                    . " di una ricerca."
                    . "<br><br>Dopo aver effettuato una ricerca con 'Search' (vedere sezione 'Ricerca') e "
                            . "cliccato su 'Export Results' "
                            . " è possibile esportare i record selezionati.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_selezioneCommenti.JPG\" alt=\"immagine\">";
                    
     
                }else if($_GET['azione'] == "export_selezione"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Dati Selezionati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Procedura che consente di esportare i record selezionati con una ricerca"
                    . ". Dopo aver effettuato una ricerca con 'Search' (vedere sezione 'Ricerca') e cliccato su 'Export Results' "
                            . " è possibile esportare i record selezionati."
                            . " Nell'esempio di seguito si esportano i dati ottenuti da una ricerca sulla "
                            . "vista vscheda1:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_selezione.JPG\" alt=\"immagine\">";
                    
     
                }else if($_GET['azione'] == "export_tabelle_collegate"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Tabelle Collegate:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">La procedura consente l'esportazione dei record di una "
                    . "tabella esterna che sono collegati ad un record specifico della vista o tabella principale. "
                    . "Dopo aver ottenuto la lista dei record di una tabella esterna collegati al record"
                    . "selezionato (vedi sezione 'Tabelle Collegate' è possibile effettuare l'export dei dati ottenuti cliccando sul tasto"
                            . "'Export Results'. L'esempio seguente è fatto a partire dalla vista vclassif:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tabelle_collegate1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/export_tabelle_collegate2.JPG\" alt=\"immagine\">";
                    
     
                }else if($_GET['azione'] == "importCommenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Import Dati Commenti:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Da qualsiasi vista, compresa la vista 'vcommenti' è possibile effettuare l'import di dati"
                    . " da file. Attraverso la procedura descritta di seguito i dati presenti nel file di import"
                            . " vengono importati nella tabella corrispondente alla vista in questione. "
                    . "Nel caso della vista 'vcommenti' i dati presenti nel file di import verranno importati"
                            . " nella tabella 'fcommenti'; la vista 'vcommenti' sarà aggiornata in modo automatico. "
                            . "(Vedi sezione Tabelle e Viste del Manuale Utente). Il file di import deve preferibilmente avere la stessa "
                            . "struttura (nei suoi campi) della tabella 'fcommenti'. E' anche possibile importare i dati da un file"
                            . " che ha i campi ordinati in modo diverso dai campi della tabella 'fcommenti', come sarà mostrato più avanti. "
                            . " Nella vista 'vcommenti' sono possibili due tipi di import da file."
                            . " Il primo è relativo all'import di tutti i campi della tabella 'fcommenti' escluso il campo "
                            . "'testocomm' (corrispondente al campo 'testo commento' della vista); il secondo tipo di import"
                            . " è relativo al solo campo 'testocomm' della tabella 'fcommenti', ovvero 'testo commento' della 'vcommenti'."
                            . " Quest'ultima tipologia di import viene trattata nella sezione 'Import testo commento' del manuale utente."
                            . "<br><br>Per iniziare la procedura di import da file cliccare sul tasto 'Import data'."
                            . " Si aprirà la schermata 'Data Import': ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/importCommenti.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/importCommenti2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\">Dopo aver cliccato sul tasto 'Browse' e selezionato il file da importare,"
                    . " cliccando sul tasto 'Next' è possibile passare alla seconda fase della procedura di import."
                             . " Si aprirà una schermata di Preview che consente di visualizzare i primi 30 record del"
                             . " file. La prima operazione da fare è cambiare il separatore dei campi selezionando"
                             . " quello utilizzato nel file per visualizzarli correttamente: ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/importCommenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Se nel file la prima riga è costituita dal nome dei campi, selezionare 'Skip the first line' per saltare l'import"
                    . " della prima riga: ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/importCommenti3_1.JPG\" alt=\"immagine\">";                                 
                     echo "<br>";
                    echo "<br>";
                    echo "Se l'ordine dei campi nel file di import corrisponde esattamente all'ordine dei campi nella tabella "
                    . "'fcommenti', allora sarà possibile passare direttamente alla schermata successiva, tramite il tasto 'Next'. "
                            . "Se invece, c'è un disallineamento tra campi del file e i campi dell'intestazione della tabella ("
                            . "che sono ordinati secondo l'ordine dei campi della 'fcommenti'), oppure si desidera saltare l'import"
                            . " di alcuni campi, è possibile farlo, cliccando sull'intestazione della tabella. Scegliendo l'opzione"
                            . " 'No such field' è possibile non importare quello specifico campo, scegliendo invece il nome di un altro campo"
                            . " è possibile inserire quel campo del file in un altro campo della tabella 'fcommenti'."
                            . "<br><br>Il campo id è impostato su 'Autoicrement' di default; questo significa che anche se presenti dei valori"
                            . " nel campo id del file, questi non saranno importati e il campo id verrà aggiornato in automatico. "
                            . "Per utiizzare i valori del file come id, cliccare sull'intestazione e selezionare la voce 'id'. "
                            . "Da notare che è consigliabile"
                            . " mantenere l'impostazione di dafault 'Autoincrement'."
                    ;
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/importCommenti4.JPG\" alt=\"immagine\">";
                     echo "<br>";
                    echo "<br>";
                    echo "Cliccare su next. La schermata successiva consente di fare alcune operazioni sul campo desiderato (ad esempio"
                    . " trasformare il testo in maiuscolo). "
                            . "";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import4.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Se non si desidera fare operazioni particolari sui campi cliccare su 'Next'. La schermata che si apre consente di effettuare l'import "
                    . "e vedere in tempo reale lo stato dell'operazione e gli eventuali errori. Alla fine della procedura, "
                            . "verrà inviata sulla propria casella di posta elettronica una mail contenente un file di log "
                            . "con i record non importati e i relativi errori. E' possibile correggere il file di Log ricevuto e re-importarlo con le correzioni effettuate. "
                            ;
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import5.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/importCommenti5.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "import"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Import Dati :</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Da una qualsiasi vista (nel caso dell’esempio che segue la vscheda1) cliccare sul tasto 'Import data' per avviare la procedura guidata di import da File. ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import0.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\">Si aprirà la schermata 'Data Import'. Cliccare su 'Browse' per importare il file e poi su 'Next': ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Selezionare il tipo di separatore utilizzato per visualizzare correttamente l'anteprima dei record:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import2.JPG\" alt=\"immagine\">";
                     echo "<br>";
                    echo "<br>";                  
                    echo "Se nel file sono presenti i nomi dei campi, selezionare 'Skip the first line' per saltare l'import"
                    . " della prima riga: ";
                     echo "<br>";
                    echo "<br>";
                     echo "<img src=\"immagini_manuale/Import2_2.JPG\" alt=\"immagine\">";                  
                     echo "<br>";
                    echo "<br>";
                    echo "Il menu a tendina nell'intestazione della tabella consente di non importare quella colonna "
                    . "cliccando su 'No such field', oppure di inserire quella colonna in un campo diverso da quello di default."
                            . " Questo è utile nei casi in cui si disponga di un file di import non ordinato secondo l'ordine di defalut"
                            . " della tabella nel database. ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import3.JPG\" alt=\"immagine\">";
                     echo "<br>";
                    echo "<br>";
                    echo "Cliccare su next. La schermata successiva consente di fare alcune operazioni sul campo desiderato (ad esempio"
                    . " trasformare il testo in maiuscolo). "
                            . "";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import4.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Se non si desidera fare operazioni particolari sui campi cliccare su 'Next'. La schermata che si apre consente di effettuare l'import "
                    . "e vedere in tempo reale lo stato dell'operazione e gli eventuali errori. Alla fine della procedura, "
                            . "verrà inviata sulla propria casella di posta elettronica una mail contenente un file di log "
                            . "con i record non importati e i relativi errori. E' possibile correggere il file di Log ricevuto e re-importarlo con le correzioni effettuate. "
                            ;
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import5.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "import_commenti"){
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Import Testo Commento:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Come visto nella sezione 'Import data' del manuale della vista 'vcommenti',"
                    . " la procedura che si avvia tramite il tasto 'Import data' consente di importare tutti i campi"
                            . " della tabella 'fcommenti', eccetto il campo 'testocomm' che corrisponde al campo 'testo commento"
                            ."' della vista 'vcommenti'. Una volta effettuato l'import dei commenti se i vuole importare il testo"
                            . " dei commenti, è necessario avviare la procedura di 'Import testi Commenti', descritta di seguito."
                            . "<br><br>Il file di import deve essere un file di testo con estensione '.txt', e deve avere una "
                            . "forma particolare. Ogni testo viene preceduto da una riga che ha la seguente sintassi:"
                            . " '$$id*nperiod*codiceCategoriaCommento'. L'aggancio tra un testo e il record commento che deve"
                            . " contenere tale 'testo commento' è il campo 'id'. Per l'inserimento del testo commento è sufficiente anche"
                            . " il solo inserimento dell'id con la seguente sintassi: '$$id**'"
                    . "<br><br>Per effettuare l'import del testo dei commenti cliccare su 'Import Testo Commenti: '";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Nella finesta che si apre cliccare su 'Browse' per selezionare il file e in seguito su 'Next'. "
                    . "Attendere il caricamenti della preview dei dati.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccare su 'Next':";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccare su 'Import' per procedere con l'Import dei Testi: ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti4.JPG\" alt=\"immagine\">";
                }else if($_GET['azione'] == "import_testiB"){
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Import TestiB:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Per effettuare l'import del testo cliccare su 'Import TestiB': ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_testiB1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Nella finesta che si apre cliccare su 'Browse' per selezionare il file e in seguito su 'Next'. "
                    . "Attendere per la preview dei dati.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccare su 'Next':";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccare su 'Import' per procedere con l'Import dei Testi: ";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/import_commenti4.JPG\" alt=\"immagine\">";
                }
                else if($_GET['azione'] == "tabelle_collegate"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Visualizzazione Tabelle Collegate :</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Subito sotto il titolo di ciascuna vista si trova la tendina "
                    . "delle Tabelle Collegate che consente di ottenere tutti i record della tabella selezionata tra le voci "
                            . "del menu a tendina che sono collegati al record in evidenza nella vista principale."
                            . " Nell'esempio seguente, applicato alla vista vclassif,"
                            . "  selezionando la voce fscheda nel menù a tendina e cliccando successivamente su Enter, si ottiene"
                            . " l'elenco dei record della tabella fscheda che hanno una classificazione corrispondente al record di vclassif in evidenza, in questo caso corrispondente"
                            . " all'id=1, ovvero 'Testo Narrativo'. E' possibile mettere in evidenza un altro record della vista vclassif"
                            . " e ripetere l'operazione per un diverso record di vclassif:"
                            . "  ";
                    echo "<br>";
                    echo "Utilizzare i tasti indicati di seguito per selezionare il record desiderato: (Vedi la sezione 'Consultazione Dati')";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/tendina0.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Selezionare la voce relativa alla tabella desiderata e cliccare sul tasto 'Enter':";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/tendina1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Si ottiene una visualizzazione tabellare dei record della tabella selezionata collegati "
                    . "al record in evidenza. E' possibile esportare i dati ottenuti con il tasto Export (Vedi sezione"
                            . "'Export' del Manuale Utente):";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/tendina2.JPG\" alt=\"immagine\">";
                     echo "<br>";
                    echo "<br>";
                    echo "E' possibile cliccare sui nomi dei vari campi nell'intestazione della tabella per ordinare i campi. Un click per averli in ordine crescente"
                    . " e doppio click per averli in ordine decrescente:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/tendina3.JPG\" alt=\"immagine\">";
                     echo "<br>";
                    echo "<br>";
                    echo "E' inoltre possibile cliccare sull'id di un record per aprirlo nella schermata relativa:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/tendina4.JPG\" alt=\"immagine\">";
                    
                    
                }else if($_GET['azione'] == "nuovo_record_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Inserimento Nuovo Record :</b>";
                    echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\">La creazione di un nuovo record della vista vcommenti può avvenire tramite il tasto"
                    . " 'New', o tramite la procedura di import da file (vedere sezione 'Import data' del manuale utente)."
                    . " La procedura di inserimento con il tasto new prevede due step. Il primo step riguarda l'inserimento "
                             . " dei campi della vista vcommenti che appartengono alla tabella corrispondente fcommenti "
                             . "ovvero 'codice totale commento' e 'testo commento' "
                             . "(nell'immagine seguente i campi del blocco 1). Il secondo step consente, invece, di inserire i campi"
                             . " relativi alle tabelle esterne (ovvero dal blocco 2 al blocco 5).";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/blocchiCommenti.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">"
                            . "<br><br> step 1. Cliccare sul tasto 'New' per entrare in modalità di modifica ed in seguito"
                            . " compilare i campi. Cliccare poi sul tasto 'Save'"
                            . " per completare l'inserimento. Questa procedura riguarda i soli campi 'codice totale commento'"
                            . " e 'testo commento', perchè il campo 'id' viene generato automaticamente in base "
                            . "al valore del massimo id presente.";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti.JPG\" alt=\"immagine\">"; 
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti1.JPG\" alt=\"immagine\">"; 
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti2.JPG\" alt=\"immagine\">"; 
                    echo "<br>";
                    echo "<br>";
                    
                    echo "<p class=\"manuale\">Il secondo step riguarda l'inserimento dei campi della vista che non appartengono "
                    . "alla tabella corrispondente 'fcommenti', ma a tabelle esterne collegate al record selezionato, tramite chiavi "
                            . "esterne."
                            . " Questi campi sono suddivisi, a seconda della tabella/vista a cui appartengono, in blocchi (vedi immagine"
                            . " precedente), preceduti da un titolo colorato in blu, cliccabile e che apre il record selezionato nella"
                            . " tabella corrispondente (vedi la sezione 'Titolo Cliccabili' del Manuale Utente)."
                            . " Nell'esempio il blocco 2 contiene i campi 'codice effetto' ed 'effetto' della vista veffetti, che "
                            . " per il record selezionato sono assenti (quando assenti i campi presentano un effetto di trasparenza)."
                            . " Il blocco 3, invece, contiene i campi 'codice categoria commento' e 'categoria commento' della vista"
                            . " vtabcatego."
                    . "<br><br> step 2. Dopo aver eseguito i passaggi dello step 1, ed aver salvato il nuovo record con tutti i campi"
                            . " relativi alla tabella fcommenti, lo step 2 prevede l'inserimento dei campi dei blocchi dal 2 al 5. "
                            . "Per ogni blocco"
                            . ", preso il record in questione e precedentemente salvato, è necessaro cliccare sul campo con "
                            . "denominazione 'codice*' (ovvero"
                            . " 'codice effetto' per il blocco 2, 'codice categoria commento' per il blocco 3, 'codice sequenza' per "
                            . "il blocco 4 e 'codice toponimo' per il blocco 5). Nell'esempio seguente, cliccando sul tasto 'codice "
                            . "categoria commento' si aprirà un menù a tendina dal quale sarà possibile selezionare il codice categoria"
                            . " commento che si desidera associare al record salvato: "
                            . " ";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti4.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti5.JPG\" alt=\"immagine\">";
              
                    echo "<br>";
                    echo "<br>";
                    echo ""
                    . "Cliccando sul tasto 'Cambia' la categoria commento selezionata viene associata al record relativo alla"
                    . " vista vcommento precedentemente creato. "
                            . "<br><br>Da notare che l'inserimento dei campi dei blocchi dal 2 al 5 è soggetto a delle regole. Ad"
                    . " esempio non può essere associato nessun effetto se prima non viene associato un record "
                            . "della categoria commenti."
                            . "<br><br>Se si desidera dissociare i campi di una sezione dal record selezionato, è necessario"
                            . " effettuare la stessa procedura e cliccare sul tasto 'Dissocia', anzichè sul tasto 'Cambia'.";
     
     
                }else if($_GET['azione'] == "nuovo_record"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Inserimento Nuovo Record :</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">La creazione di un nuovo record avviene tramite il tasto 'New', oppure attraverso import da file (Vedere sezione Import)"
                    . ". Nella creazione del nuovo record il campo id non deve essere inserito, in quanto viene generato"
                            . " automaticamente. La procedura di inserimento di un nuovo record prevede due casi possibili:"
                            . "<br><br> 1. Inserimento di un nuovo record in una vista che non ha collegamenti a tabelle esterne ("
                            . "se nella vista non ci sono titoli cliccabili in blu): in questo caso tutti i campi della vista"
                            . " corrispondono ai campi della tabella corrispondente, e quindi possono essere inseriti "
                            . "manualmente. Dopo aver cliccato sul tasto 'New', si compilano i campi che si desidera"
                            . " riempire e in seguito si clicca sul tasto 'Save'."
                            . "";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo0.JPG\" alt=\"immagine\">"; 
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo01.JPG\" alt=\"immagine\">"; 
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo02.JPG\" alt=\"immagine\">"; 
                    echo "<br>";
                    echo "<br>";
                    
                    echo "<p class=\"manuale\"><br><br> 2. Inserimento di un nuovo record che ha dei collegamenti a tabelle"
                    . " esterne (ovvero nei casi in cui all'interno della vista sono presenti dei titolo cliccabili in blu"
                            . " (vedi la sezione 'Titolo Cliccabili' del Manuale Utente)): in questo caso, la vista compone"
                            . " alcuni campi appartenenti alla tabella corrispondente, il cui inserimento avverrà tramite i passaggi"
                            . " descritti in precedenza, e dei campi appartenenti alle tabelle collegate. Nell'esempio"
                            . " seguente, la vista vscheda1 compone i campi relativi alla tabella corrispondente "
                            . "fscheda1 (Blocco 1) e i campi relativi alla tabella associata delle località, fnloc (Blocco 2)."
                            . " ";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo11.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<br>";
                    echo "<br>";
                    echo "L'inserimento dei campi del blocco 1 può avvenire manualmente seguendo i passaggi precedenti"
                            . " (Tasto New - Inserimento Manuale - Tasto Save). Dopo l'inserimento dei campi del blocco 1, "
                            . "si potrà passare all'inserimento dei campi del blocco 2 (ed eventualmente"
                            . " altri blocchi, qualora presenti). Per farlo è necessario fare un refresh"
                            . " della pagina, ritornare sul record appena inserito e cliccare sul campo con denominazione 'codice*'"
                            . " del blocco in questione. Nel caso in esempio, cliccando sul campo 'codice toponimo' si aprirà "
                            . "un menù a tendina, dal quale sarà possibile selezionare la località desiderata:";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo12.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo13.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo14.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo15.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Cliccando sul tasto 'Cambia' la località selezionata viene associata al record relativo alla"
                    . " vista vscheda1 precedentemente creato.";
     
     
                }else if($_GET['azione'] == "modifica_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Modifica Dati Vista Commenti:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Per modificare un record della vista vcommenti si utilizza il tasto 'Modify' "
                    . "se si vuole modificare un record per volta oppure il tasto 'Edit' per modificare più record "
                            . "attraverso un file di import (vedi sezione 'Edit data'). "
                            . "<br>Per modificare un record per volta è necessario seguire due passaggi, mostrati di seguito:"
                            . " <br><br> 1. Modifica dei campi che appartengono alla tabella corrispondente 'fcommenti', ovvero "
                            . " 'codice totale commento' e 'testo commento' (Blocco 1): per modificare questi campi è necessario"
                            . " cliccare sul tasto 'Modify' per entrare "
                            . "in modalità di modifica, inserire le modifiche nei campi desiderati e successivamente cliccare sul tasto"
                            . " 'Save'.";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modificaCommenti.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modificaCommenti2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modificaCommenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modificaCommenti4.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "2. Modifica dei campi che appartengono a tabelle esterne collegate alla vista vcommenti tramite chiavi esterne
                         (Blocchi dal 2 al 5). 
                       Per modificare i campi relativi alle tabelle esterne collegate (che si trovano al di sotto 
                       dei titoli cliccabili in blu), è necessario cliccare sul campo denominato 'codice*', ovvero 'codice effetto'
                        per il blocco 2, 'codice categoria commento' per il blocco 3, 'codice sequenza' per il blocco 4, 'codice toponimo'
                         per il blocco 5. 
                       Nell'esempio seguente per modificare la categoria commento associata al record selezionato 
                       si clicca sul campo 'codice categoria commento'. Si aprirà una finestra con un menù a tendina, dal quale è possibile selezionare la categoria
                       commento desiderata. Successivamente è necessario cliccare sul tasto 'Cambia'.";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/blocchiCommenti.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti4.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/newCommenti5.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "Per scollegare un record dall’id di una tabella collegata cliccare sul tasto 'Dissocia'"
                    . " invece che sul tasto 'Cambia'.";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo4.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "modifica"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Modifica Dati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Per modificare un record si utilizza il tasto 'Modify' "
                    . "direttamente dalla pagina del record oppure il tasto 'Edit' per modificare più campi "
                            . "attraverso un file di import "
                    . "(vedi sezione 'Edit data'). "
                            . "<br> E' necessario cliccare sul tasto 'Modify' per entrare in modalità di modifica, "
                            . "inserire le modifiche e successivamente cliccare su salva. "
                            . "(L’esempio che segue è applicato alla vista vcommenti).";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modifica1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modifica2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modifica3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "
                       Per modificare i campi relativi alle tabelle esterne collegate (che si trovano al di sotto 
                       dei titoli cliccabili in blu), è necessario cliccare sul campo denominato 'codice*'. 
                       Nell'esempio seguente per modificare la località associata al record selezionato, si clicca sul campo
                       'codice toponimo'. Si aprirà una finestra con un menù a tendina, dal quale è possibile selezionare la località
                       desiderata. Successivamente è necessario cliccare sul tasto 'Cambia'.";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modifica4.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/modifica5.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "Per scollegare un record dall’id di una tabella collegata cliccare sul tasto 'Dissocia'"
                    . " invece che sul tasto 'Cambia'.";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/nuovo4.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "ricerca_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Ricerca Commenti:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Per la vista 'vcommenti' sono disponibili due diverse modalità di ricerca, "
                    . "che verranno descritte di seguito. La prima (punto 1) consente di effettuare la ricerca sul campo "
                            . "'testo commento' (ricerca complessa"
                            . " su testi). La seconda (punto 2) consente, invece, di effettuare la ricerca su tutti i restanti "
                            . "campi della vista."
                    . "In entrambi i casi per effettuare una ricerca è necessario cliccare sul tasto 'Search'"
                    . " per entrare in modalità di ricerca, inserire la o le parole da cercare nei campi desiderati seguendo"
                            . " le regole delle due diverse modalità e riportate di seguito (è possibile effettuare"
                            . " la ricerca su più campi) e in seguito cliccare sul tasto 'Start Search':";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/ricercaCommenti.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/ricercaCommenti2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/ricercaCommenti3.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Di seguito vengono descritte le due modalità di ricerca. La prima "
                    . " riguarda il solo campo 'testo commento', la seconda tutti i restanti campi della vista vcommenti. Da notare"
                            . " che in ogni caso la ricerca non è mai 'Case-Sensitive', ovvero non tiene conto delle Maiuscole o "
                            . "Minuscole."
                           
                    . "<br><br> 1. Per il solo campo 'testo commento' è possibile utilizzare gli operatori di ricerca elencati "
                            . "di seguito"
                            . " a seconda del tipo di ricerca che si vuole effettuare: "
                            . "<br><br> a. parola di ricerca senza operatori: la ricerca effettuata inserendo la parola di ricerca senza"
                            . " l'utilizzo di operatori consente di cercare tutti i testi che contengono la parola di ricerca"
                            . " (comprese le declinazioni al plurale (o singolare) del termine in questione). ".
                            "<br><br> b. operatore '*' (*parola*): la ricerca effettuata inserendo la parola di ricerca "
                            . "(o una frase) tra"
                            . " l'operatore '*' consente di cercare tutti i testi che contengono esattamente la parola di ricerca (oppure"
                            . " la frase). Sono escluse le declinazioni al plurale del termine in questione. Quindi cercando *terremoto*"
                            . " troverò i testi che contengono la parola 'terremoto' ma non 'terremoti'."
                            . "<br><br> c. operatore and '&' (parola1&parola2): è possibile ricercare dei commenti che contengono contemporaneamente"
                            . " sia la parola1 che la parola2. Ad esempio se si desidera cercare i commenti che contengono sia la parola"
                            . " 'Bologna' che la parola 'Venezia' si utilizzerà la seguente forma: Bologna&Venezia."
                            . "<br><br> d. operatore or '|' (parola1|parola2): è possibile ricercare dei commenti che contengono "
                            . " la parola1 oppure la parola2. Ad esempio se si desidera cercare i commenti che contengono la parola"
                            . " 'Bologna' oppure 'Venezia' si utilizzerà la seguente forma: Bologna|Venezia."
                            . "<br><br> e. operatore negazione '!' (!parola): è possibile ricercare dei commenti che non contengono "
                            . " la parola di ricerca.. Ad esempio se si desidera cercare i commenti che non contengono la parola"
                            . " 'Bologna' si utilizzerà la seguente forma: !Bologna."
                            . "<br><br> f. operatore distanza '&lt;n&gt;' (parola1&lt;n&gt;parola2): è possibile ricercare dei commenti che contengono "
                            . " la parola2 ad una distanza specifica di n parole dalla parola1. Digitando parola1<n>parola2, si cercheranno"
                            . " i commenti che contengono pa parola2 in posizione n dopo la parola1."
                            . "Ad esempio se cerco 'catalogo<3>terremoti', otterrò tutti i commenti che contengono la parola 'terremoti' come"
                            . " terza parola dopo la parola 'catalogo' (ad esempio, 'catalogo dei forti terremoti). "
                            . "<br><br> g. operatore distanza minore di '-n-' (parola1-n-parola2): è possibile cercare dei commenti che contengono"
                            . " la parola2 ad una distanza minore o uguale ad n dalla parola1. Ad esempio ricercando 'catalogo-10-terremoti'"
                            . " otterrò i commenti che contengono la parola 'terremoti' ad una distanza minore o uguale a 10. Otterrò quindi"
                            . " i commenti che contengono 'catalogo dei forti terremoti', ma anche quelli che contengono 'terremoti' come decima,"
                            . "ad esempio, parola dopo 'catalogo'.";
                    
                    echo "<p class=\"manuale\"><br><br>2. Per tutti i restanti campi che non siano 'testo commento' ci sono le seguenti possibilità:"
                    . "<br><br> a. parola di ricerca senza operatori: la ricerca effettuata inserendo la parola di ricerca senza"
                            . " l'utilizzo di operatori consente di cercare i record che contengono nel campo in questione solo ed esattamente"
                            . " la parola di ricerca.".
                            "<br><br> b. operatore '*' (*parola*): la ricerca effettuata inserendo la parola di ricerca (o la frase) tra"
                            . " l'operatore '*' consente di cercare i record che contengono nel campo in questione"
                            . " la parola di ricerca o la frase. Sono escluse le declinazioni al plurale del termine in questione. "
                            . "Quindi cercando *terremoto*"
                            . " troverò i testi che contengono la parola 'terremoto' ma non 'terremoti'.";
     
                }else if($_GET['azione'] == "pdf_codici_biblio"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Pdf Codici Bibliografici:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Il campo 'testo commento' della vista vcommenti, contiene il testo del commento, che "
                    . "a sua volta potrebbe contenere dei riferimenti ai codici bibliografici."
                    . "Il tasto 'testo commento' consente di aprire un pop-up nel quale sono elencati i pdf relativi "
                            . "ai codici bibliografici, qualora presenti nel testo e che allo stesso tempo"
                            . " esistono in archivio. Accanto al codice bibliografico è specificato se il documento presente in archivio"
                            . " è in formato"
                            . "raster o trascritto. Cliccando sul link all'interno del pop-up è possibile aprire in una nuova finestra "
                            . "il documento pdf:";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/pdf_biblio.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "pdf_schedeb"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Pdf SchedeB:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">La vista vschedeb contiene oltre ai vari campi suddivisi in diverse sezioni, "
                    . "i campi 'pdf raster' e 'pdf trascritto', che se cliccati, rimandano al documento pdf"
                            . " in formato raster o trascritto, qualora presenti per il record selezionato. I campi"
                            . " se presenti sono alla fine della pagina:";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/pdf_schedeb.JPG\" alt=\"immagine\">";
     
                }else if($_GET['azione'] == "grid_view_commenti"){
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Modalità di visualizzazione Grid View/Form View:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Ci sono due modalità di visualizzazione dei dati di una vista o tabella:"
                    . "<br><br> 1. Form View: è la modalità di visualizzazione dati di default e consente di visualizzare "
                            . "un record alla volta, in tutti i suoi campi, suddivisi in sezioni."
                    . "<br><br> 2. Grid View: è la modalità di visualizzazione dei record che prevede la distribuzione"
                            . " dei campi in forma tabellare.";
                    echo "<br>";
                    echo "<p class=\"manuale\">Per passare da una modalità all'altra è necessario cliccare sul tasto Grid View "
                    . "oppure Form View, posti in alto a destra. L'esempio seguente è applicato alla vista vcommenti:";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/grid1.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "I record della vista 'vcommenti' mostrati nella grid view, vengono ordinati di default "
                    . "per campo id e in ordine crescente. Per modificare l'ordine di visualizzazione dei record è possibile"
                            . " cliccare sull'intestazione del campo della tabella che si desidera utilizzare per l'ordinamento "
                    . "(cliccare una volta per averli in ordine"
                    . " crescente, due volte per averli in ordine decrescente). <br><br> E' inoltre possibile "
                            . "fare doppio click su un record per aprirlo in modalità Form View:";
                    echo "<br>";
                    echo "<br>";
                    echo "<img src=\"immagini_manuale/grid2.JPG\" alt=\"immagine\">";
                    echo "<br>";
                    echo "<br>";
                    echo "Da entrambe le modalità di visualizzazione è possibile effettuare le operazioni di Export e Import "
                    . "tramite i tasti 'Export data', 'Import data', 'Edit data', 'Import testo commenti', "
                            . "Export tutti i testi commenti' (vedi le sezioni relative nel manuale dei commenti)."
                            . "<br><br>Da notare che dopo le operazioni di ricerca sui campi (vedi sezione 'Ricerca' del"
                            . " manuale utente) qualora il numero dei risultati sia superiore a 1, i record verranno visualizzati"
                            . " con la modalità grid view; a questo punto cliccando sul tasto 'Export data sarà possibile "
                            . "esportare i dati ottenuti dalla ricerca (vedi sezione 'Export data' del manuale utente).";
     
                }else if($_GET['azione'] == "export_commenti"){
                    
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Dati Commenti:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Dalla vista 'vcommenti' è possibile effettuare "
                    . "tre diverse tipologie di export:";
                    echo "<br>";
                    echo "<br>";
                    
                    echo "<p class=\"manuale\"><a href=\"manuale_utente.php?azione=export_tutto_commenti\">Export di tutti i record </a> -Procedura che consente l'esportazione"
                    . " di tutti i record della vista 'vcommenti'.";
                    echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\"><a href=\"manuale_utente.php?azione=export_selezione_commenti\">Export di dati di ricerca </a> -Procedura che consente l'esportazione"
                    . " dei risultati di una ricerca sulla vista 'vcommenti'.";
                     echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\"><a href=\"manuale_utente.php?azione=export_testo_commento\">Export testo commento </a> -Procedura che consente l'esportazione"
                    . " dei soli commenti, ovvero del solo campo 'testo commento'. ";
                     
     
                }else if($_GET['azione'] == "export"){
                    
                    
                    echo "<br>"; 
                    echo "<br>";
                    echo "-------------------------------------------------------------";
                    echo "<br>";
                    echo "<br>";
                    echo "<b class=\"manuale\">Export Dati:</b>";
                    echo "<br>";
                    echo "<br>";
                    echo "<p class=\"manuale\">Da ogni vista o tabella è possibile effettuare l'export dei dati in"
                    . " formato excel o csv. Ci sono tre diverse tipologie di export possibili:";
                    echo "<br>";
                    echo "<br>";
                    
                    echo "<p class=\"manuale\"><a href=\"manuale_utente.php?azione=export_tutto\">Export di tutti i record </a> -Procedura che consente l'esportazione"
                    . " di tutti i record di una vista o tabella";
                    echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\"><a href=\"manuale_utente.php?azione=export_selezione\">Export di dati di ricerca </a> -Procedura che consente l'esportazione"
                    . " dei risultati di una ricerca su una vista o tabella.";
                     echo "<br>";
                    echo "<br>";
                     echo "<p class=\"manuale\"><a href=\"manuale_utente.php?azione=export_tabelle_collegate\">Export record tabelle collegate </a> -Procedura che consente l'esportazione"
                    . " dei record di una tabella esterna che sono"
                            . " collegati ad un record specifico della vista o tabella principale. ";
                     
     
                }
            }

?>

