<?php
require_once("./inc/conn.php");
require_once("./inc/layouts.php");
require_once("funzioni.php");
require_once __DIR__.'/classesNuove/GestoreSchemi.php';
proteggi(1);
    
getErrori();
//recupero parametro passato tramite url
if(isset($_GET['funzione'])){
    $funzione = $_GET['funzione'];
}else{
    exit;
}

echo openLayout1(_($funzione), array(), 'popup');
echo breadcrumbs(array("HOME"));

//switch per gestire le varie funzioni
switch ($funzione){
    case 'reset':
        schermataReset();
        break;
    case 'reset2':
        resetDatabase();
        break;
    case 'logImport':
        logImport();
        break;
    case 'mostraSchemi':
        GestoreSchemi::mostraSchemi();
        break;
    default :
        header('Location: '.$_SERVER['PHP_SELF']);
        die;
        break;
    
    
}

//FUNZIONI SCHERMATE


function schermataReset(){
    echo "<h1>Reset Database</h1>";
    echo "<h2>Attenzione! Tutti i Dati nel Database verranno persi!</h2>";
    echo "<button type=\"button\" onclick=\"resetDatabase()\">Reset Database</button>";
}

//FUNZIONI
function resetDatabase() {
    global $vmsql;
    
    
    
    //recupero dal file di configurazione dell'import le tabelle
    $fileName = 'configurazioneImport.json';
    $content = file_get_contents($fileName);
    $jsonObj = json_decode($content);
    $arrayTabelle = $jsonObj->nomiTabelle;
    //l'ordine nel file di conf rispetta le chiavi. Per cancellare i contenuti parto dall'ultimo
    $arrayOrdinatoDesc = array_reverse($arrayTabelle);
    
    foreach ($arrayOrdinatoDesc AS $tabella){
        $sql = "delete from $tabella";
        $result = pg_query($vmsql->link_db, $sql);
        
        if(!$result){
            echo "<h2 style=\"background-color:Tomato;\">problema nel cancellare i dati dalla tabella $tabella</h2>";
            exit;
        }
    } 
    
    echo "<h2 style=\"background-color:Acquamarine;\">Dati cancellati correttamente</h2>";

}

function logImport(){

    global $fileErroriLog;
    
    echo '<br>';
    echo "<a href='configurazioneImport.json' target='_blank'>File di Configurazione (.json)</a>";
    echo '<br>';
    echo "<a href='configurazioneImportTesti.json' target='_blank'>File di Configurazione Testi(.json)</a>";
    echo '<br>';
    echo "<a href='$fileErroriLog' target='_blank'>File Log Errori</a>";
    echo '<br>';
    echo "<a href='?funzione=mostraSchemi'>Mostra Schemi</a>"; 
    
    
    

    
}
?>
<script type="text/javascript">
    
    function resetDatabase(){
        let cancella = confirm("Confermi di voler cancellare tutti i dati dal database?");
        
        if(cancella === true){
            location.href='database.php?funzione=reset2';
        }else{
            location.href='database.php?funzione=reset';
        }
        
        
    }
    
    </script>