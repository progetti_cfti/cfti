<?php


$MYSQLConnString = '';
$Settings = array(

	'host' => 'localhost',
	'user' => 'root',
	'password' => '',
	'dbname' => 'cfti6_seq',
	'port' => '3306'
);

	foreach($Settings as $key => $value);
	
	$MYSQLConnString .= "{$key}={$value} "; 

	$DBConn = new mysqli('localhost', 'root', '', 'cfti6_seq');
		 if ($DBConn ->connect_error) {
			die('Errore di connessione (' . $mysqli->connect_errno . ') '
			. $DBConn ->connect_error);
		} else {
			echo 'Connesso. ' . $DBConn ->host_info . "\n";
		};

	// $NPDetailQuery = mysqli_query($DBConn, "SELECT anno_1, anno_2, area_ep, nperiod FROM SEQUENZE_NPERIOD order by nperiod");
	$NPDetailQuery = mysqli_query($DBConn, "SELECT anno_1, anno_2, area_ep, nperiod FROM SEQUENZE_NPERIOD order by anno_1 ");

	$XMLSourceL = "sequenzeSources/SequenceList.xml";		
	$XMLListDetail = new DomDocument('1.0', 'UTF-8');
	$XMLListDetail->preserveWhiteSpace = true;
	$XMLListDetail->formatOutput = true;
	$SeqsRoot = $XMLListDetail->createElement('dataroot');
	$XMLListDetail->appendChild($SeqsRoot);

	


	LoopNP($DBConn,$NPDetailQuery,$XMLListDetail,$SeqsRoot);
	
	function LoopNP($DBConn,$NPDetailQuery,$XMLListDetail,$SeqsRoot) {
		
		
		while($Detail = mysqli_fetch_assoc($NPDetailQuery)){
		
		$XMLSource = "sequenzeSources/seq{$Detail['nperiod']}.xml";
		
		$XMLQuakeDetail = new DomDocument('1.0', 'UTF-8');
		$XMLQuakeDetail->preserveWhiteSpace = true;
		$XMLQuakeDetail->formatOutput = true;
		
		$Seqs = $XMLQuakeDetail->createElement('sequence');
		$XMLQuakeDetail->appendChild($Seqs);
		$Seqs->setAttribute('alone', 'true');
		
		$Seqs1 = $XMLListDetail->createElement('sequence');
		$SeqsRoot->appendChild($Seqs1);

		$child1 = $XMLListDetail->createElement('nperiod');
		$child1 = $Seqs1->appendChild($child1);	
		$value = $XMLListDetail->createTextNode($Detail['nperiod']);
		$value = $child1->appendChild($value);
		
		$child1 = $XMLListDetail->createElement('title');
		$child1 = $Seqs1->appendChild($child1);
		if ($Detail['anno_2']<>"-"){
		 $value = $XMLListDetail->createTextNode($Detail['anno_1']."-".$Detail['anno_2']." - ".$Detail['area_ep']);
		} else {
		 $value = $XMLListDetail->createTextNode($Detail['anno_1']." - ".$Detail['area_ep']);
		}
		
	//generazione scosse
		
			$SeqQuery = mysqli_query($DBConn, "select * from SEQUENZE where nperiod = '{$Detail['nperiod']}' order by anno1, mese1, giorno1, oraGMT1");
			 // $SeqQuery = mysqli_query($DBConn, "select * from SEQUENZE where nperiod = '002010'");
			 
			 $value = $child1->appendChild($value);
			while($Row = mysqli_fetch_assoc($SeqQuery)){
				// echo $Row['codicescossa'] . "\n" ;	
				$NT = strlen($Row['nterr']);
				// echo 'nterr: ' . $Row['nterr'] . "\n" ;	
				

				if ( $NT == 5) {
					// echo 'si entra nella procedura NTERR' . "\n" ;	

					$NTquery = mysqli_query($DBConn, "select * from NTERRS where nterr = '{$Row['nterr']}' ");
									
					$Row2 = mysqli_fetch_assoc($NTquery);
					
					if ($Row2['nperiod'] == '') {   //se non seleziono nulla (quell'NTERR non è in cfti5, ma solo in CFTI6)
					
					} else {
						
						$elem2 = $XMLQuakeDetail->createElement('scossa');
											
						//Recupero del D0, ma al momento non è utilizzato!
						
						// $flagcomm = $Row2['flagcomments'];			
						// if ($flagcomm == 4 or $flagcomm == 5) {
				
							// $CommentsQuery = mysqli_query($DBConn, "select codtab, testocomm from commgen where nperiod = '{$Detail['nperiod']}' order by codtab");
							
							// while($Row3 = mysqli_fetch_assoc($CommentsQuery)){
		
									// if ($Row3['codtab'] == 'D0') {
										// $D0 = $Row3['testocomm'];
									// } else {
										// $D0 = "Commento non disponibile";
									// }
		
							// }
						// } else {
							$D0 = "-";
						// }
						
					
						$child = $XMLQuakeDetail->createElement('ID');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('nperiod');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['nperiod']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('codiceScossa');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('nterr');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['nterr']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('anno1');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['anno']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('mese1');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['mese']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('giorno1');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['giorno']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('anno2');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);

						$child = $XMLQuakeDetail->createElement('mese2');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('giorno2');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('oraF');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('oraGMT1');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['time_label']);
						$value = $child->appendChild($value);

						$child = $XMLQuakeDetail->createElement('oraGMT2');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('timeLABELgmt');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['time_label']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('timeLABELfonte');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode("-");
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('Io');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['io']);
						$value = $child->appendChild($value);
						
						$child = $XMLQuakeDetail->createElement('imax');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($Row2['imax']);
						$value = $child->appendChild($value);

						$child = $XMLQuakeDetail->createElement('commentoScossa');
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($D0);
						$value = $child->appendChild($value);					
					}
				} else {
					// echo 'si entra nella procedura SCOSSA' . "\n" ;	
					
					$elem2 = $XMLQuakeDetail->createElement('scossa');
					
					foreach($Row as $key => $value){
						if ($value == "") {
							$value = "-";
						};
						if ($value == -9) {
							$value = "-";
						};
						$child = $XMLQuakeDetail->createElement($key);
						$child = $elem2->appendChild($child);

						$value = $XMLQuakeDetail->createTextNode($value);
						$value = $child->appendChild($value);
					}
				}
				$Seqs->appendChild($elem2);
				
				}
					
				//generazione località
				$SeqQuery = mysqli_query($DBConn, "select * from SEQUENZE_LOC where nperiod = '{$Detail['nperiod']}' order by codiceScossa");

			$n=1;
			while($Row = mysqli_fetch_assoc($SeqQuery)){
				$Row['nomeloc'];
				// if ($Row['nomeloc'] <> '') {
					$elem2 = $XMLQuakeDetail->createElement('localita');
					
					$child = $XMLQuakeDetail->createElement('nperiod');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['nperiod']);
					$value = $child->appendChild($value);
					
					$child = $XMLQuakeDetail->createElement('codiceScossa');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['codicescossa']);
					$value = $child->appendChild($value);
					
					$child = $XMLQuakeDetail->createElement('nterr');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode('-');
					$value = $child->appendChild($value);

					$child = $XMLQuakeDetail->createElement('nscosse');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['nscosse']);
					$value = $child->appendChild($value);

					$child = $XMLQuakeDetail->createElement('durCalc');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['durCalc']);
					$value = $child->appendChild($value);

					$child = $XMLQuakeDetail->createElement('durFonti');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['durFonti']);
					$value = $child->appendChild($value);
					
					$child = $XMLQuakeDetail->createElement('intFonte');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['intFonte']);
					$value = $child->appendChild($value);

					$child = $XMLQuakeDetail->createElement('intMCS');
					$child = $elem2->appendChild($child);
					
					if ($Row['IMCS'] == '-' || $Row['IMCS'] == '') {
					
						$value = $XMLQuakeDetail->createTextNode($Row['IMCS_nuovo']);
						$value = $child->appendChild($value);

					} else {
						
						$value = $XMLQuakeDetail->createTextNode($Row['IMCS']);
						$value = $child->appendChild($value);
					}

					$child = $XMLQuakeDetail->createElement('ID');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($n);
					$value = $child->appendChild($value);



					$child = $XMLQuakeDetail->createElement('nomeloc');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['nomeloc']);
					$value = $child->appendChild($value);

					$child = $XMLQuakeDetail->createElement('NLOC_CFTI');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['NLOC_CFTI']);
					$value = $child->appendChild($value);
					
					$child = $XMLQuakeDetail->createElement('latloc');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['latloc']);
					$value = $child->appendChild($value);
					
					$child = $XMLQuakeDetail->createElement('lonloc');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['lonloc']);
					$value = $child->appendChild($value);

					$child = $XMLQuakeDetail->createElement('commentoLoc');
					$child = $elem2->appendChild($child);
					
					$value = $XMLQuakeDetail->createTextNode($Row['commentoLoc']);
					$value = $child->appendChild($value);


					$Seqs->appendChild($elem2);
				// }
			
				$n = $n +1;
			}
			
			
						//Bibliography

		$BibliographyQueryB = mysqli_query($DBConn, "SELECT codbib, valb, desvalingl FROM schede_b WHERE nperiod = '{$Detail['nperiod']}' ");
		
		$Biblios = $XMLQuakeDetail->createElement('Biblios');
		$Seqs->appendChild($Biblios);
		
		$n=1;
		while($Row = mysqli_fetch_assoc($BibliographyQueryB)){
			
			// echo "$n \n";
			$BibliographyQuery = mysqli_query($DBConn, "SELECT schede_a.codbib, schede_a.autore1, schede_a.titolo1, schede_a.luogoed, schede_a.datauni, schede_a.dataun2, schede_b.desvalingl, schede_b.valb FROM schede_a INNER JOIN schede_b ON (schede_a.codbib = schede_b.codbib) WHERE schede_a.codbib = '{$Row['codbib']}' ");
			$elem2 = $XMLQuakeDetail->createElement('Bibliography');
			$Row2 = mysqli_fetch_assoc($BibliographyQuery);
			$data_1 = $Row2['datauni'];
			$data_2 = $Row2['dataun2'];
			if ($data_1 == '0009') {$data_1 = 'IX sec.';};
			if ($data_2 == '0009') {$data_2 = 'IX sec.';};
			if ($data_1 == '0010') {$data_1 = 'X sec.';};
			if ($data_2 == '0010') {$data_2 = 'X sec.';};
			if ($data_1 == '0011') {$data_1 = 'XI sec.';};
			if ($data_2 == '0011') {$data_2 = 'XI sec.';};
			if ($data_1 == '0012') {$data_1 = 'XII sec.';};
			if ($data_2 == '0012') {$data_2 = 'XII sec.';};
			if ($data_1 == '0013') {$data_1 = 'XIII sec.';};
			if ($data_2 == '0013') {$data_2 = 'XIII sec.';};
			if ($data_1 == '0014') {$data_1 = 'XIV sec.';};
			if ($data_2 == '0014') {$data_2 = 'XIV sec.';};
			if ($data_1 == '0015') {$data_1 = 'XV sec.';};
			if ($data_2 == '0015') {$data_2 = 'XV sec.';};
			if ($data_1 == '0016') {$data_1 = 'XVI sec.';};
			if ($data_2 == '0016') {$data_2 = 'XVI sec.';};
			if ($data_1 == '0017') {$data_1 = 'XVII sec.';};
			if ($data_2 == '0017') {$data_2 = 'XVII sec.';};
			if ($data_1 == '0018') {$data_1 = 'XVIII sec.';};
			if ($data_2 == '0018') {$data_2 = 'XVIII sec.';};
			if ($data_1 == '0019') {$data_1 = 'XIX sec.';};
			if ($data_2 == '0019') {$data_2 = 'XIX sec.';};
			if ($data_1 == '0020') {$data_1 = 'XX sec.';};
			if ($data_2 == '0020') {$data_2 = 'XX sec.';};	
			if ($data_1 == '0021') {$data_1 = 'XXI sec.';};
			if ($data_2 == '0021') {$data_2 = 'XXI sec.';};				
			
			if ($data_2=='') {
				$datacompl = $data_1;
			} else {
				$datacompl = $data_1 . " - " . $data_2;
			};
			
			$child = $XMLQuakeDetail->createElement('codbib');
			$child = $elem2->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($Row2['codbib']);
			$value = $child->appendChild($value);
			
			$child = $XMLQuakeDetail->createElement('autore1');
			$child = $elem2->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($Row2['autore1']);
			$value = $child->appendChild($value);
			
			$child = $XMLQuakeDetail->createElement('titolo1');
			$child = $elem2->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($Row2['titolo1']);
			$value = $child->appendChild($value);
			
			$child = $XMLQuakeDetail->createElement('luogoed');
			$child = $elem2
			
			->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($Row2['luogoed']);
			$value = $child->appendChild($value);
			
			$child = $XMLQuakeDetail->createElement('datacompl');
			$child = $elem2->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($datacompl);
			$value = $child->appendChild($value);
			
			$child = $XMLQuakeDetail->createElement('valb');
			$child = $elem2->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($Row2['valb']);
			$value = $child->appendChild($value);
			
			$child = $XMLQuakeDetail->createElement('desvalingl');
			$child = $elem2->appendChild($child);

			$value = $XMLQuakeDetail->createTextNode($Row2['desvalingl']);
			$value = $child->appendChild($value);

			$Biblios->appendChild($elem2);
		$n = $n + 1;
		}
	
			$XMLQuakeDetail->save($XMLSource);
			echo "$XMLSource \n";
		}

	}
	
		$XMLListDetail->save($XMLSourceL);
		echo "$XMLSourceL \n";
		

?>