<?php

class GestoreSchemi {

    protected static $jsonConfigurationFile = __DIR__."/../".'configurazioneSchemi.json';
    

    protected function __construct() {
        
    }

    //legge il file configurazione degli schemi e prende l'oggetto
    protected static function leggiConfigSchemi() {
        


        if (file_exists(static::$jsonConfigurationFile)) {
            //prendo il contenuto
            $content = file_get_contents(static::$jsonConfigurationFile);
            if ($content) {
                //dal contenuto creo un oggetto json 
                $configObj = json_decode($content);
                if($configObj){
                     
                return $configObj;
                }else{
                    throw new Exception("json_decode fallita");
                }
                
            } else {
                throw new Exception("File di configurazione vuoto");
            }
        } else {
            throw new Exception("File di configurazione non trovato. Verificare che il nome sia corretto ( .".static::$jsonConfigurationFile.")");
        }
    }
    
    //restituisce nome dello schema in base al tipo di interfaccia scelto
    public static function nomeSchema($tipoInterfaccia) {
        
        $obj = static::leggiConfigSchemi();
        $arraySchemi = $obj->schemi;
        foreach ($arraySchemi AS $oggettoSchema){
            if($oggettoSchema->tipo===$tipoInterfaccia){
                return $oggettoSchema->nome;
            }
        }
      
        
    }
    
    
    public static function mostraSchemi() {
        
        $obj = static::leggiConfigSchemi();
        
        $stringaJson = json_encode($obj);
        
        //header("Content-Type: application/json");
        echo $stringaJson;
        
    }
    
    

}
