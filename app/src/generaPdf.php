<?php

require_once("./inc/conn.php");
require_once("./inc/layouts.php");
require('./plugins/phpmailer/class.phpmailer.php');
require_once("funzioni.php");
require_once __DIR__.'/classesNuove/GestoreSchemi.php';


proteggi(1);
 
getErrori();
 
//APRO LAYOUT
echo openLayout1(_("Generazione PDF"), array("sty/admin.css"), 'popup');

//IMPOSTO LO SCHEMA 
//per CFTI_BACKEND
$nomeSchemaBackend = GestoreSchemi::nomeSchema('backend');
$nomeSchemaOnline = GestoreSchemi::nomeSchema('online');
if ($db1['dbname'] == $nomeSchemaBackend) {
    $schema = '';

} else if ($db1['dbname'] == $nomeSchemaOnline) {
    //per il CFTI_ONLINE - devo specificare lo schema del cfti_backend
    $schema = $nomeSchemaOnline.'.';

} 




//IMPOSTO CARTELLA FILE
$cartella = 'files/GENERAZIONEPDF';
$cartella_testi = 'files/GENERAZIONEPDF/TXT_TESTI';
$cartella_PDF = 'files/GENERAZIONEPDF/PDF_TESTI';
  
//pulisco il file di log
$fh = fopen( "./{$cartella}/log.txt", 'w' );
fwrite($fh, 'Id dei files non generati per mancanza di codice sequenza, codice bibliografico o testo trascritto:'.PHP_EOL);
fclose($fh);
 
//RECUPER ID RECORD SELEZIONATO
$id_record = $_GET['id'];

//VERIFICO SE EFFETTUARE GENERAZIONE PER IL SOLO RECORD SELEZIONATO O PER TUTTI
if ($id_record === 'all') {
    $sql = "SELECT * FROM {$schema}vschedeb order by id limit 10;";
    $q = $vmsql->query($sql);

    while ($result = pg_fetch_assoc($q)) {
        createTxtFile($result, $cartella_testi, $cartella);
    }
    $result = $vmsql->fetch_assoc($q);
} else if (is_numeric($id_record)) {
    //SQL PER RECUPERARE IL RECORD SELEZIONATO
    $sql = "SELECT * FROM {$schema}vschedeb WHERE id = {$id_record}";
    $q = $vmsql->query($sql);
    $result = $vmsql->fetch_assoc($q);
    createTxtFile($result, $cartella_testi, $cartella);
}



//LANCIO LA PROCEDURA PYTHON -viene lanciata su tutti i file della cartella TXT
$last_line = system("cd $cartella ; python3 create_PDF_2021.py", $output);
echo '<br>';
if ($output === 0) {
    echo 'Procedura eseguita correttamente (Ho messo un limite di 10 per Tutti i Testi per la fase di test)';
}else{
    echo 'Problema nell\'esecuzione della procedura';
}  


//MOSTRO IL LINK DELLA CARTELLA
echo "<h3>Testi intermedi in: <a href='./{$cartella_testi}'>Cartella TXT</a></h3>";
echo PHP_EOL;
echo "<h3>Testi PDF in <a href='./{$cartella_PDF}'>Cartella PDF</a></h2>";
//mostro il file di log
echo "<h3>File di Log: <a href='./{$cartella}/log.txt'>log.txt</a></h2>";


//FUNZIONE CHE CREA FILE TXT INTERMEDIO -> RESTITUISCE FILE POINTER IN CASO DI SUCCESSO O FALSE IN CASO DI FAILURE
function createTxtFile($result, $cartella_testi, $cartella){
    //CREO TESTO TXT
    $pdf = "<font size=12>{$result['autore']}, <i>{$result['titolo']}</i> {$result['luogo_di_edizione']} {$result['data_edizione_ms1']}"
            . "</font>";
    $pdf = $pdf . PHP_EOL . PHP_EOL . htmlentities($result['testo_trascritto']);

    //CREO FILE
    if (!empty($result['codice_sequenza']) && !empty($result['codice_bibliografico']) && !empty($result['testo_trascritto'])) {

        //
        $fp = fopen("./{$cartella_testi}/{$result['codice_sequenza']}-{$result['codice_bibliografico']}.txt", 'w');
        fwrite($fp, (nl2br(nl2br($pdf))));
        fclose($fp);
        
        return $fp;
    } else {
        //SE MANCA CODICE SEQUENZA O BIBLIOGRAFICO NON GENERO FILE
        //file di log
        $fp = fopen("./{$cartella}/log.txt", 'a');
        fwrite($fp, $result['id'].PHP_EOL);
        fclose($fp);
        return false;
    }
    
    
    
}