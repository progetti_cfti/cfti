<?php
require("funzioni.php");
require_once("./inc/conn.php");
require_once("./inc/layouts.php");
proteggi(1);
   
//gestione errori non catturati
getErrori();

$files = '';
echo openLayout1(_("Data import HOME"), $files, 'popup');

echo breadcrumbs(array("HOME", "Import Files"));

echo "<h1>Import All File HOME</h1>";
echo "<h2>Step1: import dei record di tutte le tabelle (Omnis+seq_scosse e seq_loc)</h2>";
echo "<button type=\"button\" onclick=\"location.href='importAllFiles.php'\">Importa</button>";
echo "<h2>Step2: import dei testi</h2>";
echo "<button type=\"button\" onclick=\"location.href='importAllTesti.php'\">Importa</button>";
echo "<h2>Step3: aggiornamento Area Epicentrale e In_Cfti nella tabella fnperiod</h2>";
echo "<button type=\"button\" onclick=\"location.href='edit.php?step=8'\">Aggiorna Nperiod</button>";

echo "<h1>Procedure di verifica</h1>";
echo "<h2>Verifica virgolette aggraziate</h2>";
echo "<button type=\"button\" onclick=\"location.href='procedureVerifica.php?modo=1'\">Verifica</button>";
echo "<h2>Verifica dash</h2>";
echo "<button type=\"button\" onclick=\"location.href='procedureVerifica.php?modo=3'\">Verifica</button>"; 
echo "<h2>Verifica Area Epicentrale</h2>";
echo "<button type=\"button\" onclick=\"location.href='procedureVerifica.php?modo=5'\">Verifica</button>"; 