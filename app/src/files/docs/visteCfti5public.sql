create table if not exists cfti5public.biblio_ee(
    id integer,
	codbib character varying(25),
	primary key(id)
);

create table if not exists cfti5public.nterrs(
    id integer,
	cat character varying(25),
	nterr character varying(25),
	nperiod character varying(25),
	datanum character varying(25),
	data_label character varying(25),
	anno character varying(25),
	mese character varying(25),
	giorno character varying(25),
	data_incert character varying(25),
	anno2 character varying(25),
	mese2 character varying(25),
	giorno2 character varying(25),
	data_incert2 character varying(25),
	time_label character varying(25),
	ora character varying(25),
	minu character varying(25),
	sec character varying(25),
	time_incert character varying(25),
	lat character varying(25),
	lon character varying(25),
	rel character varying(25),
	io character varying(25),
	imax character varying(25),
	npun character varying(25),
	mm character varying(25),
	earthquakelocation text,
	country character varying(25),
	sigla character varying(25),
	ee_nt character varying(25),
	ee_np character varying(25),
	epicenter_type character varying(25),
	flagcomments character varying(25),
	flagfalseeq character varying(25),
	level character varying(25),
	new2018 text,
	comm2018 text,
	NOTA_NOVITA text,
	NOTA_MM text,
	Time character varying(25),
	npun_all character varying(25),
	npun_boxer character varying(25),
	ndata character varying(25),
	primary key(id)
);

create table if not exists cfti5public.ee_tabella(
    id integer,
	nterr character varying(25),
	nperiod character varying(25),
	nloc character varying(25),
	codice_eff character varying(25),
	commento text,
	primary key(id)
);

create table if not exists cfti5public.codbib_list_ee(
	id integer,
	codbib character varying(25),
	primary key(id)
);

create table if not exists cfti5public.fnperiod_cfti(
	id integer,
	nperiod_cfti character varying(25),
	primary key(id)
);

create table if not exists cfti5public.fnloc_cfti(
	nloc_cfti character varying(25),
	desloc_cfti character varying(250),
	lat_wgs84 character varying(25),
	lon_wgs84 character varying(25),
	provlet_cfti character varying(250),
	nazione character varying(250),
	postal_code character varying(250),
	notesito text,
	primary key(id)
);

create table if not exists cfti5public.fnloc_cfti_5(
	id integer primary key,
	nloc_6 character varying(250),
	nloc character varying(250),
	desloc text,
	provlet character varying(25),
	nazione character varying(250),
	postal_code character varying(25),
	quot_loc character varying(250),
	numabit character varying(250),
	prov character varying(25),
	regione character varying(250),
	comune character varying(250),
	frazione character varying(25),
	notesito  text,
	lat_roma40 character varying(25),
	lon_roma40 character varying(25),
	istat character varying(250),
	istat_103 character varying(250),
	comune_nome text,
	capoluogo character varying(25),
	cap character varying(25),
	problemi text,
	istat_2011 character varying(250),
	comune_nome_2011 text,
	capoluogo_2011 character varying(25),
	in_cfti character varying(25),
	nloc_cfti character varying(250),
	desloc_cfti text,
	lat_wgs84 character varying(25),
	lon_wgs84 character varying(25),
	provlet_old character varying(25),
	cod_compl text,
	note_2018 text
);

create table if not exists cfti5public.schedec(
	id integer,
	codbib character varying(25),
	nloc character varying(25),
	primary key(id)
);

create table if not exists cfti5public.ee (id) AS
 SELECT  cfti5public.ee_tabella.id, cfti5public.ee_tabella.nterr, cfti5public.ee_tabella.nperiod, cfti5public.ee_tabella.nloc,
 cfti5public.ee_tabella.codice_eff, cfti5public.ee_tabella.commento, cfti5public.ee_tabella.risentimenti,
 cfti5public.ee_tabella.maxint,
 cfti5public.fnloc_cfti_5.nloc_cfti, cfti5public.fnloc_cfti_5.desloc_cfti, cfti5public.fnloc_cfti_5.lat_wgs84,
 cfti5public.fnloc_cfti_5.lon_wgs84, cfti5public.fnloc_cfti_5.notesito, cfti5public.fnloc_cfti_5.provlet,
 cfti5public.fnloc_cfti_5.nazione, cfti5public.fnloc_cfti_5.desloc
   FROM ((cfti5public.ee_tabella
        LEFT JOIN cfti5public.fnloc_cfti_5 ON ((cfti5public.ee_tabella.nloc = cfti5public.fnloc_cfti_5.nloc)))    
     );
	
ALTER TABLE cfti5public.ee DROP CONSTRAINT IF EXISTS ee1_pkey; 
ALTER TABLE cfti5public.ee ADD CONSTRAINT ee1_pkey PRIMARY KEY (id); 


/*
CREATE OR REPLACE VIEW  cfti5public.commgen(id) 
AS
SELECT  fcommenti.id, cfti5public.fnperiod_cfti_view.nperiod_cfti, ftabcatego.codtab, fcommenti.testocomm
FROM fcommenti, ftabcatego, cfti5public.fnperiod_cfti_view
WHERE  ftabcatego.codtab <> 'D1' and ftabcatego.codtab <> 'E1' and ftabcatego.codtab <> 'A0' and ftabcatego.codtab <> 'A1'
and fcommenti.fnperiod = fnperiod_cfti_view.id
and fcommenti.ftabcatego = ftabcatego.id;
*/

create table if not exists cfti5public.commgen (id) AS
SELECT  fcommenti.id, cfti5public.fnperiod_cfti_view.nperiod_cfti, ftabcatego.codtab, fcommenti.testocomm
FROM fcommenti, ftabcatego, cfti5public.fnperiod_cfti_view
WHERE  ftabcatego.codtab <> 'D1' and ftabcatego.codtab <> 'E1' and ftabcatego.codtab <> 'A0' and ftabcatego.codtab <> 'A1'
and fcommenti.fnperiod = fnperiod_cfti_view.id
and fcommenti.ftabcatego = ftabcatego.id;

ALTER TABLE cfti5public.commgen DROP CONSTRAINT IF EXISTS commgen_pkey;
ALTER TABLE cfti5public.commgen ADD CONSTRAINT commgen_pkey PRIMARY KEY (id);

/*
CREATE OR REPLACE VIEW  cfti5public.nterrs2(id) 
AS
SELECT  fscossec.id, fscossec.nterrs, fnperiod_cfti_view.nperiod,  fscossec.dataors, fscossec.oraorgs,
fscossec.lat_centesimale, fscossec.lon_centesimale, 
fscossec.loceps, fscossec.npunpqs, fscossec.ios, fscossec.imaxs, fscossec.mks, fscossec.livrevs,
fscossec.rellocepics, 
fscossec.statos
FROM fscossec, cfti5public.fnperiod_cfti_view
WHERE  fscossec.fnperiod = cfti5public.fnperiod_cfti_view.id ;
*/

CREATE OR REPLACE VIEW cfti5public.notesito(id) AS
SELECT
	row_number() over (order by p.nloc), p.nloc_cfti, p.desloc_cfti, p.nloc,  p.notesito
	FROM(
SELECT DISTINCT on (m.nloc_cfti)
    m.nloc_cfti, m.desloc_cfti, m.nloc, m.notesito
FROM 
		cfti5public.fnloc_cfti_5 m 	
		where m.notesito <> ''
		ORDER BY m.nloc_cfti, m.nloc
		) p 
;

 
create table if not exists cfti5public.pq (id, intpq, intpqnum, nperiod, nterr, nloc_cfti, desloc_cfti,
										   lat_wgs84, lon_wgs84, provlet, notesito, nloc, desloc, nazione) AS
SELECT  row_number() over (order by cfti5public.fnloc_cfti_5.desloc_cfti), vpiaquo.intensità_romano, 
vpiaquo.intensità_arabo, vpiaquo.nperiod, vpiaquo.nterrs,
        cfti5public.fnloc_cfti_5.nloc_cfti, cfti5public.fnloc_cfti_5.desloc_cfti, cfti5public.fnloc_cfti_5.lat_wgs84,
		cfti5public.fnloc_cfti_5.lon_wgs84, cfti5public.fnloc_cfti_5.provlet, cfti5public.fnloc_cfti_5.notesito,
		cfti5public.fnloc_cfti_5.nloc, cfti5public.fnloc_cfti_5.desloc, cfti5public.fnloc_cfti_5.nazione
  FROM (   
	  (vpiaquo
      JOIN cfti5public.fnperiod_cfti ON ((vpiaquo.nperiod = cfti5public.fnperiod_cfti.nperiod_cfti) 
										 and vpiaquo.intensità_romano <> 'EE' ))
     LEFT JOIN cfti5public.fnloc_cfti_5 ON 
		 (    vpiaquo.nloc = cfti5public.fnloc_cfti_5.nloc
	  
		 )   
	   )   
     ;
	 
	 ALTER TABLE cfti5public.pq DROP CONSTRAINT IF EXISTS pq_pkey;
     ALTER TABLE cfti5public.pq ADD CONSTRAINT pq_pkey PRIMARY KEY (id);

	  /*
CREATE OR REPLACE VIEW  cfti5public.pq(id, intpq, intpqnum, nperiod, nterr, nloc_cfti, desloc_cfti,
										   lat_wgs84, lon_wgs84, provlet, notesito, nloc, desloc, nazione ) 
AS
SELECT  row_number() over (order by cfti5public.fnloc_cfti_5.desloc_cfti), vpiaquo.intensità_romano, 
vpiaquo.intensità_arabo, vpiaquo.nperiod, vpiaquo.nterrs,
        cfti5public.fnloc_cfti_5.nloc_cfti, cfti5public.fnloc_cfti_5.desloc_cfti, cfti5public.fnloc_cfti_5.lat_wgs84,
		cfti5public.fnloc_cfti_5.lon_wgs84, cfti5public.fnloc_cfti_5.provlet, cfti5public.fnloc_cfti_5.notesito,
		cfti5public.fnloc_cfti_5.nloc, cfti5public.fnloc_cfti_5.desloc, cfti5public.fnloc_cfti_5.nazione
  FROM (   
	  (vpiaquo
      JOIN cfti5public.fnperiod_cfti ON ((vpiaquo.nperiod = cfti5public.fnperiod_cfti.nperiod_cfti) 
										 and vpiaquo.intensità_romano <> 'EE' ))
     LEFT JOIN cfti5public.fnloc_cfti_5 ON 
		 (    vpiaquo.nloc = cfti5public.fnloc_cfti_5.nloc
	  
		 )   
	   )   
     ;
	*/
	
	
	/*
CREATE OR REPLACE VIEW  cfti5public.ee
AS
SELECT  cfti5public.ee_tabella.id, cfti5public.ee_tabella.nterr, cfti5public.ee_tabella.nperiod, cfti5public.ee_tabella.nloc,
cfti5public.ee_tabella.codice_eff, cfti5public.ee_tabella.commento, cfti5public.ee_tabella.risentimenti,
cfti5public.ee_tabella.maxint,
cfti5public.fnloc_cfti_5.nloc_cfti, cfti5public.fnloc_cfti_5.desloc_cfti, cfti5public.fnloc_cfti_5.lat_wgs84,
cfti5public.fnloc_cfti_5.lon_wgs84, cfti5public.fnloc_cfti_5.notesito, cfti5public.fnloc_cfti_5.provlet,
cfti5public.fnloc_cfti_5.nazione, cfti5public.fnloc_cfti_5.desloc
  FROM ((cfti5public.ee_tabella
        LEFT JOIN cfti5public.fnloc_cfti_5 ON ((cfti5public.ee_tabella.nloc = cfti5public.fnloc_cfti_5.nloc)))    
     );
*/

 


CREATE OR REPLACE VIEW cfti5public.locind_pq(id) AS
SELECT
	row_number() over (order by p.desloc_cfti), p.nloc_cfti, p.desloc_cfti, p.lat_wgs84, p.lon_wgs84, p.nloc, 
	p.desloc, p.provlet, p.nazione,  p.notesito,
	p.risentimenti, p.maxint
	FROM(
SELECT DISTINCT on (m.nloc_cfti)
    m.nloc_cfti, m.desloc_cfti, m.lat_wgs84, m.lon_wgs84, m.nloc, m.desloc, m.provlet, m.nazione,  m.notesito,
	t.risentimenti, t.maxint
FROM (
	SELECT intpqnum, desloc_cfti, nloc_cfti, COUNT(nloc_cfti)  OVER (PARTITION BY nloc_cfti) AS risentimenti, 
	MAX(intpqnum)  OVER (PARTITION BY nloc_cfti) AS maxint
    FROM cfti5public.pq
) t JOIN cfti5public.pq m ON m.nloc_cfti = t.nloc_cfti 
		) p 
;


CREATE OR REPLACE VIEW cfti5public.locind_ee(id) AS
SELECT
	row_number() over (order by p.desloc_cfti), p.nloc_cfti, p.desloc_cfti, p.lat_wgs84,
	p.lon_wgs84, p.nloc, p.desloc, p.provlet, p.nazione,  p.notesito, p.risentimenti, p.maxint, p.ee 
	FROM(
SELECT DISTINCT on (m.nloc_cfti)
    m.nloc_cfti, m.desloc_cfti, m.lat_wgs84, m.lon_wgs84, m.nloc, m.desloc, m.provlet, m.nazione,  m.notesito,
		m.risentimenti, m.maxint, t.ee
FROM (
    SELECT desloc_cfti, nloc_cfti, COUNT(nloc_cfti)  OVER (PARTITION BY nloc_cfti) AS ee	
    FROM cfti5public.ee
) t JOIN cfti5public.ee m ON m.nloc_cfti = t.nloc_cfti 
		) p
;		


		CREATE OR REPLACE VIEW cfti5public.locind_pq_con_ee(id) AS
		SELECT
	row_number() over (order by p.desloc_cfti), p.nloc_cfti, p.desloc_cfti,
p.lat_wgs84, p.lon_wgs84, p.nloc, p.desloc, p.provlet, p.nazione,  p.notesito, p.risentimenti,  p.maxint, p.ee 
	FROM(
SELECT cfti5public.locind_pq.nloc_cfti, cfti5public.locind_pq.desloc_cfti,
cfti5public.locind_pq.lat_wgs84, cfti5public.locind_pq.lon_wgs84, cfti5public.locind_pq.nloc,
		cfti5public.locind_pq.desloc, 
cfti5public.locind_pq.provlet, cfti5public.locind_pq.nazione,  cfti5public.locind_pq.notesito,
	cfti5public.locind_pq.risentimenti, cfti5public.locind_pq.maxint, cfti5public.locind_ee.ee
FROM cfti5public.locind_pq
LEFT JOIN cfti5public.locind_ee
ON cfti5public.locind_pq.nloc_cfti = cfti5public.locind_ee.nloc_cfti
			) p 
;

CREATE OR REPLACE VIEW cfti5public.locind_ee_senza_pq(id) AS
		 SELECT  row_number() over (order by l.desloc_cfti), l.nloc_cfti, l.desloc_cfti, l.lat_wgs84, l.lon_wgs84, l.nloc, l.desloc, l.provlet, 
		 l.nazione, l.notesito, l.risentimenti, l.maxint, l.ee
FROM    cfti5public.locind_ee l
LEFT JOIN
        cfti5public.locind_pq r
ON      r.nloc_cfti = l.nloc_cfti
WHERE   r.nloc_cfti IS NULL
;

create table if not exists cfti5public.locind (id) AS
SELECT row_number() over (order by t.desloc_cfti), t.nloc_cfti, t.desloc_cfti, t.lat_wgs84, 
t.lon_wgs84, t.provlet, t.nazione, t.notesito, t.risentimenti, t.maxint, t.ee
	FROM(
SELECT cfti5public.locind_pq_con_ee.nloc_cfti, cfti5public.locind_pq_con_ee.desloc_cfti,
cfti5public.locind_pq_con_ee.lat_wgs84, cfti5public.locind_pq_con_ee.lon_wgs84, cfti5public.locind_pq_con_ee.nloc,
		cfti5public.locind_pq_con_ee.desloc, 
cfti5public.locind_pq_con_ee.provlet, cfti5public.locind_pq_con_ee.nazione,  cfti5public.locind_pq_con_ee.notesito,
	cfti5public.locind_pq_con_ee.risentimenti, cfti5public.locind_pq_con_ee.maxint, cfti5public.locind_pq_con_ee.ee
FROM cfti5public.locind_pq_con_ee
UNION
SELECT cfti5public.locind_ee_senza_pq.nloc_cfti, cfti5public.locind_ee_senza_pq.desloc_cfti,
cfti5public.locind_ee_senza_pq.lat_wgs84, cfti5public.locind_ee_senza_pq.lon_wgs84, cfti5public.locind_ee_senza_pq.nloc,
		cfti5public.locind_ee_senza_pq.desloc, cfti5public.locind_ee_senza_pq.provlet, cfti5public.locind_ee_senza_pq.nazione,  
		cfti5public.locind_ee_senza_pq.notesito, cfti5public.locind_ee_senza_pq.risentimenti, 
		cfti5public.locind_ee_senza_pq.maxint,
		cfti5public.locind_ee_senza_pq.ee
FROM cfti5public.locind_ee_senza_pq
	)t
;

ALTER TABLE cfti5public.locind DROP CONSTRAINT IF EXISTS locind_pkey;
ALTER TABLE cfti5public.locind ADD CONSTRAINT locind_pkey PRIMARY KEY (id);


CREATE OR REPLACE VIEW  cfti5public.fnperiod_cfti_view AS
SELECT cfti5public.fnperiod_cfti.nperiod_cfti, fnperiod.nperiod, fnperiod.id 
FROM fnperiod, cfti5public.fnperiod_cfti
WHERE  cfti5public.fnperiod_cfti.nperiod_cfti = fnperiod.nperiod
; 

create table if not exists cfti5public.d1 (id) AS
SELECT  fcommenti.id, cfti5public.fnperiod_cfti_view.nperiod_cfti, ftabcatego.codtab, fcommenti.testocomm, fnloc.nloc
FROM fcommenti, cfti5public.fnperiod_cfti_view, ftabcatego, fnloc
WHERE  ftabcatego.codtab = 'D1'  and fcommenti.fnperiod = cfti5public.fnperiod_cfti_view.id 
and fcommenti.ftabcatego = ftabcatego.id and fcommenti.fnloc = fnloc.id
; 

ALTER TABLE cfti5public.d1 DROP CONSTRAINT IF EXISTS d1_pkey;
ALTER TABLE cfti5public.d1 ADD CONSTRAINT d1_pkey PRIMARY KEY (id);

/*
CREATE OR REPLACE VIEW  cfti5public.d1 AS
SELECT  fcommenti.id, cfti5public.fnperiod_cfti_view.nperiod_cfti, ftabcatego.codtab, fcommenti.testocomm, fnloc.nloc
FROM fcommenti, cfti5public.fnperiod_cfti_view, ftabcatego, fnloc
WHERE  ftabcatego.codtab = 'D1'  and fcommenti.fnperiod = cfti5public.fnperiod_cfti_view.id 
and fcommenti.ftabcatego = ftabcatego.id and fcommenti.fnloc = fnloc.id
; 
*/

/*
CREATE OR REPLACE VIEW  cfti5public.schedeb AS
SELECT fschedeb.id, cfti5public.fnperiod_cfti_view.nperiod, fscheda.codbib, fschedeb.valb, fval_fonte.desvalingl, 
CONCAT('http://www.cftilab.it/file_repository/pdf_T/',cfti5public.fnperiod_cfti_view.nperiod,'-',fscheda.codbib,'_T.pdf') As pdf_trascritto,
CONCAT('http://www.cftilab.it/file_repository/pdf_R/',cfti5public.fnperiod_cfti_view.nperiod,'-',fscheda.codbib,'_R.pdf') As pdf_raster
 FROM (
	 (
	 (public.fschedeb
     INNER JOIN cfti5public.fnperiod_cfti_view ON (fschedeb.fnperiod = cfti5public.fnperiod_cfti_view.id)  
	 )
     LEFT JOIN public.fval_fonte ON (fschedeb.fval_fonte = fval_fonte.id )
		 ) LEFT JOIN public.fscheda ON (fschedeb.fscheda = fscheda.id )
	  );
*/

create table if not exists cfti5public.schedeb (id) AS
SELECT fschedeb.id, cfti5public.fnperiod_cfti_view.nperiod, fscheda.codbib, fschedeb.valb, fval_fonte.desvalingl, 
CONCAT('http://www.cftilab.it/file_repository/pdf_T/',cfti5public.fnperiod_cfti_view.nperiod,'-',fscheda.codbib,'_T.pdf') As pdf_trascritto,
CONCAT('http://www.cftilab.it/file_repository/pdf_R/',cfti5public.fnperiod_cfti_view.nperiod,'-',fscheda.codbib,'_R.pdf') As pdf_raster
 FROM (
	 (
	 (public.fschedeb
     INNER JOIN cfti5public.fnperiod_cfti_view ON (fschedeb.fnperiod = cfti5public.fnperiod_cfti_view.id)  
	 )
     LEFT JOIN public.fval_fonte ON (fschedeb.fval_fonte = fval_fonte.id )
		 ) LEFT JOIN public.fscheda ON (fschedeb.fscheda = fscheda.id )
	  );
	  
	  ALTER TABLE cfti5public.schedeb DROP CONSTRAINT IF EXISTS schedeb_pkey;
      ALTER TABLE cfti5public.schedeb ADD CONSTRAINT schedeb_pkey PRIMARY KEY (id);

/*
CREATE OR REPLACE VIEW  cfti5public.schedea AS
SELECT DISTINCT ON (fscheda.codbib) 
fscheda.id, fscheda.codbib, fscheda.autore1, fscheda.titolo1, fscheda.luogoed, fscheda.datauni, fscheda.dataun2, fscheda.ordinam
FROM fscheda, cfti5public.schedeb
WHERE  fscheda.codbib = cfti5public.schedeb.codbib
; 
*/

create table if not exists cfti5public.schedea (id) AS
SELECT DISTINCT ON (fscheda.codbib) 
fscheda.id, fscheda.codbib, fscheda.autore1, fscheda.titolo1, fscheda.luogoed, fscheda.datauni, fscheda.dataun2, fscheda.ordinam
FROM fscheda, cfti5public.schedeb
WHERE  fscheda.codbib = cfti5public.schedeb.codbib
; 

ALTER TABLE cfti5public.schedea DROP CONSTRAINT IF EXISTS schedea_pkey;
ALTER TABLE cfti5public.schedea ADD CONSTRAINT schedea_pkey PRIMARY KEY (id);

/*
CREATE OR REPLACE VIEW  cfti5public.ee_med AS
SELECT fee_med.id, fee_med.codice_eff, fee_med.nterr, fee_med.nloc,
nterrs_med.nperiod,
nloc_med.nloc_cfti, nloc_med.desloc_cfti, nloc_med.desloc, nloc_med.lat_wgs84, nloc_med.lon_wgs84,
nloc_med.nazione, nloc_med.provlet, nloc_med.notesito
FROM( 
		  (cfti5public.fee_med LEFT JOIN cfti5public.nloc_med ON (cfti5public.fee_med.id_nloc = cfti5public.nloc_med.id)  )
		 LEFT JOIN cfti5public.nterrs_med ON (fee_med.id_nterr = nterrs_med.id) 
		 )
;
*/

create table if not exists cfti5public.ee_med (id) AS
SELECT fee_med.id, fee_med.codice_eff, fee_med.nterr, fee_med.nloc,
nterrs_med.nperiod,
nloc_med.nloc_cfti, nloc_med.desloc_cfti, nloc_med.desloc, nloc_med.lat_wgs84, nloc_med.lon_wgs84,
nloc_med.nazione, nloc_med.provlet, nloc_med.notesito
FROM( 
		  (cfti5public.fee_med LEFT JOIN cfti5public.nloc_med ON (cfti5public.fee_med.id_nloc = cfti5public.nloc_med.id)  )
		 LEFT JOIN cfti5public.nterrs_med ON (fee_med.id_nterr = nterrs_med.id) 
		 )
;

 ALTER TABLE cfti5public.ee_med DROP CONSTRAINT IF EXISTS ee_med_pkey;
 ALTER TABLE cfti5public.ee_med ADD CONSTRAINT ee_med_pkey PRIMARY KEY (id);

/*
CREATE OR REPLACE VIEW cfti5public.locind_med AS
SELECT DISTINCT on (m.nloc_cfti)
    m.id, m.nloc_cfti, m.desloc_cfti, m.lat_wgs84, m.lon_wgs84, m.nloc, m.provlet, m.notesito, m.nazione, t.risentimenti, t.maxint
FROM (
    SELECT intpqnum, desloc_cfti, nloc_cfti, COUNT(nloc_cfti)  OVER (PARTITION BY nloc_cfti) AS risentimenti, 
	MAX(intpqnum)  OVER (PARTITION BY nloc_cfti) AS maxint
    FROM cfti5public.pq_med
) t JOIN cfti5public.pq_med m ON m.nloc_cfti = t.nloc_cfti 
;
*/

create table if not exists cfti5public.pq_med (id) AS
SELECT med.fpq_med.id, med.fpq_med.intpqnum, med.fpq_med.intpq, med.nterrs_med.nterr, med.nterrs_med.nperiod, med.nloc_med.nloc_cfti,
med.nloc_med.desloc_cfti, med.nloc_med.lat_wgs84, med.nloc_med.lon_wgs84, med.nloc_med.nloc, med.nloc_med.provlet, med.nloc_med.notesito, med.nloc_med.nazione
FROM med.fpq_med, med.nloc_med, med.nterrs_med
WHERE med.fpq_med.id_nterr = med.nterrs_med.id and med.fpq_med.id_nloc = med.nloc_med.id
; 

ALTER TABLE cfti5public.pq_med DROP CONSTRAINT IF EXISTS pq_med_pkey;
ALTER TABLE cfti5public.pq_med ADD CONSTRAINT pq_med_pkey PRIMARY KEY (id);

create table if not exists cfti5public.locind_med (id) AS
SELECT DISTINCT on (m.nloc_cfti)
    m.id, m.nloc_cfti, m.desloc_cfti, m.lat_wgs84, m.lon_wgs84, m.nloc, m.provlet, m.notesito, m.nazione, t.risentimenti, t.maxint
FROM (
    SELECT intpqnum, desloc_cfti, nloc_cfti, COUNT(nloc_cfti)  OVER (PARTITION BY nloc_cfti) AS risentimenti, 
	MAX(intpqnum)  OVER (PARTITION BY nloc_cfti) AS maxint
    FROM cfti5public.pq_med
) t JOIN cfti5public.pq_med m ON m.nloc_cfti = t.nloc_cfti 
;

ALTER TABLE cfti5public.locind_med DROP CONSTRAINT IF EXISTS locind_med_pkey;
ALTER TABLE cfti5public.locind_med ADD CONSTRAINT locind_med_pkey PRIMARY KEY (id);

/*
CREATE OR REPLACE VIEW  cfti5public.pq_med AS
SELECT med.fpq_med.id, med.fpq_med.intpqnum, med.fpq_med.intpq, med.nterrs_med.nterr, med.nterrs_med.nperiod, med.nloc_med.nloc_cfti,
med.nloc_med.desloc_cfti, med.nloc_med.lat_wgs84, med.nloc_med.lon_wgs84, med.nloc_med.nloc, med.nloc_med.provlet, med.nloc_med.notesito, med.nloc_med.nazione
FROM med.fpq_med, med.nloc_med, med.nterrs_med
WHERE med.fpq_med.id_nterr = med.nterrs_med.id and med.fpq_med.id_nloc = med.nloc_med.id
; 

*/



