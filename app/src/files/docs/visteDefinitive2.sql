CREATE OR REPLACE VIEW public.vclassif(id, codice_classificazione, classificazione)
   AS SELECT 
    fclassif.id,
    fclassif.codclass,
    fclassif.desclass,
	fclassif.titolo_label1
   FROM public.fclassif;

CREATE OR REPLACE VIEW public.vnloc(id, codice_toponimo, nome_toponimo, latitudine, longitudine, lat_centesimale, lon_centesimale ,quota, 
									numero_abitanti_1971, codice_provincia, provincia_nome, codice_regione, codice_comune, 
									codice_frazione, foglio_igm, tavoletta_igm, risentimenti, nazione, altro_nome_localita, note_storiche, note)
AS SELECT 
    fnloc.ID,
    fnloc.NLOC,
    fnloc.DESLOC,
    fnloc.LATLOC,
    fnloc.LONGLOG,
	fnloc.lat_centesimale,
	fnloc.lon_centesimale,
    fnloc.QUOTLOC,
    fnloc.NUMABIT,
    fnloc.PROV,
    fnloc.PROVLET,
    fnloc.REGIONE,
    fnloc.COMUNE,
    fnloc.FRAZION,
    fnloc.FOGLIO,
    fnloc.TAVOLET,
    fnloc.RISENTIMENTI,
    fnloc.SIGLASTATO,
    fnloc.DESLOC2,
    fnloc.NOTESITO,
    fnloc.NOTENL,
	fnloc.titolo_label1
   FROM public.fnloc;
   
 CREATE OR REPLACE VIEW public.vscheda1(id, codice_sede_ricerca, sede_ricerca, indirizzo, cap, localita, provincia, telefono, note,codice_toponimo,nome_toponimo,id_fnloc)
   AS SELECT 
   fscheda1.ID,
   fscheda1.CODA1,
   fscheda1.DESA1,
   fscheda1.INDA1,
   fscheda1.CAPA1,
   fscheda1.LOCA1,
   fscheda1.PROVA1,
   fscheda1.TELA1,
   fscheda1.NOTEA1,
   fnloc.nloc,
   fnloc.desloc,
   fnloc.id,
   fscheda1.titolo_label1,
   fscheda1.titolo_label2
   FROM (public.fscheda1
     LEFT JOIN public.fnloc ON ((fscheda1.fnloc = fnloc.id)))
     ;
	 
 CREATE OR REPLACE VIEW public.vtipfen(id, codice_fenomeno, fenomeno,note)
   AS SELECT 
    ftipfen.id,
    ftipfen.codtf,
    ftipfen.destf,
	ftipfen.notetf,
	ftipfen.titolo_label1
   FROM public.ftipfen;
	 

 CREATE OR REPLACE VIEW public.veffetti(Id, Codice_Effetto, Effetto, note) 
   AS SELECT 
   feffetti.id,
    feffetti.codeff,
    feffetti.deseff,
	feffetti.noteeff,
	feffetti.titolo_label1
   FROM public.feffetti;	
   
CREATE OR REPLACE VIEW public.vval_fonte(id, codice_valore, valore, note, denominazione_inglese, ordinamento)
AS SELECT 
    fval_fonte.id,
    fval_fonte.codval,
    fval_fonte.DESVAL,
	fval_fonte.noteval,
	fval_fonte.DESVALINGL,
	fval_fonte.ORDINE_VAL,
	fval_fonte.titolo_label1
   FROM public.fval_fonte;
   
   
CREATE OR REPLACE VIEW public.vcommenti(id,codice_sequenza,inizio_sequenza,fine_sequenza,id_fnperiod,codice_categoria_commento,
										categoria_commento,id_ftabcatego,
									   codice_effetto,effetto,id_feffetti, codice_totale_commento,testo_commento, 
										codice_toponimo,nome_toponimo,lat_centesimale,lon_centesimale,provincia,nazione,id_fnloc, 
										testocomm_tsv)
AS SELECT 
    fcommenti.id,
	fnperiod.nperiod,
	fnperiod.DATAINIZNP,
	fnperiod.DATAFINENP,
	fnperiod.id,
	ftabcatego.codtab,
	ftabcatego.destab,
	ftabcatego.id,
	feffetti.CODEFF,
	feffetti.DESEFF,
	feffetti.id,
    fcommenti.codcomm,
    fcommenti.testocomm,
	fnloc.NLOC,
	fnloc.DESLOC,
	fnloc.lat_centesimale,
	fnloc.lon_centesimale,
	fnloc.provlet,
	fnloc.siglastato,
	fnloc.id,
	to_tsvector(fcommenti.testocomm),
	fcommenti.titolo_label1,
	fcommenti.titolo_label2,
	fcommenti.titolo_label3,
	fcommenti.titolo_label4,
	fcommenti.titolo_label5,
	fcommenti.titolo_label6,
	fcommenti.titolo_label7
   FROM ((((public.fcommenti
     LEFT JOIN public.fnperiod ON ((fcommenti.fnperiod = fnperiod.id)))
     LEFT JOIN public.ftabcatego ON ((fcommenti.ftabcatego = ftabcatego.id)))
     LEFT JOIN public.feffetti ON ((fcommenti.feffetti = feffetti.id)))
     LEFT JOIN public.fnloc ON ((fcommenti.fnloc = fnloc.id)));
	 
CREATE OR REPLACE VIEW public.vnperiod(id,codice_sequenza,inizio_sequenza,fine_sequenza,numero_commenti,numero_referenze,numero_testi
									  ,livello_approfondimento,area_epicentrale,note)
AS SELECT 
    fnperiod.id,
    fnperiod.nperiod,
    fnperiod.DATAINIZNP,
    fnperiod.DATAFINENP,
    fnperiod.NUMCOMM,
    fnperiod.NUMBIBLIO,
    fnperiod.NUMTB,
    fnperiod.LIVNP,
	fnperiod.areaepicentrale,
	fnperiod.COMMENTONPERIOD,
	fnperiod.titolo_label1,
	fnperiod.titolo_label2
	 FROM( 
		  (public.fnperiod LEFT JOIN public.ftipfen ON (fnperiod.ftipfen = ftipfen.id)  )
		 LEFT JOIN public.feffetti ON (fnperiod.feffetti = feffetti.id) 
		 )
     ;
	 
	 
CREATE OR REPLACE VIEW public.vtabcatego(id, codice_categoria_commento, categoria_commento,ordinamento,categoria_commento_inglese)
   AS SELECT 
    ftabcatego.id,
    ftabcatego.CODTAB,
    ftabcatego.DESTAB,
	ftabcatego.ORDINE,
    ftabcatego.DESING,
	ftabcatego.titolo_label1
   FROM public.ftabcatego;
   
CREATE OR REPLACE VIEW public.vscheda(id,Codice_bibliografico,Codice_ricerca,Autore,Tipo_autore,Ruolo_autore,Riferimento_geografico,
						  ruolo_autore_da,ruolo_autore_a,titolo,DALTIT,ALTIT,Riferimento_geografico_titolo,luogo_edizione,data_edizione_ms1,
						  data_edizione_ms2,codice_microfilm,fotogramma_iniziale,codice_microfilm_enel,PROTA,NOTEA,COLLOCA,
						  ORDINAM,TIPOA,codice_sede_ricerca,sede_ricerca,id_fscheda1,codice_classificazione,classificazione,id_fclassif)
AS SELECT 
    fscheda.id,
    fscheda.CODBIB,
    fscheda.CODRIC,
    fscheda.AUTORE1,
    fscheda.EDITAUT,
    fscheda.RUOLO,
    fscheda.RIGEAUT,
    fscheda.DALAUT,
    fscheda.ALAUT,
    fscheda.TITOLO1,   
    fscheda.DALTIT,
    fscheda.ALTIT,
    fscheda.RIGETIT,
	fscheda.LUOGOED,
    fscheda.DATAUNI,   
    fscheda.DATAUN2,
    fscheda.CODMICR,
    fscheda.FOTINIZ,
	fscheda.CODMICRENEL,
	fscheda.PROTA,
    fscheda.NOTEA,   
    fscheda.COLLOCA,
    fscheda.ORDINAM,
    fscheda.TIPOA,
	fscheda1.CODA1,
	fscheda1.DESA1,
	fscheda1.ID,
	fclassif.CODCLASS,
	fclassif.DESCLASS,
	fclassif.ID,
	fscheda.titolo_label1,
	fscheda.titolo_label2
   FROM ((public.fscheda
     LEFT JOIN public.fscheda1 ON ((fscheda.fscheda1 = fscheda1.id)))
     LEFT JOIN public.fclassif ON ((fscheda.fclassif = fclassif.id)));
	 
	 
CREATE OR REPLACE VIEW public.vschedeb(id,codice_bibliografico,autore,titolo,data_edizione_ms1,data_edizione_ms2,luogo_di_edizione,
								 	   Data_evento_descritto_dal,
                                       Data_evento_descritto,Data_evento_descritto_al,Note,Codice_sede_ricerca,Sede_ricerca,Testo_trascritto,codice_effetto,descrizione_effetto,
                                       codice_fenomeno,fenomeno, Codice_sequenza,Inizio_sequenza,Fine_sequenza,codice_valore,valore,
									   id_fscheda,id_fscheda1,id_feffetti,id_ftipfen, id_fnperiod,id_fval_fonte,
									   testob_tsv, pdf_trascritto, pdf_raster
									  )
AS SELECT 
    fschedeb.id,
	fscheda.CODBIB,
	fscheda.AUTORE1,
	fscheda.TITOLO1,
	fscheda.DATAUNI,
	fscheda.DATAUN2,
	fscheda.LUOGOED,
	fschedeb.DATAB,
    fschedeb.DAB,
    fschedeb.AB,
    fschedeb.NOTEB,
	fscheda1.CODA1,
    fscheda1.DESA1,
	fschedeb.TESTOB,
	feffetti.CODEFF,
	feffetti.DESEFF,
	ftipfen.CODTF,
	ftipfen.DESTF,
    fnperiod.NPERIOD,
	fnperiod.datainiznp,
	fnperiod.DATAFINENP,
	fval_fonte.CODVAL,
	fval_fonte.DESVAL,
	fscheda.ID,
	fscheda1.id,
	feffetti.id,
	ftipfen.id,
	fnperiod.id,
	fval_fonte.id,
	to_tsvector(fschedeb.testob),
	CONCAT('http://www.cftilab.it/file_repository/pdf_T/',fnperiod.NPERIOD,'-',fscheda.CODBIB,'_T.pdf'),
	CONCAT('http://www.cftilab.it/file_repository/pdf_R/',fnperiod.NPERIOD,'-',fscheda.CODBIB,'_R.pdf'),
	fschedeb.titolo_label1,
    fschedeb.titolo_label2,
    fschedeb.titolo_label3,
	fschedeb.titolo_label4,
    fschedeb.titolo_label5,
	fschedeb.titolo_label6
   FROM ((((((public.fschedeb
     LEFT JOIN public.fscheda ON ((fschedeb.fscheda = fscheda.id)))
     LEFT JOIN public.fscheda1 ON ((fschedeb.fscheda1 = fscheda1.id)))
     LEFT JOIN public.feffetti ON ((fschedeb.feffetti = feffetti.id)))
     LEFT JOIN public.ftipfen ON ((fschedeb.ftipfen = ftipfen.id)))
     LEFT JOIN public.fnperiod ON ((fschedeb.fnperiod = fnperiod.id)))
     LEFT JOIN public.fval_fonte ON ((fschedeb.fval_fonte = fval_fonte.id)));
	 
	 
CREATE OR REPLACE VIEW public.vedifici(id, classificazione_edificio, codice_edificio,nome_edificio,note_edificio,nome_localita,
									  codice_localita,numero_abitanti,nome_provincia ,id_fnloc)
   AS SELECT 
    fedifici.id,
    fedifici.CLASSED,
    fedifici.CODICEDIF,
	fedifici.DESCRIZEDIF,
	fedifici.NOTED,
	fnloc.DESLOC,
	fnloc.NLOC,
	fnloc.NUMABIT,
	fnloc.PROVLET,
	fnloc.ID,
	fedifici.titolo_label1,
	fedifici.titolo_label2
   FROM (public.fedifici
     LEFT JOIN public.fnloc ON ((fedifici.fnloc = fnloc.id)))
     ;
	 
CREATE OR REPLACE VIEW public.vscossec(id,codice_sequenza,inizio_sequenza,fine_sequenza,numero_terremoto,data_origine,
									   orario_origine,Lat_epic_strum,Lon_epic_strum,Lat_epic_macro,Lon_epic_macro,
									   zona_epicentrale,n_punti_pq,profondità,
									  io,imax,id_fnperiod,titolo_label1,titolo_label2,lat_centesimale,lon_centesimale)
AS SELECT 
    fscossec.id,
    fnperiod.nperiod,
    fnperiod.datainiznp,
    fnperiod.datafinenp,
    fscossec.nterrs,
    fscossec.dataors,
    fscossec.oraorgs,
    fscossec.latitus,
    fscossec.longits,
    fscossec.latmacs,
    fscossec.lonmacs,
    fscossec.loceps,
    fscossec.npunpqs,
    fscossec.profsts,
    fscossec.ios,
    fscossec.imaxs,
	fnperiod.id,
    fscossec.titolo_label1,
    fscossec.titolo_label2,
	fscossec.lat_centesimale,
	fscossec.lon_centesimale
   FROM (public.fscossec
     LEFT JOIN public.fnperiod ON ((fscossec.fnperiod = fnperiod.id)));
	 
	 
	 
	 
 CREATE OR REPLACE VIEW public.vpiaquo(id, località, intensità_romano ,intensità_arabo ,provenienza_PQ,codice_totale_pq,
									  note)
   AS SELECT 
    fpiaquo.id,
	fpiaquo.DESLOCAPQ, 
	fpiaquo.INTPQ, 
	fpiaquo.INTPQNUM, 
	fpiaquo.TIPO_PQ, 
	fpiaquo.CODTOTPQ, 
	fpiaquo.NOTE_PQ,
	fnloc.nloc, 
	fnloc.desloc, 
	fnperiod.nperiod, 
	fscossec.nterrs,
	fpiaquo.titolo_label1	
	 FROM ((((public.fpiaquo
     LEFT JOIN public.fnloc ON ((fpiaquo.fnloc = fnloc.id)))
     LEFT JOIN public.fnperiod ON ((fpiaquo.fnperiod = fnperiod.id)))
     LEFT JOIN public.fscossec ON ((fpiaquo.fscossec = fscossec.id)))
     );
	
	 
	      /* FSOSSEC */
	CREATE OR REPLACE RULE vscossec_UPDATE AS ON UPDATE TO vscossec DO INSTEAD (    
	   UPDATE fscossec SET 
		nterrs=NEW.numero_terremoto, dataors=NEW.data_origine,
		ORAORGS=NEW.orario_origine, 
		LATITUS=NEW.lat_epic_strum , LONGITS=NEW.lon_epic_strum, LATMACS=NEW.lat_epic_macro, 
		LONMACS=NEW.lon_epic_macro, LOCEPS=NEW.zona_epicentrale, NPUNPQS=NEW.n_punti_pq, PROFSTS=NEW.profondità,
		IOS=NEW.io, IMAXS=NEW.imax, lat_centesimale=NEW.lat_centesimale, lon_centesimale=NEW.lon_centesimale
		WHERE id=OLD.id;	
      );  
	  
	   CREATE OR REPLACE RULE vscossec_INSERT AS ON INSERT TO vscossec DO INSTEAD(
		  INSERT INTO fscossec(id,nterrs, dataors, ORAORGS, LATITUS, LONGITS, LATMACS, LONMACS, LOCEPS, NPUNPQS
							 , PROFSTS, IOS, IMAXS, lat_centesimale, lon_centesimale)
		   VALUES (
           NEW.id,
           NEW.numero_terremoto,
		   NEW.data_origine,
		   NEW.orario_origine,
		   NEW.lat_epic_strum,
		   NEW.lon_epic_strum,
		   NEW.lat_epic_macro,
		   NEW.lon_epic_macro,
								 NEW.zona_epicentrale,
								 NEW.n_punti_pq,
								 NEW.profondità,
								 NEW.io,
								 NEW.imax,
			   NEW.lat_centesimale,
			   NEW.lon_centesimale
								
         );
	  );

	  
	      /* FSCHEDA */
	CREATE OR REPLACE RULE vscheda_UPDATE AS ON UPDATE TO vscheda DO INSTEAD (    
	   UPDATE fscheda SET 
		codbib=NEW.codice_bibliografico, codric=NEW.codice_ricerca,
		autore1=NEW.autore, editaut=NEW.tipo_autore , titolo1=NEW.titolo, ruolo=NEW.ruolo_autore, 
		dalaut=NEW.ruolo_autore_da, alaut=NEW.ruolo_autore_a,
		daltit=NEW.daltit, altit=NEW.altit, rigeaut=NEW.riferimento_geografico, rigetit=NEW.riferimento_geografico_titolo,
		luogoed=NEW.luogo_edizione
		, datauni=NEW.data_edizione_ms1, dataun2=NEW.data_edizione_ms2, fotiniz=NEW.fotogramma_iniziale, 
		codmicrenel=NEW.codice_microfilm_enel, codmicr=NEW.codice_microfilm,
		prota=NEW.prota, colloca=NEW.colloca, tipoa=NEW.tipoa, ordinam=NEW.ordinam
		, notea=NEW.notea
		WHERE id=OLD.id;	
      );  
	  
	   CREATE OR REPLACE RULE vscheda_INSERT AS ON INSERT TO vscheda DO INSTEAD(
		  INSERT INTO fscheda(id,codbib, codric, autore1, editaut, titolo1, ruolo, dalaut, alaut, daltit, altit, rigeaut
							 , rigetit, luogoed, datauni, dataun2, fotiniz, codmicrenel, codmicr, prota, colloca, tipoa, ordinam, notea)
		   VALUES (
           NEW.id,
           NEW.codice_bibliografico,
		   NEW.codice_ricerca,
		   NEW.autore,
		   NEW.tipo_autore,
		   NEW.titolo,
		   NEW.ruolo_autore,
		   NEW.ruolo_autore_da,
		   NEW.ruolo_autore_a,
								 NEW.daltit,
								 NEW.altit,
								 NEW.riferimento_geografico,
								 NEW.riferimento_geografico_titolo,
								 NEW.luogo_edizione,
								 NEW.data_edizione_ms1,
								 NEW.data_edizione_ms2,
								 NEW.fotogramma_iniziale,
								 NEW.codice_microfilm_enel,
								 NEW.codice_microfilm,
								 NEW.prota,
								 NEW.colloca,
								 NEW.tipoa,
								 NEW.ordinam,
								 NEW.notea
         );
	  );
	  
	    /* FSCHEDA1 */
	CREATE OR REPLACE RULE vscheda1_UPDATE AS ON UPDATE TO vscheda1 DO INSTEAD (    
	   UPDATE fscheda1 SET 
		coda1=NEW.codice_sede_ricerca, desa1=NEW.sede_ricerca,
		inda1=NEW.indirizzo, capa1=NEW.cap , loca1=NEW.localita, prova1=NEW.provincia, 
		tela1=NEW.telefono, notea1=NEW.note
		WHERE id=OLD.id;	
      );  
	  
	   CREATE OR REPLACE RULE vscheda1_INSERT AS ON INSERT TO vscheda1 DO INSTEAD(
		  INSERT INTO fscheda1(id,coda1, desa1, inda1, capa1, loca1, prova1, tela1, notea1 ) VALUES (
           NEW.id,
           NEW.codice_sede_ricerca,
		   NEW.sede_ricerca,
		   NEW.indirizzo,
		   NEW.cap,
		   NEW.localita,
		   NEW.provincia,
		   NEW.telefono,
		   NEW.note
         );
	  );
	  
	   /* FEDIFICI */
	CREATE OR REPLACE RULE vedifici_UPDATE AS ON UPDATE TO vedifici DO INSTEAD (    
	   UPDATE fedifici SET classed=NEW.classificazione_edificio, codicedif=NEW.codice_edificio,
		descrizedif=NEW.nome_edificio, noted=NEW.note_edificio WHERE id=OLD.id;	
      );  
	  
	   CREATE OR REPLACE RULE vedifici_INSERT AS ON INSERT TO vedifici DO INSTEAD(
		  INSERT INTO fedifici(id,classed, codicedif, descrizedif, noted ) VALUES (
           NEW.id,
           NEW.classificazione_edificio,
		   NEW.codice_edificio,
		   NEW.nome_edificio,
		   NEW.note_edificio
         );
	  );
	  
	  
	   /* COMMENTI */
	CREATE OR REPLACE RULE vcommenti_UPDATE AS ON UPDATE TO vcommenti DO INSTEAD (    
	   UPDATE fcommenti SET TESTOCOMM=NEW.testo_commento WHERE id=OLD.id;	
      );  
	  
	   CREATE OR REPLACE RULE vcommenti_INSERT AS ON INSERT TO vcommenti DO INSTEAD(
		  INSERT INTO fcommenti(id,testocomm) VALUES (
           NEW.id,
           NEW.testo_commento
         );
	  );
	  
	  /* SCHEDE B */
	  CREATE OR REPLACE RULE vschedeb_UPDATE AS ON UPDATE TO vschedeb DO INSTEAD (    
	   UPDATE fschedeb SET testob=NEW.testo_trascritto, noteb = NEW.note, dab = NEW.data_evento_descritto,
		  ab = NEW.data_evento_descritto_al, datab = NEW.data_evento_descritto_dal WHERE id=OLD.id;	
      ); 
	  CREATE OR REPLACE RULE vschedeb_INSERT AS ON INSERT TO vschedeb DO INSTEAD(
		  INSERT INTO fschedeb(id,testob, noteb, dab, ab, datab) VALUES (
              NEW.id,
              NEW.testo_trascritto,
			  NEW.note,
			  NEW.data_evento_descritto,
			  NEW.data_evento_descritto_al,
			  NEW.data_evento_descritto_dal 
         );
	  );
	  




/* FSCOSSEC trigger coordinate centesimali  */

CREATE OR REPLACE FUNCTION coordinate_sessagesimali_lat_fscossec()
RETURNS trigger
LANGUAGE plpgsql
SECURITY DEFINER
AS $BODY$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lat_centesimale::text, '.', 1);
 decimali = split_part(NEW.lat_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 
 IF( v2::numeric = 0)
  then v3 = '00';	
 elseif (LENGTH(v2::text)=1) then
   v3 = concat('0',v2);
 elseif (LENGTH(split_part(v2, '.', 1)) = 2 )
 then  v3 = split_part(v2, '.', 1);
  elseif (LENGTH(split_part(v2, '.', 1)) = 1 )
 then  v3 = concat('0',split_part(v2, '.', 1));
	ELSE v3 = v2;
 END IF;
 

  NEW.latmacs = ( 
    
	             concat(gradi,v3)::numeric
    
   );
    RETURN NEW;
END
$BODY$;


DROP TRIGGER IF EXISTS fscossec_trigger1 on fscossec;
CREATE  TRIGGER fscossec_trigger1
BEFORE INSERT OR UPDATE of lat_centesimale
ON fscossec
FOR EACH ROW 
EXECUTE PROCEDURE coordinate_sessagesimali_lat_fscossec();



CREATE OR REPLACE FUNCTION coordinate_sessagesimali_lon_fscossec()
RETURNS trigger
LANGUAGE plpgsql
SECURITY DEFINER
AS $BODY$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lon_centesimale::text, '.', 1);
 decimali = split_part(NEW.lon_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 
 IF( v2::numeric = 0)
  then v3 = '00';	
 elseif (LENGTH(v2::text)=1) then
   v3 = concat('0',v2);
 elseif (LENGTH(split_part(v2, '.', 1)) = 2 )
 then  v3 = split_part(v2, '.', 1);
  elseif (LENGTH(split_part(v2, '.', 1)) = 1 )
 then  v3 = concat('0',split_part(v2, '.', 1));
	ELSE v3 = v2;
 END IF;
 

  NEW.lonmacs = ( 
    
	             concat(gradi,v3)::numeric
    
   );
    RETURN NEW;
END
$BODY$;


DROP TRIGGER IF EXISTS fscossec_trigger2 on fscossec;
CREATE  TRIGGER fscossec_trigger2
BEFORE INSERT OR UPDATE of lon_centesimale
ON fscossec
FOR EACH ROW 
EXECUTE PROCEDURE coordinate_sessagesimali_lon_fscossec();



/* FNLOC trigger coordinate centesimali  */

CREATE OR REPLACE FUNCTION coordinate_sessagesimali_lat()
RETURNS trigger
LANGUAGE plpgsql
SECURITY DEFINER
AS $BODY$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lat_centesimale::text, '.', 1);
 decimali = split_part(NEW.lat_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 v3 = split_part(v2, '.', 1);
 IF ( LENGTH(v3)=1 ) 
 then  v3 = CONCAT('0',v3);
 END IF;
 v4 = split_part(v2, '.', 2);
 v4 = concat('0.',v4);
 v5 = round((v4::numeric * 60),2)::text;
 v6 =  split_part(v5, '.', 1);
 IF ( LENGTH(v6)=1 ) 
 then  v6 = CONCAT('0',v6);
 END IF;
 v7 =  split_part(v5, '.', 2);
  NEW.latloc = ( 
    
	             concat(gradi,v3,v6,v7)
    
   );
    RETURN NEW;
END
$BODY$;

DROP TRIGGER IF EXISTS fnloc_trigger1 on fnloc;
CREATE  TRIGGER fnloc_trigger1
BEFORE INSERT OR UPDATE of lat_centesimale
ON fnloc
FOR EACH ROW
EXECUTE PROCEDURE coordinate_sessagesimali_lat();



CREATE OR REPLACE FUNCTION coordinate_sessagesimali_lon()
RETURNS trigger
LANGUAGE plpgsql
SECURITY DEFINER
AS $BODY$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lon_centesimale::text, '.', 1);
 decimali = split_part(NEW.lon_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 v3 = split_part(v2, '.', 1);
 IF ( LENGTH(v3)=1 ) 
 then  v3 = CONCAT('0',v3);
 END IF;
 v4 = split_part(v2, '.', 2);
 v4 = concat('0.',v4);
 v5 = round((v4::numeric * 60),2)::text;
 v6 =  split_part(v5, '.', 1);
 IF ( LENGTH(v6)=1 ) 
 then  v6 = CONCAT('0',v6);
 END IF;
 v7 =  split_part(v5, '.', 2);
  NEW.longlog = ( 
    
	             concat(gradi,v3,v6,v7)
    
   );
    RETURN NEW;
END
$BODY$;

DROP TRIGGER IF EXISTS fnloc_trigger2 on fnloc;
CREATE  TRIGGER fnloc_trigger2
BEFORE INSERT OR UPDATE of lon_centesimale
ON fnloc
FOR EACH ROW
EXECUTE PROCEDURE coordinate_sessagesimali_lon();


