#! /usr/bin/python

##=========================================================================================
##                                  GENERAZIONE PDF FONTI
##                                  Giulia - Gennaio 2017
##                                  ---------------------
##
##   Il programma utilizza il toolkit ReportLab per Python (si può installare con PIP).
##   Vedi manuale nella cartella INGV/PYTHON (Giulia).
##   Costruisce file pdf partendo da file di testo. I file di testo sono contenuti nella
##   cartella definita in path_testi e poi vengono salvati in pdf con stesso nome del .txt,
##   nella cartella definita in path_out.
##   Il testo viene scritto sopra immagine sullo sfondo di tutte pagine (l'immagine del libro CFTI).
##   Banner: è costruito apposta per i PDF ed ha un'immagine e una frase con un LINK che rimanda
##   a una apposita pagina di disclaimer.

##   I VARI PARAMETRI (TRA CUI LINK E NOMI FILE IMMAGINE E PERCORSI CARTELLE) sono definiti a inizio script.
##   NB: le dimensioni degli spazi, dei loghi ecc sono definite dentro il codice, non all'inizio come gli altri input.
##       Quindi, se servisse modificarle, bisogna starci un po' attenti.
##
##   Reportlab riconosce l'HTML (ad es per impostare stili di testo..).
##   Attualmente il programma utilizza i tools di Reportlab 'canvas' e 'platypus' insieme.
##   Con canvas costruisce lo sfondo di tutte le pagine e con il flowable 'paragraph'
##   (che fa parte di Platypus) scrive il testo nelle pagine risconoscendo paragrafi sulla base
##   dei separatori specificati (\r per fine riga e \t per tab).
##   Al tab è fatta corrispondere spaziatura più grande per separare il titolo.



##   FILE INPUT:
##   - file di testo da trasformare in PDF (vedi sotto info sulla codifica) dentro una cartella il cui percorso
##      va impostato a inizio codice
##   - file 'SCHIANTSTILETAV05.jpg', che contiene l'immagine di sfondo da libro CFTI
##   - file di immagine del banner: 'banner_CFTI_PDF_noAut_ITA.jpg'

##   FILE OUTPUT:
##   - file pdf con stesso nome file input, ma con aggiunta di "_T.pdf" alla fine, vengono salvati in una cartella
##     il cui percorso va impostato a inizio codice
##   - file "TEXTproblems.txt" che elenca, eventualmente, i file che hanno dato problemi e sono stati saltati.

##   Codifica: UTF-8
##   NB: sulla codifica ci sono stati vari problemi. Nel codice, in basso, c'è una riga con %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
##       Lì ci sono due opzioni per la definizione dell'encoding. Su windows funzionava ANSI. Su mac funziona UTF-8
##       (con i file di testo codificati di conseguenza, ovviamente, altrimenti non funziona).

##   COSA MODIFICARE PER FARLO FUNZIONARE: oltre a dover installare ReportLab, impostare percorsi
##   a file e cartelle. Poi si possono modificare nomi immagini di sfondo e vari layout....

##   DEFINIZIONE COSTRUZIONE PAGINE: In questo caso definito un solo tipo di pagine,
##  usato per tutte (si può ad esempio creare la prima pagina diversa...). Qui inserisco le immagini
##  che devono fare da sfondo (logo CFTI in alto e immagine libro in basso). Questo viene salvato nella
##  canvas "myPages" che poi viene richiamata alla fine quando si costruisce il pdf, per far scrivere
##  il testo sulle pagine con lo sfondo definito qui.

##  DEFINIZIONE LAYOUT PDF: margini, giustificazione, carattere, indentazione paragrafi...
##  Creati due layout diversi per le prime righe di intestazione e per il testo. 'Justify' è
##  usato per il testo e semplicemente è Times-New-Roman giustificato.
##  'JustifySpaced' agiunge maggiore spaziatura tra le righe ed è utilizzato per le prime righe (titolo,
##  autore...) quando c'è una separazione fatta da tab. Questo perchè con font più grande è necessario
##  aumentare interlinea altrimenti si sovrappongono le righe.
##
##  SCRITTURA DEL TESTO: da platypus utilizza 'paragraphs', costruendo una storia come somma di tanti paragrafi
##  definiti sulla base di spaziatura o a capo delle righe (letti dal file di testo in input)
##  Risconosce l'HTML (es. grassetto e italics)
##  'Spacer' definisce spaziatura tra i paragrafi. In questo caso impostato più grande quando c'è il tab,
##  per separare meglio l'intestazione iniziale (che è dove ci sono i tab! - titolo, autore, anno, ecc..)
##
##                              ---------------------------------
##                              Ultimo aggiornamento: APR 2020
##
##-------------------------------------------------------------------------------------------------------------

import os
import string
import codecs
from pathlib import Path
from pdfrw import PdfReader, PdfWriter
from pdfrw.toreportlab import makerl
from pdfrw.buildxobj import pagexobj
from reportlab.lib import utils
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm, inch
from reportlab.lib.styles import getSampleStyleSheet, PropertySet, ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
from reportlab.platypus import Paragraph, SimpleDocTemplate, Spacer, Image

#------------ IMPOSTA PERCORSI CARTELLE
# Questo setta il percorso alla cartella di lavoro
path = os.getcwd()
os.chdir(path)

# Questo setta percorso a cartella con fonti .txt (dentro cartella di lavoro)
path_testi = ('./TXT_TESTI/')

# Percorso a cartella che conterrà pdf di output (dentro cartella di lavoro)
path_out = ('./PDF_TESTI/')

#------------ IMPOSTA IMMAGINI E LINK
sfondo = 'SCHIANTSTILETAV05.jpg'
linkdisclaimer = 'http://storing.ingv.it/cfti/disclaimer.html'
banner = 'banner_CFTI_PDF_noAut_ITA.jpg'
width_banner = 19.5 # larghezza figura (altezza viene scalata di conseguenza)

# calcola dimensioni banner dalla larghezza, tenendo proporzioni
img = utils.ImageReader(banner)
iw, ih = img.getSize()
aspect = iw / ih

#------------ FILE CON LISTA DI TESTI CON PROBLEMI
filelist_PDFproblems = open('TEXTproblems.txt', 'w')


#------------ CICLO TRA FILE .TXT
for filename in os.listdir(path_testi):

    try:

        # Leggi file di testo uno per uno, definendo corretto encoding %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         #f = codecs.open(path_testi+'/'+filename, mode='r', encoding='ansi', errors='strict', buffering=1) # su Windows
        f = codecs.open(path_testi+'/'+filename, mode='r', encoding='UTF-8', errors='strict', buffering=1)

        #with open(path_testi+'/'+filenamefilename, 'rb') as f: return hashlib.sha1(f.read()).hexdigest() == sha1

        # f = codecs.open(path_testi+'/'+filename, mode='rb', encoding='cp1252', errors='strict', buffering=1)
        # f = codecs.open(path_testi+'/'+filename, mode='rb', encoding='latin_1', errors='strict', buffering=1)
        content = f.read()
        f.close()

        ## corrections to text due to encoding from OMNIS
        content = content.replace("—", "\"")
        content = content.replace("˜", "\"")
        

        ## -------  ADD HYPERLINK FOR REFERENCE, 0.6 cm sotto il banner
        items = [];
        textlink = '<font color="#1f708f"><b>LINK</b></font>'
        reference = '<font size=9 color="#a9a9aa">Permission to use this file is granted subject to full acknowledgement of the source in the form available at this</font> ';
        URL = '<link href="' + linkdisclaimer + '">' + textlink + '</link>';

        #--------definisci nome file pdf output
        pdfname = path_out + filename[0:13] + '_T.pdf'
        #pdfname = filename + '_prova.pdf'

        #------------- DEFINISCI LAYOUT PDF:
        pdf = SimpleDocTemplate(pdfname, pagesize=A4, topMargin=5*cm,leftMargin=2*cm,rightMargin=2*cm,bottomMargin=8*cm)
        style = getSampleStyleSheet()
        style.add(ParagraphStyle(name='JustifyDiscl', alignment=TA_JUSTIFY, fontName='Helvetica', firstLineIndent=0))  # style disclaimer with link
        style.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY, fontName='Times-Roman', firstLineIndent=0))
        style.add(ParagraphStyle(name='Center', alignment=TA_CENTER, fontName='Times-Roman', firstLineIndent=0))
        style.add(ParagraphStyle(name='JustifySpaced', alignment=TA_JUSTIFY, fontName='Times-Roman', firstLineIndent=0,leading=20))

        #------------ DEFINISCI COSTRUZIONE PAGINE - QUI SI LEGGONO LE IMMAGINI DI SFONDO
        def myPages(canvas, pdf):

            canvas.saveState()
            canvas.drawImage(banner, cm*.5, cm*28.3, width=width_banner*cm, height = (width_banner/aspect)*cm)
            canvas.drawImage(sfondo, cm*.5, cm*1, width=20*cm, height=26*cm)
            linkP = Paragraph(reference+URL,style['JustifyDiscl'])
            linkP.wrap(20*cm, 1*cm)
            linkP.drawOn(canvas,cm*.5, cm*27.5)
            canvas.restoreState()


        #------------- SCRITTURA DEL TESTO:
        story = []
        parag = []
        paragraphs = content.split('\r')
        n = 0
        for para in paragraphs:
            n = n + 1
            if n>1:
                ## corrections to text due to HTML TAG code only after the 1st paragraph
                para = para.replace("<", "&lt;")
                para = para.replace(">˜", "&gt;")
                # para = para.append('<br>')
            pars = para.split('\t')
            if len(pars)>1:

                for par in pars:
                    uppercase = sum(1 for c in par if c.isupper())
                    if uppercase!=0 and len(par)/uppercase <= 2.5:
                        subparlist = par.split('/')
                        for subpar in subparlist:
                            story.append(Paragraph(subpar,style['Center']))
                    else:
                        story.append(Paragraph(par,style['Justify']))
                    story.append(Spacer(0, .5*cm))
            else:
                uppercase = sum(1 for c in para if c.isupper())
                if uppercase!=0 and len(para)/uppercase <= 2.5:
                    subparalist = para.split('/')
                    for subpara in subparalist:
                        story.append(Paragraph(subpara,style['Center']))
                else:
                    story.append(Paragraph(para,style['Justify']))
                story.append(Spacer(0, 0.2*cm))

        print(pdfname)
        pdf.build(story, onFirstPage=myPages, onLaterPages=myPages)

    except:
        filelist_PDFproblems.write("%s\n" % filename)
        pass

filelist_PDFproblems.close()
