<?php
require_once("funzioni.php");
require_once("./inc/conn.php"); 
require_once("./inc/layouts.php");
require('./plugins/phpmailer/class.phpmailer.php');

proteggi(1); 
        
//gestione errori non catturati
getErrori();
        
//array con elenco encoding (serve dopo per detect encoding)
$arrayEncoding = ['macintosh', 'UCS-4*', 'UCS-4BE', 'UCS-4LE*', 'UCS-2', 'UCS-2BE', 'UCS-2LE', 'UTF-32*', 'UTF-32BE*', 'UTF-32LE*', 'UTF-16*', 'UTF-16BE*', 'UTF-16LE*', 'UTF-7', 'UTF7-IMAP', 'UTF-8*', 'ASCII*', 'EUC-JP*', 'SJIS*', 'eucJP-win*', 'SJIS-win*', 'ISO-2022-JP', 'ISO-2022-JP-MS', 'CP932', 'CP51932', 'SJIS-mac', 'SJIS-win', 'SJIS', 'SJIS-Mobile#DOCOMO', 'SJIS-Mobile#KDDI', 'SJIS-Mobile#SOFTBANK', 'UTF-8-Mobile#DOCOMO', 'UTF-8-Mobile#KDDI-A', 'UTF-8-Mobile#KDDI-B', 'UTF-8-Mobile#SOFTBANK', 'ISO-2022-JP-MOBILE#KDDI', 'JIS', 'JIS-ms', 'CP50220', 'CP50220raw', 'CP50221', 'CP50222', 'ISO-8859-1*', 'ISO-8859-2*', 'ISO-8859-3*', 'ISO-8859-4*', 'ISO-8859-5*', 'ISO-8859-6*', 'ISO-8859-7*', 'ISO-8859-8*', 'ISO-8859-9*', 'ISO-8859-10*', 'ISO-8859-13*', 'ISO-8859-14*', 'ISO-8859-15*', 'ISO-8859-16*', 'byte2be', 'byte2le', 'byte4be', 'byte4le', 'BASE64', 'HTML-ENTITIES', '7bit', '8bit', 'EUC-CN*', 'CP936', 'GB18030', 'HZ', 'EUC-TW*', 'CP950', 'BIG-5*', 'EUC-KR*', 'UHC', 'ISO-2022-KR', 'Windows-1251', 'Windows-1252', 'CP866', 'KOI8-R*', 'KOI8-U*', 'ArmSCII-8'];

//leggo il file json che contiene le informazioni di configurazione dell'import
$jsonConfigurationFile = 'configurazioneImport.json';

$arrayEccezioni = [];
//se esiste
try{
    if (file_exists($jsonConfigurationFile)) {
    //prendo il contenuto
    $content = file_get_contents($jsonConfigurationFile);
    if ($content) {
        //dal contenuto creo un oggetto json 
        $configObj = json_decode($content);
    }else{
        throw new Exception("File di configurazione vuoto");

    }
    }else{
        throw new Exception("File di configurazione non trovato. Verificare che il nome sia corretto ($jsonConfigurationFile)");
    }
} catch (Exception $e){
    error_log($e->getMessage());
    $arrayEccezioni[] = $e->getMessage();
    
}

        

//ciclo sulle proprietà di configurazione e assegno le proprietà a delle variabili
foreach ($configObj AS $property => $valoreProperty) {
    switch ($property) {
        case 'nFilesDaImportare':
            $nFilesDaImportare = (int) $valoreProperty;
            break;
        case 'ordine' :
            $arrayOrdineFiles = $valoreProperty;
            break;
        case 'nomiTabelle' :
            $arrayNomiTabelle = $valoreProperty;
            break;
        case 'tipoImport' :
            $tipoImport = $valoreProperty;
            break;
        case 'primoImport' :
            $fileErroreDat = $valoreProperty;
            break;
    }
}


//controllo a che step siamo. Default step = 0
if (!isset($_GET['step']))
    $step = 0;
else
    $step = (int) $_GET['step'];

//in base allo step eseguo una funzione diversa
switch ($step) {

    case 0: import_step0();
        break;

    case 1: import_step1();
        break;

    case 11: import_step1_options();
        break;

    case 2: import_step2();
        break;

    case 21: import_step2_ins();
        break;

    case 3: import_step3();
        break;

    case 4: import_step4();
        break;

    case 5: exec_query_import();
        break;

    case 6: import_query();
        break;

    case 61: import_query(true);
        break;

    case 7: pulisci_import();
        break; 
    
    default: import_step0();
}

    

//fine area epicentrale

//workaround for 4th param.
function vf_fgetcsv($fp, $lenght = null, $sep = null, $quote = null) {

    if ($quote == null) {
        return fgetcsv($fp, $lenght, $sep);
    } else {
        return fgetcsv($fp, $lenght, $sep, $quote);
    }
}

function controllo_tipo($dato, $tipo, $is_nullable) {

    global $vmsql, $vmreg;

    if (($dato == '' || $dato == 'NULL') && $is_nullable == 'YES') {

        return "NULL,";
    } else if (in_array($tipo, array('int', 'mediumint', 'tinyint', 'bigint', 'smallint'))) {

        return (int) $dato . ",";
    } else if (in_array($tipo, array('double', 'float'))) {

        return (double) $dato . ",";
    } else {

        return "'" . $vmsql->escape($dato) . "',";
    }
}

function modificatori($input, $i, $post_data) {


    if (isset($post_data['mod'][$i])) {


        foreach ($post_data['mod'][$i] as $k => $mod) {

            switch ($mod) {

                case 'upper': $input = strtoupper($input);
                    break;
                case 'lower': $input = strtolower($input);
                    break;
                case 'upperfirst': $input = ucfirst($input);
                    break;
                case 'upperword': $input = ucwords($input);
                    break;
                case 'md5': $input = md5($input);
                    break;
                case 'sha1': $input = sha1($input);
                    break;
                case 'prefisso': $input = $post_data['pref'][$i] . $input;
                    break;
            }
        }
    }

    return $input;
}

function errors_mysql($n) {

    switch ($n) {

        case 1062: return _('Duplicate key');
        case 1364: return _('A record which is specified as "not null" doesn\'t have a default value');
        case 1471: return _('Table not writable: file permissions error');
        case 1146: return _('Non-existent table: error');
        case 1022: return _('Can\'t add record - Duplicate key');
        case 1452: return _('Unable to add a record - The reference is missing and/or not connected to the reference table.');
        default : return "";
    }
}

// STEP 0
// LAYOUT CHE PERMETTE DI SELEZIONARE LA CARTELLA CHE CONTIENE I FILES DA IMPORTARE
function import_step0() {
    global $nFilesDaImportare;
    global $arrayOrdineFiles;
    global $configObj, $arrayEccezioni;

    //in $out inserisco la struttura html della pagina
    $OUT = '';
    //apro il layout di Vfront
    echo openLayout1(_("Data import"), array(), 'popup');
    echo breadcrumbs(array("HOME", "Import Files"));
 
    echo "<h1>" . 'Importa i files' . "</h1>\n";
    
    //verifico se ci sono errori nei passi precedenti
    if(count($arrayEccezioni) > 0){
        foreach ($arrayEccezioni AS $key => $value){
            echo "<h3 style=\"background-color:Tomato;\">$value</h3>";
        }
    }
        
    //controllo la proprietà max_file_uploads di php.ini (massimo numero di files che è possibile caricare)
    $php_max_import = ini_get("max_file_uploads");
    if ($php_max_import > $nFilesDaImportare) {
        $impostazioniPhp = "max_file_uploads (Numero massimo di files che è possibile caricare): {$php_max_import}";
        $coloreStringa = 'Aquamarine';
    } else {
        $impostazioniPhp = "max_file_uploads (Numero massimo di files che è possibile caricare): {$php_max_import} è minore del numero di files da importare<br>Modificare file php.ini";
        $coloreStringa = 'Tomato';
    }

    //menu a tendina con elenco files da importare
    $selectFiles = '';
    $primoImport = $configObj->primoImport;
    $numDaImport = $configObj->nFilesDaImportare;
    foreach ($arrayOrdineFiles AS $key => $value) {
        $selected = '';
        if ($value === $primoImport) {
            $selected = 'selected';
        }
        $selectFiles .= "<option value=\"$value\" $selected>$value</option>";
    }
    //inserisco il FORM
    $OUT .= "<div id=\"form-container\">
			<form enctype=\"multipart/form-data\" method=\"post\" action=\"" . Common::phpself() . "?step=1\" >
	
			<p id=\"csvfile-box\">
                        <h2>Impostazioni PHP di import:</h2>
                        <p style=\"background-color:{$coloreStringa};\">$impostazioniPhp</p>
                        <h2>Configurazione di Import:</h2>  
                        <label>N file da importare:</label><input type=\"number\" id=\"nFilesDaImportare\" name=\"nFilesDaImportare\" value=\"$numDaImport\"><br>
                        <label>Tipo di Import:</label>
                         <select name=\"tipoImport\" id=\"tipoImport\">
                            <option value=\"dalprimo\">Dal primo file</option>
                            <option value=\"nondalprimo\">Dall'ultimo non importato</option>
                        </select>
                        <br>
                        <label>Primo File da importare:</label>                        
                        <select name=\"primoImport\" id=\"primoImport\">" . $selectFiles
            . "
                        </select>
                        <h2>Seleziona i files da importare:</h2>  
			<input type=\"file\" name=\"csvfile[]\" id=\"csvfile\" multiple/>
			</p>
			
			<p><input type=\"submit\" id=\"send\" value=\"  " . _('Next') . "  &gt;&gt; \" /></p>
			
			</form>
		</div>
		";

    echo $OUT;

    echo closeLayout1();
}

function loadAllFiles($arrayFiles, $arrayFileNames, $arrayErrors, $destination) {

    global $nFilesDaImportare;
    global $arrayOrdineFiles;
    global $configObj;
    $primoFileDaImportare = $configObj->primoImport;
    $tipoImport = $configObj->tipoImport;

    //array di errori di caricamento
    $arrayErroriCaricamento = [];
    //assegno all'array dei files caricati i files nell'ordine che serve a rispettare le chiavi esterne
    //variabile di controllo
    $import = 'no';
    $cont = 0;
    foreach ($arrayOrdineFiles AS $key => $value) {

        
        
        //variabile di controllo per verificare se file è stato già importato (leggo dal dat file il file non importato)
        if ($value === $primoFileDaImportare) {
            $import = 'si';
        }
        if($import=='si'){
            $cont++;
        }
        
        if($cont>$nFilesDaImportare){
            break;
        }

        //se nel file di config ho l'import totale, effetto l'import su tutti i files
        if ($tipoImport === 'dalprimo') {
            //cerco la posizione $k
            $k = array_search($value, $arrayFileNames);
            //controllo se il file è stato caricato correttamente e lo inserisco nell'array nella posizione adeguata in base all'ordine di import
            if ($k !== false) { 
                try { 
                    controlFile($arrayFiles, $arrayFileNames, $arrayErrors, $destination, $k);
                } catch (Exception $e) {
                    array_push($arrayErroriCaricamento, $e->getMessage);
                }
            } else {
                array_push($arrayErroriCaricamento, "non trovo il file $value tra quelli selezionati. Verificare i nomi dei file");
                break;
            }
        }
        //altrimenti effetto l'import solo su quelli non importati (utile quando la procedura si blocca per un errore su una tabella)
        else {
            //
            if ($import === 'si') {
                $k = array_search($value, $arrayFileNames);
                //controllo se il file è stato caricato correttamente e lo inserisco nell'array nella posizione adeguata in vbase all'ordine di import
                if ($k !== false) {
                    try {
                        controlFile($arrayFiles, $arrayFileNames, $arrayErrors, $destination, $k);
                    } catch (Exception $e) {
                        array_push($arrayErroriCaricamento, $e->getMessage);
                    }
                } else {
                    array_push($arrayErroriCaricamento, "non trovo il file $value tra quelli selezionati. Verificare i nomi dei file");
                    break;
                }
            }
        }
        

    }

    return $arrayErroriCaricamento;
}

//funzione che controlla se un file è stato caricato e lo inserisce nell'array dei files caricati
function controlFile($arrayFiles, $arrayFileNames, $arrayErrors, $destination, $k) {


    static $position = 0;
    if (is_uploaded_file($arrayFiles[$k]) && $arrayErrors[$k] === UPLOAD_ERR_OK) {
        //carico il file
        $result = move_uploaded_file($arrayFiles[$k], $destination . $arrayFileNames[$k]);
        //controllo se move_uploaded_file è ok
        if ($result) {
            echo "<h3 style=\"background-color:Aquamarine;\">File {$arrayFileNames[$k]} è stato caricato</h3>";
            $_SESSION['import']['files_caricati'][$position]['file'] = $arrayFiles[$k];
            $_SESSION['import']['files_caricati'][$position]['nome'] = $arrayFileNames[$k];
        } else {

            //sollevo un'eccezione
            throw new Exeption("File {$arrayFileNames[$k]} non caricato");
        }
    }

    $position++;
}

// STEP 1
// carico i files e se ci sono tutti stampo il tasto next per andare avanti
function import_step1() {

    global $configObj, $jsonConfigurationFile;
    echo openLayout1(_("Data import"), array(), 'popup');

    //recupero i valori passati dal form
    foreach ($_POST AS $key => $value) {

        $configObj->$key = $value;
    }
    $ordine = $configObj->ordine;
    if ($configObj->tipoImport === 'dalprimo') {
        $configObj->primoImport = $ordine[0];
    }

    $nFilesDaImportare = $configObj->nFilesDaimportare;

    //trasformo oggetto in json
    $json = json_encode($configObj);
    //controllo di avere i permessi di scrittura sul file e scrivo il json sul file di configurazione
    if (is_writable($jsonConfigurationFile)) {
        $flPutCont = file_put_contents($jsonConfigurationFile, $json);
        if ($flPutCont === false) {
            echo "<h3 style=\"background-color:Tomato;\">Problema nell'apertura del file di configurazione</h3>";
            exit;
        }
    } else {
        echo "<h3 style=\"background-color:Tomato;\">Il file non ha i permessi di scrittura - cambiare i permessi del file $jsonConfigurationFile</h3>";
        exit;
    }


    //prendo i files, i nomi dei files e gli errori eventuali di caricamento
    $arrayFiles = $_FILES['csvfile']['tmp_name'];
    $arrayFileNames = $_FILES['csvfile']['name'];
    $arrayErrors = $_FILES['csvfile']['error'];

    //dove copiare i files 
    $destination = _PATH_TMP . "/";

    echo "<h2>Caricamento files:</h2>";

    //devo selezionare tutti i files (per rispettare le chiavi esterne)
    if (count($arrayFiles) < $nFilesDaImportare) {
        echo "<h3 style=\"background-color:Tomato;\">Mancano dei files nella selezione!" . " nFile: " . count($arrayFiles) . "</h3>";
    } else {

        //resetto array
        unset($_SESSION['import']['files_caricati']);

        //chiamo la funzione che carica tutti i files nell'ordine di import
        $arrayErroriCaricamento = loadAllFiles($arrayFiles, $arrayFileNames, $arrayErrors, $destination);

        if (count($arrayErroriCaricamento) > 0) {
            foreach ($arrayErroriCaricamento AS $key => $value) {
                echo "<h3 style=\"background-color:Tomato;\">$value</h3>";
            }
        } else {
            echo '<br>';
            echo "<a href= './files/tmp/' target = '_blank'>Cartella tmp files</a>";
            echo '<br>';
            echo '<br>';

            //se i file sono stati caricati posso procedere allo step 2
            if (count($arrayErrors === 0)) {
                $onclick = "location.href = ' " . Common::phpself() . "?step=2' ";
                echo "<input type=\"button\" value=\"Next >>\" onclick=\"$onclick\">";
            } else {
                echo "errore di caricamento: " . $arrayErrors[0];
            }
        }
    }

    echo closeLayout1();
}

function import_step1_options() {


    $_SESSION['import']['quote'] = $_POST['quote'];
    $_SESSION['import']['sep'] = $_POST['sep'];

    header("Location: " . $_SERVER['PHP_SELF'] . "?step=2");
    exit;
}

// STEP 2
// SHOW OPTIONS AND PREVIEW
function import_step2() {

    global $vmsql, $vmreg, $db1, $arrayEncoding, $arrayNomiTabelle, $arrayOrdineFiles;

    //inclcludo alcuni files
    $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js");

    echo openLayout1(_("Data import"), $files, 'popup');
    echo "<h1>" . _('Import files selezionati nelle tabelle relative:') . "</h1>\n";
    echo '<h3>';
    //stampo elenco dei files importati
    foreach ($_SESSION['import']['files_caricati'] AS $file) {

        echo " {$file['nome']}  ";
    }
    echo '</h3>';
    //form per gestire separatore campi e quote
    echo "<form action=\"" . Common::phpself() . "?step=11\" method=\"post\">\n";
    $sepr[0] = ($sep == ';') ? ' selected="selected"' : '';
    $sepr[1] = ($sep == ',') ? ' selected="selected"' : '';
    $sepr[2] = ($sep == 'tab') ? ' selected="selected"' : '';
    $sepr[3] = ($sep == 'space') ? ' selected="selected"' : '';
    $quot[0] = ($quote == 'double') ? ' selected="selected"' : '';
    $quot[1] = ($quote == 'single') ? ' selected="selected"' : '';
    $quot[2] = ($quote == 'null' || $quote == null) ? ' selected="selected"' : '';
    echo "<p>
			<label for=\"sep\">" . _('Separator') . "</label>
			<select id=\"sep\" name=\"sep\">
				<option value=\";\"{$sepr[0]}>; </option>
				<option value=\",\"{$sepr[1]}>,</option>
				<option value=\"tab\"{$sepr[2]}>TAB</option>
				<option value=\"space\"{$sepr[3]}>SPACE</option>
			</select>
			<label for=\"quote\">" . _('Quote') . "</label>
			<select id=\"quote\" name=\"quote\">
				<option value=\"double\"{$quot[0]}>&quot;</option>
				<option value=\"single\"{$quot[1]}>'</option>
				<option value=\"null\"{$quot[2]}> </option>
			</select>	
			<input type=\"button\" onclick=\"submit()\" value=\"" . _('Change csv options') . "\" />		
		</p>\n";
    echo "</form>\n";

    //ORDINO L'ARRAY DEI FILES CARICATI IN MODO DA RISPETTARE LE CHIAVI ESTERNE 
    $problemaEncoding = array();
    //variabile che contiene html di preview di tutte le tabelle
    $tabelle = '';
    $array_encoding = array();
    //PER OGNI FILE - questo ciclo popola la variabile $tabelle 
    foreach ($_SESSION['import']['files_caricati'] AS &$file) {
        //cambio encoding del file
        $objEncoding = changeEncoding($file['nome']);
        if ($objEncoding->encodingOk === false) {
            array_push($problemaEncoding, $file['nome']);
        }

        //var_dump($objEncoding);
        //RECUPERO INFO TABELLA 
        //recupero nome tabella 
        $nome_tab = '';
        $k = array_search($file['nome'], $arrayOrdineFiles);
        $nome_tab = $arrayNomiTabelle[$k];

        $file['tabella'] = $nome_tab;

        // prendo i campi disponibili per la tabella
        $campi_tab = RegTools::prendi_colonne_frontend($nome_tab, "column_name", false);

        // leggo il file
        $handle = fopen(_PATH_TMP . "/" . $file['nome'], 'r');
        //conto il numero di righe del file
        $n_righe = 0;
        while (!feof($handle)) {
            $buffer = fgets($handle, 16384);
            if ($buffer != "\n" && $buffer != "") {
                $n_righe++;
            }
        }
        fclose($handle);

        //numero di righe
        $_SESSION['import']['length'] = ($n_righe);

        //il separatore di default è ; 
        if (isset($_SESSION['import']['sep'])) {

            // SEPARATORE
            switch ($_SESSION['import']['sep']) {

                case ';': $sep = ';';
                    break;
                case ',': $sep = ',';
                    break;
                case 'tab': $sep = "\t";
                    break;
                case 'space': $sep = " ";
                    break;
                default: $sep = ';';
            }
        } else {
            $sep = ';';
        }
        //il quote di default è null
        if (isset($_SESSION['import']['quote'])) {

            switch ($_SESSION['import']['quote']) {

                case 'single': $quote = "'";
                    break;
                case 'double': $quote = '"';
                    break;
                case 'null': $quote = null;
                    break;
                default: $quote = null;
            }
        } else {

            $quote = null;
        }


        //apro il file
        $fp = fopen(_PATH_TMP . "/" . $file['nome'], 'r');
        //prima riga
        $row = 1;

        //scrivo quante righe ha il file

        if ($_SESSION['import']['length'] === 0) {
            $style = "style=\"background-color:Tomato;\"";
        } else {
            $style = "style=\"background-color:Acquamarine;\"";
        }

        $T = "<h3 $style>Data preview tabella $nome_tab (commento:" . $objEncoding->commento . ")- numero di righe del file: " . $_SESSION['import']['length'] . '</h3>';

        //creo la tabella
        $T .= "<table summary=\"csv\" id=\"csv-table\" >\n";

        //scorro tutte le righe del file
        while (($CSV = vf_fgetcsv($fp, null, $sep, $quote)) !== FALSE) {

            //fgetcsv restiruisce un array con le colonne del csv
            $num_colonne = count($CSV);
            //se siamo nella prima riga 
            if ($row == 1) {

                //apro la prima riga
                $T .= "<tr>\n";

                //per ogni colonna creo l'intestazione con il menu a tendina 
                for ($c = 0; $c < $num_colonne; $c++) {



                    //colonna
                    $T .= "<th class=\"int$c\">" . $campi_tab[0][$c] . "</th>\n";
                    //$T .= "<th class=\"int$c\"><select name=\"{$nome_tab}_i[$c]\" id=\"i$c\" onchange=\"colora_riga($c);\">$opzioni</select></th>\n";
                }
                //chiudo riga
                $T .= "</tr>\n";
                //riga vuota di separazione
                $T .= "<tr id=\"tr-sep\"><td colspan=\"" . ($num_colonne) . "\">&nbsp;</td></tr>\n";
            }

            //per ogni riga del csv faccio una riga in tabella
            $T .= "<tr class=\"r$row\">\n";
            //aggiungo le varie colonne 
            for ($c = 0; $c < $num_colonne; $c++) {
                $T .= "<td class=\"c$c\">" . $CSV[$c] . "</td>\n";
            }
            //chiudo la riga
            $T .= "</tr>\n";
            //incremento la riga
            $row++;
            //la preview è di 20 righe
            if ($row >= 52)
                break;
        }
        //chiudo il file
        fclose($fp);
        //chiudo la tabella
        $T .= "</table>\n";

        //inserisco la tabella nella variabile che raccoglie tutte le tabelle
        $tabelle .= $T;

        //importo dei files
        $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js");
        //resetto $campi_tab
        unset($campi_tab);
        unset($T);
        unset($nome_tab);
    }


    //form con  checkbox per skip the first line
    echo "<form action=\"" . Common::phpself() . "?step=21&file={$file['nome']}\" method=\"post\">\n";
    //checkbox per non importare la prima riga (nel caso in cui nel file ho l'intestazione)
    echo "<p><input type=\"checkbox\" id=\"prima_riga\" name=\"prima_riga\" value=\"1\" onclick=\"colora_prima_riga();\" /> <label for=\"prima_riga\">" . _('Skip the first line') . "</label> </p>\n";

    //STAMPO A VIDEO EVENTUALI PROBLEMI DI ENCODING
    echo "<h3>Problemi di Encoding: </h3>";
    if (count($problemaEncoding) > 0) {
        echo "<h3 style=\"background-color:Tomato;\">";
        foreach ($problemaEncoding AS $proEnc) {
            echo $proEnc . ' ';
        }
        echo "</h3>";
    } else {
        echo "<h3 style=\"background-color:Aquamarine;\">Nessun problema di encoding</h3>";
    }

    //STAMPO A VIDEO LE PREVIEW DELLE TABELLE 
    if ($tabelle) {
        echo $tabelle;
    } else {
        echo "<h3 style=\"background-color:Tomato;\">Nessun file rispetta i criteri di import. Controllare il  ";
        echo "<a href='configurazioneImport.json'>File di Configurazione</a></h3>";
    }

    //next o previews
    echo "<p><input type=\"button\" onclick=\"history.back()\" value=\"&lt;&lt; " . _('Previous') . "\" />   <input type=\"button\" onclick=\"submit()\" value=\"" . _('Next') . " &gt;&gt;\" /></p>\n";
    //chiudo il form 
    echo "</form>\n";

    echo closeLayout1();
}

function import_step2_ins() {


    //ciclo sui post che vengono dalle varie tabelle
    foreach ($_POST AS $key => $value) {

        //recupero il nome della tabella 
        $expl = explode('_', $key);
        $nome_tab = $expl[0];

        echo '<br>';
        //escludo il campo prima riga perchè voglio solo i campi delle select che ho scelto
        if ($key != 'prima_riga') {
            $campi = '';
            // per ogni tabella ciclo sulle colonne
            for ($j = 0; $j < count($value); $j++) {

                //inserisco in $campi i campi da processare come stringa
                if ($value[$j] != '') {

                    $_da_processare[] = $j;

                    $campi .= $value[$j] . ",";
                }
            }
            //tolgo la virgola finale
            $campi = substr($campi, 0, -1);

            //inserisco la stringa dei campi da processare come elemento array
            $array_campi_da_processare[$nome_tab] = $campi;
        }
    }
    var_dump($array_campi_da_processare);

    //$_SESSION['import']['campi_da_processare'] = $_da_processare;
    $_SESSION['import']['nome_campi'] = $array_campi_da_processare;

    //se ho saltato la prima riga
    if (isset($_POST['prima_riga'])) {
        $_SESSION['import']['prima_riga'] = $_POST['prima_riga'];
    }


    header("Location: " . $_SERVER['PHP_SELF'] . "?step=5");
    exit;
}

// STEP 2 
// IMPORT DATA


function import_step3() {

    global $vmsql, $vmreg, $db1;

    $nome_tab = RegTools::oid2name($_SESSION['import']['oid']);

    // prendi i campi disponibili per la tabella
    $campi_tab = RegTools::prendi_colonne_frontend($nome_tab, "column_name,data_type,is_nullable", false);

    $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js");

    echo openLayout1(_("Data import"), $files, 'popup');

    echo "<h1>" . sprintf(_('Data import step %d of %d'), 3, 4) . "</h1>";

    echo "<p>" . _('If you need to perform operations on the fields identified for the import or insert constants, operate from this window, otherwise go ahead') . "</p>\n";

    echo "<form action=\"" . Common::phpself() . "?step=4\" method=\"post\" >\n";

    echo "<p><input type=\"button\" onclick=\"history.back()\" value=\"&lt;&lt; " . _('Previous') . "\" />   <input type=\"button\" onclick=\"submit()\" value=\"" . _('Next') . " &gt;&gt;\" /></p>\n";

    echo "<table summary=\"tabella importazione2\" id=\"tabella-conversioni\">\n";

    for ($i = 0; $i < count($campi_tab[0]); $i++) {

        echo "<tr class=\"import-campo\" id=\"imp_" . $campi_tab[0][$i] . "\">\n";

        echo "<td><strong>" . $campi_tab[0][$i] . "</strong></td>\n";

        if (in_array($campi_tab[0][$i], explode(",", $_SESSION['import']['nome_campi']))) {

            $importazione_csv = "<span class=\"verde\"><strong>" . _('Selected value') . "</strong></span>";
        } else {
            $importazione_csv = '<span class="fakelink small" onclick="set_costante(' . $i . ');" >&lt;' . _('set constant value') . '&gt;</span>' .
                    '<span id="costante_' . $i . '"  style="display:none" > <input type="text" name="costante[' . $i . ']" /> ' .
                    ' <span class="fakelink small" onclick="unset_costante(' . $i . ');" id="costante_' . $i . '_trigger">' . _('remove') . '</span></span>';
        }


        echo "<td id=\"td$i\" width=\"340\"><div class=\"up\"></div><span class=\"fakelink small\" id=\"s$i\" onclick=\"modificatore($i,this);\">&lt;" . _('add action') . "&gt;</span></td>";

        echo "<td width=\"270\">" . $importazione_csv . "</td>\n";

        echo "</tr>\n";
    }



    echo "</table>\n";

    echo "<p><input type=\"button\" onclick=\"history.back()\" value=\"&lt;&lt; " . _('Previous') . "\" />   <input type=\"button\" onclick=\"submit()\" value=\"" . _('Next') . " &gt;&gt;\" /></p>\n";

    echo "</form>\n";

    echo closeLayout1();
}

// STEP 3
// REFRESH WINDOWS

function import_step4() {

    $_SESSION['import']['post_data'] = $_POST;
    header("Location: " . $_SERVER['PHP_SELF'] . "?step=5");
    exit;
}

//step 6
function import_query($only_sql = false) {

    global $configObj, $jsonConfigurationFile;

    ini_set('max_execution_time', 2000);

    //recupero variabili dal global scope
    global $vmsql, $vmreg, $db1;

    echo '<br>';

    //imposto il separatore
    switch ($_SESSION['import']['sep']) {

        case ';': $sep = ';';
            break;
        case ',': $sep = ',';
            break;
        case 'tab': $sep = "\t";
            break;
        case 'space': $sep = " ";
            break;
        default: $sep = ';';
    }

    //recupero i files caricati
    $array_file_caricati = $_SESSION['import']['files_caricati'];

    $numeroTabelleImportate = 0;
    $numeroTabelleErrori = 0;
    $arrayTabelleImportate = [];
    $arrayTabelleErrori = [];

    //prima di iniziare l'import resetto il file .dat
    $configObj->numeroTabelleImportate = 0;
    $configObj->errore = '';
    $configObj->tipoErrore = '';
    $json = json_encode($configObj);
    $fp = fopen($jsonConfigurationFile, 'w');
    fwrite($fp, $json);
    fclose($fp);

//    $fp_json = fopen('./files/tmp/primoImport' . ".dat", 'w');
//    $JSON = "[{'numeroTabelleImportate': 0, 'errore': '', 'tipoErrore':''}]";
//    fwrite($fp_json, $JSON);
//    fclose($fp_json);
    //per ogni file
    foreach ($array_file_caricati AS $file) {

        //array con tutte le righe del file
        $arrayRigheCsv = [];
        //apro il file
        $fp = fopen('./files/tmp/' . $file['nome'], 'r');

        //ciclo che legge nel file csv 
        while (($csv_row = fgetcsv($fp, null, $sep)) !== false) {

            //riga che contiene tutte le colonne
            $riga_string = '';
            //ciclo sulle colonne della riga csv 
            foreach ($csv_row AS $val) {
                $riga_string .= $val . "\t";
            }
            //inserisco la riga come stringa nell'array $arrayRigheCsv
            array_push($arrayRigheCsv, substr($riga_string, 0, -1));
        }
        //chiudo il file
        fclose($fp);

        //prendo il nome della tabella sulla quale fare import 
        $tabella = $file['tabella'];

        //EFFETTUO LA COPIA DALL'ARRAY (FILE) ALLA TABELLA 

        $copy = pg_copy_from($vmsql->link_db, $tabella, $arrayRigheCsv, "\t", null);
        $copyError = '';
        $fileNonImportato = '';
        //controllo che la copia sia avvenuta correttamente    
        if ($copy === true) {
            $numeroTabelleImportate++;
            array_push($arrayTabelleImportate, $tabella);
        } else {

            $copyError_ = trim(preg_replace('~[\r\n]+~', ' ', pg_last_error($vmsql->link_db)));
            $copyError = str_replace("'", " ", $copyError_);
            $tabellaNonImportata = $tabella;
            $fileNonImportato = $file['nome'];
            // inserisco nel file di configurazione dell'import il valore della prima tabella da importare
            $configObj->primoImport = $fileNonImportato;
            $json = json_encode($configObj);
            $fp = fopen($jsonConfigurationFile, 'w');
            fwrite($fp, $json);
            fclose($fp);
            break;
        }
    }

//    $cardinalitPrimoImport = array_search($configObj->primoImport, $configObj->ordine);
//    $numeroTabelleImportate = $numeroTabelleImportate + $cardinalitPrimoImport;
    // Scrittura del file di stato JSON (che periodicamente viene letto e mostra i risultati all'utente mentre la procedura procede)
    $configObj->numeroTabelleImportate = $numeroTabelleImportate;
    $configObj->errore = $fileNonImportato;
    $configObj->tipoErrore = $copyError;

    $JSON = json_encode($configObj);
    $fp2 = fopen($jsonConfigurationFile, 'w');
    fwrite($fp2, $JSON);
    fclose($fp2);

//    $fp_json = fopen('./files/tmp/importTesti' . ".json", 'w');
//    $JSON = "[{'numeroTabelleImportate': $numeroTabelleImportate, 'errore': '$fileNonImportato', 'tipoErrore':'$copyError'}]";
//    fwrite($fp_json, $JSON);
//    fclose($fp_json);

    echo "<a href='./files/tmp/'>Cartella TMP</a>";

    echo '<br>';
    echo _PATH_TMP_HTTP;
}

function exec_query_import() {

    global $vmsql, $vmreg, $db1, $nFilesDaImportare, $jsonConfigurationFile;

    $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js", "js/open_window.js");

    // tipo di browser.. serve per il corretto funzionamento della percentuale
    include_once("./inc/func.browser_detection.php");
    $browser = browser_detection('full');
    if ($browser[0] == 'ie')
        $files[] = "sty/import_ie.css";

    //apro il layout
    echo openLayout1(_("Import execution"), $files, 'popup');

    echo "<h1>" . " <span class=\"var\">" . _('Import processing') . "</span></h1>\n";

    //numero tabelle
    $num_tabelle = count($_SESSION['import']['nome_campi']);

    //struttura dei campi in cui inserirò le info di import
    echo "<div>" . _('Import status') . " <span id=\"feed1\">&nbsp;</span></div>\n";
    echo "<div>" . _('Csv inseriti:') . " <span id=\"ins\">0</span> /25- " . _('Errors:') . " <span id=\"errori\">0</span></div>\n";
    echo "<div><span id=\"percento\">0% " . _('done') . "</span></div>\n";

    echo "<p><input type=\"button\" id=\"importa\" value=\" " . _('Import') . " \" />"
    . " <input type=\"button\" id=\"annulla\" value=\" " . _('Cancel') . " \" disabled=\"disabled\" />"
    . "</p>\n";

    echo "<div id=\"reset_id\">Stato:</div>";

    echo "<div id=\"txt\">
			<div id=\"txt_start\"></div>
			<div id=\"txt_errore\"></div>
			<div id=\"txt_end\"></div>
                        <br><br>
                        <div id=\"folder_link\"></div>
                        
		  </div>\n";

    echo "<div id=\"perc-barra\" ></div>\n";
    ?>

    <script type="text/javascript">

        /* <![CDATA[ */

        var stato;
        var updater = null;
        var azione_rollback = false;

        function add0(dd) {
            dd = dd + '';
            return (dd.length == 1) ? '0' + dd : dd;
        }

        function esecuzione() {


            //disabilita tasti import, cancel and close
            $('importa').disable();
            $('annulla').enable();


            //oggetto data
            var d = new Date();

            //dentro il riquadro inserisco ora di inizio
            $('txt_start').update(add0(d.getHours()) + ":" + add0(d.getMinutes()) + ":" + add0(d.getSeconds()) + ' -- <?php echo _('Begin procedure'); ?></span><br />');

            //periodicamente vado a leggere il file di stato .dat per vedere a che punto è l'import 
            updater = new PeriodicalExecuter(function (pe) {

                //lettura del file .dat
                new Ajax.Request("<?php echo $jsonConfigurationFile; ?>",
                        {
                            method: 'post',
                            onComplete: null,
                            onSuccess: function (transport) {

                                //trasport è il contenuto del file dat
                                $('feed1').update('<?php echo _('Processing...'); ?>');

                                let stato = JSON.parse(transport.responseText);


                                $('ins').update(stato.numeroTabelleImportate);

                                //blocco quando ho importato tutti i files
                                if (stato.numeroTabelleImportate == <?php echo $nFilesDaImportare ?> ) {

                                    pe.stop();

                                    d = new Date();

                                    $('feed1').update('<strong><?php echo _('Operation completed'); ?></strong>');
                                    $('annulla').disable();

                                    $('txt_end').update(add0(d.getHours()) + ":" + add0(d.getMinutes()) + ":" + add0(d.getSeconds()) + ' -- <?php echo _('Operation completed'); ?></span>');
                                    $('folder_link').update("<a href='./files/tmp/' target=\"_blank\">Cartella TMP</a>");
                                    window.opener.location = window.opener.location;
                                }

                                //se c'è errore chiudo la procedura
                                if (stato.errore != '') {
                                    console.log(stato.tipoErrore);
                                    $('txt_errore').update('Errore import tabella:' + stato.errore + '-' + stato.tipoErrore);
                                    pe.stop();

                                    d = new Date();

                                    $('feed1').update('<strong><?php echo _('Operation completed'); ?></strong>');
                                    $('annulla').disable();
                                    $('errori').update(stato.errore);

                                    $('txt_end').update(add0(d.getHours()) + ":" + add0(d.getMinutes()) + ":" + add0(d.getSeconds()) + ' -- <?php echo _('Operation completed'); ?></span>');
                                    $('folder_link').update("<a href='./files/tmp/'>Cartella TMP</a>");
                                    window.opener.location = window.opener.location;
                                }




                            },
                            onFailure: function () {
                                $('feed1').update('<?php echo _('Data pending'); ?>');
                            }
                        });


            }, 3);


            new Ajax.Request("./importAllFiles.php?step=6",
                    {
                        method: 'post',
                        onSuccess: function () {}
                    });
        }


        function reset_identity() {

            $('importa').disable();
            $('annulla').enable();




            var url_string_rpc = '<?php echo FRONT_DOCROOT; ?>/rpc/reset_identity.php';
            jQuery.ajax({
                url: url_string_rpc,
                complete: function () {

                },
                success: function (output) {
                    $('reset_id').update(output);
                }
            });



        }



        function feedback() {

            $('feed1').update('<strong><?php echo _('Operation completed'); ?></strong>');
        }

        function rollback() {

            azione_rollback = true;
            $('feed1').update('<strong><?php echo _('Closing... please wait'); ?></strong>');
        }

        function reset_import(roll) {

            new Ajax.Request("./import.php?step=7",
                    {
                        method: 'post',
                        onSuccess: function (transport) {

                            if (roll == 1) {
                                $('importa').value = '<?php echo _('Restart import process'); ?>';
                                $('importa').enable();
                                $('annulla').disable();

                                updater = null;
                                azione_rollback = false;
                            } else {

                                $('annulla').disable();
                            }
                        }
                    });
        }

        function mostra_sql() {

            var preview_sql = '<?php echo (!is_file(_PATH_TMP_HTTP . "/" . $_SESSION['import']['filename'] . ".sql")) ? 'true' : 'false'; ?>';

            if (preview_sql) {

                new Ajax.Request("./import.php?step=61",
                        {
                            method: 'post',
                            asynchronous: false,
                            onSuccess: function (transport) {}
                        });

            }

    <?php
    echo "/*";
    var_dump($_SESSION['import']);
    echo "*/";
    ?>

            openWindow('<?php echo _PATH_TMP_HTTP . "/" . $_SESSION['import']['filename'] . ".sql"; ?>', 'sql', 80);
        }

        function chiudi_import() {
            new Ajax.Request("./import.php?step=7&csvdel=1",
                    {
                        method: 'post',
                        onSuccess: function (transport) {}
                    });

            window.close();
        }


        Event.observe('annulla', 'click', rollback);
        Event.observe('importa', 'click', esecuzione);
    //        Event.observe('mostra_log', 'click', mostra_sql);
    //        Event.observe('chiudi', 'click', chiudi_import);
    //        Event.observe('reset', 'click', reset_identity);

        /* ]]> */

    </script>

    <?php
    echo closeLayout1();
}

function pulisci_import() {

    $csv_delete = (bool) $_GET['csvdel'];

    $stop = _PATH_TMP . "/" . $_SESSION['import']['filename'] . ".stop";
    $dat = _PATH_TMP . "/" . $_SESSION['import']['filename'] . ".dat";
    $csv = _PATH_TMP . "/" . $_SESSION['import']['filename'];
    $sql = _PATH_TMP . "/" . $_SESSION['import']['filename'] . ".sql";

    if (file_exists($stop))
        unlink($stop);
    if (file_exists($dat))
        unlink($dat);

    if (file_exists($csv) && $csv_delete) {
        unlink($csv);

        if (file_exists($sql))
            unlink($sql);
    }

    echo 1;
}
?>