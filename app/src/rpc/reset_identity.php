<?php
/**
* File RPC per le chiamate AJAX della scheda. 
* Questo file viene chiamato da funzioni javascript per eseguire le normali operazioni
* sulla tabella, come inserimento, modifica, cancellazione e selezione dei record.
* Se esiste una chimata post viene incluso il file {@link func.rpc_query.php}
* con le funzioni di interazione con il database
* 
* @package VFront
* @subpackage RPC
* @author Mario Marcello Verona <marcelloverona@gmail.com>
* @copyright 2007-2010 M.Marcello Verona
* @version 0.96 $Id: rpc.php 1174 2017-05-12 21:44:50Z marciuz $
* @license http://www.gnu.org/licenses/gpl.html GNU Public License
*/


require("../inc/conn.php");
require("../inc/func.browser_detection.php");




$nome_tab=RegTools::oid2name($_SESSION['import']['oid']);


// Set quiet query
$vmsql->quiet=true;

$output = "";

try {

    $q_begin=$vmsql->query("BEGIN;");
    $sql1 = "SELECT MAX(id) FROM ".$nome_tab.";";
    $q1=$vmsql->query($sql1); 
    $vett_restart = $vmsql->fetch_row($q1);
    $restart = $vett_restart[0]+1;
    $sql2 = "ALTER TABLE ".$nome_tab." ALTER COLUMN id RESTART SET START ".$restart.";";
    $q2=$vmsql->query($sql2);
    $vmsql->query("COMMIT;");
} catch (exeption $e) {
            $output = $e->getMessage();
 }

 if($output==""){
     $output = "reset completato";
 }


echo ($output);