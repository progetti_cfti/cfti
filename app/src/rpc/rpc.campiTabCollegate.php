<?php
/**
* Crea una sessione con i risultati della ricerca con record>1
* 
* @package VFront
* @subpackage RPC
* @author Mario Marcello Verona <marcelloverona@gmail.com>
* @copyright 2010 M.Marcello Verona
* @version 0.96 $Id: rpc.export_search.php 1088 2014-06-16 20:41:44Z marciuz $
* @license http://www.gnu.org/licenses/gpl.html GNU Public License
*/
require_once("../inc/conn.php");
require_once("../inc/layouts.php");
proteggi();
echo "<body>";
echo "<script src=\"js/scheda.js\"></script>";
echo "<script src=\"js/jquery/jquery.query.js\"></script>";
echo "<script src=\"js/jquery/jquery.min.js\"></script>";
echo "<script src=\"admin/export_data.php\"></script>";
echo "</body>";





$collegata = $_GET['tabCollegata'];
$RPC = new RPCGrid($collegata);
$q_info_campi_tab = $RPC->get_grid_rules();

$column_name=array();

//ciclo sui campi per inserirli come colonne della tabella
	foreach($q_info_campi_tab as $C){
		
		if($C->in_tipo=='select_from' || $C->in_tipo=='autocompleter_from' && $C->in_default!=''){

			$C->in_default=preg_replace("|\s+|", " ", $C->in_default);
			
			// key field
			preg_match("|SELECT +([^,]+) *,?(.*) *FROM *([a-z0-9_]+)(.*)|iu",$C->in_default,$fff);
			
			// if the label not exists... use the value
			if($fff[2]==''){
			    $fff[2]=$fff[1];
			}

			// Cerca alias per k
			$k =(preg_match("'AS +([\w]+) *$'i",trim($fff[1]),$alias_k)) ? $alias_k[1] : 'k';
			$print_k = ($k=='k') ? 'AS k' : '';

			// Cerca alias per v
			$v =(preg_match("'AS +([\w]+) *$'i",trim($fff[2]),$alias_v)) ? $alias_v[1] : 'v';
			$print_v = ($v=='v') ? 'AS v' : '';

			$pre_query = "SELECT {$fff[1]} $print_k , {$fff[2]} $print_v FROM {$fff[3]} {$fff[4]} ";

			$column_name[]="(SELECT $v FROM ($pre_query) t2 WHERE $k=t1.{$C->column_name}) as {$C->column_name}";
		}
                //i campi titolo_label non vanno inseriti nella grid
                else if(strpos($C->column_name, "titolo_label")=== false){
                                 
                      $column_name[]=$C->column_name;
                     
                }
			
	}

$campi_vis=implode(',',$column_name);

$oid_tabcollegata = RegTools::name2oid($collegata);
$campi_vis .= "|".$oid_tabcollegata;
            
echo $campi_vis;
