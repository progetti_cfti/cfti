<?php 
require_once("./inc/conn.php");
require_once("./inc/layouts.php");
proteggi(1);
//require_once('./plugins/phpmailer/class.phpmailer.php');

$fileErroriLog = 'errori_log.txt';

//funzione per cambiare encoding
function changeEncoding($file) {

    global $arrayEncoding;


    $commento = ''; 
    
    $obj_encoding = new stdClass();

    //prendo il contenuto
    $content = file_get_contents(_PATH_TMP . "/" . $file);
    
    //elimino virgolette aggraziate
    //$content = str_replace($content1, array(chr(126)), "\"");

    //verifico encoding
    $encoding = mb_detect_encoding($content, $arrayEncoding, true);
    $obj_encoding->encoding = $encoding;
    
    $encodingOk = true;

    //effettuo modifica encoding

    if ($encoding === 'ASCII') {
        $content_utf = iconv('ASCII', "UTF-8", $content);
        $commento = 'ASCII -> UTF-8';
    } else if ($encoding === false) {
        $content_utf = iconv('Windows-1252', "UTF-8", $content);
        $commento = 'encoding non riconosciuto. Si suppone che sia Windows-1252 -> UTF-8';
    } else if ($encoding === 'UTF-8') {
        $content_utf = $content;
        $commento = 'UTF-8 -> UTF-8';
    }
//caso in cui ho un altro encoding do un warning
    else {
        $encodingOk = false;
    }

  


    $obj_encoding->contentUtf = $content_utf;

    //problema nella conversione
    if ($content_utf === false && $content!= '') {
       $encodingOk = false;
    }else{
         //sostituisco le virgolette aggraziate
         $content_utf = sostVirgoletteAggraziate($content_utf);
         $content_utf = sostSoftHyphen($content_utf);
        
        $fp = fopen(_PATH_TMP . "/" . $file, 'w');
        fwrite($fp, $content_utf);
        fclose($fp);
    }
     
    //proprietà oggetto
    $obj_encoding->encodingOk = $encodingOk;

    //commento trasformazione encoding
    $obj_encoding->commento = $commento;
 

    return $obj_encoding;
}

//funzione che sostiruisce le virgolette aggrazziate (tilde e trattino lungo) con virgolette
function sostVirgoletteAggraziate($content) {

    //sostituzione
    $newContent1 = str_replace('˜','"', $content);
    $newContent = str_replace('—','"', $newContent1);


    return $newContent;
} 

//gestisce il problema del soft hyphen
function sostSoftHyphen($content) {

    //sostituzione
    $newContent = str_replace(IntlChar::chr(173), '-', $content);


    return $newContent;
}


//funzione che per ogni carattere presente nella stringa di input restituisce il numero di occorrenze
function mb_count_chars($input) {
    $l = mb_strlen($input, 'UTF-8');
    $unique = array();
    for($i = 0; $i < $l; $i++) {
        $char = mb_substr($input, $i, 1, 'UTF-8');
        if(!array_key_exists($char, $unique)){
             $unique[$char] = 0;
        }
           
        $unique[$char]++;
    }
    return $unique;
}

//funzione che gestisce gli errori non catturati
function getErrori() {

    global $fileErroriLog;
    //gestione errori 
    $fp = fopen($fileErroriLog, 'w');
    fwrite($fp, 'Lista errori'."\n");
    fclose($fp);
    
    error_reporting(E_ALL);

    set_error_handler(function ($livello, $messaggio, $file, $linea, $context) {

        //scrivo file di log
        global $fileErroriLog;
        $errore = "livello errore:$livello, $messaggio $file linea:$linea";
        $fp = fopen($fileErroriLog, 'a');
        fwrite($fp, $errore."\n");
        fclose($fp);
        
        //mando mail 
        //invio mail
//        $mail = new PHPMailer();
//        $mail->Host = "cfti-dev2.int.ingv.it";
//        $mail->Port = 25;
//        //$mail->addAddress($_SERVER['SERVER_ADMIN']);
//        $mail->addAddress('ritachiara.taccone@ingv.it');
//        $mail->Subject = "Invio Report Errori";
//        $mail->Body = $errore;
//        $mail->send();
//        
    });
}






