<?php

require_once("funzioni.php");
require_once("./inc/conn.php"); 
require_once("./inc/layouts.php");
require_once __DIR__ . '/classesNuove/GestoreSchemi.php';

proteggi(1);

//gestione errori non catturati
getErrori();


$nomeSchemaBackend = GestoreSchemi::nomeSchema('backend');
$nomeSchemaOnline = GestoreSchemi::nomeSchema('online');

//
if (!isset($_GET['modo']))
    $modo = 0;
else
    $modo = (int) $_GET['modo'];

//in base allo step eseguo una funzione diversa
switch ($modo) {

    case 1: stampaForm(2);
        break;
    case 2: verificaVirgolette();
        break;
    case 3: stampaForm(4);
        break;
    case 4: verificaDash();
        break;
    case 5: verificaAreaEpic();
        break;

}


//FUNZIONI
function verificaAreaEpic() {

    global $vmsql, $nomeSchemaBackend;
    
    $fileLog = 'files/tmp/AreaEpicentrale.log';
 

    //QUERY SELECT
    $sql = "SELECT * FROM $nomeSchemaBackend.fnperiod WHERE fnperiod.in_cfti = true AND (fnperiod.areaepicentrale = '' OR fnperiod.areaepicentrale IS NULL);";
 
    $result = pg_query($vmsql->link_db, $sql);
    if (!$result) {
        echo "Problema nella query di verifica area epicentrale.\n";
        exit;
    } else {
        $fp = fopen("./$fileLog", 'w');
        fwrite($fp, "Record in CFTI che non hanno l'area epicentrale \n");
        fclose($fp);

        $errori = 0;
        //LEGGO I RISULTATI QUERY E LI SCRIVO NEL FILE
        while ($row = pg_fetch_row($result)) {
            
            if(count($row) !== 0){
                $errori++;
                
            }
            $record = '';
            foreach ($row AS $campo) {
                $record .= "$campo,";
            }
            $record = substr($record, 0, -1);

            $fp = fopen("./$fileLog", 'a');
            fwrite($fp, "$record \n");
            fclose($fp);
        } 
        
        if($errori >0){
            $messaggioErrore = "<br><h3 style=\"background-color:Tomato;\">Attenzione! Ci sono $errori record della tabella fnperiod con in_cfti=true e area_epicentrale vuota! <a href='./$fileLog'>File di Log</a></h3>";
            echo $messaggioErrore;
        }
  
    }
}

//genera il form per la selezione di tabella o testo da verificare
function stampaForm($modo){
    
    //apro il layout di Vfront
    echo openLayout1(_("Verifica"), array(), 'popup');
    echo breadcrumbs(array("HOME", "Verifica"));
 
    echo "<h1>" . 'Verifica' . "</h1>\n";
    
    
    $OUT .= "<div id=\"form-container\">
			<form enctype=\"multipart/form-data\" method=\"post\" action=\"" . Common::phpself() . "?modo=$modo\" >
	
                        <select name=\"tipoFile\" id=\"tipoFile\">
                            <option value=\"tabella\">Tabella</option>
                            <option value=\"testo\">Testo</option>
                        </select> 
                        <br>
                        <br>


			<label for=\"csvfile\">" . _('File') . ":</label><br />
			<input type=\"file\" name=\"file\" id=\"file\" size=\"60\" />
			
			<p><input type=\"submit\" id=\"send\" value=\"  " . _('Next') . "  &gt;&gt; \" /></p>
			
			</form>
		</div>
		";
    
    echo $OUT;
    
    echo closeLayout1();
    
    
}
//effettua verifica delle discrepanze di virgolette in apertura e chiusura
function verificaVirgolette(){
    
     //apro il layout di Vfront
    echo openLayout1(_("Verifica Virgolette"), array(), 'popup');
    echo breadcrumbs(array("HOME", "Verifica Virgolette"));
    echo "<h1>" . 'Verifica Virgolette' . "</h1>\n";
    
    var_dump($_POST);
    
    
    $problemaEncoding = [];
    $nomeFile = $_FILES['file']['name'];
    $moveFile = move_uploaded_file($_FILES['file']['tmp_name'], _PATH_TMP . "/" . $nomeFile);
    
    //cambio encoding
    $objEncoding = changeEncoding($nomeFile);
    if ($objEncoding->encodingOk === false) {
       echo "<h3 style=\"background-color:Tomato;\">Problema Encoding nel file $nomeFile</h3>";
    }
    
    //prendo contenuto e faccio verifica
    if($moveFile){
        $content = file_get_contents(_PATH_TMP . "/" . $nomeFile);
        
        if($_POST['tipoFile']==='tabella'){
            $separatoreRecord = '';
            $arrayRecords = explode("\n", $content);
        }else if($_POST['tipoFile']==='testo'){
            $separatoreRecord = '$$';
            $arrayRecords = explode('$$', $content);
        }
        
        
        
        $fp = fopen('verifica.txt', 'w');
        fwrite($fp, "Verifica Virgolette Aggraziate \n");
        fclose($fp);
        
        foreach ($arrayRecords AS $record){
            
            $num1 = 0;
            $num2 = 0;

            foreach (mb_count_chars($record) as $i => $val) {
                
                
                
                if(($i)==='˜'){
                    $num2 = $val;
          
                }
                if(($i)==='—'){
                    $num1 = $val;
                }
            }

            if($num1 != $num2){
                $fp = fopen('verifica.txt', 'a');
                fwrite($fp, "$num1 != $num2 : \n $separatoreRecord$record\n");
                fclose($fp);
            }
        }
        
        
        
        echo '<br>';
        echo "<a href='verifica.txt' target='_blank'>Verifica Encoding</a>";
        
    }
    
    
    
    echo closeLayout1();
    
    
}

function verificaDash(){
    
     //apro il layout di Vfront
    echo openLayout1(_("Verifica"), array(), 'popup');
    echo breadcrumbs(array("HOME", "Verifica"));
    echo "<h1>" . 'Verifica' . "</h1>\n";
    
    var_dump($_POST);

    $problemaEncoding = [];
    $nomeFile = $_FILES['file']['name'];
    $moveFile = move_uploaded_file($_FILES['file']['tmp_name'], _PATH_TMP . "/" . $nomeFile);
    
    //cambio encoding
    $objEncoding = changeEncoding($nomeFile);
    if ($objEncoding->encodingOk === false) {
       echo "<h3 style=\"background-color:Tomato;\">Problema Encoding nel file $nomeFile</h3>";
    }
    
    //prendo contenuto e faccio verifica
    if($moveFile){
        $content = file_get_contents(_PATH_TMP . "/" . $nomeFile);
        
        if($_POST['tipoFile']==='tabella'){
            $separatoreRecord = '';
            $arrayRecords = explode("\n", $content);
        }else if($_POST['tipoFile']==='testo'){
            $separatoreRecord = '$$';
            $arrayRecords = explode('$$', $content);
        }
        
        
        
        $fp = fopen('verificaDash.txt', 'w');
        fwrite($fp, "Verifica Dash \n");
        fclose($fp);
        
        
        foreach ($arrayRecords AS $record){
    

            foreach (mb_count_chars($record) as $i => $val) {
                
                if(ctype_alpha($i)===false && !is_numeric($i)){
                    $fp = fopen('verificaDash.txt', 'a');
                    fwrite($fp, ($i)."=>$val ". implode(unpack('H*', iconv("UTF-8", "UCS-4BE", $i))) ." \n");
                    fclose($fp);
                }
                
                

            }
            
            $fp = fopen('verificaDash.txt', 'a');
                fwrite($fp, "-----------------\n");
                fclose($fp);

        }
        
        
        
        echo '<br>';
        echo "<a href='verificaDash.txt' target='_blank'>Verifica Dash</a>";
        
    }
    
    
    
    echo closeLayout1();
    
    
}