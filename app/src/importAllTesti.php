<?php
require_once("funzioni.php");
require_once("./inc/conn.php");
require_once("./inc/layouts.php");

proteggi(1);


//gestione errori non catturati
getErrori();

//array con elenco encoding (serve dopo per detect encoding)
$arrayEncoding = ['macintosh', 'UCS-4*', 'UCS-4BE', 'UCS-4LE*', 'UCS-2', 'UCS-2BE', 'UCS-2LE', 'UTF-32*', 'UTF-32BE*', 'UTF-32LE*', 'UTF-16*', 'UTF-16BE*', 'UTF-16LE*', 'UTF-7', 'UTF7-IMAP', 'UTF-8*', 'ASCII*', 'EUC-JP*', 'SJIS*', 'eucJP-win*', 'SJIS-win*', 'ISO-2022-JP', 'ISO-2022-JP-MS', 'CP932', 'CP51932', 'SJIS-mac', 'SJIS-win', 'SJIS', 'SJIS-Mobile#DOCOMO', 'SJIS-Mobile#KDDI', 'SJIS-Mobile#SOFTBANK', 'UTF-8-Mobile#DOCOMO', 'UTF-8-Mobile#KDDI-A', 'UTF-8-Mobile#KDDI-B', 'UTF-8-Mobile#SOFTBANK', 'ISO-2022-JP-MOBILE#KDDI', 'JIS', 'JIS-ms', 'CP50220', 'CP50220raw', 'CP50221', 'CP50222', 'ISO-8859-1*', 'ISO-8859-2*', 'ISO-8859-3*', 'ISO-8859-4*', 'ISO-8859-5*', 'ISO-8859-6*', 'ISO-8859-7*', 'ISO-8859-8*', 'ISO-8859-9*', 'ISO-8859-10*', 'ISO-8859-13*', 'ISO-8859-14*', 'ISO-8859-15*', 'ISO-8859-16*', 'byte2be', 'byte2le', 'byte4be', 'byte4le', 'BASE64', 'HTML-ENTITIES', '7bit', '8bit', 'EUC-CN*', 'CP936', 'GB18030', 'HZ', 'EUC-TW*', 'CP950', 'BIG-5*', 'EUC-KR*', 'UHC', 'ISO-2022-KR', 'Windows-1251', 'Windows-1252', 'CP866', 'KOI8-R*', 'KOI8-U*', 'ArmSCII-8'];


//per riconoscere Unix, MS-Dos or Macintosh line-ending
ini_set("auto_detect_line_endings", true);


//leggo il file json che contiene le informazioni di configurazione dell'import
$jsonConfigurationFile = 'configurazioneImportTesti.json';

$arrayEccezioni = [];



//se esiste
try{    
    if (file_exists($jsonConfigurationFile)) {
    //prendo il contenuto
    $content = file_get_contents($jsonConfigurationFile);
    if ($content) {
        //dal contenuto creo un oggetto json 
        $configObj = json_decode($content);
    }else{
        throw new Exception("File di configurazione vuoto");

    }
    }else{
        throw new Exception("File di configurazione non trovato. Verificare che il nome sia corretto ($jsonConfigurationFile)");
    }
} catch (Exception $e){
    error_log($e->getMessage());
    $arrayEccezioni[] = $e->getMessage();
    
}

    

####################################################################

if (!isset($_GET['step']))
    $step = 0;
else
    $step = (int) $_GET['step'];

switch ($step) {

    case 0: import_step0();
        break;

    case 1: import_step1();
        break;

    case 11: import_step1_options();
        break;

    case 2: import_step2();
        break;

    case 4: import_step4();
        break;

    case 5: exec_query_import();
        break;

    case 6: import_query();
        break;

    case 61: import_query(true);
        break;

    case 7: pulisci_import();
        break;

    default: import_step0();
}

function controllo_tipo($dato, $tipo, $is_nullable) {

    global $vmsql, $vmreg;

    if (($dato == '' || $dato == 'NULL') && $is_nullable == 'YES') {

        return "NULL,";
    } else if (in_array($tipo, array('int', 'mediumint', 'tinyint', 'bigint', 'smallint'))) {

        return (int) $dato . ",";
    } else if (in_array($tipo, array('double', 'float'))) {

        return (double) $dato . ",";
    } else {

        return "'" . $vmsql->escape($dato) . "',";
    }
}

function modificatori($input, $i, $post_data) {


    if (isset($post_data['mod'][$i])) {


        foreach ($post_data['mod'][$i] as $k => $mod) {

            switch ($mod) {

                case 'upper': $input = strtoupper($input);
                    break;
                case 'lower': $input = strtolower($input);
                    break;
                case 'upperfirst': $input = ucfirst($input);
                    break;
                case 'upperword': $input = ucwords($input);
                    break;
                case 'md5': $input = md5($input);
                    break;
                case 'sha1': $input = sha1($input);
                    break;
                case 'prefisso': $input = $post_data['pref'][$i] . $input;
                    break;
            }
        }
    }

    return $input;
}

function errors_mysql($n) {

    switch ($n) {

        case 1062: return _('Duplicate key');
        case 1364: return _('A record which is specified as "not null" doesn\'t have a default value');
        case 1471: return _('Table not writable: file permissions error');
        case 1146: return _('Non-existent table: error');
        case 1022: return _('Can\'t add record - Duplicate key');
        case 1452: return _('Unable to add a record - The reference is missing and/or not connected to the reference table.');
        default : return "";
    }
}

// STEP 0
// FORM FOR SELECT AND UPLOAD SOURCE TXT FILE
function import_step0() {

    global $oid, $arrayEccezioni;

    $OUT = '';

    echo openLayout1(_("Data import"), array(), 'popup');

    echo "<h1>Procedura di Import Testi</h1>\n";
    
    //verifico se ci sono errori nei passi precedenti
    if(count($arrayEccezioni) > 0){
        foreach ($arrayEccezioni AS $key => $value){
            echo "<h3 style=\"background-color:Tomato;\">$value</h3>";
        }
    }
    
    
    
    echo "<p>" . _('Seleziona i file da caricare <br /> ') . "</p>\n";

        $OUT .= "<div id=\"form-container\">
			<form enctype=\"multipart/form-data\" method=\"post\" action=\"" . Common::phpself() . "?step=1\" >
			
			<p id=\"csvfile-box\">
			<label for=\"csvfile\">" . _('Txt File ') . ":</label><br />
			<input type=\"file\" name=\"testi[]\" id=\"testi\" multiple/>
			</p>
			
			<p><input type=\"submit\" id=\"send\" value=\"  " . _('Next') . "  &gt;&gt; \" /></p>
			
			</form>
		</div>
		";
  


    echo $OUT;

    echo closeLayout1();
}

// STEP 1
// UPLOAD FILE

function import_step1() {
    
    $arrayTesti = [];
    $arrayNomiTesti = [];

    try {
        foreach ($_FILES['testi']['name'] AS $key => $nomeFile) {  
            
            $nome_file = $nomeFile;
            $tmp_name = $_FILES['testi']['tmp_name'][$key];
            $destination = _PATH_TMP . "/";
            
            
            
            $result = move_uploaded_file($tmp_name, $destination . $nome_file);
            //controllo se move_uploaded_file è ok
            if ($result) {
                array_push($arrayTesti, $tmp_name);
                array_push($arrayNomiTesti, $nome_file);
                
                echo "<h3 style=\"background-color:Aquamarine;\">File {$nome_file} è stato caricato</h3>";
                
            } else {
                //sollevo un'eccezione
                throw new Exeption("File {$nome_file} non caricato");
            }
        }
        
        
        
        $_SESSION['importTesti']['arrayTesti'] = $arrayTesti;
        $_SESSION['importTesti']['arrayNomiTesti'] = $arrayNomiTesti;

        if (count($_SESSION['importTesti']['arrayTesti']) === 0) {
            echo "<h3 style=\"background-color:Tomato;\">Nessun file caricato</h3>";
            exit;
        }

        $onclick = "location.href = ' " . Common::phpself() . "?step=2' ";
        echo "<input type=\"button\" value=\"Next >>\" onclick=\"$onclick\">";
    } catch (Exception $ex) {
        echo "<h3 style=\"background-color:Tomato;\">$ex->getMessage</h3>";
    }
    exit;

}

function import_step1_options() {


    $_SESSION['import']['quote'] = $_POST['quote'];
    $_SESSION['import']['sep'] = $_POST['sep'];

    header("Location: " . $_SERVER['PHP_SELF'] . "?step=2");
    exit;
}


// STEP 2
// SHOW OPTIONS AND PREVIEW

function import_step2() {

    global $vmsql, $vmreg, $db1, $configObj;
    
    $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js");
    echo openLayout1(_("Import Testi"), $files, 'popup');

    
    
    var_dump($_SESSION['importTesti']);
    $arrayTabelle = $configObj->tabelle;
    
    $problemaEncoding = array();
    $tuttiTesti ='';
    $arrayLunghezzaFiles = [];
    $arrayNumRecordFiles = [];
    
    foreach ($_SESSION['importTesti']['arrayNomiTesti'] AS &$nomeTesto) {
        $k = array_search($nomeTesto, $configObj->files);
        $nome_tab = $arrayTabelle[$k];

        //cambio encoding del file
        $objEncoding = changeEncoding($nomeTesto);
      
        
        //eventuali problemi
        if ($objEncoding->encodingOk === false) {
            array_push($problemaEncoding, $nomeTesto);
        }
        
        
        //conto le righe e il numero di record nel file
        $handle = fopen(_PATH_TMP . "/" .$nomeTesto, 'r');
        $n = 0;
        $numRecord = 0;
        while (!feof($handle)) {

            $buffer = fgets($handle);
            if ($buffer != "\n") {
                $n++;
            }
            if (substr($buffer, 0, 2) == "$$") {
                $numRecord++;
            }
        }
        fclose($handle);
        
        array_push($arrayLunghezzaFiles, $n);
        array_push($arrayNumRecordFiles, $numRecord);
        
        
        //messaggio rosso se il file non ha contenuto
        if($numRecord === 0){
            $style = "style=\"background-color:Tomato;\"";
        }else{
            $style = "style=\"background-color:Acquamarine;\"";
        }    
        $T = "<h3 $style>Data preview $nomeTesto ($nome_tab) -->commento:" . $objEncoding->commento . '</h3>';


        //compongo la tabella di Preview
        $fp = fopen(_PATH_TMP . "/" .$nomeTesto, 'r');
        $row = 0;
        $record = 0;      
        $T .= "<table summary=\"csv\" id=\"csv-table\" >\n";
        while (!feof($fp)) {
            $riga = fgets($fp);
            $T .= "<tr class=\"r$row\">\n";
            $T .= "<td class=\"c$row\">" . $riga . "</td>";
            $T .= "</tr>\n";
            $row++;
            if (substr($riga, 0, 2) == "$$") {
                $record++;
            }
            if ($row >= 20) {
                break;
            }
        }
        fclose($fp);
        $T .= "</table>\n";
          $tuttiTesti .= $T;
 
    }
    
    //
    $_SESSION['importTesti']['arrayLength'] = $arrayLunghezzaFiles;
    $_SESSION['importTesti']['numRecord'] = $arrayNumRecordFiles;
    
    //echo pagina
    echo "<h1>Importa Tutti i Testi</h1>";
    
    //STAMPO A VIDEO EVENTUALI PROBLEMI DI ENCODING
    echo "<h3>Problemi di Encoding: (verificare iconv implementation--> <a href=\"phpInfo.php\">PhpInfo</a>)</h3>";
    if (count($problemaEncoding) > 0) {
        echo "<h3 style=\"background-color:Tomato;\">";
        foreach ($problemaEncoding AS $proEnc) {
            echo $proEnc . ' ';
        }
        echo "</h3>";
    } else {
        echo "<h3 style=\"background-color:Aquamarine;\">Nessun problema di encoding</h3>";
    }


    //echo form e tabella
    echo "<form action=\"" . Common::phpself() . "?step=4\" method=\"post\">\n";
    echo"<br>";
    $next = "<p><input type=\"button\" onclick=\"history.back()\" value=\"&lt;&lt; " . _('Previous') . "\" />   <input type=\"button\" onclick=\"submit()\" value=\"" . _('Next') . " &gt;&gt;\" /></p>\n";

    echo $next;
    echo"<br>";
    if ($tuttiTesti) {
        echo $tuttiTesti;
    } else {
        echo "<h3 style=\"background-color:Tomato;\">Nessun Testo può essere importato";
        echo "<a href='configurazioneImportTesti.json'>File di Configurazione</a></h3>";
    }
    echo $next;

    echo "</form>\n";
    echo closeLayout1();
}

function import_step2_ins() {

    $_da_processare = '';
    $campi = '';

    // prendi i campi buoni
    for ($j = 0; $j < count($_POST['i']); $j++) {
        if ($_POST['i'][$j] != '') {

            $_da_processare[] = $j;

            $campi .= $_POST['i'][$j] . ",";
        }
    }

    $_SESSION['import']['campi_da_processare'] = $_da_processare;
    $_SESSION['import']['nome_campi'] = substr($campi, 0, -1);
    $_SESSION['import']['prima_riga'] = $_POST['prima_riga'];

    header("Location: " . $_SERVER['PHP_SELF'] . "?step=3");
    exit;
}

// STEP 2 
// IMPORT DATA


function import_step3() {

    global $vmsql, $vmreg, $db1;

    $nome_tab = RegTools::oid2name($_SESSION['import']['oid']);

    // prendi i campi disponibili per la tabella
    $campi_tab = RegTools::prendi_colonne_frontend($nome_tab, "column_name,data_type,is_nullable", false);

    $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js");

    echo openLayout1(_("Data import"), $files, 'popup');

    echo "<h1>" . sprintf(_('Data import step %d of %d'), 3, 4) . "</h1>";

    echo "<p>" . _('If you need to perform operations on the fields identified for the import or insert constants, operate from this window, otherwise go ahead') . "</p>\n";

    echo "<form action=\"" . Common::phpself() . "?step=4\" method=\"post\" >\n";

    echo "<p><input type=\"button\" onclick=\"history.back()\" value=\"&lt;&lt; " . _('Previous') . "\" />   <input type=\"button\" onclick=\"submit()\" value=\"" . _('Next') . " &gt;&gt;\" /></p>\n";

    echo "<table summary=\"tabella importazione2\" id=\"tabella-conversioni\">\n";

    for ($i = 0; $i < count($campi_tab[0]); $i++) {

        echo "<tr class=\"import-campo\" id=\"imp_" . $campi_tab[0][$i] . "\">\n";

        echo "<td><strong>" . $campi_tab[0][$i] . "</strong></td>\n";

        if (in_array($campi_tab[0][$i], explode(",", $_SESSION['import']['nome_campi']))) {

            $importazione_csv = "<span class=\"verde\"><strong>" . _('Selected value') . "</strong></span>";
        } else {
            $importazione_csv = '<span class="fakelink small" onclick="set_costante(' . $i . ');" >&lt;' . _('set constant value') . '&gt;</span>' .
                    '<span id="costante_' . $i . '"  style="display:none" > <input type="text" name="costante[' . $i . ']" /> ' .
                    ' <span class="fakelink small" onclick="unset_costante(' . $i . ');" id="costante_' . $i . '_trigger">' . _('remove') . '</span></span>';
        }


        echo "<td id=\"td$i\" width=\"340\"><div class=\"up\"></div><span class=\"fakelink small\" id=\"s$i\" onclick=\"modificatore($i,this);\">&lt;" . _('add action') . "&gt;</span></td>";

        echo "<td width=\"270\">" . $importazione_csv . "</td>\n";

        echo "</tr>\n";
    }



    echo "</table>\n";

    echo "<p><input type=\"button\" onclick=\"history.back()\" value=\"&lt;&lt; " . _('Previous') . "\" />   <input type=\"button\" onclick=\"submit()\" value=\"" . _('Next') . " &gt;&gt;\" /></p>\n";

    echo "</form>\n";

    echo closeLayout1();
}

// STEP 3
// REFRESH WINDOWS

function import_step4() {

    $_SESSION['importTesti']['post_data'] = $_POST;
    header("Location: " . $_SERVER['PHP_SELF'] . "?step=5");
    exit;
}

function import_query($only_sql = false) {

    ini_set('max_execution_time', 600);

    global $vmsql, $vmreg, $db1, $configObj, $jsonConfigurationFile;
    $arrayFileConfigurazione = $configObj->files;
    $arrayTabelleConfigurazione = $configObj->tabelle;
    $arrayCampiTestoConf = $configObj->campiTesto;
    $configObj->fineImport = false;
    $json = json_encode($configObj);
    $fp = fopen($jsonConfigurationFile, 'w');
    fwrite($fp, $json);
    fclose($fp);

    $arrayFilesNonImportati = '';
    $stringaErrori = '';
    $arrayFilesImportati = '';
    
    foreach ($_SESSION['importTesti']['arrayNomiTesti'] AS $key=>$fileNome){
        //prendo info dal file di configurazione
        $k = array_search($fileNome, $arrayFileConfigurazione);
        $nome_tab = $arrayTabelleConfigurazione[$k];
        $campoTesto = $arrayCampiTestoConf[$k];
        $PK_tab = RegTools::prendi_PK($nome_tab);

        //prendo il contenuto del file e ne ricavo un array di record
        $content = file_get_contents(_PATH_TMP . "/" . $fileNome);
        $arrayRecords = explode('$$', $content);

        //compongo transazione SQL 
        $sqlTransaction = "BEGIN;";
        $result = pg_query($vmsql->link_db, $sqlTransaction);
        //per ogni record
        $errore = false;
        foreach ($arrayRecords AS $record){
            
            if ($record != '') {

                //$parti = preg_split('~\R~', $parti);
                $parti = explode(PHP_EOL, $record, 2);
                $titolo = $parti[0];
                $contenutoRecord = $parti[1];
                $contenutoRecordEscaped = pg_escape_string($vmsql->link_db, $contenutoRecord);
                
                $arrayTitolo = explode("*", $titolo);             
                $id = $arrayTitolo[0];
                
                
                $sqlTransaction = "UPDATE $nome_tab SET $campoTesto = '$contenutoRecordEscaped' WHERE id = $id";
                $result = pg_query($vmsql->link_db, $sqlTransaction);

                if ($result === false) {
                    //$errore = var_export($id, true);
                    $errore = trim(preg_replace('~[\r\n]+~', ' ', pg_last_error($vmsql->link_db))).'\n<br>';
                    break;
                }
            }
        }
        
        //se non ci sono errori faccio il commit
        if ($errore === false) {
            $sqlTransaction = "COMMIT;";
            $result = pg_query($vmsql->link_db, $sqlTransaction);
            
            $arrayFilesImportati .= $arrayFilesImportati.','.$fileNome;
            
        }else{
            //inserisco errori nei file json
            $arrayFilesNonImportati .= $arrayFilesNonImportati.','.$fileNome;
            $stringaErrori .= $stringaErrori.','.$errore.'->'.$fileNome.',';
            $configObj->fineImport = true;
            
        }
        
        //scrivo il file di conf
        $configObj->filesImportati = $arrayFilesImportati;
        $configObj->filesNonImportati = $arrayFilesNonImportati;
        $configObj->tipoErrori = $stringaErrori;
        $json = json_encode($configObj);
        $fp = fopen($jsonConfigurationFile, 'w');
        fwrite($fp, $json);
        fclose($fp);
    }
    
        $configObj->fineImport = true;
        $json = json_encode($configObj);
        $fp = fopen($jsonConfigurationFile, 'w');
        fwrite($fp, $json);
        fclose($fp);
    
}

function exec_query_import() {

    global $vmsql, $vmreg, $db1, $jsonConfigurationFile;

    $files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js", "js/open_window.js");
    // tipo di browser.. serve per il corretto funzionamento della percentuale
    include_once("./inc/func.browser_detection.php");
    $browser = browser_detection('full');
    if ($browser[0] == 'ie')
        $files[] = "sty/import_ie.css";

    //echo html
    echo openLayout1(_("Import execution"), $files, 'popup');
    echo "<h1>" . sprintf(_('Step %s of %s'), 2, 2) . " <span class=\"var\">" . _('Import processing') . "</span></h1>\n";

    var_dump($_SESSION['importTesti']); 

    
    echo "<div>" . _('Import status') . " <span id=\"feed1\">&nbsp;</span></div>\n";
    echo "<div>" . _('Inserimenti:') . " <span id=\"ins\">0</span> - " . _('Errori:') . " <span id=\"errori\">0</span></div>\n";

    echo "<p><input type=\"button\" id=\"importa\" value=\" " . _('Import') . " \" />"
    . " <input type=\"button\" id=\"annulla\" value=\" " . _('Cancel') . " \" disabled=\"disabled\" />"
    . " <input type=\"button\" id=\"mostra_log\" value=\" " . _('Show SQL') . " \"  />" .
    " <input type=\"button\" id=\"chiudi\" value=\" " . _('Close') . " \" /></p>\n";

    echo "<div id=\"txt\">
			<div id=\"txt_start\"></div>
			<div id=\"txt_errore\"></div>
			<div id=\"txt_end\"></div>
		  </div>\n";

    echo "<div id=\"perc-barra\" ></div>\n";

    /*
      echo "<br><div id=\"campo_mail\""
      . "</div>";
     */
    ?>

    <script type="text/javascript">

        /* <![CDATA[ */

        var stato;
        var updater = null;
        var azione_rollback = false;

        function add0(dd) {
            dd = dd + '';
            return (dd.length == 1) ? '0' + dd : dd;
        }

        function esecuzione() {

            $('importa').disable();
            $('annulla').enable();
            $('chiudi').disable();

            var d = new Date();

            $('txt_start').update(add0(d.getHours()) + ":" + add0(d.getMinutes()) + ":" + add0(d.getSeconds()) + ' -- <?php echo _('Begin procedure'); ?></span><br />');

            updater = new PeriodicalExecuter(function (pe) {

                if (azione_rollback) {

                    pe.stop();

                    new Ajax.Request("./rpc/rpc.import_stop.php?h=<?php echo $_SESSION['import']['filename']; ?>", {
                        method: 'post',
                        onSuccess: function (transport) {
                            if (transport.responseText == 1) {

                                $('feed1').update("<strong><?php echo _('Execution cancelled - Rollback done'); ?></strong>");
                                $('chiudi').enable();

                                // procedura di reset
                                reset_import(1);
                            }

                        }
                    });
                } else {

                    new Ajax.Request("<?php echo $jsonConfigurationFile; ?>",
                            {
                                    method: 'post',
                                    onComplete: null,
                                    onSuccess: function (transport) {

                                            $('feed1').update('<?php echo _('Processing...'); ?>');

                                            let stato = JSON.parse(transport.responseText);
                                            console.log(stato);
                                            
                                        //blocco quando ho importato tutti i files
                                        if (stato.fineImport === true) { 

                                            pe.stop();
                                            
                                            $('errori').update(stato.filesNonImportati);
                                            
                                            if(stato.tipoErrori != ''){
                                                $('txt_errore').update('Errore import testo:' + stato.tipoErrori);
                                            }            
                                            $('ins').update(stato.filesImportati);

                                            d = new Date();
                                            $('feed1').update('<strong><?php echo _('Operation completed'); ?></strong>');
                                            $('annulla').disable();
                              
                                            $('txt_end').update(add0(d.getHours()) + ":" + add0(d.getMinutes()) + ":" + add0(d.getSeconds()) + ' -- <?php echo _('Operation completed'); ?></span>');
                                            $('folder_link').update("<a href='./files/tmp/' target=\"_blank\">Cartella TMP</a>");
                                            window.opener.location = window.opener.location;
                                        }



                                        },
                                    onFailure: function () {
                                        $('feed1').update('<?php echo _('Data pending'); ?>');
                                    }
                                });


                }


            }, 3);

            new Ajax.Request("./importAllTesti.php?step=6",
                    {
                        method: 'post',
                        onSuccess: function () {}
                    });

        }

        function feedback() {

            $('feed1').update('<strong><?php echo _('Operation completed'); ?></strong>');
        }

        function rollback() {

            azione_rollback = true;
            $('feed1').update('<strong><?php echo _('Closing... please wait'); ?></strong>');
        }

        function reset_import(roll) {

            new Ajax.Request("./import_commenti.php?step=7",
                    {
                        method: 'post',
                        onSuccess: function (transport) {

                            if (roll == 1) {
                                $('importa').value = '<?php echo _('Restart import process'); ?>';
                                $('importa').enable();
                                $('annulla').disable();
                                $('mostra_log').disable();
                                updater = null;
                                azione_rollback = false;
                            } else {
                                $('mostra_log').enable();
                                $('annulla').disable();
                            }
                        }
                    });
        }

        function mostra_sql() {

            var preview_sql = '<?php echo (!is_file(_PATH_TMP_HTTP . "/" . $_SESSION['import']['filename'] . ".sql")) ? 'true' : 'false'; ?>';

            if (preview_sql) {

                new Ajax.Request("./import_commenti.php?step=61",
                        {
                            method: 'post',
                            asynchronous: false,
                            onSuccess: function (transport) {}
                        });

            }

    <?php echo "/*";
    var_dump($_SESSION['import']);
    echo "*/"; ?>

            openWindow('<?php echo _PATH_TMP_HTTP . "/" . $_SESSION['import']['filename'] . ".sql"; ?>', 'sql', 80);
        }

        function chiudi_import() {
            new Ajax.Request("./import_commenti.php?step=7&csvdel=1",
                    {
                        method: 'post',
                        onSuccess: function (transport) {}
                    });

            window.close();
        }


        function test() {
            $('feed1').update('<strong><?php echo _('test show sql'); ?></strong>');
        }

        function testMail() {

            $('mailResult').update('<strong></strong>');
        }


        Event.observe('annulla', 'click', rollback);
        Event.observe('importa', 'click', esecuzione);
        Event.observe('mostra_log', 'click', mostra_sql);
        Event.observe('chiudi', 'click', chiudi_import);

        /* ]]> */

    </script>

    <?php
    echo closeLayout1();
}

function pulisci_import() {

    $csv_delete = (bool) $_GET['csvdel'];

    $stop = _PATH_TMP . "/" . $_SESSION['import']['filename'] . ".stop";
    $dat = _PATH_TMP . "/" . $_SESSION['import']['filename'] . ".dat";
    $csv = _PATH_TMP . "/" . $_SESSION['import']['filename'];
    $sql = _PATH_TMP . "/" . $_SESSION['import']['filename'] . ".sql";

    if (file_exists($stop))
        unlink($stop);
    if (file_exists($dat))
        unlink($dat);

    if (file_exists($csv) && $csv_delete) {
        unlink($csv);

        if (file_exists($sql))
            unlink($sql);
    }

    echo 1;
}


?>