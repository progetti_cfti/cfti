<?php

require_once("../inc/conn.php");
require_once("../inc/layouts.php");
require_once("../inc/func.browser_detection.php");
proteggi(1);
 
echo "<body>";
echo "<script src=\"js/scheda.js\"></script>";
echo "<script src=\"js/jquery/jquery.query.js\"></script>";
echo "<script src=\"js/jquery/jquery.min.js\"></script>";
echo "<script src=\"admin/export_data.php\"></script>";
echo "</body>";

require_once("./procedure_geojson.php");
require_once('../classes/ExtendedZip.php');

$files=array("js/scriptaculous/lib/prototype.js","sty/import.css","js/import.js","js/open_window.js");

echo openLayout1(_("Zip Files"),$files,'popup');

echo breadcrumbs(array("HOME",_("Schermata Zip")));
echo "<br>";

echo "<h1 class=\"titoloScheda\"> Zip Files </h1>";


try {
         
         ExtendedZip::zipTree('../files/xml', '../files/FilesXml_'.date("Y-m-d").'_'.date("h:i:sa").'.zip', ZipArchive::CREATE);

            echo "I file xml sono stati creati su : ".FRONT_DOCROOT."/files/xml";

}catch (Exception $e) {
    echo $e->getMessage();
}
       
        
         

echo closeLayout1();
