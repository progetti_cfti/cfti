<?php

require_once("../inc/conn.php");
require_once("../inc/layouts.php");
require_once("../inc/func.browser_detection.php");
proteggi(1);
  
echo "<body>";
echo "<script src=\"js/scheda.js\"></script>";
echo "<script src=\"js/jquery/jquery.query.js\"></script>";
echo "<script src=\"js/jquery/jquery.min.js\"></script>";
echo "<script src=\"admin/export_data.php\"></script>";
echo "</body>";


require_once("./procedure.php"); 
require_once('../classes/ExtendedZip.php');

$files=array("js/scriptaculous/lib/prototype.js","sty/import.css","js/import.js","js/open_window.js");

echo openLayout1(_("Lancia tutte le procedure"),$files,'popup');
 if(isset($_GET['t'])){
           $tipo = $_GET['t'];
 }
echo breadcrumbs(array("HOME",_("Schermata Procedure")));
echo "<br>";

echo "<h1 class=\"titoloScheda\"> Test procedure ".$tipo." </h1>";
echo "<small>Vengono generati i file con limit 20</small><br><br>";

try {  
    
 
    
    if($tipo=="xml"){
         createQuakeList();
         createLocList();
         createEEList();      
         createLocalityDetail("limit 20;");
         createQuakeDetail("limit 20;");
         ExtendedZip::zipTree('../files/ProcedureXML/', '../files/ProcedureXML.zip', ZipArchive::CREATE);

         echo "File zip creato su: ";
         echo "<a href=\"".FRONT_DOCROOT."/files/\">Cartella File</a>";
    }else if($tipo == "geojson"){
        createQuakeList_geojson();
        createLocList_geojson();
        createEEList_geojson();      
        createLocalityDetail_geojson("limit 20;");
        createQuakeDetail_geojson("limit 20;");
        ExtendedZip::zipTree('../files/ProcedureGeoJSON/', '../files/ProcedureGeoJSON.zip', ZipArchive::CREATE);
        echo "I file sono stati creati su : ";
        echo "<a href=\"".FRONT_DOCROOT."/files/\">Cartella File</a>";
    }    

     
}catch (Exception $e) {
    echo $e->getMessage();
}
       
        
         

echo closeLayout1();




?>