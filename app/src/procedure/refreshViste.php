<?php

require_once("../inc/conn.php"); 
require_once("../inc/layouts.php");
require_once("../inc/func.browser_detection.php");
require_once __DIR__ . '/../classesNuove/GestoreSchemi.php';
proteggi(1);

$nomeSchemaBackend = GestoreSchemi::nomeSchema('backend');
$nomeSchemaOnline = GestoreSchemi::nomeSchema('online');

echo "<body>";

echo "</body>";

require_once("./procedure_geojson.php");

$files = array("js/scriptaculous/lib/prototype.js", "sty/import.css", "js/import.js", "js/open_window.js");

echo openLayout1(_("Procedure-Refresh"), $files, 'popup');

echo breadcrumbs(array("HOME", _("Schermata Procedure")));
echo "<br>";

echo "<h1 class=\"titoloScheda\"> Procedura Refresh Locind </h1>";

try {
    global $vmsql;

    $sql = "begin;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.commgen;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.d1;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.ee;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.pq;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.notesito;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.locind;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.schedeb;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.schedea;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.quake_details;
               REFRESH MATERIALIZED VIEW $nomeSchemaOnline.biblio_details;
               commit;";

    $result = pg_query($vmsql->link_db, $sql);
    if (!$result) {
        echo "Si è verificato un errore!";
    } else {
        echo "Refresh effettuato!";
    }
} catch (Exception $e) {
    echo $e->getMessage();
}


echo closeLayout1();
?> 