<?php

require_once("../inc/conn.php"); 
require_once("../inc/layouts.php");
require_once("../inc/func.browser_detection.php");
require_once __DIR__.'/../classesNuove/GestoreSchemi.php';
proteggi(1);

$nomeSchemaBackend = GestoreSchemi::nomeSchema('backend');
$nomeSchemaOnline = GestoreSchemi::nomeSchema('online');

function createQuakeList() {
 
    global $vmsql;

    $XMLQuakeList = new DomDocument('1.0', 'UTF-8');
    $XMLQuakeList->preserveWhiteSpace = true;
    $XMLQuakeList->formatOutput = true;

    $root = $XMLQuakeList->createElement('Quakes');
    $XMLQuakeList->appendChild($root);

    $encoding = pg_client_encoding($vmsql->link_db);
    echo $encoding . " ";

    $result = pg_query($vmsql->link_db, "SELECT nterr, nperiod, anno, mese, giorno, data_label, ora, minu, sec, "
            . "time_label, lat, lon, earthquakelocation, country, epicenter_type, io, imax, mm, npun, ee_nt,"
            . " ee_np, level, rel, flagfalseeq, new2018, cat FROM nterrs order by nterr");

    if (!$result) {
        echo "An error occurred.\n";
        exit;
    }

    while ($row = pg_fetch_array($result)) {

        $elem = $XMLQuakeList->createElement('Quake');

        foreach ($row as $key => $value) {

            if (!is_numeric($key)) {
                // echo $key." - ".$value."<br>";
                $child = $XMLQuakeList->createElement($key);
                $child = $elem->appendChild($child);

                $value = $XMLQuakeList->createTextNode(trim($value));
                $value = $child->appendChild($value);
            }
        }

        $root->appendChild($elem);
    }



    $result2 = pg_query($vmsql->link_db, "SELECT nterr, nperiod, anno, mese, giorno, data_label, ora, minu, sec, "
            . "time_label, lat, lon, earthquakelocation, country, epicenter_type, io, imax, mm, npun, ee_nt, ee_np, "
            . "level, rel, flagfalseeq, cat FROM nterrs_med order by nterr");

    if (!$result2) {
        echo "An error occurred.\n";
        exit;
    }

    while ($row2 = pg_fetch_array($result2)) {

        $elem = $XMLQuakeList->createElement('Quake');

        foreach ($row2 as $key => $value) {

            if (!is_numeric($key)) {
                // echo $key." - ".$value."<br>";
                $child = $XMLQuakeList->createElement($key);
                $child = $elem->appendChild($child);

                $value = $XMLQuakeList->createTextNode(trim($value));
                $value = $child->appendChild($value);
            }
        }

        $root->appendChild($elem);
    }

    $dir = '../files/ProcedureXML/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }

    $XMLQuakeList->save($dir.'QuakeList.xml');
    //$XMLQuakeList->save('../files/xml/files/QuakeList.xml');
}

function createLocList() {

    global $vmsql;

    $XMLLocList = new DomDocument('1.0', 'UTF-8');
    $XMLLocList->preserveWhiteSpace = true;
    $XMLLocList->formatOutput = true;

    $root = $XMLLocList->createElement('Locs');
    $XMLLocList->appendChild($root);

    $result = pg_query($vmsql->link_db, "SELECT nloc_cfti, desloc_cfti, provlet, nazione, risentimenti, ee,
           maxint, lat_wgs84, lon_wgs84, notesito FROM locind ORDER BY desloc_cfti");

    if (!$result) {
        echo "An error occurred.\n";
        exit;
    }

    while ($row = pg_fetch_array($result)) {

        $elem = $XMLLocList->createElement('Loc');

        foreach ($row as $key => $value) {
            if (!is_numeric($key)) {
                $child = $XMLLocList->createElement($key);
                $child = $elem->appendChild($child);
                $value = $XMLLocList->createTextNode(trim($value));
                $value = $child->appendChild($value);
            }
        }

        $root->appendChild($elem);
    }

    $dir = '../files/ProcedureXML/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }

    $XMLLocList->save($dir.'LocList.xml');
}

function createLocalityDetail($limit) {

    global $vmsql;

    $crlf = chr(13) . chr(10);

    $dir = '../files/ProcedureXML/localitySources/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $report_codbib_not_found = fopen($dir.'testfile.txt', "w");

    $LocalityDetailQuery = pg_query($vmsql->link_db, "SELECT nloc_cfti, desloc_cfti, provlet,"
            . " nazione, risentimenti, maxint, lat_wgs84, lon_wgs84, notesito FROM locind order by nloc_cfti " . $limit);

    while ($Detail = pg_fetch_assoc($LocalityDetailQuery)) {
        $NLOC = trim($Detail['nloc_cfti']);
        // $NLOC ="052305.00";
        
        $XMLSource = $dir."{$NLOC}.xml";
        // $HTMLOutput = "../localities/{$NLOC}.html";	

        $XMLLocalityDetail = new DomDocument('1.0', 'UTF-8');
        $XMLLocalityDetail->preserveWhiteSpace = true;
        $XMLLocalityDetail->formatOutput = true;

        $root = $XMLLocalityDetail->createElement('Locality');
        $XMLLocalityDetail->appendChild($root);
        $root->setAttribute('alone', 'true');

        $Details = $XMLLocalityDetail->createElement('Details');
        $root->appendChild($Details);

        $Quakes = $XMLLocalityDetail->createElement('Quakes');
        $root->appendChild($Quakes);

        $elem = $XMLLocalityDetail->createElement('Detail');

        foreach ($Detail as $key => $value) {


            $child = $XMLLocalityDetail->createElement($key);
            $child2 = $elem->appendChild($child);
            $value1 = $XMLLocalityDetail->createTextNode(trim($value));
            $value = $child->appendChild($value1);
        }

        $Details->appendChild($elem);

        //Quakes


        $n = 0;
        $QuakesQuery = pg_query($vmsql->link_db, "SELECT * FROM quake_details WHERE nloc_cfti = '{$NLOC}'");

        while ($Row = pg_fetch_assoc($QuakesQuery)) {
            $elem = $XMLLocalityDetail->createElement('Quake');
            $sel[$n] = $Row['nperiod'];
            foreach ($Row as $key => $value) {
                if (!is_numeric($key)) {
                    $child = $XMLLocalityDetail->createElement($key);
                    $child = $elem->appendChild($child);

                    $value = $XMLLocalityDetail->createTextNode(trim($value));
                    $value = $child->appendChild($value);
                }
            }
            $nperiod = $sel[$n];

            $n = $n + 1;

            $comm = "";
            $flagcomm = $Row['flagcomments'];
            if ($flagcomm == 4) {
                $LocCommentQuery = pg_query($vmsql->link_db, "SELECT * FROM d1 WHERE nperiod_cfti = '{$nperiod}'"
                        . " and nloc LIKE '{$NLOC}%'");

                if (pg_num_rows($LocCommentQuery)) {

                    $Row2 = pg_fetch_assoc($LocCommentQuery);

                    $comm = $Row2['testocomm'];

                    $child = $XMLLocalityDetail->createElement('D1');
                    $child = $elem->appendChild($child);

                    $value = $XMLLocalityDetail->createTextNode($comm);
                    $value = $child->appendChild($value);
                } else {
                    $child = $XMLLocalityDetail->createElement('D1');
                    $child = $elem->appendChild($child);
                    fwrite($report_codbib_not_found, "D1 mancante:" . $nperiod . " " . $NLOC . $crlf);
                }
            } else {
                $child = $XMLLocalityDetail->createElement('D1');
                $child = $elem->appendChild($child);
            }
            $Quakes->appendChild($elem);
        }

        //BIBLIO

        $Biblio = $XMLLocalityDetail->createElement('Biblios');
        $root->appendChild($Biblio);

        $n = 0;
        $BiblioQuery = pg_query($vmsql->link_db, "SELECT * FROM schedec WHERE nloc LIKE '{$NLOC}%'");

        while ($Row = pg_fetch_assoc($BiblioQuery)) {
            $sel[$n] = $Row['codbib'];

            $codbib = $sel[$n];

            $n = $n + 1;

            $BiblioDetQuery = pg_query($vmsql->link_db, "SELECT codbib, autore1,titolo1, "
                    . "luogoed,datauni,dataun2 FROM schedea WHERE codbib = '{$codbib}' ");
            if (pg_num_rows($BiblioDetQuery)) {

                $Row2 = pg_fetch_assoc($BiblioDetQuery);

                $elem = $XMLLocalityDetail->createElement('Bibliography');

                $data_1 = $Row2['datauni'];
                $data_2 = $Row2['dataun2'];
                if ($data_1 == '0009') {
                    $data_1 = 'IX sec.';
                };
                if ($data_2 == '0009') {
                    $data_2 = 'IX sec.';
                };
                if ($data_1 == '0010') {
                    $data_1 = 'X sec.';
                };
                if ($data_2 == '0010') {
                    $data_2 = 'X sec.';
                };
                if ($data_1 == '0011') {
                    $data_1 = 'XI sec.';
                };
                if ($data_2 == '0011') {
                    $data_2 = 'XI sec.';
                };
                if ($data_1 == '0012') {
                    $data_1 = 'XII sec.';
                };
                if ($data_2 == '0012') {
                    $data_2 = 'XII sec.';
                };
                if ($data_1 == '0013') {
                    $data_1 = 'XIII sec.';
                };
                if ($data_2 == '0013') {
                    $data_2 = 'XIII sec.';
                };
                if ($data_1 == '0014') {
                    $data_1 = 'XIV sec.';
                };
                if ($data_2 == '0014') {
                    $data_2 = 'XIV sec.';
                };
                if ($data_1 == '0015') {
                    $data_1 = 'XV sec.';
                };
                if ($data_2 == '0015') {
                    $data_2 = 'XV sec.';
                };
                if ($data_1 == '0016') {
                    $data_1 = 'XVI sec.';
                };
                if ($data_2 == '0016') {
                    $data_2 = 'XVI sec.';
                };
                if ($data_1 == '0017') {
                    $data_1 = 'XVII sec.';
                };
                if ($data_2 == '0017') {
                    $data_2 = 'XVII sec.';
                };
                if ($data_1 == '0018') {
                    $data_1 = 'XVIII sec.';
                };
                if ($data_2 == '0018') {
                    $data_2 = 'XVIII sec.';
                };
                if ($data_1 == '0019') {
                    $data_1 = 'XIX sec.';
                };
                if ($data_2 == '0019') {
                    $data_2 = 'XIX sec.';
                };
                if ($data_1 == '0020') {
                    $data_1 = 'XX sec.';
                };
                if ($data_2 == '0020') {
                    $data_2 = 'XX sec.';
                };
                if ($data_1 == '0021') {
                    $data_1 = 'XXI sec.';
                };
                if ($data_2 == '0021') {
                    $data_2 = 'XXI sec.';
                };

                if ($data_2 == '') {
                    $datacompl = $data_1;
                } else {
                    $datacompl = $data_1 . " - " . $data_2;
                };

                $child = $XMLLocalityDetail->createElement('codbib');
                $child = $elem->appendChild($child);

                $value = $XMLLocalityDetail->createTextNode($Row2['codbib']);
                $value = $child->appendChild($value);

                $child = $XMLLocalityDetail->createElement('autore1');
                $child = $elem->appendChild($child);

                $value = $XMLLocalityDetail->createTextNode($Row2['autore1']);
                $value = $child->appendChild($value);

                $child = $XMLLocalityDetail->createElement('titolo1');
                $child = $elem->appendChild($child);

                $value = $XMLLocalityDetail->createTextNode($Row2['titolo1']);
                $value = $child->appendChild($value);

                $child = $XMLLocalityDetail->createElement('luogoed');
                $child = $elem->appendChild($child);

                $value = $XMLLocalityDetail->createTextNode($Row2['luogoed']);
                $value = $child->appendChild($value);

                $child = $XMLLocalityDetail->createElement('datacompl');
                $child = $elem->appendChild($child);

                $value = $XMLLocalityDetail->createTextNode($datacompl);
                $value = $child->appendChild($value);

                $Biblio->appendChild($elem);
            } else {
                fwrite($report_codbib_not_found, "BIBLIO D1 mancante:" . $codbib . " " . $Detail['nloc_cfti'] . $crlf);
            }
        }

        if (1) {
            $XMLLocalityDetail->save($XMLSource);
        }
    }


    fclose($report_codbib_not_found);
}

function createQuakeDetail($limit) {

    global $vmsql;

    //ITA

    $QuakeDetailQuery = pg_query($vmsql->link_db, "SELECT cat, nterr, nperiod, datanum, data_label,"
            . " anno, mese, giorno, time_label, ora, minu, sec, lat, lon, rel, level, io, imax, npun, "
            . "ee_nt, ee_np, mm, earthquakelocation, epicenter_type, country, flagcomments, flagfalseeq, "
            . "new2018 "
            . "FROM nterrs order by nterr " . $limit);

    /*
      $QuakeDetailQuery = pg_query($vmsql->link_db, "SELECT cat, nterr, nperiod, datanum, data_label,"
      . " anno, mese, giorno, time_label, ora, minu, sec, lat, lon, rel, level, io, imax, npun, "
      . "ee_nt, ee_np, mm, earthquakelocation, epicenter_type, country, flagcomments, flagfalseeq, "
      . "new2018 "
      . "FROM nterrs order by nterr DESC limit 10");
     */
    LoopQuake($QuakeDetailQuery);
}

function createQuakeDetail_med($limit) {

    global $vmsql;

    //MED
    $QuakeDetailQuery2 = pg_query($vmsql->link_db, "SELECT cat, nterr, nperiod, datanum, data_label, anno, mese,"
            . " giorno, time_label, ora, minu, sec, lat, lon, rel, level, io, imax, npun, ee_nt, ee_np, mm, "
            . "earthquakelocation, epicenter_type, country, flagcomments, flagfalseeq "
            . "FROM nterrs_med order by nterr " . $limit);

    LoopQuake($QuakeDetailQuery2);
}

function LoopQuake($QuakeDetailQuery) {

    global $vmsql;
    while ($Detail = pg_fetch_assoc($QuakeDetailQuery)) {

        try {

            $flagcomm = $Detail['flagcomments'];

            //$XMLSource = "../files/xml/quakeSources/{$Detail['nterr']}.xml";
            $dir = '../files/ProcedureXML/quakeSources/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $XMLSource = $dir."{$Detail['nterr']}.xml";

            $XMLQuakeDetail = new DomDocument('1.0', 'UTF-8');
            $XMLQuakeDetail->preserveWhiteSpace = true;
            $XMLQuakeDetail->formatOutput = true;

            $root = $XMLQuakeDetail->createElement('Quake');
            $XMLQuakeDetail->appendChild($root);
            $root->setAttribute('alone', 'true');

            $Details = $XMLQuakeDetail->createElement('Details');
            $root->appendChild($Details);

            $Comments = $XMLQuakeDetail->createElement('Comments');
            $root->appendChild($Comments);

            $Biblios = $XMLQuakeDetail->createElement('Biblios');
            $root->appendChild($Biblios);

            $Localities = $XMLQuakeDetail->createElement('Localities');
            $root->appendChild($Localities);

            $LocComments = $XMLQuakeDetail->createElement('LocComments');
            $root->appendChild($LocComments);

            $Seqs = $XMLQuakeDetail->createElement('Seqs');
            $root->appendChild($Seqs);

            // Quake Detail
            $elem = $XMLQuakeDetail->createElement('Detail');
            foreach ($Detail as $key => $value) {
                if (!is_numeric($key)) {
                    $child = $XMLQuakeDetail->createElement($key);
                    $child = $elem->appendChild($child);

                    $value = $XMLQuakeDetail->createTextNode(trim($value));
                    $value = $child->appendChild($value);
                }
            }

            $Details->appendChild($elem);

            //Comments
            if ($flagcomm == 4 or $flagcomm == 5) {

                $CommentsQuery = pg_query($vmsql->link_db, "select codtab, testocomm "
                        . "from commgen where nperiod_cfti = '{$Detail['nperiod']}' order by codtab");

                while ($Row = pg_fetch_assoc($CommentsQuery)) {
                    if (($Row['codtab'] == 'D0' && $flagcomm == 5) || ($flagcomm == 4) ||
                            ($Row['codtab'] == 'E0')) {
                        $elem = $XMLQuakeDetail->createElement('Comment');

                        foreach ($Row as $key => $value) {
                            if (!is_numeric($key)) {
                                if ($key == "testocomm") {

                                    $value2 = str_replace(array("\r\n", "\n\r", "\n", "\r"), htmlspecialchars_decode("&lt;br&gt;"), $value);

                                    $child = $XMLQuakeDetail->createElement($key);
                                    $child = $elem->appendChild($child);

                                    $value = $XMLQuakeDetail->createTextNode($value2);
                                    $value = $child->appendChild($value);
                                } else {
                                    $child = $XMLQuakeDetail->createElement($key);
                                    $child = $elem->appendChild($child);

                                    $value = $XMLQuakeDetail->createTextNode($value);
                                    $value = $child->appendChild($value);
                                }
                            }
                        }

                        $Comments->appendChild($elem);
                    }
                }
            }
            //Bibliography

            $BibliographyQueryB = pg_query($vmsql->link_db, "SELECT codbib, valb, desvalingl "
                    . "FROM schedeb WHERE nperiod = '{$Detail['nperiod']}' ");

            while ($Row = pg_fetch_assoc($BibliographyQueryB)) {
                //echo $Row['codbib'];
                $BibliographyQuery = pg_query($vmsql->link_db, "SELECT biblio_details.codbib, biblio_details.autore1, "
                        . "biblio_details.titolo1, biblio_details.luogoed, biblio_details.datauni, biblio_details.dataun2, "
                        . "biblio_details.desvalingl, biblio_details.valb "
                        . "FROM biblio_details "
                        . "WHERE biblio_details.codbib = '{$Row['codbib']}' and biblio_details.nperiod = '{$Detail['nperiod']}'");
                $elem = $XMLQuakeDetail->createElement('Bibliography');
                $Row2 = pg_fetch_assoc($BibliographyQuery);
                $data_1 = $Row2['datauni'];
                $data_2 = $Row2['dataun2'];
                if ($data_1 == '0009') {
                    $data_1 = 'IX sec.';
                };
                if ($data_2 == '0009') {
                    $data_2 = 'IX sec.';
                };
                if ($data_1 == '0010') {
                    $data_1 = 'X sec.';
                };
                if ($data_2 == '0010') {
                    $data_2 = 'X sec.';
                };
                if ($data_1 == '0011') {
                    $data_1 = 'XI sec.';
                };
                if ($data_2 == '0011') {
                    $data_2 = 'XI sec.';
                };
                if ($data_1 == '0012') {
                    $data_1 = 'XII sec.';
                };
                if ($data_2 == '0012') {
                    $data_2 = 'XII sec.';
                };
                if ($data_1 == '0013') {
                    $data_1 = 'XIII sec.';
                };
                if ($data_2 == '0013') {
                    $data_2 = 'XIII sec.';
                };
                if ($data_1 == '0014') {
                    $data_1 = 'XIV sec.';
                };
                if ($data_2 == '0014') {
                    $data_2 = 'XIV sec.';
                };
                if ($data_1 == '0015') {
                    $data_1 = 'XV sec.';
                };
                if ($data_2 == '0015') {
                    $data_2 = 'XV sec.';
                };
                if ($data_1 == '0016') {
                    $data_1 = 'XVI sec.';
                };
                if ($data_2 == '0016') {
                    $data_2 = 'XVI sec.';
                };
                if ($data_1 == '0017') {
                    $data_1 = 'XVII sec.';
                };
                if ($data_2 == '0017') {
                    $data_2 = 'XVII sec.';
                };
                if ($data_1 == '0018') {
                    $data_1 = 'XVIII sec.';
                };
                if ($data_2 == '0018') {
                    $data_2 = 'XVIII sec.';
                };
                if ($data_1 == '0019') {
                    $data_1 = 'XIX sec.';
                };
                if ($data_2 == '0019') {
                    $data_2 = 'XIX sec.';
                };
                if ($data_1 == '0020') {
                    $data_1 = 'XX sec.';
                };
                if ($data_2 == '0020') {
                    $data_2 = 'XX sec.';
                };
                if ($data_1 == '0021') {
                    $data_1 = 'XXI sec.';
                };
                if ($data_2 == '0021') {
                    $data_2 = 'XXI sec.';
                };

                if ($data_2 == '') {
                    $datacompl = $data_1;
                } else {
                    $datacompl = $data_1 . " - " . $data_2;
                };

                $child = $XMLQuakeDetail->createElement('codbib');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($Row2['codbib']);
                $value = $child->appendChild($value);

                $child = $XMLQuakeDetail->createElement('autore1');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($Row2['autore1']);
                $value = $child->appendChild($value);

                $child = $XMLQuakeDetail->createElement('titolo1');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($Row2['titolo1']);
                $value = $child->appendChild($value);

                $child = $XMLQuakeDetail->createElement('luogoed');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($Row2['luogoed']);
                $value = $child->appendChild($value);

                $child = $XMLQuakeDetail->createElement('datacompl');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($datacompl);
                $value = $child->appendChild($value);

                $child = $XMLQuakeDetail->createElement('valb');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($Row2['valb']);
                $value = $child->appendChild($value);

                $child = $XMLQuakeDetail->createElement('desvalingl');
                $child = $elem->appendChild($child);

                $value = $XMLQuakeDetail->createTextNode($Row2['desvalingl']);
                $value = $child->appendChild($value);

                $Biblios->appendChild($elem);
            }

            if ($Detail['cat'] == 'ITA') {
                //Effetti sulle localita'


                $LocalityQuery = pg_query($vmsql->link_db, "SELECT intensità_romano, desloc, lat_wgs84, lon_wgs84, provlet,"
                        . " nazione, intensità_arabo, nterrs, nloc, nloc_cfti, desloc_cfti, notesito "
                        . "FROM pq WHERE nterrs = '{$Detail['nterr']}' ORDER BY intensità_arabo DESC, desloc");
            } else {

                //Effetti sulle localita'


                $LocalityQuery = pg_query($vmsql->link_db, "SELECT intensità_romano, desloc, lat_wgs84, lon_wgs84, provlet,"
                        . " nazione, intensità_arabo, nterr, nloc, nloc_cfti, desloc_cfti, notesito "
                        . "FROM pq_med WHERE nterr = '{$Detail['nterr']}' ORDER BY intensità_arabo DESC, desloc");
            }

            while ($Row = pg_fetch_assoc($LocalityQuery)) {
                $elem = $XMLQuakeDetail->createElement('Locality');

                foreach ($Row as $key => $value) {
                    if (!is_numeric($key)) {
                        if ($key == 'nloc') {
                            
                        } else {
                            $child = $XMLQuakeDetail->createElement($key);
                            $child = $elem->appendChild($child);
                            $value = $XMLQuakeDetail->createTextNode(trim($value));
                            $value = $child->appendChild($value);
                        }
                    }
                }


                $nloc = trim($Row['nloc']);
                if ($flagcomm == 4) {
                    $LocCommentQuery = pg_query($vmsql->link_db, "SELECT * FROM d1 "
                            . "WHERE nperiod_cfti = '{$Detail['nperiod']}' and nloc = '{$nloc}'");

                    $Row2 = pg_fetch_assoc($LocCommentQuery);

                    // echo $Row2['nloc'];
                    $comm = $Row2['testocomm'];

                    $child = $XMLQuakeDetail->createElement('COMM');
                    $child = $elem->appendChild($child);

                    $value = $XMLQuakeDetail->createTextNode($comm);
                    $value = $child->appendChild($value);
                } else {
                    $child = $XMLQuakeDetail->createElement('COMM');
                    $child = $elem->appendChild($child);
                }
                $LocComments->appendChild($elem);
            }

            if (1) { //Create Debug XML files
                $XMLQuakeDetail->save($XMLSource);
                echo $XMLSource;
                echo "<br>";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

function createEEList() {
    global $vmsql;

    //ITA
    $dir = '../files/ProcedureXML/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $XMLSource = $dir."EEList.xml";

    $XMLEEDetail = new DomDocument('1.0', 'UTF-8');
    $XMLEEDetail->preserveWhiteSpace = true;
    $XMLEEDetail->formatOutput = true;

    $root = $XMLEEDetail->createElement('EELIST');
    $XMLEEDetail->appendChild($root);
    $root->setAttribute('alone', 'true');

    $EEDetailQuery = pg_query($vmsql->link_db, "SELECT NTERR, NPERIOD, NLOC_CFTI, DESLOC_CFTI, CODICE_EFF, LAT_WGS84, 
            LON_WGS84, NOTESITO, PROVLET, NAZIONE, NLOC, DESLOC, COMMENTO FROM EE order by nperiod");

    while ($Row = pg_fetch_assoc($EEDetailQuery)) {

        $elem = $XMLEEDetail->createElement('EE');

        foreach ($Row as $key => $value) {
            if (!is_numeric($key)) {
                $child = $XMLEEDetail->createElement(strtoupper($key));
                $child = $elem->appendChild($child);

                $value = $XMLEEDetail->createTextNode($value);
                $value = $child->appendChild($value);
            }
        }

        $root->appendChild($elem);
    }

    $XMLEEDetail->save($XMLSource);

    //MED 
    $elem = "";
    
    $dir = '../files/ProcedureXML/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $XMLSource2 = $dir."EEList_MED.xml";

    $XMLEE_MEDDetail = new DomDocument('1.0', 'UTF-8');
    $XMLEE_MEDDetail->preserveWhiteSpace = true;
    $XMLEE_MEDDetail->formatOutput = true;
    $root2 = $XMLEE_MEDDetail->createElement('EELIST');
    $XMLEE_MEDDetail->appendChild($root2);
    $root2->setAttribute('alone', 'true');
    $EEDetailQueryM = pg_query($vmsql->link_db, "SELECT NTERR, NPERIOD, NLOC_CFTI, DESLOC_CFTI, CODICE_EFF, LAT_WGS84, 
            LON_WGS84, NOTESITO, PROVLET, NAZIONE, NLOC, DESLOC FROM EE_MED order by nperiod");

    while ($Row = pg_fetch_assoc($EEDetailQueryM)) {
        $elem = $XMLEE_MEDDetail->createElement('EE_MED');

        foreach ($Row as $key => $value) {
            if (!is_numeric($key)) {
                $child = $XMLEE_MEDDetail->createElement(strtoupper($key));
                $child = $elem->appendChild($child);

                echo $value;

                $value = $XMLEE_MEDDetail->createTextNode($value);
                $value = $child->appendChild($value);
            }
        }
        $root2->appendChild($elem);
    }

    $XMLEE_MEDDetail->save($XMLSource2);

    //Biblio EE
    $elem = "";
    $dir = '../files/ProcedureXML/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $XMLSource3 = $dir."BiblioEE.xml";

    $XMLEE_BibDetail = new DomDocument('1.0', 'UTF-8');
    $XMLEE_BibDetail->preserveWhiteSpace = true;
    $XMLEE_BibDetail->formatOutput = true;
    $root3 = $XMLEE_BibDetail->createElement('EELIST');
    $XMLEE_BibDetail->appendChild($root3);
    $root3->setAttribute('alone', 'true');

    $BibliographyQueryB = pg_query($vmsql->link_db, "SELECT codbib FROM biblio_ee");

    while ($Row = pg_fetch_assoc($BibliographyQueryB)) {

        $BibliographyQuery = pg_query($vmsql->link_db, "SELECT codbib, autore1,titolo1, "
                . "luogoed,datauni,dataun2 FROM schedea WHERE codbib = '{$Row['codbib']}' ");
        $elem = $XMLEE_BibDetail->createElement('BIBLIO_EE');
        $Row2 = pg_fetch_assoc($BibliographyQuery);

        $data_1 = $Row2['datauni'];
        $data_2 = $Row2['dataun2'];
        if ($data_1 == '0009') {
            $data_1 = 'IX sec.';
        };
        if ($data_2 == '0009') {
            $data_2 = 'IX sec.';
        };
        if ($data_1 == '0010') {
            $data_1 = 'X sec.';
        };
        if ($data_2 == '0010') {
            $data_2 = 'X sec.';
        };
        if ($data_1 == '0011') {
            $data_1 = 'XI sec.';
        };
        if ($data_2 == '0011') {
            $data_2 = 'XI sec.';
        };
        if ($data_1 == '0012') {
            $data_1 = 'XII sec.';
        };
        if ($data_2 == '0012') {
            $data_2 = 'XII sec.';
        };
        if ($data_1 == '0013') {
            $data_1 = 'XIII sec.';
        };
        if ($data_2 == '0013') {
            $data_2 = 'XIII sec.';
        };
        if ($data_1 == '0014') {
            $data_1 = 'XIV sec.';
        };
        if ($data_2 == '0014') {
            $data_2 = 'XIV sec.';
        };
        if ($data_1 == '0015') {
            $data_1 = 'XV sec.';
        };
        if ($data_2 == '0015') {
            $data_2 = 'XV sec.';
        };
        if ($data_1 == '0016') {
            $data_1 = 'XVI sec.';
        };
        if ($data_2 == '0016') {
            $data_2 = 'XVI sec.';
        };
        if ($data_1 == '0017') {
            $data_1 = 'XVII sec.';
        };
        if ($data_2 == '0017') {
            $data_2 = 'XVII sec.';
        };
        if ($data_1 == '0018') {
            $data_1 = 'XVIII sec.';
        };
        if ($data_2 == '0018') {
            $data_2 = 'XVIII sec.';
        };
        if ($data_1 == '0019') {
            $data_1 = 'XIX sec.';
        };
        if ($data_2 == '0019') {
            $data_2 = 'XIX sec.';
        };
        if ($data_1 == '0020') {
            $data_1 = 'XX sec.';
        };
        if ($data_2 == '0020') {
            $data_2 = 'XX sec.';
        };
        if ($data_1 == '0021') {
            $data_1 = 'XXI sec.';
        };
        if ($data_2 == '0021') {
            $data_2 = 'XXI sec.';
        };

        if ($data_2 == '') {
            $datacompl = $data_1;
        } else {
            $datacompl = $data_1 . " - " . $data_2;
        };

        $child = $XMLEE_BibDetail->createElement('codbib');
        $child = $elem->appendChild($child);

        $value = $XMLEE_BibDetail->createTextNode($Row2['codbib']);
        $value = $child->appendChild($value);

        $child = $XMLEE_BibDetail->createElement('autore1');
        $child = $elem->appendChild($child);

        $value = $XMLEE_BibDetail->createTextNode($Row2['autore1']);
        $value = $child->appendChild($value);

        $child = $XMLEE_BibDetail->createElement('titolo1');
        $child = $elem->appendChild($child);

        $value = $XMLEE_BibDetail->createTextNode($Row2['titolo1']);
        $value = $child->appendChild($value);

        $child = $XMLEE_BibDetail->createElement('luogoed');
        $child = $elem->appendChild($child);

        $value = $XMLEE_BibDetail->createTextNode($Row2['luogoed']);
        $value = $child->appendChild($value);

        $child = $XMLEE_BibDetail->createElement('datacompl');
        $child = $elem->appendChild($child);

        $value = $XMLEE_BibDetail->createTextNode($datacompl);
        $value = $child->appendChild($value);

        $root3->appendChild($elem);
    }


    $XMLEE_BibDetail->save($XMLSource3);

    echo "xml creato";
}

function createSequenze() {
    global $vmsql, $nomeSchemaOnline;
    $dir1 = '../files/Sequenze_XML/';
    if (!file_exists($dir1)) {
        mkdir($dir1, 0777, true);
    }

    $dir2 = $dir1 . 'sequenzeSources/';
    if (!file_exists($dir2)) {
        mkdir($dir2, 0777, true);
    }

    $query = "SELECT intervallo_anni, area_epicentrale, codice_sequenza  FROM $nomeSchemaOnline.vnperiod_cfti order by intervallo_anni;";
    $NPDetailQuery = pg_query($vmsql->link_db, $query);

    $XMLSourceL = $dir1 . "SequenceList.xml";
    //DOCUMENTO XML
    $XMLListDetail = new DomDocument('1.0', 'UTF-8');
    $XMLListDetail->preserveWhiteSpace = true;
    $XMLListDetail->formatOutput = true;
    $SeqsRoot = $XMLListDetail->createElement('dataroot');
    $XMLListDetail->appendChild($SeqsRoot);

    if ($NPDetailQuery !== false) {
        loopNP($NPDetailQuery, $XMLListDetail, $SeqsRoot, $dir2);
    } else {
        echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
    }




    $XMLListDetail->save($XMLSourceL);
    echo "$XMLSourceL \n";
}

function loopNP($NPDetailQuery, $XMLListDetail, $SeqsRoot, $dir2) {
    global $vmsql;

    while ($Detail = pg_fetch_array($NPDetailQuery)) {


        //GENERO IL FILE SEQUENZELIST.XML
        $XMLQuakeDetail = new DomDocument('1.0', 'UTF-8');
        $XMLQuakeDetail->preserveWhiteSpace = true;
        $XMLQuakeDetail->formatOutput = true;

        $Seqs = $XMLQuakeDetail->createElement('sequence');
        $XMLQuakeDetail->appendChild($Seqs);
        $Seqs->setAttribute('alone', 'true');

        $Seqs1 = $XMLListDetail->createElement('sequence');
        $SeqsRoot->appendChild($Seqs1);

        $child1 = $XMLListDetail->createElement('nperiod');
        $child1 = $Seqs1->appendChild($child1);
        $value = $XMLListDetail->createTextNode($Detail['codice_sequenza']);
        $value = $child1->appendChild($value); 

        $child1 = $XMLListDetail->createElement('title');
        $child1 = $Seqs1->appendChild($child1);
        $value = $XMLListDetail->createTextNode($Detail['intervallo_anni'] . " - " . $Detail['area_epicentrale']);
        $value = $child1->appendChild($value);
        
        
        
        //GENERO I FILE SEQUENZE SOURCES
        $XMLSource = $dir2 . "seq{$Detail['codice_sequenza']}.xml";

        

          $query = "select * from seq_scosse where nperiod = '{$Detail['codice_sequenza']}' order by anno1, mese1, giorno1, oragmt1;";
          $SeqQuery = pg_query($vmsql->link_db, $query);

        if ($SeqQuery === false) {
            echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
            exit();
        }

        while ($Row = pg_fetch_array($SeqQuery)) {

            
            $NT = strlen($Row['nterr']);
            if ($NT == 5) {
                // echo 'si entra nella procedura NTERR' . "\n" ;	

                $query = "select * from NTERRS where nterr = '{$Row['nterr']}' ";
                $NTquery = pg_query($vmsql->link_db, $query);
    
                if ($NTquery === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
                }

                $Row2 = pg_fetch_array($NTquery);
                

                if ($Row2['nperiod'] == '') {   //se non seleziono nulla (quell'NTERR non è in cfti5, ma solo in CFTI6)
                } else {
                    

                    $elem2 = $XMLQuakeDetail->createElement('scossa');
                    $D0 = "-";

                    $child = $XMLQuakeDetail->createElement('ID');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('nperiod');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['nperiod']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('codiceScossa');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('nterr');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['nterr']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('anno1');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['anno']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('mese1');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['mese']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('giorno1');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['giorno']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('anno2');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('mese2');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('giorno2');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('oraF');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('oraGMT1');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['time_label']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('oraGMT2');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('timeLABELgmt');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['time_label']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('timeLABELfonte');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode("-");
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('Io');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['io']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('imax');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($Row2['imax']);
                    $value = $child->appendChild($value);

                    $child = $XMLQuakeDetail->createElement('commentoScossa');
                    $child = $elem2->appendChild($child);
                    $value = $XMLQuakeDetail->createTextNode($D0);
                    $value = $child->appendChild($value);
                }
            } else {
                // echo 'si entra nella procedura SCOSSA' . "\n" ;	

                $elem2 = $XMLQuakeDetail->createElement('scossa');

                
                foreach ($Row as $key => $value) {
                    if ($value == "") {
                        $value = "-";
                    };
                    if ($value == -9) {
                        $value = "-";
                    };
                    
                    if(!is_int($key)){
                        $child = $XMLQuakeDetail->createElement($key);
                        $child = $elem2->appendChild($child);
                        $value = $XMLQuakeDetail->createTextNode($value);
                        $value = $child->appendChild($value);
                    }
                    

                    
                }
            }
            $Seqs->appendChild($elem2);
        }

        //generazione località
        $query = "select * from seq_loc where nperiod = '{$Detail['codice_sequenza']}' order by codicescossa";
        $SeqQuery = pg_query($vmsql->link_db, $query);
        if ($SeqQuery === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
        }
        
        $n = 1;
        while ($Row = pg_fetch_array($SeqQuery)) {
            $Row['nomeloc'];
            
            // if ($Row['nomeloc'] <> '') {
            $elem2 = $XMLQuakeDetail->createElement('localita');

            $child = $XMLQuakeDetail->createElement('nperiod');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['nperiod']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('codiceScossa');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['codicescossa']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('nterr');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode('-');
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('nscosse');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['nscosse']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('durCalc');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['durcalc']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('durFonti');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['durfonti']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('intFonte');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['intfonte']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('intMCS');
            $child = $elem2->appendChild($child);

            if ($Row['imcs'] == '-' || $Row['imcs'] == '') {

                $value = $XMLQuakeDetail->createTextNode($Row['imcs_nuovo']);
                $value = $child->appendChild($value);
            } else {

                $value = $XMLQuakeDetail->createTextNode($Row['imcs']);
                $value = $child->appendChild($value);
            }

            $child = $XMLQuakeDetail->createElement('ID');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($n);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('nomeloc');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['nomeloc']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('NLOC_CFTI');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['nloc_cfti']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('latloc');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['latloc']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('lonloc');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['lonloc']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('commentoLoc');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row['commentoloc']);
            $value = $child->appendChild($value);

            $Seqs->appendChild($elem2);
            // }

            $n = $n + 1;
        }


        //Bibliography

        $query = "SELECT codbib, valb, desvalingl FROM schedeb WHERE nperiod = '{$Detail['codice_sequenza']}' ";
        $BibliographyQueryB = pg_query($vmsql->link_db, $query);
        if ($BibliographyQueryB === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
        }
        
        $Biblios = $XMLQuakeDetail->createElement('Biblios');
        $Seqs->appendChild($Biblios);

        $n = 1;
        while ($Row = pg_fetch_array($BibliographyQueryB)) {

            // echo "$n \n";
            $query = "SELECT schedea.codbib, schedea.autore1, schedea.titolo1, schedea.luogoed, schedea.datauni, schedea.dataun2, schedeb.desvalingl, schedeb.valb FROM schedea INNER JOIN schedeb ON (schedea.codbib = schedeb.codbib) WHERE schedea.codbib = '{$Row['codbib']}' ";
            $BibliographyQuery = pg_query($vmsql->link_db, $query);
            if ($BibliographyQuery === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
            }
            
            $elem2 = $XMLQuakeDetail->createElement('Bibliography');
            $Row2 = pg_fetch_array($BibliographyQuery);
            $data_1 = $Row2['datauni'];
            $data_2 = $Row2['dataun2'];
            if ($data_1 == '0009') {
                $data_1 = 'IX sec.';
            };
            if ($data_2 == '0009') {
                $data_2 = 'IX sec.';
            };
            if ($data_1 == '0010') {
                $data_1 = 'X sec.';
            };
            if ($data_2 == '0010') {
                $data_2 = 'X sec.';
            };
            if ($data_1 == '0011') {
                $data_1 = 'XI sec.';
            };
            if ($data_2 == '0011') {
                $data_2 = 'XI sec.';
            };
            if ($data_1 == '0012') {
                $data_1 = 'XII sec.';
            };
            if ($data_2 == '0012') {
                $data_2 = 'XII sec.';
            };
            if ($data_1 == '0013') {
                $data_1 = 'XIII sec.';
            };
            if ($data_2 == '0013') {
                $data_2 = 'XIII sec.';
            };
            if ($data_1 == '0014') {
                $data_1 = 'XIV sec.';
            };
            if ($data_2 == '0014') {
                $data_2 = 'XIV sec.';
            };
            if ($data_1 == '0015') {
                $data_1 = 'XV sec.';
            };
            if ($data_2 == '0015') {
                $data_2 = 'XV sec.';
            };
            if ($data_1 == '0016') {
                $data_1 = 'XVI sec.';
            };
            if ($data_2 == '0016') {
                $data_2 = 'XVI sec.';
            };
            if ($data_1 == '0017') {
                $data_1 = 'XVII sec.';
            };
            if ($data_2 == '0017') {
                $data_2 = 'XVII sec.';
            };
            if ($data_1 == '0018') {
                $data_1 = 'XVIII sec.';
            };
            if ($data_2 == '0018') {
                $data_2 = 'XVIII sec.';
            };
            if ($data_1 == '0019') {
                $data_1 = 'XIX sec.';
            };
            if ($data_2 == '0019') {
                $data_2 = 'XIX sec.';
            };
            if ($data_1 == '0020') {
                $data_1 = 'XX sec.';
            };
            if ($data_2 == '0020') {
                $data_2 = 'XX sec.';
            };
            if ($data_1 == '0021') {
                $data_1 = 'XXI sec.';
            };
            if ($data_2 == '0021') {
                $data_2 = 'XXI sec.';
            };

            if ($data_2 == '') {
                $datacompl = $data_1;
            } else {
                $datacompl = $data_1 . " - " . $data_2;
            };

            $child = $XMLQuakeDetail->createElement('codbib');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row2['codbib']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('autore1');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row2['autore1']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('titolo1');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row2['titolo1']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('luogoed');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row2['luogoed']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('datacompl');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($datacompl);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('valb');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row2['valb']);
            $value = $child->appendChild($value);

            $child = $XMLQuakeDetail->createElement('desvalingl');
            $child = $elem2->appendChild($child);
            $value = $XMLQuakeDetail->createTextNode($Row2['desvalingl']);
            $value = $child->appendChild($value);

            $Biblios->appendChild($elem2);
            $n = $n + 1;
        }

        $XMLQuakeDetail->save($XMLSource);
    }
}

//procedure che generano geojson
function createQuakeList_geojson() {

    global $vmsql;

    $result = pg_query($vmsql->link_db, "SELECT nterr, nperiod, anno, mese, giorno, data_label, ora, minu, sec, "
            . "time_label, lat, lon, earthquakelocation, country, epicenter_type, io, imax, mm, npun, ee_nt,"
            . " ee_np, level, rel, flagfalseeq, new2018, cat FROM nterrs order by nterr");

    if (!$result) {
        echo "An error occurred.\n";
        exit;
    }

    $result2 = pg_query($vmsql->link_db, "SELECT nterr, nperiod, anno, mese, giorno, data_label, ora, minu, sec, "
            . "time_label, lat, lon, earthquakelocation, country, epicenter_type, io, imax, mm, npun, ee_nt, ee_np, "
            . "level, rel, flagfalseeq, cat FROM nterrs_med order by nterr");

    if (!$result2) {
        echo "An error occurred.\n";
        exit;
    }

    $feature = array();
    $features = array();

    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {


        $geometry = ["type" => "Point", "coordinates" => [(float) $row['lon'], (float) $row['lat']]];
        unset($row['lat']);
        unset($row['lon']);

        $feature = ["type" => "Feature", "properties" => $row, "geometry" => $geometry];

        array_push($features, $feature);
    }

    while ($row = pg_fetch_array($result2, null, PGSQL_ASSOC)) {


        $geometry = ["type" => "Point", "coordinates" => [(float) $row['lon'], (float) $row['lat']]];
        unset($row['lat']);
        unset($row['lon']);

        $feature = ["type" => "Feature", "properties" => $row, "geometry" => $geometry];

        array_push($features, $feature);
    }




    $geojson = ["type" => "FeatureCollection", "features" => $features];

    $dir = '../files/ProcedureGeoJSON/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $fp3 = fopen($dir."QuakeList.geojson", 'w');
    fwrite($fp3, json_encode($geojson));
    fclose($fp3);
}

function createLocList_geojson() {

    global $vmsql;
    $geojson = "";

    $result = pg_query($vmsql->link_db, "SELECT nloc_cfti, desloc_cfti, provlet, nazione, risentimenti, ee,
           maxint, lat_wgs84, lon_wgs84, notesito FROM locind ORDER BY desloc_cfti");

    if (!$result) {
        echo "An error occurred.\n";
        exit;
    }

    $feature = array();
    $features = array();

    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {


        $geometry = ["type" => "Point", "coordinates" => [(float) $row['lon_wgs84'], (float) $row['lat_wgs84']]];
        unset($row['lat_wgs84']);
        unset($row['lon_wgs84']);

        $feature = ["type" => "Feature", "properties" => $row, "geometry" => $geometry];

        array_push($features, $feature);
    }
    $geojson = ["type" => "FeatureCollection", "features" => $features];

    $dir = '../files/ProcedureGeoJSON/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $fp3 = fopen($dir."LocList.geojson", 'w');
    fwrite($fp3, json_encode($geojson));
    fclose($fp3);
}

function createLocalityDetail_geojson($limit) {

    global $vmsql;

    $crlf = chr(13) . chr(10);

    $dir = '../files/ProcedureGeoJSON/localitySources/';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true); 
    }
    $report_codbib_not_found = fopen($dir.'testfile.txt', "w");

    $LocalityDetailQuery = pg_query($vmsql->link_db, "SELECT nloc_cfti, desloc_cfti, provlet,"
            . " nazione, risentimenti, maxint, lat_wgs84, lon_wgs84, notesito FROM locind order by nloc_cfti " . $limit);

    $locality_detail = array();
    $features = array();

    while ($Detail = pg_fetch_array($LocalityDetailQuery, null, PGSQL_ASSOC)) {
        $NLOC = trim($Detail['nloc_cfti']);

        //detail
        $geometry = ["type" => "Point", "coordinates" => [(float) $Detail['lon_wgs84'], (float) $Detail['lat_wgs84']]];
        unset($Detail['lat_wgs84']);
        unset($Detail['lon_wgs84']);
        $locality_detail = ["type" => "Feature", "properties" => $Detail, "geometry" => $geometry];

        //quakes
        $QuakesQuery = pg_query($vmsql->link_db, "SELECT * FROM quake_details WHERE nloc_cfti = '{$NLOC}'");
        $quakes = array();
        while ($quake = pg_fetch_array($QuakesQuery, null, PGSQL_ASSOC)) {

            $nperiod = $quake['nperiod'];
            $comm = "";
            $d1 = array();
            $flagcomm = $quake['flagcomments'];
            if ($flagcomm == 4) {
                $LocCommentQuery = pg_query($vmsql->link_db, "SELECT * FROM d1 WHERE nperiod_cfti = '{$nperiod}'"
                        . " and nloc LIKE '{$NLOC}%'");

                if (pg_num_rows($LocCommentQuery)) {

                    $row_d1 = pg_fetch_assoc($LocCommentQuery);
                    $comm = $row_d1['testocomm'];
                    $d1['D1'] = $comm;
                } else {

                    fwrite($report_codbib_not_found, "D1 mancante:" . $nperiod . " " . $NLOC . $crlf);
                }
            }

            $geometry = ["type" => "Point", "coordinates" => [(float) $quake['lon'], (float) $quake['lat']]];
            unset($quake['lat']);
            unset($quake['lon']);
            $feature_quake = ["type" => "Feature", "properties" => array_merge($quake, $d1), "geometry" => $geometry];
            array_push($quakes, $feature_quake);
        }

        //BIBLIO
        $BiblioQuery = pg_query($vmsql->link_db, "SELECT * FROM schedec WHERE nloc LIKE '{$NLOC}%'");
        $biblio = array();
        $biblios = array();
        while ($biblio_row = pg_fetch_array($BiblioQuery, null, PGSQL_ASSOC)) {

            $codbib = $biblio_row['codbib'];
            $BiblioDetQuery = pg_query($vmsql->link_db, "SELECT codbib, autore1,titolo1, "
                    . "luogoed,datauni,dataun2 FROM schedea WHERE codbib = '{$codbib}' ");
            if (pg_num_rows($BiblioDetQuery)) {

                $schedea_row = pg_fetch_assoc($BiblioDetQuery);

                $data_1 = $schedea_row['datauni'];
                $data_2 = $schedea_row['dataun2'];
                if ($data_1 == '0009') {
                    $data_1 = 'IX sec.';
                };
                if ($data_2 == '0009') {
                    $data_2 = 'IX sec.';
                };
                if ($data_1 == '0010') {
                    $data_1 = 'X sec.';
                };
                if ($data_2 == '0010') {
                    $data_2 = 'X sec.';
                };
                if ($data_1 == '0011') {
                    $data_1 = 'XI sec.';
                };
                if ($data_2 == '0011') {
                    $data_2 = 'XI sec.';
                };
                if ($data_1 == '0012') {
                    $data_1 = 'XII sec.';
                };
                if ($data_2 == '0012') {
                    $data_2 = 'XII sec.';
                };
                if ($data_1 == '0013') {
                    $data_1 = 'XIII sec.';
                };
                if ($data_2 == '0013') {
                    $data_2 = 'XIII sec.';
                };
                if ($data_1 == '0014') {
                    $data_1 = 'XIV sec.';
                };
                if ($data_2 == '0014') {
                    $data_2 = 'XIV sec.';
                };
                if ($data_1 == '0015') {
                    $data_1 = 'XV sec.';
                };
                if ($data_2 == '0015') {
                    $data_2 = 'XV sec.';
                };
                if ($data_1 == '0016') {
                    $data_1 = 'XVI sec.';
                };
                if ($data_2 == '0016') {
                    $data_2 = 'XVI sec.';
                };
                if ($data_1 == '0017') {
                    $data_1 = 'XVII sec.';
                };
                if ($data_2 == '0017') {
                    $data_2 = 'XVII sec.';
                };
                if ($data_1 == '0018') {
                    $data_1 = 'XVIII sec.';
                };
                if ($data_2 == '0018') {
                    $data_2 = 'XVIII sec.';
                };
                if ($data_1 == '0019') {
                    $data_1 = 'XIX sec.';
                };
                if ($data_2 == '0019') {
                    $data_2 = 'XIX sec.';
                };
                if ($data_1 == '0020') {
                    $data_1 = 'XX sec.';
                };
                if ($data_2 == '0020') {
                    $data_2 = 'XX sec.';
                };
                if ($data_1 == '0021') {
                    $data_1 = 'XXI sec.';
                };
                if ($data_2 == '0021') {
                    $data_2 = 'XXI sec.';
                };

                if ($data_2 == '') {
                    $datacompl = $data_1;
                } else {
                    $datacompl = $data_1 . " - " . $data_2;
                };
            } else {
                fwrite($report_codbib_not_found, "BIBLIO D1 mancante:" . $codbib . " " . $Detail['nloc_cfti'] . $crlf);
            }

            unset($schedea_row['datauni']);
            unset($schedea_row['dataun2']);
            $datacompl_ass['datacompl'] = $datacompl;

            $biblio = ["type" => "Feature", "properties" => array_merge($schedea_row, $datacompl_ass)];

            array_push($biblios, $biblio);
        }


        $dir = '../files/ProcedureGeoJSON/localitySources/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        array_push($features, $locality_detail);
        array_push($features, $quakes);
        array_push($features, $biblios);
        $geojson = ["type" => "FeatureCollection", "features" => $features];
        $fp = fopen($dir."{$NLOC}.geojson", 'w');
        fwrite($fp, json_encode($geojson));
        fclose($fp);
        unset($geojson);
        unset($features);
    }
}

function createQuakeDetail_geojson($limit) {

    global $vmsql;

    //ITA

    $QuakeDetailQuery = pg_query($vmsql->link_db, "SELECT cat, nterr, nperiod, datanum, data_label,"
            . " anno, mese, giorno, time_label, ora, minu, sec, lat, lon, rel, level, io, imax, npun, "
            . "ee_nt, ee_np, mm, earthquakelocation, epicenter_type, country, flagcomments, flagfalseeq, "
            . "new2018 "
            . "FROM nterrs order by nterr " . $limit);

    /*
      $QuakeDetailQuery = pg_query($vmsql->link_db, "SELECT cat, nterr, nperiod, datanum, data_label,"
      . " anno, mese, giorno, time_label, ora, minu, sec, lat, lon, rel, level, io, imax, npun, "
      . "ee_nt, ee_np, mm, earthquakelocation, epicenter_type, country, flagcomments, flagfalseeq, "
      . "new2018 "
      . "FROM nterrs order by nterr DESC limit 10");
     */
    LoopQuake_geojson($QuakeDetailQuery);
}

function createQuakeDetail_med_geojson($limit) {

    global $vmsql;

    //MED
    $QuakeDetailQuery2 = pg_query($vmsql->link_db, "SELECT cat, nterr, nperiod, datanum, data_label, anno, mese,"
            . " giorno, time_label, ora, minu, sec, lat, lon, rel, level, io, imax, npun, ee_nt, ee_np, mm, "
            . "earthquakelocation, epicenter_type, country, flagcomments, flagfalseeq "
            . "FROM nterrs_med order by nterr " . $limit);

    LoopQuake_geojson($QuakeDetailQuery2);
}

function LoopQuake_geojson($QuakeDetailQuery) {

    global $vmsql;

    $quake_detail = array();

    while ($quake_row = pg_fetch_array($QuakeDetailQuery, null, PGSQL_ASSOC)) {

        $features = array();

        //quake detail
        $flagcomm = $quake_row['flagcomments'];
        $geometry = ["type" => "Point", "coordinates" => [(float) $quake_row['lon'], (float) $quake_row['lat']]];
        unset($quake_row['lat']);
        unset($quake_row['lon']);
        $quake_detail = ["type" => "Feature", "properties" => $quake_row, "geometry" => $geometry];

        //Comments

        if ($flagcomm == 4 or $flagcomm == 5) {

            $CommentsQuery = pg_query($vmsql->link_db, "select codtab, testocomm "
                    . "from commgen where nperiod_cfti = '{$quake_row['nperiod']}' order by codtab");

            $commenti = array();
            while ($commgen_row = pg_fetch_array($CommentsQuery, null, PGSQL_ASSOC)) {

                if (($commgen_row['codtab'] == 'D0' && $flagcomm == 5) || ($flagcomm == 4) || ($commgen_row['codtab'] == 'E0')) {

                    $commento = ["type" => "Feature", "properties" => $commgen_row];
                    array_push($commenti, $commento);
                }
            }
        }


        //Bibliography
        $BibliographyQueryB = pg_query($vmsql->link_db, "SELECT codbib, valb, desvalingl "
                . "FROM schedeb WHERE nperiod = '{$quake_row['nperiod']}' ");
        $biblios = array();
        while ($biblio_row = pg_fetch_assoc($BibliographyQueryB)) {
            //echo $Row['codbib'];
            $BibliographyQuery = pg_query($vmsql->link_db, "SELECT biblio_details.codbib, biblio_details.autore1, "
                    . "biblio_details.titolo1, biblio_details.luogoed, biblio_details.datauni, biblio_details.dataun2, "
                    . "biblio_details.desvalingl, biblio_details.valb "
                    . "FROM biblio_details "
                    . "WHERE biblio_details.codbib = '{$biblio_row['codbib']}' and biblio_details.nperiod = '{$quake_row['nperiod']}'");

            $schedea_row = pg_fetch_assoc($BibliographyQuery);
            $data_1 = $schedea_row['datauni'];
            $data_2 = $schedea_row['dataun2'];
            if ($data_1 == '0009') {
                $data_1 = 'IX sec.';
            };
            if ($data_2 == '0009') {
                $data_2 = 'IX sec.';
            };
            if ($data_1 == '0010') {
                $data_1 = 'X sec.';
            };
            if ($data_2 == '0010') {
                $data_2 = 'X sec.';
            };
            if ($data_1 == '0011') {
                $data_1 = 'XI sec.';
            };
            if ($data_2 == '0011') {
                $data_2 = 'XI sec.';
            };
            if ($data_1 == '0012') {
                $data_1 = 'XII sec.';
            };
            if ($data_2 == '0012') {
                $data_2 = 'XII sec.';
            };
            if ($data_1 == '0013') {
                $data_1 = 'XIII sec.';
            };
            if ($data_2 == '0013') {
                $data_2 = 'XIII sec.';
            };
            if ($data_1 == '0014') {
                $data_1 = 'XIV sec.';
            };
            if ($data_2 == '0014') {
                $data_2 = 'XIV sec.';
            };
            if ($data_1 == '0015') {
                $data_1 = 'XV sec.';
            };
            if ($data_2 == '0015') {
                $data_2 = 'XV sec.';
            };
            if ($data_1 == '0016') {
                $data_1 = 'XVI sec.';
            };
            if ($data_2 == '0016') {
                $data_2 = 'XVI sec.';
            };
            if ($data_1 == '0017') {
                $data_1 = 'XVII sec.';
            };
            if ($data_2 == '0017') {
                $data_2 = 'XVII sec.';
            };
            if ($data_1 == '0018') {
                $data_1 = 'XVIII sec.';
            };
            if ($data_2 == '0018') {
                $data_2 = 'XVIII sec.';
            };
            if ($data_1 == '0019') {
                $data_1 = 'XIX sec.';
            };
            if ($data_2 == '0019') {
                $data_2 = 'XIX sec.';
            };
            if ($data_1 == '0020') {
                $data_1 = 'XX sec.';
            };
            if ($data_2 == '0020') {
                $data_2 = 'XX sec.';
            };
            if ($data_1 == '0021') {
                $data_1 = 'XXI sec.';
            };
            if ($data_2 == '0021') {
                $data_2 = 'XXI sec.';
            };

            if ($data_2 == '') {
                $datacompl = $data_1;
            } else {
                $datacompl = $data_1 . " - " . $data_2;
            };

            unset($schedea_row['datauni']);
            unset($schedea_row['dataun2']);
            $datacompl_ass['datacompl'] = $datacompl;

            $biblio = ["type" => "Feature", "properties" => array_merge($schedea_row, $datacompl_ass)];

            array_push($biblios, $biblio);
        }


        $dir = '../files/ProcedureGeoJSON/quakeSources/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        array_push($features, $quake_detail);
        array_push($features, $commenti);
        array_push($features, $biblios);
        unset($commenti);
        unset($biblios);
        $geojson = ["type" => "FeatureCollection", "features" => $features];
        $fp = fopen($dir."{$quake_row['nterr']}.geojson", 'w');
        fwrite($fp, json_encode($geojson));
        fclose($fp);
        unset($geojson);
        unset($features);
    }
}

function createEEList_geojson() {
    global $vmsql;

    //eelist
    $EEDetailQuery = pg_query($vmsql->link_db, "SELECT NTERR, NPERIOD, NLOC_CFTI, DESLOC_CFTI, CODICE_EFF, LAT_WGS84, 
            LON_WGS84, NOTESITO, PROVLET, NAZIONE, NLOC, DESLOC, COMMENTO FROM EE order by nperiod");
    $feature = array();
    $features = array();
    while ($row = pg_fetch_array($EEDetailQuery, null, PGSQL_ASSOC)) {
        $geometry = ["type" => "Point", "coordinates" => [(float) $row['lon_wgs84'], (float) $row['lat_wgs84']]];
        unset($row['lat_wgs84']);
        unset($row['lon_wgs84']);
        $feature = ["type" => "Feature", "properties" => $row, "geometry" => $geometry];
        array_push($features, $feature);
    }
    $geojson = ["type" => "FeatureCollection", "features" => $features];
   

    //eelist_med 
    $EEDetailQueryM = pg_query($vmsql->link_db, "SELECT NTERR, NPERIOD, NLOC_CFTI, DESLOC_CFTI, CODICE_EFF, LAT_WGS84, 
            LON_WGS84, NOTESITO, PROVLET, NAZIONE, NLOC, DESLOC FROM EE_MED order by nperiod");
    $features_med = array();
    while ($row = pg_fetch_array($EEDetailQueryM, null, PGSQL_ASSOC)) {
        $geometry = ["type" => "Point", "coordinates" => [(float) $row['lon_wgs84'], (float) $row['lat_wgs84']]];
        unset($row['lat_wgs84']);
        unset($row['lon_wgs84']);
        $feature = ["type" => "Feature", "properties" => $row, "geometry" => $geometry];
        array_push($features_med, $feature);
    }
    $geojson2 = ["type" => "FeatureCollection", "features" => $features_med];
    
    $dir = '../files/ProcedureGeoJSON/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    
    $fp1 = fopen($dir."EEList_MED.geojson", 'w');
    fwrite($fp1, json_encode($geojson2));
    fclose($fp1);

    //biblio_ee 
    $BibliographyQueryB = pg_query($vmsql->link_db, "SELECT codbib FROM biblio_ee");
    $features_biblio = array();
    while ($row_biblio = pg_fetch_array($BibliographyQueryB, null, PGSQL_ASSOC)) {

        $BibliographyQuery = pg_query($vmsql->link_db, "SELECT codbib, autore1,titolo1, "
                . "luogoed,datauni,dataun2 FROM schedea WHERE codbib = '{$row_biblio['codbib']}' ");
        $Row_codbib = pg_fetch_assoc($BibliographyQuery);
        $data_1 = $Row_codbib['datauni'];
        $data_2 = $Row_codbib['dataun2'];
        if ($data_1 == '0009') {
            $data_1 = 'IX sec.';
        };
        if ($data_2 == '0009') {
            $data_2 = 'IX sec.';
        };
        if ($data_1 == '0010') {
            $data_1 = 'X sec.';
        };
        if ($data_2 == '0010') {
            $data_2 = 'X sec.';
        };
        if ($data_1 == '0011') {
            $data_1 = 'XI sec.';
        };
        if ($data_2 == '0011') {
            $data_2 = 'XI sec.';
        };
        if ($data_1 == '0012') {
            $data_1 = 'XII sec.';
        };
        if ($data_2 == '0012') {
            $data_2 = 'XII sec.';
        };
        if ($data_1 == '0013') {
            $data_1 = 'XIII sec.';
        };
        if ($data_2 == '0013') {
            $data_2 = 'XIII sec.';
        };
        if ($data_1 == '0014') {
            $data_1 = 'XIV sec.';
        };
        if ($data_2 == '0014') {
            $data_2 = 'XIV sec.';
        };
        if ($data_1 == '0015') {
            $data_1 = 'XV sec.';
        };
        if ($data_2 == '0015') {
            $data_2 = 'XV sec.';
        };
        if ($data_1 == '0016') {
            $data_1 = 'XVI sec.';
        };
        if ($data_2 == '0016') {
            $data_2 = 'XVI sec.';
        };
        if ($data_1 == '0017') {
            $data_1 = 'XVII sec.';
        };
        if ($data_2 == '0017') {
            $data_2 = 'XVII sec.';
        };
        if ($data_1 == '0018') {
            $data_1 = 'XVIII sec.';
        };
        if ($data_2 == '0018') {
            $data_2 = 'XVIII sec.';
        };
        if ($data_1 == '0019') {
            $data_1 = 'XIX sec.';
        };
        if ($data_2 == '0019') {
            $data_2 = 'XIX sec.';
        };
        if ($data_1 == '0020') {
            $data_1 = 'XX sec.';
        };
        if ($data_2 == '0020') {
            $data_2 = 'XX sec.';
        };
        if ($data_1 == '0021') {
            $data_1 = 'XXI sec.';
        };
        if ($data_2 == '0021') {
            $data_2 = 'XXI sec.';
        };

        if ($data_2 == '') {
            $datacompl = $data_1;
        } else {
            $datacompl = $data_1 . " - " . $data_2;
        };

        unset($Row_codbib['datauni']);
        unset($Row_codbib['dataun2']);
        $datacompl_ass['datacompl'] = $datacompl;

        $feature = ["type" => "Feature", "properties" => array_merge($Row_codbib, $datacompl_ass)];
        array_push($features_biblio, $feature);
    }
    $geojson3 = ["type" => "FeatureCollection", "features" => $features_biblio];
    
    $dir = '../files/ProcedureGeoJSON/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    
    $fp2 = fopen($dir."BiblioEE.geojson", 'w');
    fwrite($fp2, json_encode($geojson3));
    fclose($fp2);
}

function createSequenze_geojson() {
    
    global $vmsql, $nomeSchemaOnline;
    $dir1 = '../files/Sequenze_Geojson/';
    if (!file_exists($dir1)) {
        mkdir($dir1, 0777, true); 
    }

    $dir2 = $dir1 . 'sequenzeSources/';
    if (!file_exists($dir2)) {
        mkdir($dir2, 0777, true);
    }

    $query = "SELECT intervallo_anni, area_epicentrale, codice_sequenza  FROM $nomeSchemaOnline.vnperiod_cfti order by intervallo_anni ;";
    $NPDetailQuery = pg_query($vmsql->link_db, $query);
    

    if ($NPDetailQuery !== false) {
        $arrayFeatures = loopNP_Geojson($NPDetailQuery, $dir2);
    } else {
        echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
    }

    //CREO ARRAY (poi con json_encode ottengo un oggetto)
    $geojson = ["type" => "FeatureCollection", "features" => $arrayFeatures];
    $fp = fopen($dir1."SequenceList.json", 'w');
    fwrite($fp, json_encode($geojson));
    fclose($fp);

    
}


function loopNP_Geojson($NPDetailQuery, $dir2) {
    global $vmsql;

    $arrayFeatures = array();
    
    while ($Detail = pg_fetch_array($NPDetailQuery)) {
        
        
       
        $arrayProperties = $Detail;
        
            
            
            $arrayProperties['nperiod']=$arrayProperties['codice_sequenza'];
            $arrayProperties['title']= "{$arrayProperties['intervallo_anni']} - {$arrayProperties['area_epicentrale']}";
          
        foreach ($arrayProperties AS $key=>$value){
            if($key !== 'nperiod' && $key !== 'title'){
                unset($arrayProperties[$key]);
            }
        }
        
        

        $arrayFeature = array(); 
        array_push($arrayFeature, ["type"=>"Feature", "properties"=>$arrayProperties]);
        
        array_push($arrayFeatures, $arrayFeature);

        
        
        //GENERO I FILE SEQUENZE SOURCES
        $arrayFeatures2 = array();

        $query = "select * from seq_scosse where nperiod = '{$Detail['codice_sequenza']}' order by anno1, mese1, giorno1, oragmt1;";
        $SeqQuery = pg_query($vmsql->link_db, $query);

        if ($SeqQuery === false) {
            echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
            exit();
        }

        $arrayScosse = array();
        while ($Row = pg_fetch_array($SeqQuery)) {
            $arrayProperties = $Row;
         
            
            $NT = strlen($Row['nterr']);
            if ($NT == 5) {	

                $query = "select * from NTERRS where nterr = '{$Row['nterr']}' ";
                $NTquery = pg_query($vmsql->link_db, $query);
    
                if ($NTquery === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
                }

                $Row2 = pg_fetch_array($NTquery);
                
                $arrayProperties2 = $Row2;
                

                if ($Row2['nperiod'] == '') {   //se non seleziono nulla (quell'NTERR non è in cfti5, ma solo in CFTI6)
                } else {
                    
                    $arrayProperties2['ID']='-';
                    $arrayProperties2['codiceScossa']='-';
                    $arrayProperties2['anno1']=$arrayProperties2['anno'];
                    $arrayProperties2['mese1']=$arrayProperties2['mese'];
                    $arrayProperties2['giorno1']=$arrayProperties2['giorno'];
                    $arrayProperties2['anno2']=$arrayProperties2['anno'];
                    $arrayProperties2['mese2']='-';
                    $arrayProperties2['giorno2']='-';
                    $arrayProperties2['oraF']= '-';
                    $arrayProperties2['oraGMT1']=$arrayProperties2['time_label'];
                    $arrayProperties2['oraGMT2']='-';
                    $arrayProperties2['timeLABELgmt']=$arrayProperties2['time_label'];
                    $arrayProperties2['timeLABELfonte']='-';
                    $arrayProperties2['Io']=$arrayProperties2['io'];
                    $arrayProperties2['commentoScossa']='-';
                    
                    $arrayChiaviDaInserire = ['ID','nperiod','codiceScossa' ,'nterr', 'anno1' ,'mese1','giorno1','anno2','mese2','giorno2','oraF','oraGMT1','oraGMT2','timeLABELgmt','timeLABELfonte','Io', 'imax', 'commentoScossa'];
                    
                    foreach ($arrayProperties2 AS $key => $value) {
                        if (!array_search($key, $arrayChiaviDaInserire)) {
                            unset($arrayProperties2[$key]);
                        }
                    }
                    
                    $arrayScossa = ["type"=>"Feature", "properties"=>$arrayProperties2];

                }
            } else {
                
                foreach ($Row as $key => $value) {
                    if ($value == "") {
                        $value = "-";
                    };
                    if ($value == -9) {
                        $value = "-";
                    };
                    
                    if(is_int($key)){
                        unset($arrayProperties[$key]);
                    }
                    

                    
                }
                
                $arrayScossa = ["type"=>"Feature", "properties"=>$arrayProperties];

                
                
            }
            array_push($arrayScosse, $arrayScossa);
        }

        //generazione località
        $arrayLocalita = array();
        $query = "select * from seq_loc where nperiod = '{$Detail['codice_sequenza']}' order by codicescossa";
        $SeqQuery = pg_query($vmsql->link_db, $query);
        if ($SeqQuery === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
        }
        
        $n = 1;
        while ($Row = pg_fetch_array($SeqQuery)) {
            
            
            $arrayProperties = array();
            $arrayProperties = $Row;
            
//            echo '<br>';
//            echo '<br>';
//            var_dump($arrayProperties);
//            echo '<br>';
//            echo '<br>';
            
            $arrayProperties['nterr'] = '-';
            
            
 
           
            if ($Row['imcs'] == '-' || $Row['imcs'] == '') {

                $arrayProperties['intMCS'] = $arrayProperties['imcs_nuovo'];
            
            } else {

                $arrayProperties['intMCS'] = $arrayProperties['imcs'];
                
            }
            $arrayProperties['ID'] = $n;


            $n = $n + 1;
            
            $geometry = ["type" => "Point", "coordinates" => [(float) $arrayProperties['lonloc'], (float) $arrayProperties['latloc']]];
            
            $arrayChiaviDaInserire = ['ID', 'nperiod', 'codiceScossa', 'nterr',  'nscosse', 'durcalc', 'durfonti', 'intfonte', 'nomeloc', 'nloc_cfti', 'commentoloc'];

            foreach ($arrayProperties AS $key => $value) {
                if (!array_search($key, $arrayChiaviDaInserire)) {
                    unset($arrayProperties[$key]);
                }
            }


            $featureLocalita = ["type"=>"Feature", "properties"=>$arrayProperties, "geometry"=>$geometry];
            array_push($arrayLocalita, $featureLocalita);
            unset($featureLocalita);
        }
        
        
        
        


        //Bibliography
        
        $arrayBiblios = array();

        $query = "SELECT codbib, valb, desvalingl FROM schedeb WHERE nperiod = '{$Detail['codice_sequenza']}' ";
        $BibliographyQueryB = pg_query($vmsql->link_db, $query);
        if ($BibliographyQueryB === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
        }
      

        $n = 1;
        
        
        while ($Row = pg_fetch_array($BibliographyQueryB)) {
            
            

            // echo "$n \n";
            $query = "SELECT schedea.codbib, schedea.autore1, schedea.titolo1, schedea.luogoed, schedea.datauni, schedea.dataun2, schedeb.desvalingl, schedeb.valb FROM schedea INNER JOIN schedeb ON (schedea.codbib = schedeb.codbib) WHERE schedea.codbib = '{$Row['codbib']}' ";
            $BibliographyQuery = pg_query($vmsql->link_db, $query);
            if ($BibliographyQuery === false) {
                    echo "<h3 style=\"background-color:Tomato;\">select error: $query</h3>";
                    exit();
            }
            
           
            $Row2 = pg_fetch_array($BibliographyQuery);
            $propertiesBiblio = array();
            $propertiesBiblio = $Row2;
            
            $data_1 = $Row2['datauni'];
            $data_2 = $Row2['dataun2'];
            if ($data_1 == '0009') {
                $data_1 = 'IX sec.';
            };
            if ($data_2 == '0009') {
                $data_2 = 'IX sec.';
            };
            if ($data_1 == '0010') {
                $data_1 = 'X sec.';
            };
            if ($data_2 == '0010') {
                $data_2 = 'X sec.';
            };
            if ($data_1 == '0011') {
                $data_1 = 'XI sec.';
            };
            if ($data_2 == '0011') {
                $data_2 = 'XI sec.';
            };
            if ($data_1 == '0012') {
                $data_1 = 'XII sec.';
            };
            if ($data_2 == '0012') {
                $data_2 = 'XII sec.';
            };
            if ($data_1 == '0013') {
                $data_1 = 'XIII sec.';
            };
            if ($data_2 == '0013') {
                $data_2 = 'XIII sec.';
            };
            if ($data_1 == '0014') {
                $data_1 = 'XIV sec.';
            };
            if ($data_2 == '0014') {
                $data_2 = 'XIV sec.';
            };
            if ($data_1 == '0015') {
                $data_1 = 'XV sec.';
            };
            if ($data_2 == '0015') {
                $data_2 = 'XV sec.';
            };
            if ($data_1 == '0016') {
                $data_1 = 'XVI sec.';
            };
            if ($data_2 == '0016') {
                $data_2 = 'XVI sec.';
            };
            if ($data_1 == '0017') {
                $data_1 = 'XVII sec.';
            };
            if ($data_2 == '0017') {
                $data_2 = 'XVII sec.';
            };
            if ($data_1 == '0018') {
                $data_1 = 'XVIII sec.';
            };
            if ($data_2 == '0018') {
                $data_2 = 'XVIII sec.';
            };
            if ($data_1 == '0019') {
                $data_1 = 'XIX sec.';
            };
            if ($data_2 == '0019') {
                $data_2 = 'XIX sec.';
            };
            if ($data_1 == '0020') {
                $data_1 = 'XX sec.';
            };
            if ($data_2 == '0020') {
                $data_2 = 'XX sec.';
            };
            if ($data_1 == '0021') {
                $data_1 = 'XXI sec.';
            };
            if ($data_2 == '0021') {
                $data_2 = 'XXI sec.';
            };

            if ($data_2 == '') {
                $datacompl = $data_1;
            } else {
                $datacompl = $data_1 . " - " . $data_2;
            };

            $propertiesBiblio[datacompl] = $datacompl;
            unset($propertiesBiblio['datauni']);
            unset($propertiesBiblio['dataun2']);
            
            foreach ($propertiesBiblio AS $key => $value) {
                if (is_int($key)) {
                    unset($propertiesBiblio[$key]);
                }
            }
            
            $featureBiblio = ["type"=>"Feature", "properties"=>$propertiesBiblio];
            array_push($arrayBiblios, $featureBiblio);
            unset($featureLocalita);
       
            $n = $n + 1;
        }
//
        array_push($arrayFeatures2, $arrayScosse);
        array_push($arrayFeatures2, $arrayLocalita);
        array_push($arrayFeatures2, $arrayBiblios);
        $geojson = ["type" => "FeatureCollection", "features" => $arrayFeatures2];
        $fp = fopen($dir2."seq{$Detail['codice_sequenza']}.json", 'w');
        fwrite($fp, json_encode($geojson));
        fclose($fp);
    
        
    }
    
    
    return $arrayFeatures;
}