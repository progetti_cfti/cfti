<?php

require_once("../inc/conn.php");
require_once("../inc/layouts.php");
require_once("../inc/func.browser_detection.php");
proteggi(1);
 
echo "<body>";
echo "<script src=\"js/scheda.js\"></script>";
echo "<script src=\"js/jquery/jquery.query.js\"></script>";
echo "<script src=\"js/jquery/jquery.min.js\"></script>";
echo "<script src=\"admin/export_data.php\"></script>";
echo "</body>";


require_once("./procedure.php"); 


$files=array("js/scriptaculous/lib/prototype.js","sty/import.css","js/import.js","js/open_window.js");

echo openLayout1(_("Procedure-Sequenze"),$files,'popup');
if(isset($_GET['t'])){
           $tipo = $_GET['t'];
 }
echo breadcrumbs(array("HOME",_("Schermata Procedure")));
echo "<br>";

echo "<h1 class=\"titoloScheda\"> Procedura Sequenze ".$tipo." </h1>";

try {
      
    
    if($tipo=="xml"){
          createSequenze();
          echo "File creato su: ";
          echo "<a href=\"".FRONT_DOCROOT."/files\">Cartella File</a>";
    }else if($tipo == "geojson"){
        
          createSequenze_geojson();       
          echo "File creato su:  ";
          echo "<a href=\"".FRONT_DOCROOT."/files\">Cartella File</a>";
    }    


}catch (Exception $e) {
    echo 'Errore Sequenze'. $e->getMessage();
}

         echo closeLayout1();

?>