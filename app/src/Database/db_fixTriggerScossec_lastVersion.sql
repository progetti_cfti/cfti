--
-- PostgreSQL database dump
--

-- Dumped from database version 11.17 (Debian 11.17-0+deb10u1)
-- Dumped by pg_dump version 12.12 (Debian 12.12-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cfti5; Type: SCHEMA; Schema: -; Owner: utente
--

CREATE SCHEMA cfti5;


ALTER SCHEMA cfti5 OWNER TO utente;

--
-- Name: SCHEMA cfti5; Type: COMMENT; Schema: -; Owner: utente
--

COMMENT ON SCHEMA cfti5 IS 'standard public schema';


--
-- Name: cfti5web; Type: SCHEMA; Schema: -; Owner: utente
--

CREATE SCHEMA cfti5web;


ALTER SCHEMA cfti5web OWNER TO utente;

--
-- Name: frontend; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA frontend;


ALTER SCHEMA frontend OWNER TO postgres;

--
-- Name: SCHEMA frontend; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA frontend IS 'Schema dedicato alla gestione del frontend web VFront.';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA cfti5;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: coordinate_centesimali(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_centesimali() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  /*  NEW.lat_centesimale = NEW.latloc * 2;*/
  NEW.lat_centesimale = ( 
    CASE
        WHEN (LENGTH( NEW.latloc::text ) = 8) THEN   round( (    ( ( ( ( ( CONCAT(SUBSTRING(NEW.latloc::text,5,2),'.',SUBSTRING(NEW.latloc::text,7,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.latloc::text,3,2))::numeric   )/60  ) + ( (SUBSTRING(NEW.latloc::text,1,2))::numeric )   ) , 4 )  
        WHEN (LENGTH( NEW.latloc::text ) = 7) THEN 0
    END 
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.coordinate_centesimali() OWNER TO utente;

--
-- Name: coordinate_centesimali_lat_scossec(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_centesimali_lat_scossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi numeric;
v1 numeric;
totale_centesimale numeric;
BEGIN
 totale_centesimale = 5;
 IF ( LENGTH(NEW.latmacs::text)=4 ) 
 then   v1 =  ( ( SUBSTRING(NEW.latmacs::text,3,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.latmacs::text,1,2)    )::numeric;
		totale_centesimale = round((gradi + v1),2);
       
 END IF;

NEW.lat_centesimale = ( totale_centesimale::text);
RETURN NEW;

END
$$;


ALTER FUNCTION cfti5.coordinate_centesimali_lat_scossec() OWNER TO utente;

--
-- Name: coordinate_centesimali_lon(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_centesimali_lon() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  /*  NEW.lat_centesimale = NEW.latloc * 2;*/
  NEW.lon_centesimale = ( 
    CASE
          WHEN  (  ((SUBSTRING(NEW.longlog::text,1,1))!= '-') and  (LENGTH( NEW.longlog::text ) = 8) ) THEN  round(  (     ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,5,2),'.',SUBSTRING(NEW.longlog::text,7,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,3,2))::numeric   )/60  ) + ( (SUBSTRING(NEW.longlog::text,1,2))::numeric) ) ,  4 )
          WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))!= '-') and (LENGTH( NEW.longlog::text ) = 7) ) THEN   round(  (     ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,4,2),'.',SUBSTRING(NEW.longlog::text,6,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,2,2))::numeric   )/60  ) + ( (SUBSTRING(NEW.longlog::text,1,1))::numeric)  ) ,  4 )
		  WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))='-') and  LENGTH( NEW.longlog::text ) = 6 ) THEN    round(   ( CONCAT( '-',  ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,3,2),'.',SUBSTRING(NEW.longlog::text,5,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,2,1))::numeric   )/60  )::text)::numeric ),  4)
		  WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))='-') and  LENGTH( NEW.longlog::text ) = 7 ) THEN     round(   ( CONCAT( '-',  ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,4,2),'.',SUBSTRING(NEW.longlog::text,6,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,2,2))::numeric   )/60  )::text)::numeric  ),  4)
          WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))='-') and  LENGTH( NEW.longlog::text ) = 8 ) THEN   round(   (  CONCAT( '-',  ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,5,2),'.',SUBSTRING(NEW.longlog::text,7,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,3,2))::numeric   )/60   + ( (SUBSTRING(NEW.longlog::text,2,1))::numeric)    )::text)::numeric   ),  4)  
    END 
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.coordinate_centesimali_lon() OWNER TO utente;

--
-- Name: coordinate_centesimali_lon_scossec(); Type: FUNCTION; Schema: cfti5; Owner: postgres
--

CREATE FUNCTION cfti5.coordinate_centesimali_lon_scossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi numeric;
v1 numeric;
totale_centesimale numeric;
BEGIN

 IF ( LENGTH(NEW.lonmacs::text)=4 ) 
 then   v1 =  ( ( SUBSTRING(NEW.lonmacs::text,3,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.lonmacs::text,1,2)    )::numeric;
		totale_centesimale = round((gradi + v1),2);   
 END IF;
 
 IF ( LENGTH(NEW.lonmacs::text)=3 ) 
 then   v1 =  ( ( SUBSTRING(NEW.lonmacs::text,2,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.lonmacs::text,1,1)    )::numeric;
		totale_centesimale = round((gradi + v1),2);
 END IF;

 NEW.lon_centesimale = ( 
    
	            totale_centesimale::text
    
        );
        RETURN NEW;

END
$$;


ALTER FUNCTION cfti5.coordinate_centesimali_lon_scossec() OWNER TO postgres;

--
-- Name: coordinate_centesimali_scossec(); Type: FUNCTION; Schema: cfti5; Owner: postgres
--

CREATE FUNCTION cfti5.coordinate_centesimali_scossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi numeric;
v1 numeric;
totale_centesimale numeric;
BEGIN
 IF ( LENGTH(NEW.latmacs::text)=4 ) then   
 		v1 =  ( ( SUBSTRING(NEW.latmacs::text,3,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.latmacs::text,1,2)    )::numeric;
		totale_centesimale = round((gradi + v1),2);     	
 END IF;
 
 NEW.lat_centesimale = ( 
    
	            totale_centesimale::text
    
        );
        RETURN NEW;

END
$$;


ALTER FUNCTION cfti5.coordinate_centesimali_scossec() OWNER TO postgres;

--
-- Name: coordinate_sessagesimali_lat(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_sessagesimali_lat() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lat_centesimale::text, '.', 1);
 decimali = split_part(NEW.lat_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 v3 = split_part(v2, '.', 1);
 IF ( LENGTH(v3)=1 ) 
 then  v3 = CONCAT('0',v3);
 END IF;
 v4 = split_part(v2, '.', 2);
 v4 = concat('0.',v4);
 v5 = round((v4::numeric * 60),2)::text;
 v6 =  split_part(v5, '.', 1);
 IF ( LENGTH(v6)=1 ) 
 then  v6 = CONCAT('0',v6);
 END IF;
 v7 =  split_part(v5, '.', 2);
  NEW.latloc = ( 
    
	             concat(gradi,v3,v6,v7)
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.coordinate_sessagesimali_lat() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lat_fscossec(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_sessagesimali_lat_fscossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lat_centesimale::text, '.', 1);
 decimali = split_part(NEW.lat_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 
 IF( v2::numeric = 0)
  then v3 = '00';	
 elseif (LENGTH(v2::text)=1) then
   v3 = concat('0',v2);
 elseif (LENGTH(split_part(v2, '.', 1)) = 2 )
 then  v3 = split_part(v2, '.', 1);
  elseif (LENGTH(split_part(v2, '.', 1)) = 1 )
 then  v3 = concat('0',split_part(v2, '.', 1));
	ELSE v3 = v2;
 END IF;
 

  NEW.latmacs = ( 
    
	             concat(gradi,v3)::numeric
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.coordinate_sessagesimali_lat_fscossec() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lon(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_sessagesimali_lon() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lon_centesimale::text, '.', 1);
 decimali = split_part(NEW.lon_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 v3 = split_part(v2, '.', 1);
 IF ( LENGTH(v3)=1 ) 
 then  v3 = CONCAT('0',v3);
 END IF;
 v4 = split_part(v2, '.', 2);
 v4 = concat('0.',v4);
 v5 = round((v4::numeric * 60),2)::text;
 v6 =  split_part(v5, '.', 1);
 IF ( LENGTH(v6)=1 ) 
 then  v6 = CONCAT('0',v6);
 END IF;
 v7 =  split_part(v5, '.', 2);
  NEW.longlog = ( 
    
	             concat(gradi,v3,v6,v7)
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.coordinate_sessagesimali_lon() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lon_fscossec(); Type: FUNCTION; Schema: cfti5; Owner: utente
--

CREATE FUNCTION cfti5.coordinate_sessagesimali_lon_fscossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lon_centesimale::text, '.', 1);
 decimali = split_part(NEW.lon_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 
 IF( v2::numeric = 0)
  then v3 = '00';	
 elseif (LENGTH(v2::text)=1) then
   v3 = concat('0',v2);
 elseif (LENGTH(split_part(v2, '.', 1)) = 2 )
 then  v3 = split_part(v2, '.', 1);
  elseif (LENGTH(split_part(v2, '.', 1)) = 1 )
 then  v3 = concat('0',split_part(v2, '.', 1));
	ELSE v3 = v2;
 END IF;
 

  NEW.lonmacs = ( 
    
	             concat(gradi,v3)::numeric
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.coordinate_sessagesimali_lon_fscossec() OWNER TO utente;

--
-- Name: fnloc_geometry(); Type: FUNCTION; Schema: cfti5; Owner: postgres
--

CREATE FUNCTION cfti5.fnloc_geometry() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
  NEW.geom = ST_SetSRID(ST_MakePoint(NEW.lon_centesimale , NEW.lat_centesimale), 4326);
  RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.fnloc_geometry() OWNER TO postgres;

--
-- Name: fscossec_geometry(); Type: FUNCTION; Schema: cfti5; Owner: postgres
--

CREATE FUNCTION cfti5.fscossec_geometry() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
  NEW.geom = ST_SetSRID(ST_MakePoint(NEW.lon_centesimale , NEW.lat_centesimale), 4326);
  RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.fscossec_geometry() OWNER TO postgres;

--
-- Name: genera_ordinam(); Type: FUNCTION; Schema: cfti5; Owner: postgres
--

CREATE FUNCTION cfti5.genera_ordinam() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
  NEW.ordinam = ( 
    CASE
        WHEN ( NEW.autore1 ) = '*' THEN NEW.titolo1   
        WHEN ( NEW.autore1 ) <> '*' THEN NEW.autore1  
    END 
   );
  RETURN NEW;
END
$$;


ALTER FUNCTION cfti5.genera_ordinam() OWNER TO postgres;

SET default_tablespace = '';

--
-- Name: fclassif; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fclassif (
    id integer NOT NULL,
    codclass character varying(4) NOT NULL,
    desclass text
);


ALTER TABLE cfti5.fclassif OWNER TO utente;

--
-- Name: fclassif_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fclassif ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fclassif_id_seq1
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fcodric; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fcodric (
    id integer NOT NULL,
    codrice character varying(4),
    desrice text
);


ALTER TABLE cfti5.fcodric OWNER TO utente;

--
-- Name: fcod_ric_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fcodric ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fcod_ric_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fcommenti; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fcommenti (
    id integer NOT NULL,
    codcomm character varying(20),
    testocomm text,
    nperiodcomm character varying(10),
    codfencomm character varying(3),
    cric_cm character varying(4),
    fnloc integer,
    ftabcatego integer,
    fnperiod integer,
    feffetti integer
);


ALTER TABLE cfti5.fcommenti OWNER TO utente;

--
-- Name: fcommenti_id_seq; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fcommenti ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fcommenti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fdemo; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fdemo (
    id integer NOT NULL,
    nc character varying(3),
    attf character varying(1),
    daldem integer,
    aldem integer,
    attd character varying(1),
    demog numeric,
    id2 character varying(1),
    attid character varying(1),
    trend integer,
    notedem character varying(150),
    demcompl character varying(100),
    demnloc character varying(9),
    demfonti character varying(2),
    demtm character varying(4),
    demcb character varying(6),
    dempic character varying(2),
    ffontidemo integer,
    fnloc integer,
    fscheda integer
);


ALTER TABLE cfti5.fdemo OWNER TO utente;

--
-- Name: fdemo_id_seq; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fdemo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fdemo_id_seq
    START WITH 3085
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fedifici; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fedifici (
    id integer NOT NULL,
    classed character varying(4),
    descrizedif text,
    codicedif character varying(13),
    noted text,
    cric_ed character varying(4),
    fnloc integer
);


ALTER TABLE cfti5.fedifici OWNER TO utente;

--
-- Name: fedifici_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fedifici ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fedifici_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: feffetti; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.feffetti (
    id integer NOT NULL,
    codeff character varying(4),
    deseff text,
    cric_ef character varying(4),
    noteeff text,
    codeff_new character varying(2),
    deseff_new text,
    desing_new text
);


ALTER TABLE cfti5.feffetti OWNER TO utente;

--
-- Name: feffetti_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.feffetti ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.feffetti_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ffontidemo; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.ffontidemo (
    id integer NOT NULL,
    codfdem character varying(4),
    desfdem character varying(250),
    notefdem character varying(250)
);


ALTER TABLE cfti5.ffontidemo OWNER TO utente;

--
-- Name: ffontidemo_id_seq; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.ffontidemo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.ffontidemo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ficon; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.ficon (
    id integer NOT NULL,
    codico character varying(17),
    didasico text,
    codtotico character varying(100),
    fotoinc character varying(100),
    noteico text,
    data_ic date,
    uti_ic character varying(4),
    psw_ic character varying(4),
    cric_ic character varying(4),
    fedifici integer,
    feffetti integer,
    fnloc integer,
    fnperiod integer,
    fscheda integer,
    fschedac integer,
    fschedeb integer,
    ftipfen integer
);


ALTER TABLE cfti5.ficon OWNER TO utente;

--
-- Name: ficon_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.ficon ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.ficon_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fnfiu; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fnfiu (
    id integer NOT NULL,
    nfiu character varying(5),
    nomfium character varying(22),
    tavfiu character varying(12),
    areafiu character varying(2),
    notefiu character varying(20)
);


ALTER TABLE cfti5.fnfiu OWNER TO utente;

--
-- Name: fnfiu_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fnfiu ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fnfiu_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fnloc; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fnloc (
    id integer NOT NULL,
    nloc character varying(250),
    desloc character varying(250),
    latloc integer,
    longlog integer,
    quotloc character varying(250),
    numabit character varying(250),
    prov character varying(250),
    provlet character varying(250),
    regione character varying(250),
    comune character varying(250),
    frazion character varying(250),
    foglio character varying(250),
    tavolet character varying(250),
    risentimenti integer,
    siglastato character varying(250),
    desloc2 character varying(250),
    notesito text,
    cric_nl character varying(250),
    notenl text,
    lat_centesimale numeric,
    lon_centesimale numeric,
    geom cfti5.geometry(Point,4326)
);


ALTER TABLE cfti5.fnloc OWNER TO utente;

--
-- Name: fnloc2; Type: TABLE; Schema: cfti5; Owner: postgres
--

CREATE TABLE cfti5.fnloc2 (
    id integer NOT NULL,
    nloc character varying(250),
    desloc character varying(250),
    latloc integer,
    longlog integer,
    quotloc character varying(250),
    numabit character varying(250),
    prov character varying(250),
    provlet character varying(250),
    regione character varying(250),
    comune character varying(250),
    frazion character varying(250),
    foglio character varying(250),
    tavolet character varying(250),
    latlocart integer,
    longlocart integer,
    risentimenti integer,
    flagrisent integer,
    siglastato character varying(250),
    desloc2 character varying(250),
    notesito text,
    data_nl character varying(25),
    uti_nl character varying(250),
    psw_nl character varying(250),
    cric_nl character varying(250),
    nsito character varying(250),
    dt_dal character varying(250),
    dt_al character varying(250),
    notenl text,
    titolo_label1 character varying(50),
    lat_centesimale numeric,
    lon_centesimale numeric,
    geometry cfti5.geometry(Point,4326)
);


ALTER TABLE cfti5.fnloc2 OWNER TO postgres;

--
-- Name: fnloc_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fnloc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fnloc_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fnperiod; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fnperiod (
    id integer NOT NULL,
    nperiod character varying(6),
    datainiznp integer,
    datafinenp integer,
    codtotnperiod character varying(16),
    commentonperiod text,
    numcomm character varying(6),
    numbiblio character varying(12),
    numtb integer,
    livnp character varying(1),
    cric_np character varying(4),
    feffetti integer,
    ftipfen integer,
    areaepicentrale character varying,
    in_cfti boolean
);


ALTER TABLE cfti5.fnperiod OWNER TO utente;

--
-- Name: fnperiod_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fnperiod ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fnperiod_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fpiaquo; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fpiaquo (
    id integer NOT NULL,
    intpq character varying(10),
    deslocapq character varying(40),
    intpqnum numeric,
    codtotpq character varying(18),
    cric_pq character varying(4),
    tipo_pq character varying(1),
    dist_pqep integer,
    diff_ipqep numeric,
    flag_ee character varying(2),
    azimut_pqep character varying(7),
    n_morti character varying(7),
    n_feriti character varying(7),
    flag_mor character varying(2),
    flag_fer character varying(2),
    fnperiod integer,
    fscossec integer,
    fnloc integer,
    note_pq text
);


ALTER TABLE cfti5.fpiaquo OWNER TO utente;

--
-- Name: fpiaquo_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fpiaquo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fpiaquo_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fscheda; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fscheda (
    id integer NOT NULL,
    codbib character varying(6),
    codric character varying(4),
    codmat character varying(8),
    autore1 text,
    ruolo character varying(40),
    rigeaut character varying(25),
    dalaut character varying(4),
    alaut character varying(4),
    titolo1 text,
    rigetit character varying(50),
    editaut integer,
    telrepe character varying(25),
    luogoed character varying(200),
    datauni character varying(4),
    testall1 integer,
    inizdis character varying(4),
    finedis character varying(4),
    complsn integer,
    statola character varying(2),
    classif character varying(4),
    nloca character varying(5),
    codmicr integer,
    fotiniz integer,
    codmicrenel integer,
    prota character varying(6),
    dataggscha character varying(25),
    oraggscha time without time zone,
    notea text,
    colloca character varying(65),
    tipodat integer,
    daltit character varying(4),
    altit character varying(4),
    dataun2 character varying(4),
    ordinam character varying(75),
    tipoa character varying(10),
    foto integer,
    grafici integer,
    tabelle integer,
    disegni integer,
    fscheda1 integer,
    fclassif integer
);


ALTER TABLE cfti5.fscheda OWNER TO utente;

--
-- Name: fscheda1; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fscheda1 (
    id integer NOT NULL,
    coda1 character varying(7),
    desa1 text,
    inda1 text,
    capa1 character varying(5),
    loca1 character varying(30),
    prova1 character varying(2),
    tela1 character varying(23),
    notea1 text,
    cric_a1 character varying(4),
    fnloc integer
);


ALTER TABLE cfti5.fscheda1 OWNER TO utente;

--
-- Name: fscheda1_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fscheda1 ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fscheda1_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fscheda_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fscheda ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fscheda_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedac; Type: TABLE; Schema: cfti5; Owner: postgres
--

CREATE TABLE cfti5.fschedac (
    id integer NOT NULL,
    deslocac character varying(40),
    codctot character varying(36),
    cric_c character varying(4),
    fscheda integer,
    fschedeb integer,
    feffetti integer,
    ftipfen integer,
    fnperiod integer,
    fnloc integer,
    fnfiu integer
);


ALTER TABLE cfti5.fschedac OWNER TO postgres;

--
-- Name: fschedac_id_seq; Type: SEQUENCE; Schema: cfti5; Owner: postgres
--

ALTER TABLE cfti5.fschedac ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fschedac_id_seq
    START WITH 8473
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedad; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fschedad (
    id integer NOT NULL,
    intensd numeric,
    testod text,
    codtotd character varying(40),
    flagintpq integer,
    fscossec integer,
    ftipfen integer,
    fschedeb integer,
    fschedac integer,
    fscheda integer,
    fpiaquo integer,
    fnperiod integer,
    fnloc integer,
    feffetti integer
);


ALTER TABLE cfti5.fschedad OWNER TO utente;

--
-- Name: fschedad_id_seq; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fschedad ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fschedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedae; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fschedae (
    id integer NOT NULL,
    codtote character varying(40),
    testoe text,
    cric_e character varying(4),
    ftipfen integer,
    feffetti integer,
    fschedeb integer,
    fschedac integer,
    fscheda integer,
    fnperiod integer,
    fnloc integer,
    fedifici integer
);


ALTER TABLE cfti5.fschedae OWNER TO utente;

--
-- Name: fschedae_id_seq; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fschedae ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fschedae_id_seq
    START WITH 6808
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedeb; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fschedeb (
    id integer NOT NULL,
    datab integer,
    dab integer,
    ab integer,
    usorar character varying(5),
    guerra integer,
    crisiec integer,
    tensoc integer,
    epidem integer,
    caresti integer,
    efflper integer,
    teorimp integer,
    noteb text,
    negativob integer,
    protb character varying(15),
    nperiob character varying(6),
    lungtb character varying(10),
    ncomp character varying(3),
    valb character varying(4),
    danni integer,
    codbtot character varying(30),
    codricb character varying(4),
    testob text,
    cbnptot character varying(12),
    fscheda integer,
    fscheda1 integer,
    feffetti integer,
    ftipfen integer,
    fnperiod integer,
    fvalfonte integer
);


ALTER TABLE cfti5.fschedeb OWNER TO utente;

--
-- Name: fschedeb_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fschedeb ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fschedeb_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fscossec; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fscossec (
    id integer NOT NULL,
    nterrs character varying(9),
    nperios integer,
    tipoevs character varying(1),
    longits character varying(4),
    oraorgs character varying(8),
    latitus character varying(4),
    dataors character varying(9),
    numstas integer,
    latmacs integer,
    lonmacs integer,
    npunpqs integer,
    profsts numeric,
    ios numeric,
    imaxs numeric,
    tios numeric,
    imins numeric,
    mes numeric,
    mls numeric,
    loceps character varying(30),
    statos character varying(3),
    regamms character varying(3),
    provams character varying(3),
    livrevs character varying(3),
    codigms character varying(9),
    codopes character varying(2),
    datares character varying(25),
    reliabs character varying(1),
    delta_io character varying(2),
    esoras character varying(1),
    eslats character varying(1),
    eslons character varying(1),
    riseqpr integer,
    promacs numeric,
    gameds numeric,
    tipoeqs character varying(1),
    mks integer,
    mps integer,
    mss integer,
    mbs integer,
    corpfgs character varying(3),
    dataors2 character varying(9),
    noterrs character varying(150),
    valsism character varying(2),
    rellocepics character varying(2),
    relmms character varying(2),
    azimiepic integer,
    rellocnum integer,
    datanum integer,
    dataunis character varying(8),
    data_sep character varying(10),
    ora_sep character varying(5),
    ios_old numeric,
    cric_sc character varying(4),
    npunpq_enel integer,
    nbiblio_enel integer,
    imax_enel integer,
    sito_enel character varying(3),
    err_mes numeric,
    fnperiod integer,
    lat_centesimale numeric,
    lon_centesimale numeric,
    geom cfti5.geometry(Point,4326)
);


ALTER TABLE cfti5.fscossec OWNER TO utente;

--
-- Name: fscossec2; Type: TABLE; Schema: cfti5; Owner: postgres
--

CREATE TABLE cfti5.fscossec2 (
    id integer,
    nterrs character varying(9),
    nperios integer,
    tipoevs character varying(1),
    longits character varying(4),
    oraorgs character varying(8),
    latitus character varying(4),
    dataors character varying(9),
    numstas integer,
    latmacs integer,
    lonmacs integer,
    npunpqs integer,
    profsts numeric,
    longmedian numeric,
    latmedian numeric,
    ios numeric,
    imaxs numeric,
    tios numeric,
    imins numeric,
    mes numeric,
    mls numeric,
    loceps character varying(30),
    statos character varying(3),
    regamms character varying(3),
    provams character varying(3),
    livrevs character varying(3),
    codigms character varying(9),
    codopes character varying(2),
    datares character varying(25),
    reliabs character varying(1),
    delta_io character varying(2),
    esoras character varying(1),
    eslats character varying(1),
    eslons character varying(1),
    seqsec integer,
    riseqpr integer,
    promacs numeric,
    gameds numeric,
    tipoeqs character varying(1),
    mks integer,
    mps integer,
    mss integer,
    mbs integer,
    corpfgs character varying(3),
    vuotosec character varying(5),
    latcart integer,
    longcart integer,
    sinoestraz character varying(1),
    dataors2 character varying(9),
    noterrs character varying(150),
    valsism character varying(2),
    rellocepics character varying(2),
    relmms character varying(2),
    azimiepic integer,
    rellocnum integer,
    datanum integer,
    dataunis character varying(8),
    data_sep character varying(10),
    ora_sep character varying(5),
    ios_old numeric,
    data_sc character varying(25),
    uti_sc character varying(4),
    psw_sc character varying(4),
    cric_sc character varying(4),
    seqcd integer,
    npunpq_enel integer,
    nbiblio_enel integer,
    imax_enel integer,
    sito_enel character varying(3),
    err_mes numeric,
    fnperiod integer,
    titolo_label1 character varying(30),
    titolo_label2 character varying(30),
    lat_centesimale numeric,
    lon_centesimale numeric,
    geometry cfti5.geometry(Point,4326)
);


ALTER TABLE cfti5.fscossec2 OWNER TO postgres;

--
-- Name: fscossec_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fscossec ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fscossec_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ftabcatego; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.ftabcatego (
    id integer NOT NULL,
    codtab character varying(10),
    destab text,
    ordine numeric,
    desing text,
    cric_ca character varying(4)
);


ALTER TABLE cfti5.ftabcatego OWNER TO utente;

--
-- Name: ftabcatego_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.ftabcatego ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.ftabcatego_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ftipfen; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.ftipfen (
    id integer NOT NULL,
    codtf character varying(4),
    destf text,
    notetf text
);


ALTER TABLE cfti5.ftipfen OWNER TO utente;

--
-- Name: ftipfen_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.ftipfen ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.ftipfen_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fvalfonte; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.fvalfonte (
    id integer NOT NULL,
    codval character varying(3),
    desval text,
    desvalingl text,
    noteval text,
    ordine_val numeric,
    cric_vb character varying(4)
);


ALTER TABLE cfti5.fvalfonte OWNER TO utente;

--
-- Name: fval_fonte_id_seq1; Type: SEQUENCE; Schema: cfti5; Owner: utente
--

ALTER TABLE cfti5.fvalfonte ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME cfti5.fval_fonte_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: seq_loc; Type: TABLE; Schema: cfti5; Owner: postgres
--

CREATE TABLE cfti5.seq_loc (
    id integer NOT NULL,
    nperiod character varying(255),
    codicescossa character varying(255),
    nscosse character varying(255),
    durcalc character varying(255),
    durfonti character varying(255),
    intfonte character varying(255),
    imcs character varying(255),
    imcs_nuovo character varying(255),
    id_ character varying(255),
    nomeloc character varying(255),
    nome_trovato_utilizzato character varying(255),
    note text,
    nloc_cfti character varying(255),
    commentoloc text,
    commentoe1 text,
    codice_effetto_nuovo character varying(255),
    latloc double precision,
    lonloc double precision
);


ALTER TABLE cfti5.seq_loc OWNER TO postgres;

--
-- Name: seq_scosse; Type: TABLE; Schema: cfti5; Owner: postgres
--

CREATE TABLE cfti5.seq_scosse (
    id integer NOT NULL,
    nperiod character varying(255),
    codicescossa character varying(255),
    nterr character varying(255),
    anno1 character varying(255),
    mese1 character varying(255),
    giorno1 character varying(255),
    anno2 character varying(255),
    mese2 character varying(255),
    giorno2 character varying(255),
    oraf character varying(255),
    oragmt1 character varying(255),
    oragmt2 character varying(255),
    timelabelgmt character varying(255),
    timelabelfonte character varying(255),
    io character varying(255),
    commentoscossa text
);


ALTER TABLE cfti5.seq_scosse OWNER TO postgres;

--
-- Name: test3; Type: TABLE; Schema: cfti5; Owner: utente
--

CREATE TABLE cfti5.test3 (
    id integer NOT NULL,
    test character varying
);


ALTER TABLE cfti5.test3 OWNER TO utente;

--
-- Name: vclassif; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vclassif AS
 SELECT fclassif.id,
    fclassif.codclass AS codice_classificazione,
    fclassif.desclass AS classificazione
   FROM cfti5.fclassif;


ALTER TABLE cfti5.vclassif OWNER TO postgres;

--
-- Name: vcodric; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vcodric AS
 SELECT fcodric.id,
    fcodric.codrice AS codice_di_ricerca,
    fcodric.desrice AS ricerca
   FROM cfti5.fcodric;


ALTER TABLE cfti5.vcodric OWNER TO postgres;

--
-- Name: vcommenti; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vcommenti AS
 SELECT fcommenti.id,
    fcommenti.codcomm AS codice_totale_commento,
    fcommenti.testocomm AS testo_commento,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    fnloc.lat_centesimale,
    fnloc.lon_centesimale,
    fnloc.provlet AS provincia,
    fnloc.siglastato AS nazione,
    ''::text AS "titolo_label_vtabcatego_Cat_Commento",
    ftabcatego.codtab AS codice_categoria_commento,
    ftabcatego.destab AS categoria_commento,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    to_tsvector(fcommenti.testocomm) AS testocomm_tsv,
    fnperiod.id AS id_fnperiod,
    fnloc.id AS id_fnloc,
    ftabcatego.id AS id_ftabcatego,
    feffetti.id AS id_feffetti
   FROM ((((cfti5.fcommenti
     LEFT JOIN cfti5.fnperiod ON ((fcommenti.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.ftabcatego ON ((fcommenti.ftabcatego = ftabcatego.id)))
     LEFT JOIN cfti5.feffetti ON ((fcommenti.feffetti = feffetti.id)))
     LEFT JOIN cfti5.fnloc ON ((fcommenti.fnloc = fnloc.id)));


ALTER TABLE cfti5.vcommenti OWNER TO postgres;

--
-- Name: vdemo; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vdemo AS
 SELECT fdemo.id,
    fdemo.nc,
    fdemo.attf,
    fdemo.daldem,
    fdemo.aldem,
    fdemo.attd,
    fdemo.demog,
    ''::text AS "titolo_label_vfontidemo_Fonte_Demografica",
    ffontidemo.codfdem AS codice_fonte_demografica,
    ffontidemo.desfdem AS fonte_demografica,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ffontidemo.id AS id_ffontidemo,
    fnloc.id AS id_fnloc,
    fscheda.id AS id_fscheda
   FROM (((cfti5.fdemo
     LEFT JOIN cfti5.ffontidemo ON ((fdemo.ffontidemo = ffontidemo.id)))
     LEFT JOIN cfti5.fnloc ON ((fdemo.fnloc = fnloc.id)))
     LEFT JOIN cfti5.fscheda ON ((fdemo.fscheda = fscheda.id)));


ALTER TABLE cfti5.vdemo OWNER TO postgres;

--
-- Name: vedifici; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vedifici AS
 SELECT fedifici.id,
    fedifici.classed AS classificazione_edificio,
    fedifici.codicedif AS codice_edificio,
    fedifici.descrizedif AS nome_edificio,
    fedifici.noted AS note_edificio,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_localita,
    fnloc.desloc AS nome_localita,
    fnloc.numabit AS numero_abitanti,
    fnloc.provlet AS nome_provincia,
    fnloc.id AS id_fnloc
   FROM (cfti5.fedifici
     LEFT JOIN cfti5.fnloc ON ((fedifici.fnloc = fnloc.id)));


ALTER TABLE cfti5.vedifici OWNER TO postgres;

--
-- Name: veffetti; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.veffetti AS
 SELECT feffetti.id,
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    feffetti.noteeff AS note
   FROM cfti5.feffetti;


ALTER TABLE cfti5.veffetti OWNER TO postgres;

--
-- Name: vfontidemo; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vfontidemo AS
 SELECT ffontidemo.id,
    ffontidemo.codfdem AS codice_tipo_fonte_demografica,
    ffontidemo.desfdem AS tipo_fonte_demografica,
    ffontidemo.notefdem AS note_fonte_demografica
   FROM cfti5.ffontidemo;


ALTER TABLE cfti5.vfontidemo OWNER TO postgres;

--
-- Name: vicon; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vicon AS
 SELECT ficon.id,
    ficon.codico,
    ficon.didasico,
    ficon.codtotico,
    ficon.fotoinc,
    ficon.noteico,
    ''::text AS "titolo_label_vedifici_Edificio",
    fedifici.codicedif AS codice_edificio,
    fedifici.descrizedif AS edificio,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza_sismica,
    fnperiod.datainiznp AS data_inizio_sequenza,
    fnperiod.datafinenp AS data_fine_sequenza,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ''::text AS "titolo_label_vschedac_Scheda_C",
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    fedifici.id AS id_fedifici,
    feffetti.id AS id_feffetti,
    fnloc.id AS id_fnloc,
    fnperiod.id AS id_fnperiod,
    fscheda.id AS id_fscheda,
    fschedac.id AS id_fschedac,
    fschedeb.id AS id_fschedeb,
    ftipfen.id AS id_ftipfen
   FROM ((((((((cfti5.ficon
     LEFT JOIN cfti5.fedifici ON ((ficon.fedifici = fedifici.id)))
     LEFT JOIN cfti5.feffetti ON ((ficon.feffetti = feffetti.id)))
     LEFT JOIN cfti5.fnloc ON ((ficon.fnloc = fnloc.id)))
     LEFT JOIN cfti5.fnperiod ON ((ficon.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.fscheda ON ((ficon.fscheda = fscheda.id)))
     LEFT JOIN cfti5.fschedac ON ((ficon.fschedac = fschedac.id)))
     LEFT JOIN cfti5.fschedeb ON ((ficon.fschedeb = fschedeb.id)))
     LEFT JOIN cfti5.ftipfen ON ((ficon.ftipfen = ftipfen.id)));


ALTER TABLE cfti5.vicon OWNER TO postgres;

--
-- Name: vnfiu; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vnfiu AS
 SELECT fnfiu.id,
    fnfiu.nomfium AS nome_evidenza_geomorfologica,
    fnfiu.tavfiu AS tavoletta_igm,
    fnfiu.areafiu AS area,
    fnfiu.notefiu AS note
   FROM cfti5.fnfiu;


ALTER TABLE cfti5.vnfiu OWNER TO postgres;

--
-- Name: vnloc; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vnloc AS
 SELECT fnloc.id,
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    fnloc.latloc AS latitudine,
    fnloc.longlog AS longitudine,
    fnloc.lat_centesimale,
    fnloc.lon_centesimale,
    fnloc.quotloc AS quota,
    fnloc.numabit AS numero_abitanti_1971,
    fnloc.prov AS codice_provincia,
    fnloc.provlet AS provincia_nome,
    fnloc.regione AS codice_regione,
    fnloc.comune AS codice_comune,
    fnloc.frazion AS codice_frazione,
    fnloc.foglio AS foglio_igm,
    fnloc.tavolet AS tavoletta_igm,
    fnloc.risentimenti,
    fnloc.siglastato AS nazione,
    fnloc.desloc2 AS altro_nome_localita,
    fnloc.notesito AS note_storiche,
    fnloc.notenl AS note
   FROM cfti5.fnloc;


ALTER TABLE cfti5.vnloc OWNER TO postgres;

--
-- Name: vnperiod; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vnperiod AS
 SELECT fnperiod.id,
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    fnperiod.livnp AS livello_approfondimento,
    fnperiod.numcomm AS numero_commenti,
    fnperiod.numbiblio AS numero_referenze,
    fnperiod.numtb AS numero_testi,
    fnperiod.areaepicentrale AS area_epicentrale,
    fnperiod.in_cfti,
        CASE
            WHEN (length((fnperiod.datainiznp)::text) < 4) THEN '0'::text
            WHEN (length((fnperiod.datafinenp)::text) < 4) THEN '0'::text
            WHEN (substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4)) = substr((fnperiod.datafinenp)::text, 1, (length((fnperiod.datafinenp)::text) - 4))) THEN substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4))
            WHEN (substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4)) <> substr((fnperiod.datafinenp)::text, 1, (length((fnperiod.datafinenp)::text) - 4))) THEN ((substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4)) || '-'::text) || substr((fnperiod.datafinenp)::text, 1, (length((fnperiod.datafinenp)::text) - 4)))
            ELSE NULL::text
        END AS intervallo_anni,
    fnperiod.commentonperiod AS note,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    feffetti.id AS id_feffetti,
    ftipfen.id AS id_ftipfen
   FROM ((cfti5.fnperiod
     LEFT JOIN cfti5.ftipfen ON ((fnperiod.ftipfen = ftipfen.id)))
     LEFT JOIN cfti5.feffetti ON ((fnperiod.feffetti = feffetti.id)));


ALTER TABLE cfti5.vnperiod OWNER TO postgres;

--
-- Name: vpiaquo; Type: VIEW; Schema: cfti5; Owner: utente
--

CREATE VIEW cfti5.vpiaquo AS
 SELECT fpiaquo.id,
    fpiaquo.deslocapq AS "località",
    fpiaquo.intpq AS "intensità_romano",
    fpiaquo.intpqnum AS "intensità_arabo",
    fpiaquo.tipo_pq AS provenienza_pq,
    fpiaquo.codtotpq AS codice_totale_pq,
    fpiaquo.note_pq AS note,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc,
    fnloc.desloc,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod,
    fnperiod.datainiznp,
    fnperiod.datafinenp,
    ''::text AS "titolo_label_vscossec_Terremoto",
    fscossec.nterrs,
    fnloc.id AS id_fnloc,
    fnperiod.id AS id_fnperiod,
    fscossec.id AS id_fscossec
   FROM (((cfti5.fpiaquo
     LEFT JOIN cfti5.fnloc ON ((fpiaquo.fnloc = fnloc.id)))
     LEFT JOIN cfti5.fnperiod ON ((fpiaquo.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.fscossec ON ((fpiaquo.fscossec = fscossec.id)));


ALTER TABLE cfti5.vpiaquo OWNER TO utente;

--
-- Name: vscheda; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vscheda AS
 SELECT fscheda.id,
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.editaut AS tipo_autore,
    fscheda.ruolo AS ruolo_autore,
    fscheda.dalaut AS ruolo_autore_da,
    fscheda.alaut AS ruolo_autore_a,
    fscheda.rigeaut AS riferimento_geografico,
    fscheda.titolo1 AS titolo,
    fscheda.daltit,
    fscheda.altit,
    fscheda.rigetit AS riferimento_geografico_titolo,
    fscheda.luogoed AS luogo_edizione,
    fscheda.datauni AS data_edizione_ms1,
    fscheda.dataun2 AS data_edizione_ms2,
    fscheda.codmicr AS codice_microfilm,
    fscheda.fotiniz AS fotogramma_iniziale,
    fscheda.codmicrenel AS codice_microfilm_enel,
    fscheda.prota,
    fscheda.notea,
    fscheda.colloca,
    fscheda.ordinam,
    fscheda.tipoa,
    ''::text AS "titolo_label_vscheda1_Sede_Ricerca",
    fscheda1.coda1 AS codice_sede_ricerca,
    fscheda1.desa1 AS sede_ricerca,
    ''::text AS "titolo_label_vclassif_Classificazione",
    fclassif.codclass AS codice_classificazione,
    fclassif.desclass AS classificazione,
    fscheda1.id AS id_fscheda1,
    fclassif.id AS id_fclassif
   FROM ((cfti5.fscheda
     LEFT JOIN cfti5.fscheda1 ON ((fscheda.fscheda1 = fscheda1.id)))
     LEFT JOIN cfti5.fclassif ON ((fscheda.fclassif = fclassif.id)));


ALTER TABLE cfti5.vscheda OWNER TO postgres;

--
-- Name: vscheda1; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vscheda1 AS
 SELECT fscheda1.id,
    fscheda1.coda1 AS codice_sede_ricerca,
    fscheda1.desa1 AS sede_ricerca,
    fscheda1.inda1 AS indirizzo,
    fscheda1.capa1 AS cap,
    fscheda1.loca1 AS localita,
    fscheda1.prova1 AS provincia,
    fscheda1.tela1 AS telefono,
    fscheda1.notea1 AS note,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    fnloc.id AS id_fnloc
   FROM (cfti5.fscheda1
     LEFT JOIN cfti5.fnloc ON ((fscheda1.fnloc = fnloc.id)));


ALTER TABLE cfti5.vscheda1 OWNER TO postgres;

--
-- Name: vschedac; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vschedac AS
 SELECT fschedac.id,
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.datab AS data_evento_descritto,
    fschedeb.dab AS data_evento_descritto_dal,
    fschedeb.ab AS data_evento_descritto_al,
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS descrizione_effetto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    ''::text AS "titolo_label_vnfiu_Fiume",
    fnfiu.nomfium AS nome_fiume,
    fnfiu.tavfiu AS tav_fiume,
    fnfiu.areafiu AS area_fiume,
    fschedeb.id AS id_fschedeb,
    feffetti.id AS id_feffetti,
    ftipfen.id AS id_ftipfen,
    fnperiod.id AS id_fnperiod,
    fnloc.id AS id_fnloc,
    fnfiu.id AS id_fnfiu
   FROM (((((((cfti5.fschedac
     LEFT JOIN cfti5.fscheda ON ((fschedac.fscheda = fscheda.id)))
     LEFT JOIN cfti5.fschedeb ON ((fschedac.fschedeb = fschedeb.id)))
     LEFT JOIN cfti5.feffetti ON ((fschedac.feffetti = feffetti.id)))
     LEFT JOIN cfti5.ftipfen ON ((fschedac.ftipfen = ftipfen.id)))
     LEFT JOIN cfti5.fnperiod ON ((fschedac.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.fnloc ON ((fschedac.fnloc = fnloc.id)))
     LEFT JOIN cfti5.fnfiu ON ((fschedac.fnfiu = fnfiu.id)));


ALTER TABLE cfti5.vschedac OWNER TO postgres;

--
-- Name: vschedad; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vschedad AS
 SELECT fschedad.id,
    fschedad.intensd,
    fschedad.codtotd,
    fschedad.flagintpq,
    fschedad.testod,
    ''::text AS "titolo_label_vscossec_Terremoto",
    fscossec.nterrs,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_vschedac_Scheda_C",
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza_sismica,
    fnperiod.datainiznp AS data_inizio_sequenza,
    fnperiod.datafinenp AS data_fine_sequenza,
    ''::text AS "titolo_label_vpiaquo_Piano_Quotato",
    fpiaquo.deslocapq AS "des_località",
    fpiaquo.intpq AS "intensità_arabo",
    fpiaquo.intpqnum AS "intensità_romano",
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    fscossec.id AS id_fscossec,
    ftipfen.id AS id_ftipfen,
    fschedeb.id AS id_fschedeb,
    fschedac.id AS id_fschedac,
    fscheda.id AS id_fscheda,
    fnperiod.id AS id_fnperiod,
    fpiaquo.id AS id_fpiaquo,
    fnloc.id AS id_fnloc,
    feffetti.id AS id_feffetti
   FROM (((((((((cfti5.fschedad
     LEFT JOIN cfti5.fscossec ON ((fschedad.fscossec = fscossec.id)))
     LEFT JOIN cfti5.ftipfen ON ((fschedad.ftipfen = ftipfen.id)))
     LEFT JOIN cfti5.fschedeb ON ((fschedad.fschedeb = fschedeb.id)))
     LEFT JOIN cfti5.fschedac ON ((fschedad.fschedac = fschedac.id)))
     LEFT JOIN cfti5.fscheda ON ((fschedad.fscheda = fscheda.id)))
     LEFT JOIN cfti5.fnperiod ON ((fschedad.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.fpiaquo ON ((fschedad.fpiaquo = fpiaquo.id)))
     LEFT JOIN cfti5.fnloc ON ((fschedad.fnloc = fnloc.id)))
     LEFT JOIN cfti5.feffetti ON ((fschedad.feffetti = feffetti.id)));


ALTER TABLE cfti5.vschedad OWNER TO postgres;

--
-- Name: vschedae; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vschedae AS
 SELECT fschedae.id,
    fschedae.codtote,
    fschedae.cric_e,
    fschedae.testoe,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_vschedac_Scheda_C",
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza_sismica,
    fnperiod.datainiznp AS data_inizio_sequenza,
    fnperiod.datafinenp AS data_fine_sequenza,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_vedifici_Edificio",
    fedifici.codicedif AS codice_edificio,
    fedifici.descrizedif AS edificio,
    fedifici.id AS id_fedifici,
    feffetti.id AS id_feffetti,
    fnloc.id AS id_fnloc,
    fnperiod.id AS id_fnperiod,
    fscheda.id AS id_fscheda,
    fschedac.id AS id_fschedac,
    fschedeb.id AS id_fschedeb,
    ftipfen.id AS id_ftipfen
   FROM ((((((((cfti5.fschedae
     LEFT JOIN cfti5.ftipfen ON ((fschedae.ftipfen = ftipfen.id)))
     LEFT JOIN cfti5.feffetti ON ((fschedae.feffetti = feffetti.id)))
     LEFT JOIN cfti5.fschedeb ON ((fschedae.fschedeb = fschedeb.id)))
     LEFT JOIN cfti5.fschedac ON ((fschedae.fschedac = fschedac.id)))
     LEFT JOIN cfti5.fscheda ON ((fschedae.fscheda = fscheda.id)))
     LEFT JOIN cfti5.fnperiod ON ((fschedae.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.fnloc ON ((fschedae.fnloc = fnloc.id)))
     LEFT JOIN cfti5.fedifici ON ((fschedae.fedifici = fedifici.id)));


ALTER TABLE cfti5.vschedae OWNER TO postgres;

--
-- Name: vschedeb; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vschedeb AS
 SELECT fschedeb.id,
    fschedeb.datab AS data_evento_descritto_dal,
    fschedeb.dab AS data_evento_descritto,
    fschedeb.ab AS data_evento_descritto_al,
    fschedeb.testob AS testo_trascritto,
    fschedeb.noteb AS note,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    fscheda.datauni AS data_edizione_ms1,
    fscheda.dataun2 AS data_edizione_ms2,
    fscheda.luogoed AS luogo_di_edizione,
    ''::text AS "titolo_label_vscheda1_Sede_Ricerca",
    fscheda1.coda1 AS codice_sede_ricerca,
    fscheda1.desa1 AS sede_ricerca,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS descrizione_effetto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    ''::text AS "titolo_label_vvalfonte_Valore_Del_Testo",
    fvalfonte.codval AS codice_valore,
    fvalfonte.desval AS valore,
    concat('http://www.cftilab.it/file_repository/pdf_T/', fnperiod.nperiod, '-', fscheda.codbib, '_T.pdf') AS pdf_trascritto,
    concat('http://www.cftilab.it/file_repository/pdf_R/', fnperiod.nperiod, '-', fscheda.codbib, '_R.pdf') AS pdf_raster,
    to_tsvector(fschedeb.testob) AS testob_tsv,
    fscheda.id AS id_fscheda,
    fscheda1.id AS id_fscheda1,
    feffetti.id AS id_feffetti,
    ftipfen.id AS id_ftipfen,
    fnperiod.id AS id_fnperiod,
    fvalfonte.id AS id_fvalfonte
   FROM ((((((cfti5.fschedeb
     LEFT JOIN cfti5.fscheda ON ((fschedeb.fscheda = fscheda.id)))
     LEFT JOIN cfti5.fscheda1 ON ((fschedeb.fscheda1 = fscheda1.id)))
     LEFT JOIN cfti5.feffetti ON ((fschedeb.feffetti = feffetti.id)))
     LEFT JOIN cfti5.ftipfen ON ((fschedeb.ftipfen = ftipfen.id)))
     LEFT JOIN cfti5.fnperiod ON ((fschedeb.fnperiod = fnperiod.id)))
     LEFT JOIN cfti5.fvalfonte ON ((fschedeb.fvalfonte = fvalfonte.id)));


ALTER TABLE cfti5.vschedeb OWNER TO postgres;

--
-- Name: vscossec; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vscossec AS
 SELECT fscossec.id,
    fscossec.nterrs AS numero_terremoto,
    fscossec.dataors AS data_origine,
    fscossec.oraorgs AS orario_origine,
    fscossec.latitus AS lat_epic_strum,
    fscossec.longits AS lon_epic_strum,
    fscossec.latmacs AS lat_epic_macro,
    fscossec.lonmacs AS lon_epic_macro,
    fscossec.loceps AS zona_epicentrale,
    fscossec.npunpqs AS n_punti_pq,
    fscossec.profsts AS "profondità",
    fscossec.ios AS io,
    fscossec.imaxs AS imax,
    fscossec.lat_centesimale,
    fscossec.lon_centesimale,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    fnperiod.id AS id_fnperiod
   FROM (cfti5.fscossec
     LEFT JOIN cfti5.fnperiod ON ((fscossec.fnperiod = fnperiod.id)));


ALTER TABLE cfti5.vscossec OWNER TO postgres;

--
-- Name: vtabcatego; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vtabcatego AS
 SELECT ftabcatego.id,
    ftabcatego.codtab AS codice_categoria_commento,
    ftabcatego.destab AS categoria_commento,
    ftabcatego.ordine AS ordinamento,
    ftabcatego.desing AS categoria_commento_inglese
   FROM cfti5.ftabcatego;


ALTER TABLE cfti5.vtabcatego OWNER TO postgres;

--
-- Name: vtipfen; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vtipfen AS
 SELECT ftipfen.id,
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ftipfen.notetf AS note
   FROM cfti5.ftipfen;


ALTER TABLE cfti5.vtipfen OWNER TO postgres;

--
-- Name: vvalfonte; Type: VIEW; Schema: cfti5; Owner: postgres
--

CREATE VIEW cfti5.vvalfonte AS
 SELECT fvalfonte.id,
    fvalfonte.codval AS codice_valore,
    fvalfonte.desval AS valore,
    fvalfonte.noteval AS note,
    fvalfonte.desvalingl AS denominazione_inglese,
    fvalfonte.ordine_val AS ordinamento
   FROM cfti5.fvalfonte;


ALTER TABLE cfti5.vvalfonte OWNER TO postgres;

--
-- Name: fnperiod_cfti; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fnperiod_cfti (
    id integer NOT NULL,
    nperiod_cfti character varying(25)
);


ALTER TABLE cfti5web.fnperiod_cfti OWNER TO utente;

--
-- Name: fnperiod_cfti_view; Type: VIEW; Schema: cfti5web; Owner: utente
--

CREATE VIEW cfti5web.fnperiod_cfti_view AS
 SELECT fnperiod_cfti.nperiod_cfti,
    fnperiod.nperiod,
    fnperiod.id
   FROM cfti5.fnperiod,
    cfti5web.fnperiod_cfti
  WHERE ((fnperiod_cfti.nperiod_cfti)::text = (fnperiod.nperiod)::text);


ALTER TABLE cfti5web.fnperiod_cfti_view OWNER TO utente;

--
-- Name: schedeb; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.schedeb AS
 SELECT fschedeb.id,
    fnperiod_cfti_view.nperiod,
    fscheda.codbib,
    fschedeb.valb,
    fvalfonte.desvalingl,
    concat('http://www.cftilab.it/file_repository/pdf_T/', fnperiod_cfti_view.nperiod, '-', fscheda.codbib, '_T.pdf') AS pdf_trascritto,
    concat('http://www.cftilab.it/file_repository/pdf_R/', fnperiod_cfti_view.nperiod, '-', fscheda.codbib, '_R.pdf') AS pdf_raster
   FROM (((cfti5.fschedeb
     JOIN cfti5web.fnperiod_cfti_view ON ((fschedeb.fnperiod = fnperiod_cfti_view.id)))
     LEFT JOIN cfti5.fvalfonte ON ((fschedeb.fvalfonte = fvalfonte.id)))
     LEFT JOIN cfti5.fscheda ON ((fschedeb.fscheda = fscheda.id)))
  WITH NO DATA;


ALTER TABLE cfti5web.schedeb OWNER TO utente;

--
-- Name: schedea; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.schedea AS
 SELECT DISTINCT ON (fscheda.codbib) fscheda.id,
    fscheda.codbib,
    fscheda.autore1,
    fscheda.titolo1,
    fscheda.luogoed,
    fscheda.datauni,
    fscheda.dataun2,
    fscheda.ordinam
   FROM cfti5.fscheda,
    cfti5web.schedeb
  WHERE ((fscheda.codbib)::text = (schedeb.codbib)::text)
  WITH NO DATA;


ALTER TABLE cfti5web.schedea OWNER TO utente;

--
-- Name: biblio_details; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.biblio_details AS
 SELECT schedea.codbib,
    schedea.autore1,
    schedea.titolo1,
    schedea.luogoed,
    schedea.datauni,
    schedea.dataun2,
    schedeb.desvalingl,
    schedeb.valb,
    schedeb.nperiod
   FROM (cfti5web.schedea
     JOIN cfti5web.schedeb ON (((schedea.codbib)::text = (schedeb.codbib)::text)))
  WITH NO DATA;


ALTER TABLE cfti5web.biblio_details OWNER TO utente;

--
-- Name: biblio_ee; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.biblio_ee (
    id integer NOT NULL,
    codbib character varying(25)
);


ALTER TABLE cfti5web.biblio_ee OWNER TO utente;

--
-- Name: codbib_list_ee; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.codbib_list_ee (
    id integer NOT NULL,
    codbib character varying(25)
);


ALTER TABLE cfti5web.codbib_list_ee OWNER TO utente;

--
-- Name: commgen; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.commgen AS
 SELECT fcommenti.id,
    fnperiod_cfti_view.nperiod_cfti,
    ftabcatego.codtab,
    fcommenti.testocomm
   FROM cfti5.fcommenti,
    cfti5.ftabcatego,
    cfti5web.fnperiod_cfti_view
  WHERE (((ftabcatego.codtab)::text <> 'D1'::text) AND ((ftabcatego.codtab)::text <> 'E1'::text) AND ((ftabcatego.codtab)::text <> 'A0'::text) AND ((ftabcatego.codtab)::text <> 'A1'::text) AND (fcommenti.fnperiod = fnperiod_cfti_view.id) AND (fcommenti.ftabcatego = ftabcatego.id))
  WITH NO DATA;


ALTER TABLE cfti5web.commgen OWNER TO utente;

--
-- Name: d1; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.d1 AS
 SELECT fcommenti.id,
    fnperiod_cfti_view.nperiod_cfti,
    ftabcatego.codtab,
    fcommenti.testocomm,
    fnloc.nloc
   FROM cfti5.fcommenti,
    cfti5web.fnperiod_cfti_view,
    cfti5.ftabcatego,
    cfti5.fnloc
  WHERE (((ftabcatego.codtab)::text = 'D1'::text) AND (fcommenti.fnperiod = fnperiod_cfti_view.id) AND (fcommenti.ftabcatego = ftabcatego.id) AND (fcommenti.fnloc = fnloc.id))
  WITH NO DATA;


ALTER TABLE cfti5web.d1 OWNER TO utente;

--
-- Name: ee_tabella; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.ee_tabella (
    id integer NOT NULL,
    nterr character varying(25),
    nperiod character varying(25),
    nloc character varying(25),
    codice_eff character varying(25),
    commento text,
    risentimenti integer,
    maxint numeric
);


ALTER TABLE cfti5web.ee_tabella OWNER TO utente;

--
-- Name: fnloc_cfti_5; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fnloc_cfti_5 (
    id integer NOT NULL,
    nloc_6 character varying(250),
    nloc character varying(250),
    desloc text,
    provlet character varying(25),
    nazione character varying(250),
    postal_code character varying(25),
    quot_loc character varying(250),
    numabit character varying(250),
    prov character varying(25),
    regione character varying(250),
    comune character varying(250),
    frazione character varying(25),
    notesito text,
    lat_roma40 character varying(25),
    lon_roma40 character varying(25),
    istat character varying(250),
    istat_103 character varying(250),
    comune_nome text,
    capoluogo character varying(25),
    cap character varying(25),
    problemi text,
    istat_2011 character varying(250),
    comune_nome_2011 text,
    capoluogo_2011 character varying(25),
    in_cfti character varying(25),
    nloc_cfti character varying(250),
    desloc_cfti text,
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet_old character varying(25),
    cod_compl text,
    note_2018 text
);


ALTER TABLE cfti5web.fnloc_cfti_5 OWNER TO utente;

--
-- Name: ee; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.ee AS
 SELECT ee_tabella.id,
    ee_tabella.nterr,
    ee_tabella.nperiod,
    ee_tabella.nloc,
    ee_tabella.codice_eff,
    ee_tabella.commento,
    ee_tabella.risentimenti,
    ee_tabella.maxint,
    fnloc_cfti_5.nloc_cfti,
    fnloc_cfti_5.desloc_cfti,
    fnloc_cfti_5.lat_wgs84,
    fnloc_cfti_5.lon_wgs84,
    COALESCE(fnloc_cfti_5.notesito, '-'::text) AS notesito,
    fnloc_cfti_5.provlet,
    fnloc_cfti_5.nazione,
    fnloc_cfti_5.desloc
   FROM (cfti5web.ee_tabella
     LEFT JOIN cfti5web.fnloc_cfti_5 ON (((ee_tabella.nloc)::text = (fnloc_cfti_5.nloc)::text)))
  WITH NO DATA;


ALTER TABLE cfti5web.ee OWNER TO utente;

--
-- Name: ee_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.ee_med (
    id integer NOT NULL,
    codice_eff character varying(2),
    nterr character varying(25),
    nloc character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    desloc character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    nazione character varying(25),
    provlet character varying(25),
    notesito text
);


ALTER TABLE cfti5web.ee_med OWNER TO utente;

--
-- Name: fee_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fee_med (
    id integer NOT NULL,
    codice_eff character varying(2),
    nterr character varying(25),
    id_nterr integer,
    nloc character varying(25),
    id_nloc integer
);


ALTER TABLE cfti5web.fee_med OWNER TO utente;

--
-- Name: fee_med_total; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fee_med_total (
    id integer NOT NULL,
    nterr character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    codice_eff character varying(2),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    notesito text,
    provlet character varying(2),
    nazione character varying(25),
    nloc character varying(25),
    desloc character varying(250)
);


ALTER TABLE cfti5web.fee_med_total OWNER TO utente;

--
-- Name: fnloc_cfti; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fnloc_cfti (
    id integer NOT NULL,
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet_cfti character varying(250),
    nazione character varying(250),
    postal_code character varying(250),
    notesito text
);


ALTER TABLE cfti5web.fnloc_cfti OWNER TO utente;

--
-- Name: fpq_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fpq_med (
    id integer NOT NULL,
    intpqnum character varying(25),
    intpq character varying(25),
    nterr character varying(25),
    id_nterr integer,
    nloc character varying(25),
    id_nloc integer
);


ALTER TABLE cfti5web.fpq_med OWNER TO utente;

--
-- Name: fpq_med_total; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.fpq_med_total (
    id integer NOT NULL,
    nterr character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    intpqnum character varying(25),
    intpq character varying(25),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet character varying(25),
    notesito text,
    nloc character varying(25),
    desloc character varying(250),
    nazione character varying(25)
);


ALTER TABLE cfti5web.fpq_med_total OWNER TO utente;

--
-- Name: locind_ee; Type: VIEW; Schema: cfti5web; Owner: utente
--

CREATE VIEW cfti5web.locind_ee AS
 SELECT row_number() OVER (ORDER BY p.desloc_cfti) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.lat_wgs84,
    p.lon_wgs84,
    p.nloc,
    p.desloc,
    p.provlet,
    p.nazione,
    p.notesito,
    p.risentimenti,
    p.maxint,
    p.ee
   FROM ( SELECT DISTINCT ON (m.nloc_cfti) m.nloc_cfti,
            m.desloc_cfti,
            m.lat_wgs84,
            m.lon_wgs84,
            m.nloc,
            m.desloc,
            m.provlet,
            m.nazione,
            m.notesito,
            m.risentimenti,
            m.maxint,
            t.ee
           FROM (( SELECT ee.desloc_cfti,
                    ee.nloc_cfti,
                    count(ee.nloc_cfti) OVER (PARTITION BY ee.nloc_cfti) AS ee
                   FROM cfti5web.ee) t
             JOIN cfti5web.ee m ON (((m.nloc_cfti)::text = (t.nloc_cfti)::text)))) p;


ALTER TABLE cfti5web.locind_ee OWNER TO utente;

--
-- Name: pq; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.pq AS
 SELECT row_number() OVER (ORDER BY fnloc_cfti_5.desloc_cfti) AS id,
    vpiaquo."intensità_romano",
    vpiaquo."intensità_arabo",
    vpiaquo.nperiod,
    vpiaquo.nterrs,
    fnloc_cfti_5.nloc_cfti,
    fnloc_cfti_5.desloc_cfti,
    fnloc_cfti_5.lat_wgs84,
    fnloc_cfti_5.lon_wgs84,
    fnloc_cfti_5.provlet,
    fnloc_cfti_5.notesito,
    fnloc_cfti_5.nloc,
    fnloc_cfti_5.desloc,
    fnloc_cfti_5.nazione
   FROM ((cfti5.vpiaquo
     JOIN cfti5web.fnperiod_cfti ON ((((vpiaquo.nperiod)::text = (fnperiod_cfti.nperiod_cfti)::text) AND ((vpiaquo."intensità_romano")::text <> 'EE'::text))))
     LEFT JOIN cfti5web.fnloc_cfti_5 ON (((vpiaquo.nloc)::text = (fnloc_cfti_5.nloc)::text)))
  WITH NO DATA;


ALTER TABLE cfti5web.pq OWNER TO utente;

--
-- Name: locind_pq; Type: VIEW; Schema: cfti5web; Owner: utente
--

CREATE VIEW cfti5web.locind_pq AS
 SELECT row_number() OVER (ORDER BY p.desloc_cfti) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.lat_wgs84,
    p.lon_wgs84,
    p.nloc,
    p.desloc,
    p.provlet,
    p.nazione,
    p.notesito,
    p.risentimenti,
    p.maxint
   FROM ( SELECT DISTINCT ON (m.nloc_cfti) m.nloc_cfti,
            m.desloc_cfti,
            m.lat_wgs84,
            m.lon_wgs84,
            m.nloc,
            m.desloc,
            m.provlet,
            m.nazione,
            m.notesito,
            t.risentimenti,
            t.maxint
           FROM (( SELECT pq."intensità_arabo",
                    pq.desloc_cfti,
                    pq.nloc_cfti,
                    count(pq.nloc_cfti) OVER (PARTITION BY pq.nloc_cfti) AS risentimenti,
                    max(pq."intensità_arabo") OVER (PARTITION BY pq.nloc_cfti) AS maxint
                   FROM cfti5web.pq) t
             JOIN cfti5web.pq m ON (((m.nloc_cfti)::text = (t.nloc_cfti)::text)))) p;


ALTER TABLE cfti5web.locind_pq OWNER TO utente;

--
-- Name: locind_ee_senza_pq; Type: VIEW; Schema: cfti5web; Owner: utente
--

CREATE VIEW cfti5web.locind_ee_senza_pq AS
 SELECT row_number() OVER (ORDER BY l.desloc_cfti) AS id,
    l.nloc_cfti,
    l.desloc_cfti,
    l.lat_wgs84,
    l.lon_wgs84,
    l.nloc,
    l.desloc,
    l.provlet,
    l.nazione,
    l.notesito,
    l.risentimenti,
    l.maxint,
    l.ee
   FROM (cfti5web.locind_ee l
     LEFT JOIN cfti5web.locind_pq r ON (((r.nloc_cfti)::text = (l.nloc_cfti)::text)))
  WHERE (r.nloc_cfti IS NULL);


ALTER TABLE cfti5web.locind_ee_senza_pq OWNER TO utente;

--
-- Name: locind_pq_con_ee; Type: VIEW; Schema: cfti5web; Owner: utente
--

CREATE VIEW cfti5web.locind_pq_con_ee AS
 SELECT row_number() OVER (ORDER BY p.desloc_cfti) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.lat_wgs84,
    p.lon_wgs84,
    p.nloc,
    p.desloc,
    p.provlet,
    p.nazione,
    p.notesito,
    p.risentimenti,
    p.maxint,
    p.ee
   FROM ( SELECT locind_pq.nloc_cfti,
            locind_pq.desloc_cfti,
            locind_pq.lat_wgs84,
            locind_pq.lon_wgs84,
            locind_pq.nloc,
            locind_pq.desloc,
            locind_pq.provlet,
            locind_pq.nazione,
            locind_pq.notesito,
            locind_pq.risentimenti,
            locind_pq.maxint,
            locind_ee.ee
           FROM (cfti5web.locind_pq
             LEFT JOIN cfti5web.locind_ee ON (((locind_pq.nloc_cfti)::text = (locind_ee.nloc_cfti)::text)))) p;


ALTER TABLE cfti5web.locind_pq_con_ee OWNER TO utente;

--
-- Name: notesito; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.notesito AS
 SELECT row_number() OVER (ORDER BY p.nloc) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.nloc,
    p.notesito
   FROM ( SELECT DISTINCT ON (m.nloc_cfti) m.nloc_cfti,
            m.desloc_cfti,
            m.nloc,
            m.notesito
           FROM cfti5web.fnloc_cfti_5 m
          WHERE (m.notesito <> ''::text)
          ORDER BY m.nloc_cfti, m.nloc) p
  WITH NO DATA;


ALTER TABLE cfti5web.notesito OWNER TO utente;

--
-- Name: locind; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.locind AS
 SELECT row_number() OVER (ORDER BY t.desloc_cfti) AS id,
    t.nloc_cfti,
    t.desloc_cfti,
    t.lat_wgs84,
    t.lon_wgs84,
    t.provlet,
    t.nazione,
    COALESCE(notesito.notesito, t.notesito) AS notesito,
    COALESCE(t.risentimenti, (0)::bigint) AS risentimenti,
    COALESCE(t.maxint, ('-2'::integer)::numeric) AS maxint,
    COALESCE(t.ee, (0)::bigint) AS ee
   FROM (( SELECT locind_pq_con_ee.nloc_cfti,
            locind_pq_con_ee.desloc_cfti,
            locind_pq_con_ee.lat_wgs84,
            locind_pq_con_ee.lon_wgs84,
            locind_pq_con_ee.nloc,
            locind_pq_con_ee.desloc,
            locind_pq_con_ee.provlet,
            locind_pq_con_ee.nazione,
            locind_pq_con_ee.notesito,
            locind_pq_con_ee.risentimenti,
            locind_pq_con_ee.maxint,
            locind_pq_con_ee.ee
           FROM cfti5web.locind_pq_con_ee
        UNION
         SELECT locind_ee_senza_pq.nloc_cfti,
            locind_ee_senza_pq.desloc_cfti,
            locind_ee_senza_pq.lat_wgs84,
            locind_ee_senza_pq.lon_wgs84,
            locind_ee_senza_pq.nloc,
            locind_ee_senza_pq.desloc,
            locind_ee_senza_pq.provlet,
            locind_ee_senza_pq.nazione,
            locind_ee_senza_pq.notesito,
            locind_ee_senza_pq.risentimenti,
            locind_ee_senza_pq.maxint,
            locind_ee_senza_pq.ee
           FROM cfti5web.locind_ee_senza_pq) t
     LEFT JOIN cfti5web.notesito ON (((t.nloc_cfti)::text = (notesito.nloc_cfti)::text)))
  WITH NO DATA;


ALTER TABLE cfti5web.locind OWNER TO utente;

--
-- Name: locind_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.locind_med (
    id integer NOT NULL,
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    nloc character varying(25),
    provlet character varying(25),
    notesito text,
    nazione character varying(25),
    risentimenti bigint,
    maxint text
);


ALTER TABLE cfti5web.locind_med OWNER TO utente;

--
-- Name: nloc_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.nloc_med (
    id integer NOT NULL,
    nloc6 character varying(25),
    seqnloc integer,
    nloc character varying(25),
    desloc character varying(250),
    provlet character varying(25),
    nazione character varying(25),
    postal_code character varying(25),
    quotloc character varying(25),
    numabit character varying(25),
    prov character varying(5),
    regione character varying(5),
    comune character varying(5),
    frazion character varying(25),
    notesito text,
    lat_roma40 character varying(25),
    lon_roma40 character varying(25),
    istat character varying(25),
    istat_103 character varying(25),
    comune_nome character varying(25),
    capoluogo character varying(25),
    cap character varying(25),
    problemi character varying(25),
    istat_2011 character varying(25),
    comune_nome_2011 character varying(25),
    capoluogo_2011 character varying(25),
    in_cfti character varying(2),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet_old character varying(25)
);


ALTER TABLE cfti5web.nloc_med OWNER TO utente;

--
-- Name: nterrs; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.nterrs (
    id integer NOT NULL,
    cat character varying(25),
    nterr character varying(25),
    nperiod character varying(25),
    datanum character varying(25),
    data_label character varying(25),
    anno character varying(25),
    mese character varying(25),
    giorno character varying(25),
    data_incert character varying(25),
    anno2 character varying(25),
    mese2 character varying(25),
    giorno2 character varying(25),
    data_incert2 character varying(25),
    time_label character varying(25),
    ora character varying(25),
    minu character varying(25),
    sec character varying(25),
    time_incert character varying(25),
    lat character varying(25),
    lon character varying(25),
    rel character varying(25),
    io character varying(25),
    imax character varying(25),
    npun character varying(25),
    mm character varying(25),
    earthquakelocation text,
    country character varying(25),
    sigla character varying(25),
    ee_nt character varying(25),
    ee_np character varying(25),
    epicenter_type character varying(25),
    flagcomments character varying(25),
    flagfalseeq character varying(25),
    level character varying(25),
    new2018 text,
    comm2018 text,
    nota_novita text,
    nota_mm text,
    "time" character varying(25),
    npun_all character varying(25),
    npun_boxer character varying(25),
    ndata character varying(25)
);


ALTER TABLE cfti5web.nterrs OWNER TO utente;

--
-- Name: nterrs2; Type: VIEW; Schema: cfti5web; Owner: utente
--

CREATE VIEW cfti5web.nterrs2 AS
 SELECT fscossec.id,
    fscossec.nterrs,
    fnperiod_cfti_view.nperiod,
    fscossec.dataors,
    fscossec.oraorgs,
    fscossec.lat_centesimale,
    fscossec.lon_centesimale,
    fscossec.loceps,
    fscossec.npunpqs,
    fscossec.ios,
    fscossec.imaxs,
    fscossec.mks,
    fscossec.livrevs,
    fscossec.rellocepics,
    fscossec.statos
   FROM cfti5.fscossec,
    cfti5web.fnperiod_cfti_view
  WHERE (fscossec.fnperiod = fnperiod_cfti_view.id);


ALTER TABLE cfti5web.nterrs2 OWNER TO utente;

--
-- Name: nterrs_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.nterrs_med (
    id integer NOT NULL,
    cat character varying(25),
    nterr character varying(25),
    nperiod character varying(25),
    datanum character varying(25),
    data_label character varying(250),
    anno character varying(25),
    mese character varying(25),
    giorno character varying(25),
    data_incert character varying(25),
    anno2 character varying(25),
    mese2 character varying(25),
    giorno2 character varying(25),
    data_incert2 character varying(25),
    time_label character varying(25),
    ora character varying(25),
    minu character varying(25),
    sec character varying(25),
    time_incert character varying(25),
    lat character varying(25),
    lon character varying(25),
    rel character varying(25),
    io character varying(25),
    imax character varying(25),
    npun character varying(25),
    mm character varying(25),
    earthquakelocation character varying(250),
    country character varying(25),
    sigla character varying(25),
    ee_nt character varying(2),
    ee_np character varying(2),
    epicenter_type character varying(250),
    flagcomments character varying(2),
    flagfalseeq character varying(2),
    level character varying(25)
);


ALTER TABLE cfti5web.nterrs_med OWNER TO utente;

--
-- Name: pq_med; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.pq_med (
    id integer NOT NULL,
    intpqnum character varying(25),
    intpq character varying(25),
    nterr character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    nloc character varying(25),
    provlet character varying(25),
    notesito text,
    nazione character varying(25)
);


ALTER TABLE cfti5web.pq_med OWNER TO utente;

--
-- Name: quake_details; Type: MATERIALIZED VIEW; Schema: cfti5web; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5web.quake_details AS
 SELECT nterrs.cat,
    nterrs.nterr,
    nterrs.nperiod,
    nterrs.datanum,
    nterrs.data_label,
    nterrs.anno,
    nterrs.mese,
    nterrs.giorno,
    nterrs.time_label,
    nterrs.ora,
    nterrs.minu,
    nterrs.sec,
    nterrs.lat,
    nterrs.lon,
    nterrs.rel,
    nterrs.level,
    nterrs.io,
    nterrs.imax,
    nterrs.npun,
    nterrs.mm,
    nterrs.earthquakelocation,
    nterrs.epicenter_type,
    nterrs.country,
    nterrs.ee_nt,
    nterrs.ee_np,
    nterrs.flagcomments,
    nterrs.flagfalseeq,
    pq."intensità_arabo",
    pq."intensità_romano",
    pq.nloc_cfti
   FROM (cfti5web.nterrs
     JOIN cfti5web.pq ON (((nterrs.nterr)::text = (pq.nterrs)::text)))
  WITH NO DATA;


ALTER TABLE cfti5web.quake_details OWNER TO utente;

--
-- Name: schedec; Type: TABLE; Schema: cfti5web; Owner: utente
--

CREATE TABLE cfti5web.schedec (
    id integer NOT NULL,
    codbib character varying(25),
    nloc character varying(25)
);


ALTER TABLE cfti5web.schedec OWNER TO utente;

--
-- Name: seq_loc; Type: VIEW; Schema: cfti5web; Owner: postgres
--

CREATE VIEW cfti5web.seq_loc AS
 SELECT seq_loc.id,
    seq_loc.nperiod,
    seq_loc.codicescossa,
    seq_loc.nscosse,
    seq_loc.durcalc,
    seq_loc.durfonti,
    seq_loc.intfonte,
    seq_loc.imcs,
    seq_loc.imcs_nuovo,
    seq_loc.id_,
    seq_loc.nomeloc,
    seq_loc.nome_trovato_utilizzato,
    seq_loc.note,
    seq_loc.nloc_cfti,
    seq_loc.commentoloc,
    seq_loc.commentoe1,
    seq_loc.codice_effetto_nuovo,
    seq_loc.latloc,
    seq_loc.lonloc
   FROM cfti5.seq_loc;


ALTER TABLE cfti5web.seq_loc OWNER TO postgres;

--
-- Name: seq_scosse; Type: VIEW; Schema: cfti5web; Owner: postgres
--

CREATE VIEW cfti5web.seq_scosse AS
 SELECT seq_scosse.id,
    seq_scosse.nperiod,
    seq_scosse.codicescossa,
    seq_scosse.nterr,
    seq_scosse.anno1,
    seq_scosse.mese1,
    seq_scosse.giorno1,
    seq_scosse.anno2,
    seq_scosse.mese2,
    seq_scosse.giorno2,
    seq_scosse.oraf,
    seq_scosse.oragmt1,
    seq_scosse.oragmt2,
    seq_scosse.timelabelgmt,
    seq_scosse.timelabelfonte,
    seq_scosse.io,
    seq_scosse.commentoscossa
   FROM cfti5.seq_scosse;


ALTER TABLE cfti5web.seq_scosse OWNER TO postgres;

--
-- Name: vnperiod_cfti; Type: VIEW; Schema: cfti5web; Owner: postgres
--

CREATE VIEW cfti5web.vnperiod_cfti AS
 SELECT vnperiod.id,
    vnperiod.codice_sequenza,
    vnperiod.inizio_sequenza,
    vnperiod.fine_sequenza,
    vnperiod.area_epicentrale,
    vnperiod.in_cfti,
    vnperiod.intervallo_anni
   FROM cfti5.vnperiod
  WHERE (vnperiod.in_cfti = true);


ALTER TABLE cfti5web.vnperiod_cfti OWNER TO postgres;

--
-- Name: allegato; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.allegato (
    codiceallegato integer NOT NULL,
    tipoentita character varying(100) DEFAULT NULL::character varying,
    codiceentita character varying(255) DEFAULT NULL::character varying,
    descroggall character varying(250) DEFAULT NULL::character varying,
    autoreall character varying(250) DEFAULT NULL::character varying,
    versioneall character varying(250) DEFAULT NULL::character varying,
    lastdata timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    nomefileall character varying(250) NOT NULL
);


ALTER TABLE frontend.allegato OWNER TO postgres;

--
-- Name: allegato_codiceallegato_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.allegato_codiceallegato_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.allegato_codiceallegato_seq OWNER TO postgres;

--
-- Name: allegato_codiceallegato_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.allegato_codiceallegato_seq OWNED BY frontend.allegato.codiceallegato;


--
-- Name: button; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.button (
    id_button integer NOT NULL,
    id_table integer NOT NULL,
    definition text NOT NULL,
    button_type character varying(25) NOT NULL,
    background character varying(7),
    color character varying(7),
    button_name character varying(50) NOT NULL,
    last_data timestamp without time zone DEFAULT now() NOT NULL,
    id_utente integer NOT NULL,
    settings text
);


ALTER TABLE frontend.button OWNER TO postgres;

--
-- Name: button_id_button_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.button_id_button_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.button_id_button_seq OWNER TO postgres;

--
-- Name: button_id_button_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.button_id_button_seq OWNED BY frontend.button.id_button;


--
-- Name: cache_reg; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.cache_reg (
    id integer NOT NULL,
    obj bytea,
    last_update timestamp without time zone
);


ALTER TABLE frontend.cache_reg OWNER TO postgres;

--
-- Name: cache_reg_id_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.cache_reg_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.cache_reg_id_seq OWNER TO postgres;

--
-- Name: cache_reg_id_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.cache_reg_id_seq OWNED BY frontend.cache_reg.id;


--
-- Name: gruppo; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.gruppo (
    gid bigint NOT NULL,
    nome_gruppo character varying(50) NOT NULL,
    descrizione_gruppo text,
    data_gruppo timestamp without time zone DEFAULT now()
);


ALTER TABLE frontend.gruppo OWNER TO postgres;

--
-- Name: COLUMN gruppo.gid; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.gruppo.gid IS 'ID del gruppo';


--
-- Name: COLUMN gruppo.nome_gruppo; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.gruppo.nome_gruppo IS 'Nome del gruppo';


--
-- Name: link; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.link (
    codicelink integer NOT NULL,
    tipoentita character varying(100) DEFAULT NULL::character varying,
    codiceentita character varying(255) DEFAULT NULL::character varying,
    link character varying(250) DEFAULT NULL::character varying,
    descrizione character varying(250) DEFAULT NULL::character varying,
    lastdata timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE frontend.link OWNER TO postgres;

--
-- Name: link_codicelink_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.link_codicelink_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.link_codicelink_seq OWNER TO postgres;

--
-- Name: link_codicelink_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.link_codicelink_seq OWNED BY frontend.link.codicelink;


--
-- Name: log; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.log (
    id_log integer NOT NULL,
    op character varying(20) NOT NULL,
    uid bigint NOT NULL,
    gid bigint NOT NULL,
    data timestamp without time zone DEFAULT now(),
    tabella character varying(100) NOT NULL,
    id_record character varying(100) DEFAULT NULL::character varying,
    storico_pre text,
    storico_post text,
    fonte character(1) DEFAULT 'm'::bpchar NOT NULL,
    info_browser character varying(20) DEFAULT NULL::character varying
);


ALTER TABLE frontend.log OWNER TO postgres;

--
-- Name: log_id_log_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.log_id_log_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.log_id_log_seq OWNER TO postgres;

--
-- Name: log_id_log_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.log_id_log_seq OWNED BY frontend.log.id_log;


--
-- Name: recordlock; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.recordlock (
    tabella character varying(50) NOT NULL,
    colonna character varying(50) NOT NULL,
    id character varying(50) NOT NULL,
    tempo integer NOT NULL
);


ALTER TABLE frontend.recordlock OWNER TO postgres;

--
-- Name: registro_col; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_col (
    id_reg integer NOT NULL,
    id_table bigint,
    gid bigint,
    column_name character varying(255) DEFAULT NULL::character varying,
    ordinal_position smallint,
    column_default character varying(255) DEFAULT NULL::character varying,
    is_nullable character varying(3) DEFAULT NULL::character varying,
    column_type character varying(255) DEFAULT NULL::character varying,
    character_maximum_length integer,
    data_type character varying(255) DEFAULT NULL::character varying,
    extra character varying(200) DEFAULT NULL::character varying,
    in_tipo text,
    in_default text,
    in_visibile smallint DEFAULT 1,
    in_richiesto smallint DEFAULT 0,
    in_suggest smallint DEFAULT 0,
    in_table smallint DEFAULT 1,
    in_line smallint,
    in_ordine smallint DEFAULT 0,
    jstest text,
    commento character varying(255) DEFAULT NULL::character varying,
    alias_frontend character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE frontend.registro_col OWNER TO postgres;

--
-- Name: registro_col_id_reg_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_col_id_reg_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_col_id_reg_seq OWNER TO postgres;

--
-- Name: registro_col_id_reg_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_col_id_reg_seq OWNED BY frontend.registro_col.id_reg;


--
-- Name: registro_submask; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_submask (
    id_submask integer NOT NULL,
    id_table bigint NOT NULL,
    sub_select smallint DEFAULT 0,
    sub_insert smallint DEFAULT 0,
    sub_update smallint DEFAULT 0,
    sub_delete smallint DEFAULT 0,
    nome_tabella character varying(255) DEFAULT NULL::character varying,
    nome_frontend character varying(250) DEFAULT NULL::character varying,
    campo_pk_parent character varying(80) DEFAULT NULL::character varying,
    campo_fk_sub character varying(80) DEFAULT NULL::character varying,
    orderby_sub character varying(80) DEFAULT NULL::character varying,
    orderby_sub_sort character(4) DEFAULT 'ASC'::bpchar,
    data_modifica bigint,
    max_records smallint DEFAULT '10'::smallint,
    tipo_vista character varying(8) DEFAULT 'scheda'::character varying NOT NULL
);


ALTER TABLE frontend.registro_submask OWNER TO postgres;

--
-- Name: registro_submask_col; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_submask_col (
    id_reg_sub integer NOT NULL,
    id_submask bigint NOT NULL,
    column_name character varying(255) DEFAULT NULL::character varying,
    ordinal_position smallint,
    column_default character varying(255) DEFAULT NULL::character varying,
    is_nullable character varying(3) DEFAULT NULL::character varying,
    column_type character varying(255) DEFAULT NULL::character varying,
    character_maximum_length integer,
    data_type character varying(255) DEFAULT NULL::character varying,
    extra character varying(200) DEFAULT NULL::character varying,
    in_tipo text,
    in_default text,
    in_visibile smallint DEFAULT 1,
    in_richiesto smallint DEFAULT 0,
    commento character varying(255) DEFAULT NULL::character varying,
    alias_frontend character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE frontend.registro_submask_col OWNER TO postgres;

--
-- Name: registro_submask_col_id_reg_sub_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_submask_col_id_reg_sub_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_submask_col_id_reg_sub_seq OWNER TO postgres;

--
-- Name: registro_submask_col_id_reg_sub_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_submask_col_id_reg_sub_seq OWNED BY frontend.registro_submask_col.id_reg_sub;


--
-- Name: registro_submask_id_submask_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_submask_id_submask_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_submask_id_submask_seq OWNER TO postgres;

--
-- Name: registro_submask_id_submask_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_submask_id_submask_seq OWNED BY frontend.registro_submask.id_submask;


--
-- Name: registro_tab; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_tab (
    id_table integer NOT NULL,
    gid bigint,
    visibile smallint DEFAULT 0,
    in_insert smallint DEFAULT 0,
    in_duplica smallint DEFAULT 0,
    in_update smallint DEFAULT 0,
    in_delete smallint DEFAULT 0,
    in_export smallint DEFAULT 0,
    in_import smallint DEFAULT 0,
    data_modifica integer DEFAULT 0,
    orderby character varying(255) DEFAULT NULL::character varying,
    table_name character varying(100) DEFAULT NULL::character varying,
    table_type character varying(20) DEFAULT 'BASE TABLE'::character varying,
    commento character varying(255) DEFAULT NULL::character varying,
    orderby_sort character varying(255) DEFAULT 'ASC'::character varying,
    permetti_allegati smallint DEFAULT 0,
    permetti_allegati_ins smallint DEFAULT 0,
    permetti_allegati_del smallint DEFAULT 0,
    permetti_link smallint DEFAULT 0,
    permetti_link_ins smallint DEFAULT 0,
    permetti_link_del smallint DEFAULT 0,
    view_pk character varying(60) DEFAULT NULL::character varying,
    fonte_al character varying(100) DEFAULT NULL::character varying,
    table_alias character varying(100) DEFAULT NULL::character varying,
    allow_filters smallint DEFAULT 0,
    default_view character varying(5) DEFAULT 'form'::character varying,
    default_filters text
);


ALTER TABLE frontend.registro_tab OWNER TO postgres;

--
-- Name: registro_tab_id_table_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_tab_id_table_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_tab_id_table_seq OWNER TO postgres;

--
-- Name: registro_tab_id_table_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_tab_id_table_seq OWNED BY frontend.registro_tab.id_table;


--
-- Name: stat; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.stat (
    id_stat integer NOT NULL,
    nome_stat character varying(250) NOT NULL,
    desc_stat text,
    def_stat text,
    auth_stat smallint DEFAULT 3,
    tipo_graph character varying(8) DEFAULT 'barre'::character varying,
    data_stat timestamp without time zone DEFAULT now() NOT NULL,
    autore bigint NOT NULL,
    settings text,
    published smallint DEFAULT 0 NOT NULL
);


ALTER TABLE frontend.stat OWNER TO postgres;

--
-- Name: stat_id_stat_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.stat_id_stat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.stat_id_stat_seq OWNER TO postgres;

--
-- Name: stat_id_stat_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.stat_id_stat_seq OWNED BY frontend.stat.id_stat;


--
-- Name: utente; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.utente (
    id_utente integer NOT NULL,
    nick character varying(80) DEFAULT NULL::character varying,
    passwd character(32) DEFAULT NULL::bpchar,
    nome character varying(50) DEFAULT NULL::character varying,
    cognome character varying(50) DEFAULT NULL::character varying,
    email character varying(80) DEFAULT NULL::character varying,
    info text,
    data_ins date DEFAULT now(),
    gid bigint DEFAULT '0'::bigint NOT NULL,
    livello smallint DEFAULT '1'::smallint NOT NULL,
    recover_passwd character varying(32) DEFAULT NULL::character varying
);


ALTER TABLE frontend.utente OWNER TO postgres;

--
-- Name: utente_id_utente_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.utente_id_utente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.utente_id_utente_seq OWNER TO postgres;

--
-- Name: utente_id_utente_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.utente_id_utente_seq OWNED BY frontend.utente.id_utente;


--
-- Name: variabili; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.variabili (
    variabile character varying(32) NOT NULL,
    gid bigint DEFAULT 0 NOT NULL,
    valore character varying(255) DEFAULT NULL::character varying,
    descrizione character varying(255) DEFAULT NULL::character varying,
    tipo_var character varying(20) DEFAULT NULL::character varying,
    pubvar smallint DEFAULT 1 NOT NULL
);


ALTER TABLE frontend.variabili OWNER TO postgres;

--
-- Name: widget; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.widget (
    id_widget integer NOT NULL,
    id_table integer NOT NULL,
    widget_name character varying(255) DEFAULT ''::character varying NOT NULL,
    form_position character varying(11) DEFAULT '0'::character varying NOT NULL,
    widget_type character varying(100) DEFAULT ''::character varying NOT NULL,
    settings text NOT NULL
);


ALTER TABLE frontend.widget OWNER TO postgres;

--
-- Name: widget_id_widget_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.widget_id_widget_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.widget_id_widget_seq OWNER TO postgres;

--
-- Name: widget_id_widget_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.widget_id_widget_seq OWNED BY frontend.widget.id_widget;


--
-- Name: xml_rules; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.xml_rules (
    id_xml_rules integer NOT NULL,
    tabella character varying(50) NOT NULL,
    accesso character varying(20) DEFAULT 'RESTRICT'::character varying,
    accesso_gruppo character varying(100),
    autore integer,
    lastdata timestamp without time zone DEFAULT now(),
    xsl character varying(80),
    xslfo character varying(80),
    tipo_report character(1) DEFAULT 't'::bpchar,
    def_query text,
    nome_report character varying(255)
);


ALTER TABLE frontend.xml_rules OWNER TO postgres;

--
-- Name: COLUMN xml_rules.accesso; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.xml_rules.accesso IS 'RESTRICT,PUBLIC,FRONTEND,GROUP';


--
-- Name: COLUMN xml_rules.tipo_report; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.xml_rules.tipo_report IS 'Report basato su tabella (t) o su query (q)';


--
-- Name: COLUMN xml_rules.def_query; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.xml_rules.def_query IS 'Query di definizione per i report di tipo "query"';


--
-- Name: xml_rules_id_xml_rules_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.xml_rules_id_xml_rules_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.xml_rules_id_xml_rules_seq OWNER TO postgres;

--
-- Name: xml_rules_id_xml_rules_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.xml_rules_id_xml_rules_seq OWNED BY frontend.xml_rules.id_xml_rules;


--
-- Name: allegato codiceallegato; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.allegato ALTER COLUMN codiceallegato SET DEFAULT nextval('frontend.allegato_codiceallegato_seq'::regclass);


--
-- Name: button id_button; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.button ALTER COLUMN id_button SET DEFAULT nextval('frontend.button_id_button_seq'::regclass);


--
-- Name: cache_reg id; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.cache_reg ALTER COLUMN id SET DEFAULT nextval('frontend.cache_reg_id_seq'::regclass);


--
-- Name: link codicelink; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.link ALTER COLUMN codicelink SET DEFAULT nextval('frontend.link_codicelink_seq'::regclass);


--
-- Name: log id_log; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.log ALTER COLUMN id_log SET DEFAULT nextval('frontend.log_id_log_seq'::regclass);


--
-- Name: registro_col id_reg; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_col ALTER COLUMN id_reg SET DEFAULT nextval('frontend.registro_col_id_reg_seq'::regclass);


--
-- Name: registro_submask id_submask; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask ALTER COLUMN id_submask SET DEFAULT nextval('frontend.registro_submask_id_submask_seq'::regclass);


--
-- Name: registro_submask_col id_reg_sub; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask_col ALTER COLUMN id_reg_sub SET DEFAULT nextval('frontend.registro_submask_col_id_reg_sub_seq'::regclass);


--
-- Name: registro_tab id_table; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_tab ALTER COLUMN id_table SET DEFAULT nextval('frontend.registro_tab_id_table_seq'::regclass);


--
-- Name: stat id_stat; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.stat ALTER COLUMN id_stat SET DEFAULT nextval('frontend.stat_id_stat_seq'::regclass);


--
-- Name: utente id_utente; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.utente ALTER COLUMN id_utente SET DEFAULT nextval('frontend.utente_id_utente_seq'::regclass);


--
-- Name: widget id_widget; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.widget ALTER COLUMN id_widget SET DEFAULT nextval('frontend.widget_id_widget_seq'::regclass);


--
-- Name: xml_rules id_xml_rules; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.xml_rules ALTER COLUMN id_xml_rules SET DEFAULT nextval('frontend.xml_rules_id_xml_rules_seq'::regclass);


--
-- Name: fcommenti codice_totale_commento_unique; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcommenti
    ADD CONSTRAINT codice_totale_commento_unique UNIQUE (fnperiod, ftabcatego, fnloc, feffetti);


--
-- Name: fclassif fclassif_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fclassif
    ADD CONSTRAINT fclassif_pkey PRIMARY KEY (id);


--
-- Name: fcodric fcod_ric_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcodric
    ADD CONSTRAINT fcod_ric_pkey PRIMARY KEY (id);


--
-- Name: fcommenti fcommenti_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcommenti
    ADD CONSTRAINT fcommenti_pkey PRIMARY KEY (id);


--
-- Name: fdemo fdemo_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fdemo
    ADD CONSTRAINT fdemo_pkey PRIMARY KEY (id);


--
-- Name: fedifici fedifici_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fedifici
    ADD CONSTRAINT fedifici_pkey PRIMARY KEY (id);


--
-- Name: feffetti feffetti_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.feffetti
    ADD CONSTRAINT feffetti_pkey PRIMARY KEY (id);


--
-- Name: ffontidemo ffontidemo_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ffontidemo
    ADD CONSTRAINT ffontidemo_pkey PRIMARY KEY (id);


--
-- Name: ficon ficon_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_pkey PRIMARY KEY (id);


--
-- Name: fnfiu fnfiu_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fnfiu
    ADD CONSTRAINT fnfiu_pkey PRIMARY KEY (id);


--
-- Name: fnloc2 fnloc2_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: postgres
--

ALTER TABLE ONLY cfti5.fnloc2
    ADD CONSTRAINT fnloc2_pkey PRIMARY KEY (id);


--
-- Name: fnloc fnloc_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fnloc
    ADD CONSTRAINT fnloc_pkey PRIMARY KEY (id);


--
-- Name: fnperiod fnperiod_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fnperiod
    ADD CONSTRAINT fnperiod_pkey PRIMARY KEY (id);


--
-- Name: fpiaquo fpiaquo_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fpiaquo
    ADD CONSTRAINT fpiaquo_pkey PRIMARY KEY (id);


--
-- Name: fscheda1 fscheda1_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscheda1
    ADD CONSTRAINT fscheda1_pkey PRIMARY KEY (id);


--
-- Name: fscheda fscheda_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscheda
    ADD CONSTRAINT fscheda_pkey PRIMARY KEY (id);


--
-- Name: fschedac fschedac_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: postgres
--

ALTER TABLE ONLY cfti5.fschedac
    ADD CONSTRAINT fschedac_pkey PRIMARY KEY (id);


--
-- Name: fschedad fschedad_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_pkey PRIMARY KEY (id);


--
-- Name: fschedae fschedae_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_pkey PRIMARY KEY (id);


--
-- Name: fschedeb fschedeb_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_pkey PRIMARY KEY (id);


--
-- Name: fscossec fscossec_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscossec
    ADD CONSTRAINT fscossec_pkey PRIMARY KEY (id);


--
-- Name: ftabcatego ftabcatego_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ftabcatego
    ADD CONSTRAINT ftabcatego_pkey PRIMARY KEY (id);


--
-- Name: ftipfen ftipfen_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ftipfen
    ADD CONSTRAINT ftipfen_pkey PRIMARY KEY (id);


--
-- Name: fvalfonte fval_fonte_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fvalfonte
    ADD CONSTRAINT fval_fonte_pkey PRIMARY KEY (id);


--
-- Name: seq_loc seq_loc_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: postgres
--

ALTER TABLE ONLY cfti5.seq_loc
    ADD CONSTRAINT seq_loc_pkey PRIMARY KEY (id);


--
-- Name: seq_scosse seq_scosse_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: postgres
--

ALTER TABLE ONLY cfti5.seq_scosse
    ADD CONSTRAINT seq_scosse_pkey PRIMARY KEY (id);


--
-- Name: test3 test3_pkey; Type: CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.test3
    ADD CONSTRAINT test3_pkey PRIMARY KEY (id);


--
-- Name: biblio_ee biblio_ee_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.biblio_ee
    ADD CONSTRAINT biblio_ee_pkey PRIMARY KEY (id);


--
-- Name: codbib_list_ee codbib_list_ee_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.codbib_list_ee
    ADD CONSTRAINT codbib_list_ee_pkey PRIMARY KEY (id);


--
-- Name: ee_med ee_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.ee_med
    ADD CONSTRAINT ee_med_pkey PRIMARY KEY (id);


--
-- Name: ee_tabella ee_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.ee_tabella
    ADD CONSTRAINT ee_pkey PRIMARY KEY (id);


--
-- Name: fee_med fee_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fee_med
    ADD CONSTRAINT fee_med_pkey PRIMARY KEY (id);


--
-- Name: fee_med_total fee_med_total_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fee_med_total
    ADD CONSTRAINT fee_med_total_pkey PRIMARY KEY (id);


--
-- Name: fnloc_cfti_5 fnloc_cfti_5_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fnloc_cfti_5
    ADD CONSTRAINT fnloc_cfti_5_pkey PRIMARY KEY (id);


--
-- Name: fnloc_cfti fnloc_cfti_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fnloc_cfti
    ADD CONSTRAINT fnloc_cfti_pkey PRIMARY KEY (id);


--
-- Name: fnperiod_cfti fnperiod_cfti_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fnperiod_cfti
    ADD CONSTRAINT fnperiod_cfti_pkey PRIMARY KEY (id);


--
-- Name: fpq_med fpq_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fpq_med
    ADD CONSTRAINT fpq_med_pkey PRIMARY KEY (id);


--
-- Name: fpq_med_total fpq_med_total_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.fpq_med_total
    ADD CONSTRAINT fpq_med_total_pkey PRIMARY KEY (id);


--
-- Name: locind_med locind_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.locind_med
    ADD CONSTRAINT locind_med_pkey PRIMARY KEY (id);


--
-- Name: nloc_med nloc_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.nloc_med
    ADD CONSTRAINT nloc_med_pkey PRIMARY KEY (id);


--
-- Name: nterrs_med nterrs_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.nterrs_med
    ADD CONSTRAINT nterrs_med_pkey PRIMARY KEY (id);


--
-- Name: nterrs nterrs_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.nterrs
    ADD CONSTRAINT nterrs_pkey PRIMARY KEY (id);


--
-- Name: pq_med pq_med_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.pq_med
    ADD CONSTRAINT pq_med_pkey PRIMARY KEY (id);


--
-- Name: schedec schedec_pkey; Type: CONSTRAINT; Schema: cfti5web; Owner: utente
--

ALTER TABLE ONLY cfti5web.schedec
    ADD CONSTRAINT schedec_pkey PRIMARY KEY (id);


--
-- Name: button button_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.button
    ADD CONSTRAINT button_pkey PRIMARY KEY (id_button);


--
-- Name: cache_reg cache_reg_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.cache_reg
    ADD CONSTRAINT cache_reg_pkey PRIMARY KEY (id);


--
-- Name: allegato pk_allegato; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.allegato
    ADD CONSTRAINT pk_allegato PRIMARY KEY (codiceallegato);


--
-- Name: gruppo pk_gruppo; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.gruppo
    ADD CONSTRAINT pk_gruppo PRIMARY KEY (gid);


--
-- Name: registro_tab pk_id_table; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_tab
    ADD CONSTRAINT pk_id_table PRIMARY KEY (id_table);


--
-- Name: link pk_link; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.link
    ADD CONSTRAINT pk_link PRIMARY KEY (codicelink);


--
-- Name: log pk_log; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.log
    ADD CONSTRAINT pk_log PRIMARY KEY (id_log);


--
-- Name: recordlock pk_recordlock; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.recordlock
    ADD CONSTRAINT pk_recordlock PRIMARY KEY (tabella, colonna, id);


--
-- Name: registro_col pk_registro_col; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_col
    ADD CONSTRAINT pk_registro_col PRIMARY KEY (id_reg);


--
-- Name: registro_submask pk_registro_submask; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask
    ADD CONSTRAINT pk_registro_submask PRIMARY KEY (id_submask);


--
-- Name: registro_submask_col pk_registro_submask_col; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask_col
    ADD CONSTRAINT pk_registro_submask_col PRIMARY KEY (id_reg_sub);


--
-- Name: stat pk_stat; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.stat
    ADD CONSTRAINT pk_stat PRIMARY KEY (id_stat);


--
-- Name: utente pk_utente; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.utente
    ADD CONSTRAINT pk_utente PRIMARY KEY (id_utente);


--
-- Name: variabili pk_variabili; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.variabili
    ADD CONSTRAINT pk_variabili PRIMARY KEY (variabile, gid);


--
-- Name: xml_rules pk_xml_rules; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.xml_rules
    ADD CONSTRAINT pk_xml_rules PRIMARY KEY (id_xml_rules);


--
-- Name: gruppo u_gruppo_nome_gruppo; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.gruppo
    ADD CONSTRAINT u_gruppo_nome_gruppo UNIQUE (nome_gruppo);


--
-- Name: widget widget_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.widget
    ADD CONSTRAINT widget_pkey PRIMARY KEY (id_widget);


--
-- Name: fnloc2_wkb_geometry_geometry_idx; Type: INDEX; Schema: cfti5; Owner: postgres
--

CREATE INDEX fnloc2_wkb_geometry_geometry_idx ON cfti5.fnloc2 USING gist (geometry);


--
-- Name: fscossec2_wkb_geometry_geometry_idx; Type: INDEX; Schema: cfti5; Owner: postgres
--

CREATE INDEX fscossec2_wkb_geometry_geometry_idx ON cfti5.fscossec2 USING gist (geometry);


--
-- Name: idx_biblio_details_codbib_nperiod; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_biblio_details_codbib_nperiod ON cfti5web.biblio_details USING btree (codbib, nperiod);


--
-- Name: idx_commgen_nperiod_cfti; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_commgen_nperiod_cfti ON cfti5web.commgen USING btree (nperiod_cfti);


--
-- Name: idx_d1_nperiod_cfti; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_d1_nperiod_cfti ON cfti5web.d1 USING btree (nperiod_cfti);


--
-- Name: idx_pq_med_nterrs; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_pq_med_nterrs ON cfti5web.pq_med USING btree (nterr);


--
-- Name: idx_pq_nterr; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_pq_nterr ON cfti5web.pq USING btree (nterrs);


--
-- Name: idx_quake_details_nloc_cfti; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_quake_details_nloc_cfti ON cfti5web.quake_details USING btree (nloc_cfti);


--
-- Name: idx_schedea_codbib; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_schedea_codbib ON cfti5web.schedea USING btree (codbib);


--
-- Name: idx_schedeb_nperiod; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_schedeb_nperiod ON cfti5web.schedeb USING btree (nperiod);


--
-- Name: idx_schedec_nloc; Type: INDEX; Schema: cfti5web; Owner: utente
--

CREATE INDEX idx_schedec_nloc ON cfti5web.schedec USING btree (nloc);


--
-- Name: i_autore_stat; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_autore_stat ON frontend.stat USING btree (autore);


--
-- Name: i_button_id_table; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_button_id_table ON frontend.button USING btree (id_table);


--
-- Name: i_gid; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_gid ON frontend.registro_tab USING btree (gid);


--
-- Name: i_id_submask; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_id_submask ON frontend.registro_submask_col USING btree (id_submask);


--
-- Name: i_id_table; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_id_table ON frontend.registro_col USING btree (id_table);


--
-- Name: i_registro_col_gid; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_registro_col_gid ON frontend.registro_col USING btree (gid);


--
-- Name: i_table_name; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_table_name ON frontend.registro_tab USING btree (table_name);


--
-- Name: i_utente_gid; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_utente_gid ON frontend.utente USING btree (gid);


--
-- Name: i_variabile; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_variabile ON frontend.variabili USING btree (variabile);


--
-- Name: i_widget_id_table; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_widget_id_table ON frontend.widget USING btree (id_table);


--
-- Name: vcommenti vcommenti_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vcommenti_insert AS
    ON INSERT TO cfti5.vcommenti DO INSTEAD  INSERT INTO cfti5.fcommenti (testocomm)
  VALUES (new.testo_commento);


--
-- Name: vcommenti vcommenti_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vcommenti_update AS
    ON UPDATE TO cfti5.vcommenti DO INSTEAD  UPDATE cfti5.fcommenti SET testocomm = new.testo_commento
  WHERE (fcommenti.id = old.id);


--
-- Name: vdemo vdemo_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vdemo_insert AS
    ON INSERT TO cfti5.vdemo DO INSTEAD  INSERT INTO cfti5.fdemo (nc, attf, daldem, aldem, attd, demog)
  VALUES (new.nc, new.attf, new.daldem, new.aldem, new.attd, new.demog);


--
-- Name: vdemo vdemo_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vdemo_update AS
    ON UPDATE TO cfti5.vdemo DO INSTEAD  UPDATE cfti5.fdemo SET nc = new.nc, attf = new.attf, daldem = new.daldem, aldem = new.aldem, attd = new.attd, demog = new.demog
  WHERE (fdemo.id = old.id);


--
-- Name: vedifici vedifici_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vedifici_insert AS
    ON INSERT TO cfti5.vedifici DO INSTEAD  INSERT INTO cfti5.fedifici (classed, codicedif, descrizedif, noted)
  VALUES (new.classificazione_edificio, new.codice_edificio, new.nome_edificio, new.note_edificio);


--
-- Name: vedifici vedifici_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vedifici_update AS
    ON UPDATE TO cfti5.vedifici DO INSTEAD  UPDATE cfti5.fedifici SET classed = new.classificazione_edificio, codicedif = new.codice_edificio, descrizedif = new.nome_edificio, noted = new.note_edificio
  WHERE (fedifici.id = old.id);


--
-- Name: vicon vicon_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vicon_insert AS
    ON INSERT TO cfti5.vicon DO INSTEAD  INSERT INTO cfti5.ficon (codico, didasico, codtotico, fotoinc, noteico)
  VALUES (new.codico, new.didasico, new.codtotico, new.fotoinc, new.noteico);


--
-- Name: vicon vicon_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vicon_update AS
    ON UPDATE TO cfti5.vicon DO INSTEAD  UPDATE cfti5.ficon SET codico = new.codico, didasico = new.didasico, codtotico = new.codtotico, fotoinc = new.fotoinc, noteico = new.noteico
  WHERE (ficon.id = old.id);


--
-- Name: vscheda1 vscheda1_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vscheda1_insert AS
    ON INSERT TO cfti5.vscheda1 DO INSTEAD  INSERT INTO cfti5.fscheda1 (coda1, desa1, inda1, capa1, loca1, prova1, tela1, notea1)
  VALUES (new.codice_sede_ricerca, new.sede_ricerca, new.indirizzo, new.cap, new.localita, new.provincia, new.telefono, new.note);


--
-- Name: vscheda1 vscheda1_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vscheda1_update AS
    ON UPDATE TO cfti5.vscheda1 DO INSTEAD  UPDATE cfti5.fscheda1 SET coda1 = new.codice_sede_ricerca, desa1 = new.sede_ricerca, inda1 = new.indirizzo, capa1 = new.cap, loca1 = new.localita, prova1 = new.provincia, tela1 = new.telefono, notea1 = new.note
  WHERE (fscheda1.id = old.id);


--
-- Name: vscheda vscheda_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vscheda_insert AS
    ON INSERT TO cfti5.vscheda DO INSTEAD  INSERT INTO cfti5.fscheda (codbib, codric, autore1, editaut, titolo1, ruolo, dalaut, alaut, daltit, altit, rigeaut, rigetit, luogoed, datauni, dataun2, fotiniz, codmicrenel, codmicr, prota, colloca, tipoa, ordinam, notea)
  VALUES (new.codice_bibliografico, new.codice_ricerca, new.autore, new.tipo_autore, new.titolo, new.ruolo_autore, new.ruolo_autore_da, new.ruolo_autore_a, new.daltit, new.altit, new.riferimento_geografico, new.riferimento_geografico_titolo, new.luogo_edizione, new.data_edizione_ms1, new.data_edizione_ms2, new.fotogramma_iniziale, new.codice_microfilm_enel, new.codice_microfilm, new.prota, new.colloca, new.tipoa, new.ordinam, new.notea);


--
-- Name: vscheda vscheda_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vscheda_update AS
    ON UPDATE TO cfti5.vscheda DO INSTEAD  UPDATE cfti5.fscheda SET codbib = new.codice_bibliografico, codric = new.codice_ricerca, autore1 = new.autore, editaut = new.tipo_autore, titolo1 = new.titolo, ruolo = new.ruolo_autore, dalaut = new.ruolo_autore_da, alaut = new.ruolo_autore_a, daltit = new.daltit, altit = new.altit, rigeaut = new.riferimento_geografico, rigetit = new.riferimento_geografico_titolo, luogoed = new.luogo_edizione, datauni = new.data_edizione_ms1, dataun2 = new.data_edizione_ms2, fotiniz = new.fotogramma_iniziale, codmicrenel = new.codice_microfilm_enel, codmicr = new.codice_microfilm, prota = new.prota, colloca = new.colloca, tipoa = new.tipoa, ordinam = new.ordinam, notea = new.notea
  WHERE (fscheda.id = old.id);


--
-- Name: vschedad vschedad_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vschedad_insert AS
    ON INSERT TO cfti5.vschedad DO INSTEAD  INSERT INTO cfti5.fschedad (intensd, codtotd, flagintpq, testod)
  VALUES (new.intensd, new.codtotd, new.flagintpq, new.testod);


--
-- Name: vschedad vschedad_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vschedad_update AS
    ON UPDATE TO cfti5.vschedad DO INSTEAD  UPDATE cfti5.fschedad SET intensd = new.intensd, codtotd = new.codtotd, flagintpq = new.flagintpq, testod = new.testod
  WHERE (fschedad.id = old.id);


--
-- Name: vschedeb vschedeb_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vschedeb_insert AS
    ON INSERT TO cfti5.vschedeb DO INSTEAD  INSERT INTO cfti5.fschedeb (testob, noteb, dab, ab, datab)
  VALUES (new.testo_trascritto, new.note, new.data_evento_descritto, new.data_evento_descritto_al, new.data_evento_descritto_dal);


--
-- Name: vschedeb vschedeb_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vschedeb_update AS
    ON UPDATE TO cfti5.vschedeb DO INSTEAD  UPDATE cfti5.fschedeb SET testob = new.testo_trascritto, noteb = new.note, dab = new.data_evento_descritto, ab = new.data_evento_descritto_al, datab = new.data_evento_descritto_dal
  WHERE (fschedeb.id = old.id);


--
-- Name: vscossec vscossec_insert; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vscossec_insert AS
    ON INSERT TO cfti5.vscossec DO INSTEAD  INSERT INTO cfti5.fscossec (nterrs, dataors, oraorgs, latitus, longits, latmacs, lonmacs, loceps, npunpqs, profsts, ios, imaxs, lat_centesimale, lon_centesimale)
  VALUES (new.numero_terremoto, new.data_origine, new.orario_origine, new.lat_epic_strum, new.lon_epic_strum, new.lat_epic_macro, new.lon_epic_macro, new.zona_epicentrale, new.n_punti_pq, new."profondità", new.io, new.imax, new.lat_centesimale, new.lon_centesimale);


--
-- Name: vscossec vscossec_update; Type: RULE; Schema: cfti5; Owner: postgres
--

CREATE RULE vscossec_update AS
    ON UPDATE TO cfti5.vscossec DO INSTEAD  UPDATE cfti5.fscossec SET nterrs = new.numero_terremoto, dataors = new.data_origine, oraorgs = new.orario_origine, latitus = new.lat_epic_strum, longits = new.lon_epic_strum, latmacs = new.lat_epic_macro, lonmacs = new.lon_epic_macro, loceps = new.zona_epicentrale, npunpqs = new.n_punti_pq, profsts = new."profondità", ios = new.io, imaxs = new.imax, lat_centesimale = new.lat_centesimale, lon_centesimale = new.lon_centesimale
  WHERE (fscossec.id = old.id);


--
-- Name: fnloc lat_centesimale; Type: TRIGGER; Schema: cfti5; Owner: utente
--

CREATE TRIGGER lat_centesimale BEFORE INSERT OR UPDATE OF latloc ON cfti5.fnloc FOR EACH ROW EXECUTE PROCEDURE cfti5.coordinate_centesimali();


--
-- Name: fnloc lon_centesimale; Type: TRIGGER; Schema: cfti5; Owner: utente
--

CREATE TRIGGER lon_centesimale BEFORE INSERT OR UPDATE OF longlog ON cfti5.fnloc FOR EACH ROW EXECUTE PROCEDURE cfti5.coordinate_centesimali_lon();


--
-- Name: fscheda ordinam; Type: TRIGGER; Schema: cfti5; Owner: utente
--

CREATE TRIGGER ordinam BEFORE INSERT OR UPDATE OF autore1, titolo1 ON cfti5.fscheda FOR EACH ROW EXECUTE PROCEDURE cfti5.genera_ordinam();

ALTER TABLE cfti5.fscheda DISABLE TRIGGER ordinam;


--
-- Name: fnloc z_campo_geometry; Type: TRIGGER; Schema: cfti5; Owner: utente
--

CREATE TRIGGER z_campo_geometry BEFORE INSERT OR UPDATE OF latloc, longlog ON cfti5.fnloc FOR EACH ROW EXECUTE PROCEDURE cfti5.fnloc_geometry();


--
-- Name: fscossec z_campo_geometry; Type: TRIGGER; Schema: cfti5; Owner: utente
--

CREATE TRIGGER z_campo_geometry BEFORE INSERT OR UPDATE OF latmacs, lonmacs ON cfti5.fscossec FOR EACH ROW EXECUTE PROCEDURE cfti5.fscossec_geometry();


--
-- Name: fcommenti fcommenti_feffetti_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcommenti
    ADD CONSTRAINT fcommenti_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES cfti5.feffetti(id);


--
-- Name: fcommenti fcommenti_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcommenti
    ADD CONSTRAINT fcommenti_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: fcommenti fcommenti_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcommenti
    ADD CONSTRAINT fcommenti_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: fcommenti fcommenti_ftabcatego_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fcommenti
    ADD CONSTRAINT fcommenti_ftabcatego_fkey FOREIGN KEY (ftabcatego) REFERENCES cfti5.ftabcatego(id);


--
-- Name: fdemo fdemo_ffontidemo_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fdemo
    ADD CONSTRAINT fdemo_ffontidemo_fkey FOREIGN KEY (ffontidemo) REFERENCES cfti5.ffontidemo(id);


--
-- Name: fdemo fdemo_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fdemo
    ADD CONSTRAINT fdemo_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: fdemo fdemo_fscheda_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fdemo
    ADD CONSTRAINT fdemo_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES cfti5.fscheda(id);


--
-- Name: fedifici fedifici_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fedifici
    ADD CONSTRAINT fedifici_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: ficon ficon_fedifici_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_fedifici_fkey FOREIGN KEY (fedifici) REFERENCES cfti5.fedifici(id);


--
-- Name: ficon ficon_feffetti_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES cfti5.feffetti(id);


--
-- Name: ficon ficon_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: ficon ficon_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: ficon ficon_fscheda_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES cfti5.fscheda(id);


--
-- Name: ficon ficon_fschedeb_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_fschedeb_fkey FOREIGN KEY (fschedeb) REFERENCES cfti5.fschedeb(id);


--
-- Name: ficon ficon_ftipfen_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.ficon
    ADD CONSTRAINT ficon_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES cfti5.ftipfen(id);


--
-- Name: fnperiod fnperiod_feffetti_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fnperiod
    ADD CONSTRAINT fnperiod_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES cfti5.feffetti(id);


--
-- Name: fnperiod fnperiod_ftipfen_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fnperiod
    ADD CONSTRAINT fnperiod_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES cfti5.ftipfen(id);


--
-- Name: fpiaquo fpiaquo_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fpiaquo
    ADD CONSTRAINT fpiaquo_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: fpiaquo fpiaquo_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fpiaquo
    ADD CONSTRAINT fpiaquo_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: fpiaquo fpiaquo_fscossec_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fpiaquo
    ADD CONSTRAINT fpiaquo_fscossec_fkey FOREIGN KEY (fscossec) REFERENCES cfti5.fscossec(id);


--
-- Name: fscheda1 fscheda1_id_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscheda1
    ADD CONSTRAINT fscheda1_id_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: fscheda fscheda_fclassif_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscheda
    ADD CONSTRAINT fscheda_fclassif_fkey FOREIGN KEY (fclassif) REFERENCES cfti5.fclassif(id);


--
-- Name: fscheda fscheda_fscheda1_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscheda
    ADD CONSTRAINT fscheda_fscheda1_fkey FOREIGN KEY (fscheda1) REFERENCES cfti5.fscheda1(id);


--
-- Name: fschedad fschedad_feffetti_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES cfti5.feffetti(id);


--
-- Name: fschedad fschedad_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: fschedad fschedad_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: fschedad fschedad_fpiaquo_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_fpiaquo_fkey FOREIGN KEY (fpiaquo) REFERENCES cfti5.fpiaquo(id);


--
-- Name: fschedad fschedad_fscheda_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES cfti5.fscheda(id);


--
-- Name: fschedad fschedad_fschedeb_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_fschedeb_fkey FOREIGN KEY (fschedeb) REFERENCES cfti5.fschedeb(id);


--
-- Name: fschedad fschedad_fscossec_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_fscossec_fkey FOREIGN KEY (fscossec) REFERENCES cfti5.fscossec(id);


--
-- Name: fschedad fschedad_ftipfen_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedad
    ADD CONSTRAINT fschedad_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES cfti5.ftipfen(id);


--
-- Name: fschedae fschedae_fedifici_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_fedifici_fkey FOREIGN KEY (fedifici) REFERENCES cfti5.fedifici(id);


--
-- Name: fschedae fschedae_feffetti_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES cfti5.feffetti(id);


--
-- Name: fschedae fschedae_fnloc_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES cfti5.fnloc(id);


--
-- Name: fschedae fschedae_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: fschedae fschedae_fscheda_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES cfti5.fscheda(id);


--
-- Name: fschedae fschedae_fschedeb_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_fschedeb_fkey FOREIGN KEY (fschedeb) REFERENCES cfti5.fschedeb(id);


--
-- Name: fschedae fschedae_ftipfen_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedae
    ADD CONSTRAINT fschedae_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES cfti5.ftipfen(id);


--
-- Name: fschedeb fschedeb_feffetti_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES cfti5.feffetti(id);


--
-- Name: fschedeb fschedeb_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: fschedeb fschedeb_fscheda1_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_fscheda1_fkey FOREIGN KEY (fscheda1) REFERENCES cfti5.fscheda1(id);


--
-- Name: fschedeb fschedeb_fscheda_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES cfti5.fscheda(id);


--
-- Name: fschedeb fschedeb_ftipfen_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES cfti5.ftipfen(id);


--
-- Name: fschedeb fschedeb_fvalfonte_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fschedeb
    ADD CONSTRAINT fschedeb_fvalfonte_fkey FOREIGN KEY (fvalfonte) REFERENCES cfti5.fvalfonte(id);


--
-- Name: fscossec fscossec_fnperiod_fkey; Type: FK CONSTRAINT; Schema: cfti5; Owner: utente
--

ALTER TABLE ONLY cfti5.fscossec
    ADD CONSTRAINT fscossec_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES cfti5.fnperiod(id);


--
-- Name: button fk_button_id_table; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.button
    ADD CONSTRAINT fk_button_id_table FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registro_col fk_registro_col_1; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_col
    ADD CONSTRAINT fk_registro_col_1 FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON DELETE CASCADE;


--
-- Name: registro_submask fk_registro_submask; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask
    ADD CONSTRAINT fk_registro_submask FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON DELETE CASCADE;


--
-- Name: registro_submask_col fk_registro_submask_col; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask_col
    ADD CONSTRAINT fk_registro_submask_col FOREIGN KEY (id_submask) REFERENCES frontend.registro_submask(id_submask) ON DELETE CASCADE;


--
-- Name: utente fk_utente; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.utente
    ADD CONSTRAINT fk_utente FOREIGN KEY (gid) REFERENCES frontend.gruppo(gid) ON UPDATE CASCADE;


--
-- Name: widget fk_widget_id_table; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.widget
    ADD CONSTRAINT fk_widget_id_table FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registro_tab gid; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_tab
    ADD CONSTRAINT gid FOREIGN KEY (gid) REFERENCES frontend.gruppo(gid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA cfti5; Type: ACL; Schema: -; Owner: utente
--

REVOKE ALL ON SCHEMA cfti5 FROM postgres;
REVOKE ALL ON SCHEMA cfti5 FROM PUBLIC;
GRANT ALL ON SCHEMA cfti5 TO utente;
GRANT ALL ON SCHEMA cfti5 TO PUBLIC;


--
-- Name: SCHEMA frontend; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA frontend TO utente;


--
-- Name: SCHEMA pg_catalog; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA pg_catalog TO utente;


--
-- Name: FUNCTION raster_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_in(cstring) TO utente;


--
-- Name: FUNCTION raster_out(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_out(cfti5.raster) TO utente;


--
-- Name: FUNCTION box2d_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box2d_in(cstring) TO utente;


--
-- Name: FUNCTION box2d_out(cfti5.box2d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box2d_out(cfti5.box2d) TO utente;


--
-- Name: FUNCTION box2df_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box2df_in(cstring) TO utente;


--
-- Name: FUNCTION box2df_out(cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box2df_out(cfti5.box2df) TO utente;


--
-- Name: FUNCTION box3d_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box3d_in(cstring) TO utente;


--
-- Name: FUNCTION box3d_out(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box3d_out(cfti5.box3d) TO utente;


--
-- Name: FUNCTION geography_analyze(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_analyze(internal) TO utente;


--
-- Name: FUNCTION geography_in(cstring, oid, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_in(cstring, oid, integer) TO utente;


--
-- Name: FUNCTION geography_out(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_out(cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_recv(internal, oid, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_recv(internal, oid, integer) TO utente;


--
-- Name: FUNCTION geography_send(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_send(cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_typmod_in(cstring[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_typmod_in(cstring[]) TO utente;


--
-- Name: FUNCTION geography_typmod_out(integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_typmod_out(integer) TO utente;


--
-- Name: FUNCTION geometry_analyze(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_analyze(internal) TO utente;


--
-- Name: FUNCTION geometry_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_in(cstring) TO utente;


--
-- Name: FUNCTION geometry_out(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_out(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_recv(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_recv(internal) TO utente;


--
-- Name: FUNCTION geometry_send(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_send(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_typmod_in(cstring[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_typmod_in(cstring[]) TO utente;


--
-- Name: FUNCTION geometry_typmod_out(integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_typmod_out(integer) TO utente;


--
-- Name: FUNCTION gidx_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gidx_in(cstring) TO utente;


--
-- Name: FUNCTION gidx_out(cfti5.gidx); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gidx_out(cfti5.gidx) TO utente;


--
-- Name: FUNCTION spheroid_in(cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.spheroid_in(cstring) TO utente;


--
-- Name: FUNCTION spheroid_out(cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.spheroid_out(cfti5.spheroid) TO utente;


--
-- Name: FUNCTION box3d(cfti5.box2d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box3d(cfti5.box2d) TO utente;


--
-- Name: FUNCTION geometry(cfti5.box2d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(cfti5.box2d) TO utente;


--
-- Name: FUNCTION box(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box(cfti5.box3d) TO utente;


--
-- Name: FUNCTION box2d(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box2d(cfti5.box3d) TO utente;


--
-- Name: FUNCTION geometry(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(cfti5.box3d) TO utente;


--
-- Name: FUNCTION geography(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography(bytea) TO utente;


--
-- Name: FUNCTION geometry(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(bytea) TO utente;


--
-- Name: FUNCTION bytea(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.bytea(cfti5.geography) TO utente;


--
-- Name: FUNCTION geography(cfti5.geography, integer, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography(cfti5.geography, integer, boolean) TO utente;


--
-- Name: FUNCTION geometry(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(cfti5.geography) TO utente;


--
-- Name: FUNCTION box(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box(cfti5.geometry) TO utente;


--
-- Name: FUNCTION box2d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box2d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION box3d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box3d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION bytea(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.bytea(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geography(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry(cfti5.geometry, integer, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(cfti5.geometry, integer, boolean) TO utente;


--
-- Name: FUNCTION path(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.path(cfti5.geometry) TO utente;


--
-- Name: FUNCTION point(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.point(cfti5.geometry) TO utente;


--
-- Name: FUNCTION polygon(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.polygon(cfti5.geometry) TO utente;


--
-- Name: FUNCTION text(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.text(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry(path); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(path) TO utente;


--
-- Name: FUNCTION geometry(point); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(point) TO utente;


--
-- Name: FUNCTION geometry(polygon); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(polygon) TO utente;


--
-- Name: FUNCTION box3d(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box3d(cfti5.raster) TO utente;


--
-- Name: FUNCTION bytea(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.bytea(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_convexhull(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_convexhull(cfti5.raster) TO utente;


--
-- Name: FUNCTION geometry(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry(text) TO utente;


--
-- Name: FUNCTION __st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.__st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer) TO utente;


--
-- Name: FUNCTION _add_raster_constraint(cn name, sql text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint(cn name, sql text) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_overview_constraint(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_overview_constraint(ovschema name, ovtable name, ovcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint(rastschema name, rasttable name, cn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint(rastschema name, rasttable name, cn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _overview_constraint(ov cfti5.raster, factor integer, refschema name, reftable name, refcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._overview_constraint(ov cfti5.raster, factor integer, refschema name, reftable name, refcolumn name) TO utente;


--
-- Name: FUNCTION _overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer) TO utente;


--
-- Name: FUNCTION _postgis_deprecate(oldname text, newname text, version text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_deprecate(oldname text, newname text, version text) TO utente;


--
-- Name: FUNCTION _postgis_index_extent(tbl regclass, col text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_index_extent(tbl regclass, col text) TO utente;


--
-- Name: FUNCTION _postgis_join_selectivity(regclass, text, regclass, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_join_selectivity(regclass, text, regclass, text, text) TO utente;


--
-- Name: FUNCTION _postgis_pgsql_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_pgsql_version() TO utente;


--
-- Name: FUNCTION _postgis_scripts_pgsql_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_scripts_pgsql_version() TO utente;


--
-- Name: FUNCTION _postgis_selectivity(tbl regclass, att_name text, geom cfti5.geometry, mode text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_selectivity(tbl regclass, att_name text, geom cfti5.geometry, mode text) TO utente;


--
-- Name: FUNCTION _postgis_stats(tbl regclass, att_name text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._postgis_stats(tbl regclass, att_name text, text) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_nodata_values(rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_nodata_values(rast cfti5.raster) TO utente;


--
-- Name: FUNCTION _raster_constraint_out_db(rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_out_db(rast cfti5.raster) TO utente;


--
-- Name: FUNCTION _raster_constraint_pixel_types(rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._raster_constraint_pixel_types(rast cfti5.raster) TO utente;


--
-- Name: FUNCTION _st_3ddfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_3ddfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_3ddwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_3ddwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_3dintersects(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_3dintersects(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_asgeojson(integer, cfti5.geography, integer, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_asgeojson(integer, cfti5.geography, integer, integer) TO utente;


--
-- Name: FUNCTION _st_asgeojson(integer, cfti5.geometry, integer, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_asgeojson(integer, cfti5.geometry, integer, integer) TO utente;


--
-- Name: FUNCTION _st_asgml(integer, cfti5.geography, integer, integer, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_asgml(integer, cfti5.geography, integer, integer, text, text) TO utente;


--
-- Name: FUNCTION _st_asgml(integer, cfti5.geometry, integer, integer, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_asgml(integer, cfti5.geometry, integer, integer, text, text) TO utente;


--
-- Name: FUNCTION _st_askml(integer, cfti5.geography, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_askml(integer, cfti5.geography, integer, text) TO utente;


--
-- Name: FUNCTION _st_askml(integer, cfti5.geometry, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_askml(integer, cfti5.geometry, integer, text) TO utente;


--
-- Name: FUNCTION _st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION _st_asx3d(integer, cfti5.geometry, integer, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_asx3d(integer, cfti5.geometry, integer, integer, text) TO utente;


--
-- Name: FUNCTION _st_bestsrid(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_bestsrid(cfti5.geography) TO utente;


--
-- Name: FUNCTION _st_bestsrid(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_bestsrid(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION _st_buffer(cfti5.geometry, double precision, cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_buffer(cfti5.geometry, double precision, cstring) TO utente;


--
-- Name: FUNCTION _st_clip(rast cfti5.raster, nband integer[], geom cfti5.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_clip(rast cfti5.raster, nband integer[], geom cfti5.geometry, nodataval double precision[], crop boolean) TO utente;


--
-- Name: FUNCTION _st_colormap(rast cfti5.raster, nband integer, colormap text, method text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_colormap(rast cfti5.raster, nband integer, colormap text, method text) TO utente;


--
-- Name: FUNCTION _st_concavehull(param_inputgeom cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_concavehull(param_inputgeom cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_contains(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_contains(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_contains(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_contains(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_containsproperly(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_containsproperly(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_containsproperly(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_containsproperly(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_convertarray4ma(value double precision[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_convertarray4ma(value double precision[]) TO utente;


--
-- Name: FUNCTION _st_count(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_count(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_countagg_finalfn(agg cfti5.agg_count); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_countagg_finalfn(agg cfti5.agg_count) TO utente;


--
-- Name: FUNCTION _st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION _st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION _st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_countagg_transfn(agg cfti5.agg_count, rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_coveredby(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_coveredby(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_coveredby(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_coveredby(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_covers(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_covers(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION _st_covers(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_covers(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_covers(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_covers(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_crosses(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_crosses(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_dfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_dfullywithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dfullywithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION _st_distance(cfti5.geography, cfti5.geography, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_distance(cfti5.geography, cfti5.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_distancetree(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_distancetree(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION _st_distancetree(cfti5.geography, cfti5.geography, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_distancetree(cfti5.geography, cfti5.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_distanceuncached(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_distanceuncached(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION _st_distanceuncached(cfti5.geography, cfti5.geography, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_distanceuncached(cfti5.geography, cfti5.geography, boolean) TO utente;


--
-- Name: FUNCTION _st_distanceuncached(cfti5.geography, cfti5.geography, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_distanceuncached(cfti5.geography, cfti5.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_dwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_dwithin(cfti5.geography, cfti5.geography, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dwithin(cfti5.geography, cfti5.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_dwithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dwithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION _st_dwithinuncached(cfti5.geography, cfti5.geography, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dwithinuncached(cfti5.geography, cfti5.geography, double precision) TO utente;


--
-- Name: FUNCTION _st_dwithinuncached(cfti5.geography, cfti5.geography, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_dwithinuncached(cfti5.geography, cfti5.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_equals(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_equals(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_expand(cfti5.geography, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_expand(cfti5.geography, double precision) TO utente;


--
-- Name: FUNCTION _st_gdalwarp(rast cfti5.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_gdalwarp(rast cfti5.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer) TO utente;


--
-- Name: FUNCTION _st_geomfromgml(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_geomfromgml(text, integer) TO utente;


--
-- Name: FUNCTION _st_grayscale4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_grayscale4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_histogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_histogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_intersects(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_intersects(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_intersects(geom cfti5.geometry, rast cfti5.raster, nband integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_intersects(geom cfti5.geometry, rast cfti5.raster, nband integer) TO utente;


--
-- Name: FUNCTION _st_intersects(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_intersects(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_linecrossingdirection(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_linecrossingdirection(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_longestline(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_longestline(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset cfti5.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_mapalgebra(rastbandargset cfti5.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset cfti5.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent cfti5.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_mapalgebra(rastbandargset cfti5.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent cfti5.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_maxdistance(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_maxdistance(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_neighborhood(rast cfti5.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_neighborhood(rast cfti5.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION _st_orderingequals(geometrya cfti5.geometry, geometryb cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_orderingequals(geometrya cfti5.geometry, geometryb cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_overlaps(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_overlaps(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_overlaps(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_overlaps(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_pixelaspolygons(rast cfti5.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_pixelaspolygons(rast cfti5.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION _st_pointoutside(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_pointoutside(cfti5.geography) TO utente;


--
-- Name: FUNCTION _st_quantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_quantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION _st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION _st_rastertoworldcoord(rast cfti5.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_rastertoworldcoord(rast cfti5.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO utente;


--
-- Name: FUNCTION _st_reclass(rast cfti5.raster, VARIADIC reclassargset cfti5.reclassarg[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_reclass(rast cfti5.raster, VARIADIC reclassargset cfti5.reclassarg[]) TO utente;


--
-- Name: FUNCTION _st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_samealignment_finalfn(agg cfti5.agg_samealignment); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_samealignment_finalfn(agg cfti5.agg_samealignment) TO utente;


--
-- Name: FUNCTION _st_samealignment_transfn(agg cfti5.agg_samealignment, rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_samealignment_transfn(agg cfti5.agg_samealignment, rast cfti5.raster) TO utente;


--
-- Name: FUNCTION _st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION _st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_summarystats(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_summarystats(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_summarystats_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_summarystats_finalfn(internal) TO utente;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, cfti5.raster, boolean, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_summarystats_transfn(internal, cfti5.raster, boolean, double precision) TO utente;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, cfti5.raster, integer, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_summarystats_transfn(internal, cfti5.raster, integer, boolean) TO utente;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, cfti5.raster, integer, boolean, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_summarystats_transfn(internal, cfti5.raster, integer, boolean, double precision) TO utente;


--
-- Name: FUNCTION _st_tile(rast cfti5.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_tile(rast cfti5.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION _st_touches(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_touches(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_touches(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_touches(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_union_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_union_finalfn(internal) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_union_transfn(internal, cfti5.raster) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, cfti5.raster, cfti5.unionarg[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_union_transfn(internal, cfti5.raster, cfti5.unionarg[]) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, cfti5.raster, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_union_transfn(internal, cfti5.raster, integer) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, cfti5.raster, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_union_transfn(internal, cfti5.raster, text) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, cfti5.raster, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_union_transfn(internal, cfti5.raster, integer, text) TO utente;


--
-- Name: FUNCTION _st_valuecount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_valuecount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_voronoi(g1 cfti5.geometry, clip cfti5.geometry, tolerance double precision, return_polygons boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_voronoi(g1 cfti5.geometry, clip cfti5.geometry, tolerance double precision, return_polygons boolean) TO utente;


--
-- Name: FUNCTION _st_within(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_within(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION _st_within(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_within(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_worldtorastercoord(rast cfti5.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._st_worldtorastercoord(rast cfti5.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO utente;


--
-- Name: FUNCTION _updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5._updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO utente;


--
-- Name: FUNCTION addauth(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addauth(text) TO utente;


--
-- Name: FUNCTION addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO utente;


--
-- Name: FUNCTION addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO utente;


--
-- Name: FUNCTION addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean) TO utente;


--
-- Name: FUNCTION addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer) TO utente;


--
-- Name: FUNCTION addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION box3dtobox(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.box3dtobox(cfti5.box3d) TO utente;


--
-- Name: FUNCTION checkauth(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.checkauth(text, text) TO utente;


--
-- Name: FUNCTION checkauth(text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.checkauth(text, text, text) TO utente;


--
-- Name: FUNCTION checkauthtrigger(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.checkauthtrigger() TO utente;


--
-- Name: FUNCTION contains_2d(cfti5.box2df, cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.contains_2d(cfti5.box2df, cfti5.box2df) TO utente;


--
-- Name: FUNCTION contains_2d(cfti5.box2df, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.contains_2d(cfti5.box2df, cfti5.geometry) TO utente;


--
-- Name: FUNCTION contains_2d(cfti5.geometry, cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.contains_2d(cfti5.geometry, cfti5.box2df) TO utente;


--
-- Name: FUNCTION disablelongtransactions(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.disablelongtransactions() TO utente;


--
-- Name: FUNCTION dropgeometrycolumn(table_name character varying, column_name character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropgeometrycolumn(table_name character varying, column_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrytable(table_name character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropgeometrytable(table_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrytable(schema_name character varying, table_name character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropgeometrytable(schema_name character varying, table_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying) TO utente;


--
-- Name: FUNCTION dropoverviewconstraints(ovtable name, ovcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropoverviewconstraints(ovtable name, ovcolumn name) TO utente;


--
-- Name: FUNCTION dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION enablelongtransactions(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.enablelongtransactions() TO utente;


--
-- Name: FUNCTION equals(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.equals(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION find_srid(character varying, character varying, character varying); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.find_srid(character varying, character varying, character varying) TO utente;


--
-- Name: FUNCTION fnloc_geometry(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.fnloc_geometry() TO utente;


--
-- Name: FUNCTION fscossec_geometry(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.fscossec_geometry() TO utente;


--
-- Name: FUNCTION genera_ordinam(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.genera_ordinam() TO utente;


--
-- Name: FUNCTION geog_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geog_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geography_cmp(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_cmp(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_distance_knn(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_distance_knn(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_eq(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_eq(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_ge(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_ge(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_gist_compress(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_compress(internal) TO utente;


--
-- Name: FUNCTION geography_gist_consistent(internal, cfti5.geography, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_consistent(internal, cfti5.geography, integer) TO utente;


--
-- Name: FUNCTION geography_gist_decompress(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_decompress(internal) TO utente;


--
-- Name: FUNCTION geography_gist_distance(internal, cfti5.geography, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_distance(internal, cfti5.geography, integer) TO utente;


--
-- Name: FUNCTION geography_gist_penalty(internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_penalty(internal, internal, internal) TO utente;


--
-- Name: FUNCTION geography_gist_picksplit(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_picksplit(internal, internal) TO utente;


--
-- Name: FUNCTION geography_gist_same(cfti5.box2d, cfti5.box2d, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_same(cfti5.box2d, cfti5.box2d, internal) TO utente;


--
-- Name: FUNCTION geography_gist_union(bytea, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gist_union(bytea, internal) TO utente;


--
-- Name: FUNCTION geography_gt(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_gt(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_le(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_le(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_lt(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_lt(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geography_overlaps(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geography_overlaps(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION geom2d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geom2d_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geom3d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geom3d_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geom4d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geom4d_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geometry_above(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_above(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_below(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_below(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_cmp(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_cmp(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_contained_3d(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_contained_3d(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_contained_by_raster(cfti5.geometry, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_contained_by_raster(cfti5.geometry, cfti5.raster) TO utente;


--
-- Name: FUNCTION geometry_contains(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_contains(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_contains_3d(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_contains_3d(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_box(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_distance_box(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_centroid(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_distance_centroid(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_centroid_nd(cfti5.geometry, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_distance_centroid_nd(cfti5.geometry, cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_cpa(cfti5.geometry, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_distance_cpa(cfti5.geometry, cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_eq(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_eq(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_ge(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_ge(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_gist_compress_2d(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_compress_2d(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_compress_nd(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_compress_nd(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_consistent_2d(internal, cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_consistent_2d(internal, cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_consistent_nd(internal, cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_consistent_nd(internal, cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_decompress_2d(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_decompress_2d(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_decompress_nd(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_decompress_nd(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_distance_2d(internal, cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_distance_2d(internal, cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_distance_nd(internal, cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_distance_nd(internal, cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_penalty_2d(internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_penalty_2d(internal, internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_penalty_nd(internal, internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_penalty_nd(internal, internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_picksplit_2d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_picksplit_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_picksplit_nd(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_picksplit_nd(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_same_2d(geom1 cfti5.geometry, geom2 cfti5.geometry, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_same_2d(geom1 cfti5.geometry, geom2 cfti5.geometry, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_same_nd(cfti5.geometry, cfti5.geometry, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_same_nd(cfti5.geometry, cfti5.geometry, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_union_2d(bytea, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_union_2d(bytea, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_union_nd(bytea, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gist_union_nd(bytea, internal) TO utente;


--
-- Name: FUNCTION geometry_gt(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_gt(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_hash(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_hash(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_le(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_le(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_left(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_left(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_lt(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_lt(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overabove(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overabove(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overbelow(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overbelow(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overlaps(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overlaps(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overlaps_3d(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overlaps_3d(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overlaps_nd(cfti5.geometry, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overlaps_nd(cfti5.geometry, cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overleft(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overleft(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_overright(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_overright(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_raster_contain(cfti5.geometry, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_raster_contain(cfti5.geometry, cfti5.raster) TO utente;


--
-- Name: FUNCTION geometry_raster_overlap(cfti5.geometry, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_raster_overlap(cfti5.geometry, cfti5.raster) TO utente;


--
-- Name: FUNCTION geometry_right(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_right(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_same(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_same(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_same_3d(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_same_3d(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometry_spgist_choose_2d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_choose_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_choose_3d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_choose_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_compress_2d(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_compress_2d(internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_compress_3d(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_compress_3d(internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_config_2d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_config_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_config_3d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_config_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_inner_consistent_2d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_inner_consistent_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_inner_consistent_3d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_inner_consistent_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_leaf_consistent_2d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_leaf_consistent_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_leaf_consistent_3d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_leaf_consistent_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_picksplit_2d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_picksplit_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_picksplit_3d(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_spgist_picksplit_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_within(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometry_within(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION geometrytype(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometrytype(cfti5.geography) TO utente;


--
-- Name: FUNCTION geometrytype(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geometrytype(cfti5.geometry) TO utente;


--
-- Name: FUNCTION geomfromewkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geomfromewkb(bytea) TO utente;


--
-- Name: FUNCTION geomfromewkt(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.geomfromewkt(text) TO utente;


--
-- Name: FUNCTION get_proj4_from_srid(integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.get_proj4_from_srid(integer) TO utente;


--
-- Name: FUNCTION gettransactionid(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gettransactionid() TO utente;


--
-- Name: FUNCTION gserialized_gist_joinsel_2d(internal, oid, internal, smallint); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gserialized_gist_joinsel_2d(internal, oid, internal, smallint) TO utente;


--
-- Name: FUNCTION gserialized_gist_joinsel_nd(internal, oid, internal, smallint); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gserialized_gist_joinsel_nd(internal, oid, internal, smallint) TO utente;


--
-- Name: FUNCTION gserialized_gist_sel_2d(internal, oid, internal, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gserialized_gist_sel_2d(internal, oid, internal, integer) TO utente;


--
-- Name: FUNCTION gserialized_gist_sel_nd(internal, oid, internal, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.gserialized_gist_sel_nd(internal, oid, internal, integer) TO utente;


--
-- Name: FUNCTION is_contained_2d(cfti5.box2df, cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.is_contained_2d(cfti5.box2df, cfti5.box2df) TO utente;


--
-- Name: FUNCTION is_contained_2d(cfti5.box2df, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.is_contained_2d(cfti5.box2df, cfti5.geometry) TO utente;


--
-- Name: FUNCTION is_contained_2d(cfti5.geometry, cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.is_contained_2d(cfti5.geometry, cfti5.box2df) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.lockrow(text, text, text) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.lockrow(text, text, text, text) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text, timestamp without time zone); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.lockrow(text, text, text, timestamp without time zone) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text, text, timestamp without time zone); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.lockrow(text, text, text, text, timestamp without time zone) TO utente;


--
-- Name: FUNCTION longtransactionsenabled(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.longtransactionsenabled() TO utente;


--
-- Name: FUNCTION overlaps_2d(cfti5.box2df, cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_2d(cfti5.box2df, cfti5.box2df) TO utente;


--
-- Name: FUNCTION overlaps_2d(cfti5.box2df, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_2d(cfti5.box2df, cfti5.geometry) TO utente;


--
-- Name: FUNCTION overlaps_2d(cfti5.geometry, cfti5.box2df); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_2d(cfti5.geometry, cfti5.box2df) TO utente;


--
-- Name: FUNCTION overlaps_geog(cfti5.geography, cfti5.gidx); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_geog(cfti5.geography, cfti5.gidx) TO utente;


--
-- Name: FUNCTION overlaps_geog(cfti5.gidx, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_geog(cfti5.gidx, cfti5.geography) TO utente;


--
-- Name: FUNCTION overlaps_geog(cfti5.gidx, cfti5.gidx); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_geog(cfti5.gidx, cfti5.gidx) TO utente;


--
-- Name: FUNCTION overlaps_nd(cfti5.geometry, cfti5.gidx); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_nd(cfti5.geometry, cfti5.gidx) TO utente;


--
-- Name: FUNCTION overlaps_nd(cfti5.gidx, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_nd(cfti5.gidx, cfti5.geometry) TO utente;


--
-- Name: FUNCTION overlaps_nd(cfti5.gidx, cfti5.gidx); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.overlaps_nd(cfti5.gidx, cfti5.gidx) TO utente;


--
-- Name: FUNCTION pgis_asgeobuf_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asgeobuf_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_asgeobuf_transfn(internal, anyelement); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asgeobuf_transfn(internal, anyelement) TO utente;


--
-- Name: FUNCTION pgis_asgeobuf_transfn(internal, anyelement, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asgeobuf_transfn(internal, anyelement, text) TO utente;


--
-- Name: FUNCTION pgis_asmvt_combinefn(internal, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_combinefn(internal, internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_deserialfn(bytea, internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_deserialfn(bytea, internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_serialfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_serialfn(internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_transfn(internal, anyelement) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_transfn(internal, anyelement, text) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_transfn(internal, anyelement, text, integer) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_asmvt_transfn(internal, anyelement, text, integer, text) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_accum_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(internal, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_accum_transfn(internal, cfti5.geometry) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(internal, cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_accum_transfn(internal, cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(internal, cfti5.geometry, double precision, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_accum_transfn(internal, cfti5.geometry, double precision, integer) TO utente;


--
-- Name: FUNCTION pgis_geometry_clusterintersecting_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_clusterintersecting_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_clusterwithin_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_clusterwithin_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_collect_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_collect_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_makeline_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_makeline_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_polygonize_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_polygonize_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_union_finalfn(internal); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.pgis_geometry_union_finalfn(internal) TO utente;


--
-- Name: FUNCTION populate_geometry_columns(use_typmod boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.populate_geometry_columns(use_typmod boolean) TO utente;


--
-- Name: FUNCTION populate_geometry_columns(tbl_oid oid, use_typmod boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.populate_geometry_columns(tbl_oid oid, use_typmod boolean) TO utente;


--
-- Name: FUNCTION postgis_addbbox(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_addbbox(cfti5.geometry) TO utente;


--
-- Name: FUNCTION postgis_cache_bbox(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_cache_bbox() TO utente;


--
-- Name: FUNCTION postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text) TO utente;


--
-- Name: FUNCTION postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text) TO utente;


--
-- Name: FUNCTION postgis_constraint_type(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_constraint_type(geomschema text, geomtable text, geomcolumn text) TO utente;


--
-- Name: FUNCTION postgis_dropbbox(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_dropbbox(cfti5.geometry) TO utente;


--
-- Name: FUNCTION postgis_extensions_upgrade(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_extensions_upgrade() TO utente;


--
-- Name: FUNCTION postgis_full_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_full_version() TO utente;


--
-- Name: FUNCTION postgis_gdal_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_gdal_version() TO utente;


--
-- Name: FUNCTION postgis_geos_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_geos_version() TO utente;


--
-- Name: FUNCTION postgis_getbbox(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_getbbox(cfti5.geometry) TO utente;


--
-- Name: FUNCTION postgis_hasbbox(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_hasbbox(cfti5.geometry) TO utente;


--
-- Name: FUNCTION postgis_lib_build_date(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_lib_build_date() TO utente;


--
-- Name: FUNCTION postgis_lib_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_lib_version() TO utente;


--
-- Name: FUNCTION postgis_libjson_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_libjson_version() TO utente;


--
-- Name: FUNCTION postgis_liblwgeom_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_liblwgeom_version() TO utente;


--
-- Name: FUNCTION postgis_libprotobuf_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_libprotobuf_version() TO utente;


--
-- Name: FUNCTION postgis_libxml_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_libxml_version() TO utente;


--
-- Name: FUNCTION postgis_noop(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_noop(cfti5.geometry) TO utente;


--
-- Name: FUNCTION postgis_noop(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_noop(cfti5.raster) TO utente;


--
-- Name: FUNCTION postgis_proj_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_proj_version() TO utente;


--
-- Name: FUNCTION postgis_raster_lib_build_date(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_raster_lib_build_date() TO utente;


--
-- Name: FUNCTION postgis_raster_lib_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_raster_lib_version() TO utente;


--
-- Name: FUNCTION postgis_raster_scripts_installed(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_raster_scripts_installed() TO utente;


--
-- Name: FUNCTION postgis_scripts_build_date(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_scripts_build_date() TO utente;


--
-- Name: FUNCTION postgis_scripts_installed(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_scripts_installed() TO utente;


--
-- Name: FUNCTION postgis_scripts_released(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_scripts_released() TO utente;


--
-- Name: FUNCTION postgis_svn_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_svn_version() TO utente;


--
-- Name: FUNCTION postgis_transform_geometry(cfti5.geometry, text, text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_transform_geometry(cfti5.geometry, text, text, integer) TO utente;


--
-- Name: FUNCTION postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean) TO utente;


--
-- Name: FUNCTION postgis_typmod_dims(integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_typmod_dims(integer) TO utente;


--
-- Name: FUNCTION postgis_typmod_srid(integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_typmod_srid(integer) TO utente;


--
-- Name: FUNCTION postgis_typmod_type(integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_typmod_type(integer) TO utente;


--
-- Name: FUNCTION postgis_version(); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.postgis_version() TO utente;


--
-- Name: FUNCTION raster_above(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_above(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_below(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_below(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_contain(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_contain(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_contained(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_contained(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_contained_by_geometry(cfti5.raster, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_contained_by_geometry(cfti5.raster, cfti5.geometry) TO utente;


--
-- Name: FUNCTION raster_eq(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_eq(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_geometry_contain(cfti5.raster, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_geometry_contain(cfti5.raster, cfti5.geometry) TO utente;


--
-- Name: FUNCTION raster_geometry_overlap(cfti5.raster, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_geometry_overlap(cfti5.raster, cfti5.geometry) TO utente;


--
-- Name: FUNCTION raster_hash(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_hash(cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_left(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_left(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_overabove(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_overabove(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_overbelow(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_overbelow(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_overlap(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_overlap(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_overleft(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_overleft(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_overright(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_overright(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_right(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_right(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION raster_same(cfti5.raster, cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.raster_same(cfti5.raster, cfti5.raster) TO utente;


--
-- Name: FUNCTION st_3dclosestpoint(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dclosestpoint(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3ddfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3ddfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_3ddistance(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3ddistance(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3ddwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3ddwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_3dintersects(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dintersects(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3dlength(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dlength(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3dlength_spheroid(cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dlength_spheroid(cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_3dlongestline(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dlongestline(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3dmakebox(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dmakebox(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3dmaxdistance(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dmaxdistance(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3dperimeter(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dperimeter(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_3dshortestline(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dshortestline(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_addband(rast cfti5.raster, addbandargset cfti5.addbandarg[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(rast cfti5.raster, addbandargset cfti5.addbandarg[]) TO utente;


--
-- Name: FUNCTION st_addband(torast cfti5.raster, fromrasts cfti5.raster[], fromband integer, torastindex integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(torast cfti5.raster, fromrasts cfti5.raster[], fromband integer, torastindex integer) TO utente;


--
-- Name: FUNCTION st_addband(torast cfti5.raster, fromrast cfti5.raster, fromband integer, torastindex integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(torast cfti5.raster, fromrast cfti5.raster, fromband integer, torastindex integer) TO utente;


--
-- Name: FUNCTION st_addband(rast cfti5.raster, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(rast cfti5.raster, pixeltype text, initialvalue double precision, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addband(rast cfti5.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(rast cfti5.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addband(rast cfti5.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(rast cfti5.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addband(rast cfti5.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addband(rast cfti5.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addmeasure(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addmeasure(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_addpoint(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addpoint(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_addpoint(geom1 cfti5.geometry, geom2 cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_addpoint(geom1 cfti5.geometry, geom2 cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_affine(cfti5.geometry, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_affine(cfti5.geometry, double precision, double precision, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_affine(cfti5.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_affine(cfti5.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_angle(line1 cfti5.geometry, line2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_angle(line1 cfti5.geometry, line2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_angle(pt1 cfti5.geometry, pt2 cfti5.geometry, pt3 cfti5.geometry, pt4 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_angle(pt1 cfti5.geometry, pt2 cfti5.geometry, pt3 cfti5.geometry, pt4 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_approxcount(rast cfti5.raster, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rast cfti5.raster, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rast cfti5.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rast cfti5.raster, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rast cfti5.raster, nband integer, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rast cfti5.raster, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rastertable text, rastercolumn text, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast cfti5.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rast cfti5.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast cfti5.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rast cfti5.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast cfti5.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rast cfti5.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast cfti5.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rast cfti5.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, sample_percent double precision, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, nband integer, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast cfti5.raster, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rast cfti5.raster, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast cfti5.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rast cfti5.raster, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast cfti5.raster, nband integer, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rast cfti5.raster, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rast cfti5.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_area(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_area(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_area(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_area(text) TO utente;


--
-- Name: FUNCTION st_area(geog cfti5.geography, use_spheroid boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_area(geog cfti5.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_area2d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_area2d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_asbinary(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asbinary(cfti5.geography) TO utente;


--
-- Name: FUNCTION st_asbinary(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asbinary(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_asbinary(cfti5.geography, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asbinary(cfti5.geography, text) TO utente;


--
-- Name: FUNCTION st_asbinary(cfti5.geometry, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asbinary(cfti5.geometry, text) TO utente;


--
-- Name: FUNCTION st_asbinary(cfti5.raster, outasin boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asbinary(cfti5.raster, outasin boolean) TO utente;


--
-- Name: FUNCTION st_asencodedpolyline(geom cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asencodedpolyline(geom cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_asewkb(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asewkb(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_asewkb(cfti5.geometry, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asewkb(cfti5.geometry, text) TO utente;


--
-- Name: FUNCTION st_asewkt(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asewkt(cfti5.geography) TO utente;


--
-- Name: FUNCTION st_asewkt(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asewkt(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_asewkt(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asewkt(text) TO utente;


--
-- Name: FUNCTION st_asgdalraster(rast cfti5.raster, format text, options text[], srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgdalraster(rast cfti5.raster, format text, options text[], srid integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeojson(text) TO utente;


--
-- Name: FUNCTION st_asgeojson(geog cfti5.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeojson(geog cfti5.geography, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(geom cfti5.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeojson(geom cfti5.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geog cfti5.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeojson(gj_version integer, geog cfti5.geography, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geom cfti5.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeojson(gj_version integer, geom cfti5.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgml(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgml(text) TO utente;


--
-- Name: FUNCTION st_asgml(geog cfti5.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgml(geog cfti5.geography, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgml(geom cfti5.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgml(geom cfti5.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgml(version integer, geog cfti5.geography, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgml(version integer, geog cfti5.geography, maxdecimaldigits integer, options integer, nprefix text, id text) TO utente;


--
-- Name: FUNCTION st_asgml(version integer, geom cfti5.geometry, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgml(version integer, geom cfti5.geometry, maxdecimaldigits integer, options integer, nprefix text, id text) TO utente;


--
-- Name: FUNCTION st_ashexewkb(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ashexewkb(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_ashexewkb(cfti5.geometry, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ashexewkb(cfti5.geometry, text) TO utente;


--
-- Name: FUNCTION st_ashexwkb(cfti5.raster, outasin boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ashexwkb(cfti5.raster, outasin boolean) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast cfti5.raster, options text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asjpeg(rast cfti5.raster, options text[]) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast cfti5.raster, nbands integer[], options text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asjpeg(rast cfti5.raster, nbands integer[], options text[]) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast cfti5.raster, nbands integer[], quality integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asjpeg(rast cfti5.raster, nbands integer[], quality integer) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast cfti5.raster, nband integer, options text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asjpeg(rast cfti5.raster, nband integer, options text[]) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast cfti5.raster, nband integer, quality integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asjpeg(rast cfti5.raster, nband integer, quality integer) TO utente;


--
-- Name: FUNCTION st_askml(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_askml(text) TO utente;


--
-- Name: FUNCTION st_askml(geog cfti5.geography, maxdecimaldigits integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_askml(geog cfti5.geography, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_askml(geom cfti5.geometry, maxdecimaldigits integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_askml(geom cfti5.geometry, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_askml(version integer, geog cfti5.geography, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_askml(version integer, geog cfti5.geography, maxdecimaldigits integer, nprefix text) TO utente;


--
-- Name: FUNCTION st_askml(version integer, geom cfti5.geometry, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_askml(version integer, geom cfti5.geometry, maxdecimaldigits integer, nprefix text) TO utente;


--
-- Name: FUNCTION st_aslatlontext(geom cfti5.geometry, tmpl text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aslatlontext(geom cfti5.geometry, tmpl text) TO utente;


--
-- Name: FUNCTION st_asmvtgeom(geom cfti5.geometry, bounds cfti5.box2d, extent integer, buffer integer, clip_geom boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asmvtgeom(geom cfti5.geometry, bounds cfti5.box2d, extent integer, buffer integer, clip_geom boolean) TO utente;


--
-- Name: FUNCTION st_aspect(rast cfti5.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspect(rast cfti5.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_aspect(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspect(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, units text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_aspng(rast cfti5.raster, options text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspng(rast cfti5.raster, options text[]) TO utente;


--
-- Name: FUNCTION st_aspng(rast cfti5.raster, nbands integer[], options text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspng(rast cfti5.raster, nbands integer[], options text[]) TO utente;


--
-- Name: FUNCTION st_aspng(rast cfti5.raster, nbands integer[], compression integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspng(rast cfti5.raster, nbands integer[], compression integer) TO utente;


--
-- Name: FUNCTION st_aspng(rast cfti5.raster, nband integer, options text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspng(rast cfti5.raster, nband integer, options text[]) TO utente;


--
-- Name: FUNCTION st_aspng(rast cfti5.raster, nband integer, compression integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aspng(rast cfti5.raster, nband integer, compression integer) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, ref cfti5.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, ref cfti5.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, ref cfti5.raster, pixeltype text, value double precision, nodataval double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, ref cfti5.raster, pixeltype text, value double precision, nodataval double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom cfti5.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asraster(geom cfti5.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_assvg(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_assvg(text) TO utente;


--
-- Name: FUNCTION st_assvg(geog cfti5.geography, rel integer, maxdecimaldigits integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_assvg(geog cfti5.geography, rel integer, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_assvg(geom cfti5.geometry, rel integer, maxdecimaldigits integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_assvg(geom cfti5.geometry, rel integer, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_astext(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astext(cfti5.geography) TO utente;


--
-- Name: FUNCTION st_astext(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astext(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_astext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astext(text) TO utente;


--
-- Name: FUNCTION st_astext(cfti5.geography, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astext(cfti5.geography, integer) TO utente;


--
-- Name: FUNCTION st_astext(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astext(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast cfti5.raster, options text[], srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astiff(rast cfti5.raster, options text[], srid integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast cfti5.raster, compression text, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astiff(rast cfti5.raster, compression text, srid integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast cfti5.raster, nbands integer[], options text[], srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astiff(rast cfti5.raster, nbands integer[], options text[], srid integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast cfti5.raster, nbands integer[], compression text, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astiff(rast cfti5.raster, nbands integer[], compression text, srid integer) TO utente;


--
-- Name: FUNCTION st_astwkb(geom cfti5.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astwkb(geom cfti5.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO utente;


--
-- Name: FUNCTION st_astwkb(geom cfti5.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_astwkb(geom cfti5.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO utente;


--
-- Name: FUNCTION st_aswkb(cfti5.raster, outasin boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_aswkb(cfti5.raster, outasin boolean) TO utente;


--
-- Name: FUNCTION st_asx3d(geom cfti5.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asx3d(geom cfti5.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_azimuth(geog1 cfti5.geography, geog2 cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_azimuth(geog1 cfti5.geography, geog2 cfti5.geography) TO utente;


--
-- Name: FUNCTION st_azimuth(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_azimuth(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_band(rast cfti5.raster, nbands integer[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_band(rast cfti5.raster, nbands integer[]) TO utente;


--
-- Name: FUNCTION st_band(rast cfti5.raster, nband integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_band(rast cfti5.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_band(rast cfti5.raster, nbands text, delimiter character); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_band(rast cfti5.raster, nbands text, delimiter character) TO utente;


--
-- Name: FUNCTION st_bandfilesize(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandfilesize(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandfiletimestamp(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandfiletimestamp(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandisnodata(rast cfti5.raster, forcechecking boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandisnodata(rast cfti5.raster, forcechecking boolean) TO utente;


--
-- Name: FUNCTION st_bandisnodata(rast cfti5.raster, band integer, forcechecking boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandisnodata(rast cfti5.raster, band integer, forcechecking boolean) TO utente;


--
-- Name: FUNCTION st_bandmetadata(rast cfti5.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandmetadata(rast cfti5.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint) TO utente;


--
-- Name: FUNCTION st_bandmetadata(rast cfti5.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandmetadata(rast cfti5.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint) TO utente;


--
-- Name: FUNCTION st_bandnodatavalue(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandnodatavalue(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandpath(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandpath(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandpixeltype(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bandpixeltype(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bdmpolyfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bdmpolyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_bdpolyfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_bdpolyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_boundary(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_boundary(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_boundingdiagonal(geom cfti5.geometry, fits boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_boundingdiagonal(geom cfti5.geometry, fits boolean) TO utente;


--
-- Name: FUNCTION st_box2dfromgeohash(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_box2dfromgeohash(text, integer) TO utente;


--
-- Name: FUNCTION st_buffer(cfti5.geography, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(cfti5.geography, double precision) TO utente;


--
-- Name: FUNCTION st_buffer(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_buffer(text, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(text, double precision) TO utente;


--
-- Name: FUNCTION st_buffer(cfti5.geography, double precision, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(cfti5.geography, double precision, integer) TO utente;


--
-- Name: FUNCTION st_buffer(cfti5.geography, double precision, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(cfti5.geography, double precision, text) TO utente;


--
-- Name: FUNCTION st_buffer(cfti5.geometry, double precision, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(cfti5.geometry, double precision, integer) TO utente;


--
-- Name: FUNCTION st_buffer(cfti5.geometry, double precision, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(cfti5.geometry, double precision, text) TO utente;


--
-- Name: FUNCTION st_buffer(text, double precision, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(text, double precision, integer) TO utente;


--
-- Name: FUNCTION st_buffer(text, double precision, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buffer(text, double precision, text) TO utente;


--
-- Name: FUNCTION st_buildarea(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_buildarea(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_centroid(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_centroid(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_centroid(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_centroid(text) TO utente;


--
-- Name: FUNCTION st_centroid(cfti5.geography, use_spheroid boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_centroid(cfti5.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_chaikinsmoothing(cfti5.geometry, integer, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_chaikinsmoothing(cfti5.geometry, integer, boolean) TO utente;


--
-- Name: FUNCTION st_cleangeometry(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_cleangeometry(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_clip(rast cfti5.raster, geom cfti5.geometry, crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clip(rast cfti5.raster, geom cfti5.geometry, crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast cfti5.raster, geom cfti5.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clip(rast cfti5.raster, geom cfti5.geometry, nodataval double precision[], crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast cfti5.raster, geom cfti5.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clip(rast cfti5.raster, geom cfti5.geometry, nodataval double precision, crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast cfti5.raster, nband integer, geom cfti5.geometry, crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clip(rast cfti5.raster, nband integer, geom cfti5.geometry, crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast cfti5.raster, nband integer[], geom cfti5.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clip(rast cfti5.raster, nband integer[], geom cfti5.geometry, nodataval double precision[], crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast cfti5.raster, nband integer, geom cfti5.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clip(rast cfti5.raster, nband integer, geom cfti5.geometry, nodataval double precision, crop boolean) TO utente;


--
-- Name: FUNCTION st_clipbybox2d(geom cfti5.geometry, box cfti5.box2d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clipbybox2d(geom cfti5.geometry, box cfti5.box2d) TO utente;


--
-- Name: FUNCTION st_closestpoint(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_closestpoint(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_closestpointofapproach(cfti5.geometry, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_closestpointofapproach(cfti5.geometry, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_clusterdbscan(cfti5.geometry, eps double precision, minpoints integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clusterdbscan(cfti5.geometry, eps double precision, minpoints integer) TO utente;


--
-- Name: FUNCTION st_clusterintersecting(cfti5.geometry[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clusterintersecting(cfti5.geometry[]) TO utente;


--
-- Name: FUNCTION st_clusterkmeans(geom cfti5.geometry, k integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clusterkmeans(geom cfti5.geometry, k integer) TO utente;


--
-- Name: FUNCTION st_clusterwithin(cfti5.geometry[], double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clusterwithin(cfti5.geometry[], double precision) TO utente;


--
-- Name: FUNCTION st_collect(cfti5.geometry[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_collect(cfti5.geometry[]) TO utente;


--
-- Name: FUNCTION st_collect(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_collect(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_collectionextract(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_collectionextract(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_collectionhomogenize(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_collectionhomogenize(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_colormap(rast cfti5.raster, colormap text, method text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_colormap(rast cfti5.raster, colormap text, method text) TO utente;


--
-- Name: FUNCTION st_colormap(rast cfti5.raster, nband integer, colormap text, method text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_colormap(rast cfti5.raster, nband integer, colormap text, method text) TO utente;


--
-- Name: FUNCTION st_combine_bbox(cfti5.box2d, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_combine_bbox(cfti5.box2d, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_combine_bbox(cfti5.box3d, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_combine_bbox(cfti5.box3d, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_combinebbox(cfti5.box2d, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_combinebbox(cfti5.box2d, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_combinebbox(cfti5.box3d, cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_combinebbox(cfti5.box3d, cfti5.box3d) TO utente;


--
-- Name: FUNCTION st_combinebbox(cfti5.box3d, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_combinebbox(cfti5.box3d, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_concavehull(param_geom cfti5.geometry, param_pctconvex double precision, param_allow_holes boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_concavehull(param_geom cfti5.geometry, param_pctconvex double precision, param_allow_holes boolean) TO utente;


--
-- Name: FUNCTION st_contains(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_contains(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_contains(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_contains(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_contains(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_contains(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_containsproperly(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_containsproperly(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_containsproperly(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_containsproperly(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_containsproperly(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_containsproperly(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_convexhull(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_convexhull(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_coorddim(geometry cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_coorddim(geometry cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_count(rast cfti5.raster, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_count(rast cfti5.raster, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_count(rast cfti5.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_count(rast cfti5.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_coveredby(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_coveredby(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION st_coveredby(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_coveredby(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_coveredby(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_coveredby(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_coveredby(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_coveredby(text, text) TO utente;


--
-- Name: FUNCTION st_coveredby(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_coveredby(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_covers(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_covers(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION st_covers(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_covers(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_covers(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_covers(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_covers(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_covers(text, text) TO utente;


--
-- Name: FUNCTION st_covers(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_covers(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_cpawithin(cfti5.geometry, cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_cpawithin(cfti5.geometry, cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_createoverview(tab regclass, col name, factor integer, algo text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_createoverview(tab regclass, col name, factor integer, algo text) TO utente;


--
-- Name: FUNCTION st_crosses(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_crosses(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_curvetoline(geom cfti5.geometry, tol double precision, toltype integer, flags integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_curvetoline(geom cfti5.geometry, tol double precision, toltype integer, flags integer) TO utente;


--
-- Name: FUNCTION st_delaunaytriangles(g1 cfti5.geometry, tolerance double precision, flags integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_delaunaytriangles(g1 cfti5.geometry, tolerance double precision, flags integer) TO utente;


--
-- Name: FUNCTION st_dfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dfullywithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_dfullywithin(rast1 cfti5.raster, rast2 cfti5.raster, distance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dfullywithin(rast1 cfti5.raster, rast2 cfti5.raster, distance double precision) TO utente;


--
-- Name: FUNCTION st_dfullywithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dfullywithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION st_difference(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_difference(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_dimension(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dimension(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_disjoint(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_disjoint(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_disjoint(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_disjoint(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_disjoint(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_disjoint(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_distance(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distance(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION st_distance(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distance(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_distance(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distance(text, text) TO utente;


--
-- Name: FUNCTION st_distance(cfti5.geography, cfti5.geography, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distance(cfti5.geography, cfti5.geography, boolean) TO utente;


--
-- Name: FUNCTION st_distance_sphere(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distance_sphere(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_distance_spheroid(geom1 cfti5.geometry, geom2 cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distance_spheroid(geom1 cfti5.geometry, geom2 cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_distancecpa(cfti5.geometry, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distancecpa(cfti5.geometry, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_distancesphere(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distancesphere(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_distancespheroid(geom1 cfti5.geometry, geom2 cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distancespheroid(geom1 cfti5.geometry, geom2 cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_dump(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dump(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_dumpaspolygons(rast cfti5.raster, band integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dumpaspolygons(rast cfti5.raster, band integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_dumppoints(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dumppoints(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_dumprings(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dumprings(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_dumpvalues(rast cfti5.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dumpvalues(rast cfti5.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]) TO utente;


--
-- Name: FUNCTION st_dumpvalues(rast cfti5.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dumpvalues(rast cfti5.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_dwithin(cfti5.geography, cfti5.geography, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dwithin(cfti5.geography, cfti5.geography, double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dwithin(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(rast1 cfti5.raster, rast2 cfti5.raster, distance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dwithin(rast1 cfti5.raster, rast2 cfti5.raster, distance double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(text, text, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dwithin(text, text, double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(cfti5.geography, cfti5.geography, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dwithin(cfti5.geography, cfti5.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION st_dwithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_dwithin(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION st_endpoint(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_endpoint(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_envelope(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_envelope(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_envelope(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_envelope(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_equals(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_equals(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_estimated_extent(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_estimated_extent(text, text) TO utente;


--
-- Name: FUNCTION st_estimated_extent(text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_estimated_extent(text, text, text) TO utente;


--
-- Name: FUNCTION st_estimatedextent(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_estimatedextent(text, text) TO utente;


--
-- Name: FUNCTION st_estimatedextent(text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_estimatedextent(text, text, text) TO utente;


--
-- Name: FUNCTION st_estimatedextent(text, text, text, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_estimatedextent(text, text, text, boolean) TO utente;


--
-- Name: FUNCTION st_expand(cfti5.box2d, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_expand(cfti5.box2d, double precision) TO utente;


--
-- Name: FUNCTION st_expand(cfti5.box3d, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_expand(cfti5.box3d, double precision) TO utente;


--
-- Name: FUNCTION st_expand(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_expand(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_expand(box cfti5.box2d, dx double precision, dy double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_expand(box cfti5.box2d, dx double precision, dy double precision) TO utente;


--
-- Name: FUNCTION st_expand(box cfti5.box3d, dx double precision, dy double precision, dz double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_expand(box cfti5.box3d, dx double precision, dy double precision, dz double precision) TO utente;


--
-- Name: FUNCTION st_expand(geom cfti5.geometry, dx double precision, dy double precision, dz double precision, dm double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_expand(geom cfti5.geometry, dx double precision, dy double precision, dz double precision, dm double precision) TO utente;


--
-- Name: FUNCTION st_exteriorring(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_exteriorring(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_filterbym(cfti5.geometry, double precision, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_filterbym(cfti5.geometry, double precision, double precision, boolean) TO utente;


--
-- Name: FUNCTION st_find_extent(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_find_extent(text, text) TO utente;


--
-- Name: FUNCTION st_find_extent(text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_find_extent(text, text, text) TO utente;


--
-- Name: FUNCTION st_findextent(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_findextent(text, text) TO utente;


--
-- Name: FUNCTION st_findextent(text, text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_findextent(text, text, text) TO utente;


--
-- Name: FUNCTION st_flipcoordinates(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_flipcoordinates(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force2d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force2d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force3d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force3d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force3dm(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force3dm(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force3dz(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force3dz(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force4d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force4d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force_2d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force_2d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force_3d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force_3d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force_3dm(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force_3dm(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force_3dz(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force_3dz(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force_4d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force_4d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_force_collection(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_force_collection(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcecollection(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcecollection(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcecurve(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcecurve(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcepolygonccw(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcepolygonccw(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcepolygoncw(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcepolygoncw(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcerhr(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcerhr(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcesfs(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcesfs(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_forcesfs(cfti5.geometry, version text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_forcesfs(cfti5.geometry, version text) TO utente;


--
-- Name: FUNCTION st_frechetdistance(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_frechetdistance(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_fromgdalraster(gdaldata bytea, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_fromgdalraster(gdaldata bytea, srid integer) TO utente;


--
-- Name: FUNCTION st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT can_read boolean, OUT can_write boolean, OUT create_options text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT can_read boolean, OUT can_write boolean, OUT create_options text) TO utente;


--
-- Name: FUNCTION st_generatepoints(area cfti5.geometry, npoints numeric); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_generatepoints(area cfti5.geometry, npoints numeric) TO utente;


--
-- Name: FUNCTION st_geogfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geogfromtext(text) TO utente;


--
-- Name: FUNCTION st_geogfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geogfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geographyfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geographyfromtext(text) TO utente;


--
-- Name: FUNCTION st_geohash(geog cfti5.geography, maxchars integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geohash(geog cfti5.geography, maxchars integer) TO utente;


--
-- Name: FUNCTION st_geohash(geom cfti5.geometry, maxchars integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geohash(geom cfti5.geometry, maxchars integer) TO utente;


--
-- Name: FUNCTION st_geomcollfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomcollfromtext(text) TO utente;


--
-- Name: FUNCTION st_geomcollfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomcollfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_geomcollfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomcollfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomcollfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomcollfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_geometricmedian(g cfti5.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geometricmedian(g cfti5.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean) TO utente;


--
-- Name: FUNCTION st_geometryfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geometryfromtext(text) TO utente;


--
-- Name: FUNCTION st_geometryfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geometryfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_geometryn(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geometryn(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_geometrytype(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geometrytype(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_geomfromewkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromewkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomfromewkt(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromewkt(text) TO utente;


--
-- Name: FUNCTION st_geomfromgeohash(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromgeohash(text, integer) TO utente;


--
-- Name: FUNCTION st_geomfromgeojson(json); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromgeojson(json) TO utente;


--
-- Name: FUNCTION st_geomfromgeojson(jsonb); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromgeojson(jsonb) TO utente;


--
-- Name: FUNCTION st_geomfromgeojson(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromgeojson(text) TO utente;


--
-- Name: FUNCTION st_geomfromgml(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromgml(text) TO utente;


--
-- Name: FUNCTION st_geomfromgml(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromgml(text, integer) TO utente;


--
-- Name: FUNCTION st_geomfromkml(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromkml(text) TO utente;


--
-- Name: FUNCTION st_geomfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromtext(text) TO utente;


--
-- Name: FUNCTION st_geomfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_geomfromtwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromtwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geomfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_georeference(rast cfti5.raster, format text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_georeference(rast cfti5.raster, format text) TO utente;


--
-- Name: FUNCTION st_geotransform(cfti5.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_geotransform(cfti5.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision) TO utente;


--
-- Name: FUNCTION st_gmltosql(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_gmltosql(text) TO utente;


--
-- Name: FUNCTION st_gmltosql(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_gmltosql(text, integer) TO utente;


--
-- Name: FUNCTION st_grayscale(rastbandargset cfti5.rastbandarg[], extenttype text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_grayscale(rastbandargset cfti5.rastbandarg[], extenttype text) TO utente;


--
-- Name: FUNCTION st_grayscale(rast cfti5.raster, redband integer, greenband integer, blueband integer, extenttype text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_grayscale(rast cfti5.raster, redband integer, greenband integer, blueband integer, extenttype text) TO utente;


--
-- Name: FUNCTION st_hasarc(geometry cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_hasarc(geometry cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_hasnoband(rast cfti5.raster, nband integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_hasnoband(rast cfti5.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_hausdorffdistance(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_hausdorffdistance(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_hausdorffdistance(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_hausdorffdistance(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_height(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_height(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_hillshade(rast cfti5.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_hillshade(rast cfti5.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_hillshade(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_hillshade(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_histogram(rast cfti5.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rast cfti5.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rast cfti5.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rast cfti5.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rast cfti5.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_interiorringn(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_interiorringn(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_interpolatepoint(line cfti5.geometry, point cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_interpolatepoint(line cfti5.geometry, point cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION st_intersection(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(rast cfti5.raster, geomin cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast cfti5.raster, geomin cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(text, text) TO utente;


--
-- Name: FUNCTION st_intersection(geomin cfti5.geometry, rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(geomin cfti5.geometry, rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, nodataval double precision[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersection(rast cfti5.raster, band integer, geomin cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast cfti5.raster, band integer, geomin cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, returnband text, nodataval double precision[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, returnband text, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, returnband text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, rast2 cfti5.raster, returnband text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, nodataval double precision[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, returnband text, nodataval double precision[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, returnband text, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, returnband text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersection(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, returnband text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersects(cfti5.geography, cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(cfti5.geography, cfti5.geography) TO utente;


--
-- Name: FUNCTION st_intersects(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_intersects(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_intersects(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(text, text) TO utente;


--
-- Name: FUNCTION st_intersects(geom cfti5.geometry, rast cfti5.raster, nband integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(geom cfti5.geometry, rast cfti5.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_intersects(rast cfti5.raster, geom cfti5.geometry, nband integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(rast cfti5.raster, geom cfti5.geometry, nband integer) TO utente;


--
-- Name: FUNCTION st_intersects(rast cfti5.raster, nband integer, geom cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(rast cfti5.raster, nband integer, geom cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_intersects(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_intersects(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_isclosed(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isclosed(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_iscollection(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_iscollection(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_iscoveragetile(rast cfti5.raster, coverage cfti5.raster, tilewidth integer, tileheight integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_iscoveragetile(rast cfti5.raster, coverage cfti5.raster, tilewidth integer, tileheight integer) TO utente;


--
-- Name: FUNCTION st_isempty(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isempty(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_isempty(rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isempty(rast cfti5.raster) TO utente;


--
-- Name: FUNCTION st_ispolygonccw(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ispolygonccw(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_ispolygoncw(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ispolygoncw(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_isring(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isring(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_issimple(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_issimple(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_isvalid(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvalid(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_isvalid(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvalid(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_isvaliddetail(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvaliddetail(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_isvaliddetail(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvaliddetail(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_isvalidreason(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvalidreason(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_isvalidreason(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvalidreason(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_isvalidtrajectory(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_isvalidtrajectory(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_length(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_length(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length(text) TO utente;


--
-- Name: FUNCTION st_length(geog cfti5.geography, use_spheroid boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length(geog cfti5.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_length2d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length2d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_length2d_spheroid(cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length2d_spheroid(cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_length2dspheroid(cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length2dspheroid(cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_length_spheroid(cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_length_spheroid(cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_lengthspheroid(cfti5.geometry, cfti5.spheroid); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_lengthspheroid(cfti5.geometry, cfti5.spheroid) TO utente;


--
-- Name: FUNCTION st_line_interpolate_point(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_line_interpolate_point(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_line_locate_point(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_line_locate_point(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_line_substring(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_line_substring(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_linecrossingdirection(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linecrossingdirection(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_linefromencodedpolyline(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linefromencodedpolyline(text, integer) TO utente;


--
-- Name: FUNCTION st_linefrommultipoint(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linefrommultipoint(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_linefromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linefromtext(text) TO utente;


--
-- Name: FUNCTION st_linefromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linefromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_linefromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linefromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_linefromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linefromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_lineinterpolatepoint(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_lineinterpolatepoint(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_lineinterpolatepoints(cfti5.geometry, double precision, repeat boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_lineinterpolatepoints(cfti5.geometry, double precision, repeat boolean) TO utente;


--
-- Name: FUNCTION st_linelocatepoint(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linelocatepoint(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_linemerge(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linemerge(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_linestringfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linestringfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_linestringfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linestringfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_linesubstring(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linesubstring(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_linetocurve(geometry cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_linetocurve(geometry cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_locate_along_measure(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_locate_along_measure(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_locate_between_measures(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_locate_between_measures(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_locatealong(geometry cfti5.geometry, measure double precision, leftrightoffset double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_locatealong(geometry cfti5.geometry, measure double precision, leftrightoffset double precision) TO utente;


--
-- Name: FUNCTION st_locatebetween(geometry cfti5.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_locatebetween(geometry cfti5.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision) TO utente;


--
-- Name: FUNCTION st_locatebetweenelevations(geometry cfti5.geometry, fromelevation double precision, toelevation double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_locatebetweenelevations(geometry cfti5.geometry, fromelevation double precision, toelevation double precision) TO utente;


--
-- Name: FUNCTION st_longestline(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_longestline(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_m(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_m(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_makebox2d(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makebox2d(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_makeemptycoverage(tilewidth integer, tileheight integer, width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeemptycoverage(tilewidth integer, tileheight integer, width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO utente;


--
-- Name: FUNCTION st_makeemptyraster(rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeemptyraster(rast cfti5.raster) TO utente;


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision) TO utente;


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO utente;


--
-- Name: FUNCTION st_makeenvelope(double precision, double precision, double precision, double precision, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeenvelope(double precision, double precision, double precision, double precision, integer) TO utente;


--
-- Name: FUNCTION st_makeline(cfti5.geometry[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeline(cfti5.geometry[]) TO utente;


--
-- Name: FUNCTION st_makeline(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeline(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_makepoint(double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makepoint(double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makepoint(double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makepoint(double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepointm(double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makepointm(double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepolygon(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makepolygon(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_makepolygon(cfti5.geometry, cfti5.geometry[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makepolygon(cfti5.geometry, cfti5.geometry[]) TO utente;


--
-- Name: FUNCTION st_makevalid(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makevalid(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast cfti5.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast cfti5.raster, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast cfti5.raster, nband integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast cfti5.raster, nband integer, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rastbandargset cfti5.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rastbandargset cfti5.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast1 cfti5.raster, rast2 cfti5.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast1 cfti5.raster, rast2 cfti5.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast cfti5.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast cfti5.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast cfti5.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent cfti5.raster, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast cfti5.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent cfti5.raster, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast cfti5.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast cfti5.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebra(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent cfti5.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast cfti5.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebraexpr(rast cfti5.raster, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast cfti5.raster, band integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebraexpr(rast cfti5.raster, band integer, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 cfti5.raster, rast2 cfti5.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebraexpr(rast1 cfti5.raster, rast2 cfti5.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebraexpr(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, onerastuserfunc regprocedure); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, band integer, onerastuserfunc regprocedure); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, band integer, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, pixeltype text, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, band integer, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, band integer, pixeltype text, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast cfti5.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast cfti5.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast1 cfti5.raster, rast2 cfti5.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast1 cfti5.raster, rast2 cfti5.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafct(rast1 cfti5.raster, band1 integer, rast2 cfti5.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafctngb(rast cfti5.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mapalgebrafctngb(rast cfti5.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_maxdistance(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_maxdistance(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mem_size(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mem_size(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_memsize(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_memsize(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_memsize(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_memsize(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_metadata(rast cfti5.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_metadata(rast cfti5.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer) TO utente;


--
-- Name: FUNCTION st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_minconvexhull(rast cfti5.raster, nband integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_minconvexhull(rast cfti5.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_minimumboundingcircle(inputgeom cfti5.geometry, segs_per_quarter integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_minimumboundingcircle(inputgeom cfti5.geometry, segs_per_quarter integer) TO utente;


--
-- Name: FUNCTION st_minimumboundingradius(cfti5.geometry, OUT center cfti5.geometry, OUT radius double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_minimumboundingradius(cfti5.geometry, OUT center cfti5.geometry, OUT radius double precision) TO utente;


--
-- Name: FUNCTION st_minimumclearance(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_minimumclearance(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_minimumclearanceline(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_minimumclearanceline(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_minpossiblevalue(pixeltype text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_minpossiblevalue(pixeltype text) TO utente;


--
-- Name: FUNCTION st_mlinefromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mlinefromtext(text) TO utente;


--
-- Name: FUNCTION st_mlinefromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mlinefromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_mlinefromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mlinefromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_mlinefromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mlinefromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_mpointfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpointfromtext(text) TO utente;


--
-- Name: FUNCTION st_mpointfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpointfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_mpointfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpointfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_mpointfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpointfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_mpolyfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpolyfromtext(text) TO utente;


--
-- Name: FUNCTION st_mpolyfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpolyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_mpolyfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpolyfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_mpolyfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_mpolyfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_multi(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multi(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_multilinefromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multilinefromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_multilinestringfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multilinestringfromtext(text) TO utente;


--
-- Name: FUNCTION st_multilinestringfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multilinestringfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_multipointfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipointfromtext(text) TO utente;


--
-- Name: FUNCTION st_multipointfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipointfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_multipointfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipointfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_multipolyfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipolyfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_multipolyfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipolyfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_multipolygonfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipolygonfromtext(text) TO utente;


--
-- Name: FUNCTION st_multipolygonfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_multipolygonfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_ndims(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ndims(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast cfti5.raster, pt cfti5.geometry, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_nearestvalue(rast cfti5.raster, pt cfti5.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast cfti5.raster, band integer, pt cfti5.geometry, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_nearestvalue(rast cfti5.raster, band integer, pt cfti5.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast cfti5.raster, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_nearestvalue(rast cfti5.raster, columnx integer, rowy integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast cfti5.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_nearestvalue(rast cfti5.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast cfti5.raster, pt cfti5.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_neighborhood(rast cfti5.raster, pt cfti5.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast cfti5.raster, band integer, pt cfti5.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_neighborhood(rast cfti5.raster, band integer, pt cfti5.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast cfti5.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_neighborhood(rast cfti5.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast cfti5.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_neighborhood(rast cfti5.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_node(g cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_node(g cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_normalize(geom cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_normalize(geom cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_notsamealignmentreason(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_notsamealignmentreason(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_npoints(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_npoints(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_nrings(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_nrings(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_numbands(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_numbands(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_numgeometries(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_numgeometries(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_numinteriorring(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_numinteriorring(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_numinteriorrings(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_numinteriorrings(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_numpatches(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_numpatches(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_numpoints(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_numpoints(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_offsetcurve(line cfti5.geometry, distance double precision, params text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_offsetcurve(line cfti5.geometry, distance double precision, params text) TO utente;


--
-- Name: FUNCTION st_orderingequals(geometrya cfti5.geometry, geometryb cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_orderingequals(geometrya cfti5.geometry, geometryb cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_orientedenvelope(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_orientedenvelope(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_overlaps(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_overlaps(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_overlaps(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_overlaps(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_overlaps(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_overlaps(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_patchn(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_patchn(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_perimeter(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_perimeter(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_perimeter(geog cfti5.geography, use_spheroid boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_perimeter(geog cfti5.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_perimeter2d(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_perimeter2d(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_pixelascentroid(rast cfti5.raster, x integer, y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelascentroid(rast cfti5.raster, x integer, y integer) TO utente;


--
-- Name: FUNCTION st_pixelascentroids(rast cfti5.raster, band integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelascentroids(rast cfti5.raster, band integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspoint(rast cfti5.raster, x integer, y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelaspoint(rast cfti5.raster, x integer, y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspoints(rast cfti5.raster, band integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelaspoints(rast cfti5.raster, band integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspolygon(rast cfti5.raster, x integer, y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelaspolygon(rast cfti5.raster, x integer, y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspolygons(rast cfti5.raster, band integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelaspolygons(rast cfti5.raster, band integer, exclude_nodata_value boolean, OUT geom cfti5.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelheight(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelheight(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast cfti5.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelofvalue(rast cfti5.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast cfti5.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelofvalue(rast cfti5.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast cfti5.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelofvalue(rast cfti5.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast cfti5.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelofvalue(rast cfti5.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelwidth(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pixelwidth(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_point(double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_point(double precision, double precision) TO utente;


--
-- Name: FUNCTION st_point_inside_circle(cfti5.geometry, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_point_inside_circle(cfti5.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_pointfromgeohash(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointfromgeohash(text, integer) TO utente;


--
-- Name: FUNCTION st_pointfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointfromtext(text) TO utente;


--
-- Name: FUNCTION st_pointfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_pointfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_pointfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_pointinsidecircle(cfti5.geometry, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointinsidecircle(cfti5.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_pointn(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointn(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_pointonsurface(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_pointonsurface(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_points(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_points(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_polyfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polyfromtext(text) TO utente;


--
-- Name: FUNCTION st_polyfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_polyfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polyfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_polyfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polyfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_polygon(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygon(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_polygon(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygon(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_polygonfromtext(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygonfromtext(text) TO utente;


--
-- Name: FUNCTION st_polygonfromtext(text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygonfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_polygonfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygonfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_polygonfromwkb(bytea, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygonfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_polygonize(cfti5.geometry[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygonize(cfti5.geometry[]) TO utente;


--
-- Name: FUNCTION st_project(geog cfti5.geography, distance double precision, azimuth double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_project(geog cfti5.geography, distance double precision, azimuth double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, nband integer, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, nband integer, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rast cfti5.raster, nband integer, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantizecoordinates(g cfti5.geometry, prec_x integer, prec_y integer, prec_z integer, prec_m integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_quantizecoordinates(g cfti5.geometry, prec_x integer, prec_y integer, prec_z integer, prec_m integer) TO utente;


--
-- Name: FUNCTION st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoord(rast cfti5.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastertoworldcoord(rast cfti5.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordx(rast cfti5.raster, xr integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastertoworldcoordx(rast cfti5.raster, xr integer) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordx(rast cfti5.raster, xr integer, yr integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastertoworldcoordx(rast cfti5.raster, xr integer, yr integer) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordy(rast cfti5.raster, yr integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastertoworldcoordy(rast cfti5.raster, yr integer) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordy(rast cfti5.raster, xr integer, yr integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastertoworldcoordy(rast cfti5.raster, xr integer, yr integer) TO utente;


--
-- Name: FUNCTION st_rastfromhexwkb(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastfromhexwkb(text) TO utente;


--
-- Name: FUNCTION st_rastfromwkb(bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rastfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_reclass(rast cfti5.raster, VARIADIC reclassargset cfti5.reclassarg[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_reclass(rast cfti5.raster, VARIADIC reclassargset cfti5.reclassarg[]) TO utente;


--
-- Name: FUNCTION st_reclass(rast cfti5.raster, reclassexpr text, pixeltype text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_reclass(rast cfti5.raster, reclassexpr text, pixeltype text) TO utente;


--
-- Name: FUNCTION st_reclass(rast cfti5.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_reclass(rast cfti5.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_relate(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_relate(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_relate(geom1 cfti5.geometry, geom2 cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_relate(geom1 cfti5.geometry, geom2 cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_relate(geom1 cfti5.geometry, geom2 cfti5.geometry, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_relate(geom1 cfti5.geometry, geom2 cfti5.geometry, text) TO utente;


--
-- Name: FUNCTION st_relatematch(text, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_relatematch(text, text) TO utente;


--
-- Name: FUNCTION st_removepoint(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_removepoint(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_removerepeatedpoints(geom cfti5.geometry, tolerance double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_removerepeatedpoints(geom cfti5.geometry, tolerance double precision) TO utente;


--
-- Name: FUNCTION st_resample(rast cfti5.raster, ref cfti5.raster, usescale boolean, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resample(rast cfti5.raster, ref cfti5.raster, usescale boolean, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resample(rast cfti5.raster, ref cfti5.raster, algorithm text, maxerr double precision, usescale boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resample(rast cfti5.raster, ref cfti5.raster, algorithm text, maxerr double precision, usescale boolean) TO utente;


--
-- Name: FUNCTION st_resample(rast cfti5.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resample(rast cfti5.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resample(rast cfti5.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resample(rast cfti5.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_rescale(rast cfti5.raster, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rescale(rast cfti5.raster, scalexy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_rescale(rast cfti5.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rescale(rast cfti5.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resize(rast cfti5.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resize(rast cfti5.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resize(rast cfti5.raster, width integer, height integer, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resize(rast cfti5.raster, width integer, height integer, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resize(rast cfti5.raster, width text, height text, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_resize(rast cfti5.raster, width text, height text, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_reskew(rast cfti5.raster, skewxy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_reskew(rast cfti5.raster, skewxy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_reskew(rast cfti5.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_reskew(rast cfti5.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_retile(tab regclass, col name, ext cfti5.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_retile(tab regclass, col name, ext cfti5.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text) TO utente;


--
-- Name: FUNCTION st_reverse(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_reverse(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_rotate(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotate(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotate(cfti5.geometry, double precision, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotate(cfti5.geometry, double precision, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_rotate(cfti5.geometry, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotate(cfti5.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_rotatex(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotatex(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotatey(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotatey(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotatez(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotatez(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotation(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_rotation(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_roughness(rast cfti5.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_roughness(rast cfti5.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_roughness(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_roughness(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_samealignment(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_samealignment(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision) TO utente;


--
-- Name: FUNCTION st_scale(cfti5.geometry, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_scale(cfti5.geometry, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_scale(cfti5.geometry, cfti5.geometry, origin cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_scale(cfti5.geometry, cfti5.geometry, origin cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_scale(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_scale(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_scale(cfti5.geometry, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_scale(cfti5.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_scalex(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_scalex(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_scaley(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_scaley(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_segmentize(geog cfti5.geography, max_segment_length double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_segmentize(geog cfti5.geography, max_segment_length double precision) TO utente;


--
-- Name: FUNCTION st_segmentize(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_segmentize(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_setbandindex(rast cfti5.raster, band integer, outdbindex integer, force boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setbandindex(rast cfti5.raster, band integer, outdbindex integer, force boolean) TO utente;


--
-- Name: FUNCTION st_setbandisnodata(rast cfti5.raster, band integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setbandisnodata(rast cfti5.raster, band integer) TO utente;


--
-- Name: FUNCTION st_setbandnodatavalue(rast cfti5.raster, nodatavalue double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setbandnodatavalue(rast cfti5.raster, nodatavalue double precision) TO utente;


--
-- Name: FUNCTION st_setbandnodatavalue(rast cfti5.raster, band integer, nodatavalue double precision, forcechecking boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setbandnodatavalue(rast cfti5.raster, band integer, nodatavalue double precision, forcechecking boolean) TO utente;


--
-- Name: FUNCTION st_setbandpath(rast cfti5.raster, band integer, outdbpath text, outdbindex integer, force boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setbandpath(rast cfti5.raster, band integer, outdbpath text, outdbindex integer, force boolean) TO utente;


--
-- Name: FUNCTION st_seteffectivearea(cfti5.geometry, double precision, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_seteffectivearea(cfti5.geometry, double precision, integer) TO utente;


--
-- Name: FUNCTION st_setgeoreference(rast cfti5.raster, georef text, format text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setgeoreference(rast cfti5.raster, georef text, format text) TO utente;


--
-- Name: FUNCTION st_setgeoreference(rast cfti5.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setgeoreference(rast cfti5.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision) TO utente;


--
-- Name: FUNCTION st_setgeotransform(rast cfti5.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setgeotransform(rast cfti5.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision) TO utente;


--
-- Name: FUNCTION st_setpoint(cfti5.geometry, integer, cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setpoint(cfti5.geometry, integer, cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_setrotation(rast cfti5.raster, rotation double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setrotation(rast cfti5.raster, rotation double precision) TO utente;


--
-- Name: FUNCTION st_setscale(rast cfti5.raster, scale double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setscale(rast cfti5.raster, scale double precision) TO utente;


--
-- Name: FUNCTION st_setscale(rast cfti5.raster, scalex double precision, scaley double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setscale(rast cfti5.raster, scalex double precision, scaley double precision) TO utente;


--
-- Name: FUNCTION st_setskew(rast cfti5.raster, skew double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setskew(rast cfti5.raster, skew double precision) TO utente;


--
-- Name: FUNCTION st_setskew(rast cfti5.raster, skewx double precision, skewy double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setskew(rast cfti5.raster, skewx double precision, skewy double precision) TO utente;


--
-- Name: FUNCTION st_setsrid(geog cfti5.geography, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setsrid(geog cfti5.geography, srid integer) TO utente;


--
-- Name: FUNCTION st_setsrid(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setsrid(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_setsrid(rast cfti5.raster, srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setsrid(rast cfti5.raster, srid integer) TO utente;


--
-- Name: FUNCTION st_setupperleft(rast cfti5.raster, upperleftx double precision, upperlefty double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setupperleft(rast cfti5.raster, upperleftx double precision, upperlefty double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast cfti5.raster, geom cfti5.geometry, newvalue double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalue(rast cfti5.raster, geom cfti5.geometry, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast cfti5.raster, nband integer, geom cfti5.geometry, newvalue double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalue(rast cfti5.raster, nband integer, geom cfti5.geometry, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast cfti5.raster, x integer, y integer, newvalue double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalue(rast cfti5.raster, x integer, y integer, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast cfti5.raster, band integer, x integer, y integer, newvalue double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalue(rast cfti5.raster, band integer, x integer, y integer, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalues(rast cfti5.raster, nband integer, geomvalset cfti5.geomval[], keepnodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalues(rast cfti5.raster, nband integer, geomvalset cfti5.geomval[], keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast cfti5.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalues(rast cfti5.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_setvalues(rast cfti5.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_sharedpaths(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_sharedpaths(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_shift_longitude(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_shift_longitude(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_shiftlongitude(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_shiftlongitude(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_shortestline(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_shortestline(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_simplify(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_simplify(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_simplify(cfti5.geometry, double precision, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_simplify(cfti5.geometry, double precision, boolean) TO utente;


--
-- Name: FUNCTION st_simplifypreservetopology(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_simplifypreservetopology(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_simplifyvw(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_simplifyvw(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_skewx(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_skewx(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_skewy(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_skewy(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_slope(rast cfti5.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_slope(rast cfti5.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_slope(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_slope(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_snap(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snap(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(cfti5.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(cfti5.geometry, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(geom1 cfti5.geometry, geom2 cfti5.geometry, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(rast cfti5.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(rast cfti5.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(rast cfti5.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(rast cfti5.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(rast cfti5.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_snaptogrid(rast cfti5.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO utente;


--
-- Name: FUNCTION st_split(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_split(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_srid(geog cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_srid(geog cfti5.geography) TO utente;


--
-- Name: FUNCTION st_srid(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_srid(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_srid(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_srid(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_startpoint(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_startpoint(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_subdivide(geom cfti5.geometry, maxvertices integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_subdivide(geom cfti5.geometry, maxvertices integer) TO utente;


--
-- Name: FUNCTION st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_summary(cfti5.geography); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summary(cfti5.geography) TO utente;


--
-- Name: FUNCTION st_summary(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summary(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_summary(rast cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summary(rast cfti5.raster) TO utente;


--
-- Name: FUNCTION st_summarystats(rast cfti5.raster, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystats(rast cfti5.raster, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_summarystats(rast cfti5.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystats(rast cfti5.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_swapordinates(geom cfti5.geometry, ords cstring); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_swapordinates(geom cfti5.geometry, ords cstring) TO utente;


--
-- Name: FUNCTION st_symdifference(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_symdifference(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_symmetricdifference(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_symmetricdifference(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_tile(rast cfti5.raster, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tile(rast cfti5.raster, width integer, height integer, padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_tile(rast cfti5.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tile(rast cfti5.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_tile(rast cfti5.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tile(rast cfti5.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_touches(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_touches(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_touches(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_touches(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_touches(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_touches(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_tpi(rast cfti5.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tpi(rast cfti5.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_tpi(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tpi(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_transform(cfti5.geometry, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(cfti5.geometry, integer) TO utente;


--
-- Name: FUNCTION st_transform(geom cfti5.geometry, to_proj text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(geom cfti5.geometry, to_proj text) TO utente;


--
-- Name: FUNCTION st_transform(geom cfti5.geometry, from_proj text, to_srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(geom cfti5.geometry, from_proj text, to_srid integer) TO utente;


--
-- Name: FUNCTION st_transform(geom cfti5.geometry, from_proj text, to_proj text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(geom cfti5.geometry, from_proj text, to_proj text) TO utente;


--
-- Name: FUNCTION st_transform(rast cfti5.raster, alignto cfti5.raster, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(rast cfti5.raster, alignto cfti5.raster, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_transform(rast cfti5.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(rast cfti5.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_transform(rast cfti5.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(rast cfti5.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_transform(rast cfti5.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transform(rast cfti5.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO utente;


--
-- Name: FUNCTION st_translate(cfti5.geometry, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_translate(cfti5.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_translate(cfti5.geometry, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_translate(cfti5.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_transscale(cfti5.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_transscale(cfti5.geometry, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_tri(rast cfti5.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tri(rast cfti5.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_tri(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_tri(rast cfti5.raster, nband integer, customextent cfti5.raster, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_unaryunion(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_unaryunion(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_union(cfti5.geometry[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.geometry[]) TO utente;


--
-- Name: FUNCTION st_union(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_upperleftx(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_upperleftx(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_upperlefty(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_upperlefty(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_value(rast cfti5.raster, pt cfti5.geometry, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_value(rast cfti5.raster, pt cfti5.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_value(rast cfti5.raster, band integer, pt cfti5.geometry, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_value(rast cfti5.raster, band integer, pt cfti5.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_value(rast cfti5.raster, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_value(rast cfti5.raster, x integer, y integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_value(rast cfti5.raster, band integer, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_value(rast cfti5.raster, band integer, x integer, y integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_valuecount(rast cfti5.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rast cfti5.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rast cfti5.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rast cfti5.raster, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rast cfti5.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rast cfti5.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rast cfti5.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rast cfti5.raster, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast cfti5.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rast cfti5.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast cfti5.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rast cfti5.raster, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast cfti5.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rast cfti5.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast cfti5.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rast cfti5.raster, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rast cfti5.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_voronoilines(g1 cfti5.geometry, tolerance double precision, extend_to cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_voronoilines(g1 cfti5.geometry, tolerance double precision, extend_to cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_voronoipolygons(g1 cfti5.geometry, tolerance double precision, extend_to cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_voronoipolygons(g1 cfti5.geometry, tolerance double precision, extend_to cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_width(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_width(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_within(geom1 cfti5.geometry, geom2 cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_within(geom1 cfti5.geometry, geom2 cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_within(rast1 cfti5.raster, rast2 cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_within(rast1 cfti5.raster, rast2 cfti5.raster) TO utente;


--
-- Name: FUNCTION st_within(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_within(rast1 cfti5.raster, nband1 integer, rast2 cfti5.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_wkbtosql(wkb bytea); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_wkbtosql(wkb bytea) TO utente;


--
-- Name: FUNCTION st_wkttosql(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_wkttosql(text) TO utente;


--
-- Name: FUNCTION st_worldtorastercoord(rast cfti5.raster, pt cfti5.geometry, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoord(rast cfti5.raster, pt cfti5.geometry, OUT columnx integer, OUT rowy integer) TO utente;


--
-- Name: FUNCTION st_worldtorastercoord(rast cfti5.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoord(rast cfti5.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordx(rast cfti5.raster, pt cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoordx(rast cfti5.raster, pt cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordx(rast cfti5.raster, xw double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoordx(rast cfti5.raster, xw double precision) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordx(rast cfti5.raster, xw double precision, yw double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoordx(rast cfti5.raster, xw double precision, yw double precision) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordy(rast cfti5.raster, pt cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoordy(rast cfti5.raster, pt cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordy(rast cfti5.raster, yw double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoordy(rast cfti5.raster, yw double precision) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordy(rast cfti5.raster, xw double precision, yw double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_worldtorastercoordy(rast cfti5.raster, xw double precision, yw double precision) TO utente;


--
-- Name: FUNCTION st_wrapx(geom cfti5.geometry, wrap double precision, move double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_wrapx(geom cfti5.geometry, wrap double precision, move double precision) TO utente;


--
-- Name: FUNCTION st_x(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_x(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_xmax(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_xmax(cfti5.box3d) TO utente;


--
-- Name: FUNCTION st_xmin(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_xmin(cfti5.box3d) TO utente;


--
-- Name: FUNCTION st_y(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_y(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_ymax(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ymax(cfti5.box3d) TO utente;


--
-- Name: FUNCTION st_ymin(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_ymin(cfti5.box3d) TO utente;


--
-- Name: FUNCTION st_z(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_z(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_zmax(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_zmax(cfti5.box3d) TO utente;


--
-- Name: FUNCTION st_zmflag(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_zmflag(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_zmin(cfti5.box3d); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_zmin(cfti5.box3d) TO utente;


--
-- Name: FUNCTION unlockrows(text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.unlockrows(text) TO utente;


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.updategeometrysrid(character varying, character varying, integer) TO utente;


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, character varying, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.updategeometrysrid(character varying, character varying, character varying, integer) TO utente;


--
-- Name: FUNCTION updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer) TO utente;


--
-- Name: FUNCTION updaterastersrid(table_name name, column_name name, new_srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.updaterastersrid(table_name name, column_name name, new_srid integer) TO utente;


--
-- Name: FUNCTION updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO utente;


--
-- Name: FUNCTION st_3dextent(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_3dextent(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_accum(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_accum(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_asgeobuf(anyelement); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeobuf(anyelement) TO utente;


--
-- Name: FUNCTION st_asgeobuf(anyelement, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asgeobuf(anyelement, text) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asmvt(anyelement) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asmvt(anyelement, text) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement, text, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asmvt(anyelement, text, integer) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement, text, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_asmvt(anyelement, text, integer, text) TO utente;


--
-- Name: FUNCTION st_clusterintersecting(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clusterintersecting(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_clusterwithin(cfti5.geometry, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_clusterwithin(cfti5.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_collect(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_collect(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_countagg(cfti5.raster, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_countagg(cfti5.raster, boolean) TO utente;


--
-- Name: FUNCTION st_countagg(cfti5.raster, integer, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_countagg(cfti5.raster, integer, boolean) TO utente;


--
-- Name: FUNCTION st_countagg(cfti5.raster, integer, boolean, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_countagg(cfti5.raster, integer, boolean, double precision) TO utente;


--
-- Name: FUNCTION st_extent(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_extent(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_makeline(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_makeline(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_memcollect(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_memcollect(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_memunion(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_memunion(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_polygonize(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_polygonize(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_samealignment(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_samealignment(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_summarystatsagg(cfti5.raster, boolean, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystatsagg(cfti5.raster, boolean, double precision) TO utente;


--
-- Name: FUNCTION st_summarystatsagg(cfti5.raster, integer, boolean); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystatsagg(cfti5.raster, integer, boolean) TO utente;


--
-- Name: FUNCTION st_summarystatsagg(cfti5.raster, integer, boolean, double precision); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_summarystatsagg(cfti5.raster, integer, boolean, double precision) TO utente;


--
-- Name: FUNCTION st_union(cfti5.geometry); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.geometry) TO utente;


--
-- Name: FUNCTION st_union(cfti5.raster); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.raster) TO utente;


--
-- Name: FUNCTION st_union(cfti5.raster, cfti5.unionarg[]); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.raster, cfti5.unionarg[]) TO utente;


--
-- Name: FUNCTION st_union(cfti5.raster, integer); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.raster, integer) TO utente;


--
-- Name: FUNCTION st_union(cfti5.raster, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.raster, text) TO utente;


--
-- Name: FUNCTION st_union(cfti5.raster, integer, text); Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON FUNCTION cfti5.st_union(cfti5.raster, integer, text) TO utente;


--
-- Name: TABLE fnloc2; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.fnloc2 TO utente;


--
-- Name: TABLE fschedac; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.fschedac TO utente;


--
-- Name: SEQUENCE fschedac_id_seq; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT ALL ON SEQUENCE cfti5.fschedac_id_seq TO utente;


--
-- Name: TABLE fscossec2; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.fscossec2 TO utente;


--
-- Name: TABLE geography_columns; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.geography_columns TO utente;


--
-- Name: TABLE geometry_columns; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.geometry_columns TO utente;


--
-- Name: TABLE raster_columns; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.raster_columns TO utente;


--
-- Name: TABLE raster_overviews; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.raster_overviews TO utente;


--
-- Name: TABLE seq_loc; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.seq_loc TO utente;


--
-- Name: TABLE seq_scosse; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.seq_scosse TO utente;


--
-- Name: TABLE spatial_ref_sys; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.spatial_ref_sys TO utente;


--
-- Name: TABLE vclassif; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vclassif TO utente;


--
-- Name: TABLE vcodric; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vcodric TO utente;


--
-- Name: TABLE vcommenti; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vcommenti TO utente;


--
-- Name: TABLE vdemo; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vdemo TO utente;


--
-- Name: TABLE vedifici; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vedifici TO utente;


--
-- Name: TABLE veffetti; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.veffetti TO utente;


--
-- Name: TABLE vfontidemo; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vfontidemo TO utente;


--
-- Name: TABLE vicon; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vicon TO utente;


--
-- Name: TABLE vnfiu; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vnfiu TO utente;


--
-- Name: TABLE vnloc; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vnloc TO utente;


--
-- Name: TABLE vnperiod; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vnperiod TO utente;


--
-- Name: TABLE vscheda; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vscheda TO utente;


--
-- Name: TABLE vscheda1; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vscheda1 TO utente;


--
-- Name: TABLE vschedac; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vschedac TO utente;


--
-- Name: TABLE vschedad; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vschedad TO utente;


--
-- Name: TABLE vschedae; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vschedae TO utente;


--
-- Name: TABLE vschedeb; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vschedeb TO utente;


--
-- Name: TABLE vscossec; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vscossec TO utente;


--
-- Name: TABLE vtabcatego; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vtabcatego TO utente;


--
-- Name: TABLE vtipfen; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vtipfen TO utente;


--
-- Name: TABLE vvalfonte; Type: ACL; Schema: cfti5; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE cfti5.vvalfonte TO utente;


--
-- Name: TABLE allegato; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.allegato TO utente;


--
-- Name: SEQUENCE allegato_codiceallegato_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.allegato_codiceallegato_seq TO utente;


--
-- Name: TABLE button; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.button TO utente;


--
-- Name: SEQUENCE button_id_button_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.button_id_button_seq TO utente;


--
-- Name: TABLE cache_reg; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.cache_reg TO utente;


--
-- Name: SEQUENCE cache_reg_id_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.cache_reg_id_seq TO utente;


--
-- Name: TABLE gruppo; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.gruppo TO utente;


--
-- Name: TABLE link; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.link TO utente;


--
-- Name: SEQUENCE link_codicelink_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.link_codicelink_seq TO utente;


--
-- Name: TABLE log; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.log TO utente;


--
-- Name: SEQUENCE log_id_log_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.log_id_log_seq TO utente;


--
-- Name: TABLE recordlock; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.recordlock TO utente;


--
-- Name: TABLE registro_col; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.registro_col TO utente;


--
-- Name: SEQUENCE registro_col_id_reg_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.registro_col_id_reg_seq TO utente;


--
-- Name: TABLE registro_submask; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.registro_submask TO utente;


--
-- Name: TABLE registro_submask_col; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.registro_submask_col TO utente;


--
-- Name: SEQUENCE registro_submask_col_id_reg_sub_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.registro_submask_col_id_reg_sub_seq TO utente;


--
-- Name: SEQUENCE registro_submask_id_submask_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.registro_submask_id_submask_seq TO utente;


--
-- Name: TABLE registro_tab; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.registro_tab TO utente;


--
-- Name: SEQUENCE registro_tab_id_table_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.registro_tab_id_table_seq TO utente;


--
-- Name: TABLE stat; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.stat TO utente;


--
-- Name: SEQUENCE stat_id_stat_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.stat_id_stat_seq TO utente;


--
-- Name: TABLE utente; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.utente TO utente;


--
-- Name: SEQUENCE utente_id_utente_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.utente_id_utente_seq TO utente;


--
-- Name: TABLE variabili; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.variabili TO utente;


--
-- Name: TABLE widget; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.widget TO utente;


--
-- Name: SEQUENCE widget_id_widget_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.widget_id_widget_seq TO utente;


--
-- Name: TABLE xml_rules; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE frontend.xml_rules TO utente;


--
-- Name: SEQUENCE xml_rules_id_xml_rules_seq; Type: ACL; Schema: frontend; Owner: postgres
--

GRANT ALL ON SEQUENCE frontend.xml_rules_id_xml_rules_seq TO utente;


--
-- PostgreSQL database dump complete
--

