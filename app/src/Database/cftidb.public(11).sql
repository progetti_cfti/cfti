--
-- PostgreSQL database dump
--

-- Dumped from database version 11.17 (Debian 11.17-0+deb10u1)
-- Dumped by pg_dump version 12.12 (Debian 12.12-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cfti5public; Type: SCHEMA; Schema: -; Owner: utente
--

CREATE SCHEMA cfti5public;


ALTER SCHEMA cfti5public OWNER TO utente;

--
-- Name: frontend; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA frontend;


ALTER SCHEMA frontend OWNER TO postgres;

--
-- Name: SCHEMA frontend; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA frontend IS 'Schema dedicato alla gestione del frontend web VFront.';


--
-- Name: frontend2; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA frontend2;


ALTER SCHEMA frontend2 OWNER TO postgres;

--
-- Name: SCHEMA frontend2; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA frontend2 IS 'Schema dedicato alla gestione del frontend web VFront.';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: coordinate_centesimali(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_centesimali() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  /*  NEW.lat_centesimale = NEW.latloc * 2;*/
  NEW.lat_centesimale = ( 
    CASE
        WHEN (LENGTH( NEW.latloc::text ) = 8) THEN   round( (    ( ( ( ( ( CONCAT(SUBSTRING(NEW.latloc::text,5,2),'.',SUBSTRING(NEW.latloc::text,7,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.latloc::text,3,2))::numeric   )/60  ) + ( (SUBSTRING(NEW.latloc::text,1,2))::numeric )   ) , 4 )  
        WHEN (LENGTH( NEW.latloc::text ) = 7) THEN 0
    END 
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION public.coordinate_centesimali() OWNER TO utente;

--
-- Name: coordinate_centesimali_lat_scossec(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_centesimali_lat_scossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi numeric;
v1 numeric;
totale_centesimale numeric;
BEGIN
 totale_centesimale = 5;
 IF ( LENGTH(NEW.latmacs::text)=4 ) 
 then   v1 =  ( ( SUBSTRING(NEW.latmacs::text,3,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.latmacs::text,1,2)    )::numeric;
		totale_centesimale = round((gradi + v1),2);
       
 END IF;

NEW.lat_centesimale = ( totale_centesimale::text);
RETURN NEW;

END
$$;


ALTER FUNCTION public.coordinate_centesimali_lat_scossec() OWNER TO utente;

--
-- Name: coordinate_centesimali_lon(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_centesimali_lon() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  /*  NEW.lat_centesimale = NEW.latloc * 2;*/
  NEW.lon_centesimale = ( 
    CASE
          WHEN  (  ((SUBSTRING(NEW.longlog::text,1,1))!= '-') and  (LENGTH( NEW.longlog::text ) = 8) ) THEN  round(  (     ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,5,2),'.',SUBSTRING(NEW.longlog::text,7,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,3,2))::numeric   )/60  ) + ( (SUBSTRING(NEW.longlog::text,1,2))::numeric) ) ,  4 )
          WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))!= '-') and (LENGTH( NEW.longlog::text ) = 7) ) THEN   round(  (     ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,4,2),'.',SUBSTRING(NEW.longlog::text,6,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,2,2))::numeric   )/60  ) + ( (SUBSTRING(NEW.longlog::text,1,1))::numeric)  ) ,  4 )
		  WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))='-') and  LENGTH( NEW.longlog::text ) = 6 ) THEN    round(   ( CONCAT( '-',  ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,3,2),'.',SUBSTRING(NEW.longlog::text,5,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,2,1))::numeric   )/60  )::text)::numeric ),  4)
		  WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))='-') and  LENGTH( NEW.longlog::text ) = 7 ) THEN     round(   ( CONCAT( '-',  ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,4,2),'.',SUBSTRING(NEW.longlog::text,6,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,2,2))::numeric   )/60  )::text)::numeric  ),  4)
          WHEN ( ((SUBSTRING(NEW.longlog::text,1,1))='-') and  LENGTH( NEW.longlog::text ) = 8 ) THEN   round(   (  CONCAT( '-',  ( ( ( ( ( CONCAT(SUBSTRING(NEW.longlog::text,5,2),'.',SUBSTRING(NEW.longlog::text,7,2) )   )::numeric    )/60  )  +  (SUBSTRING(NEW.longlog::text,3,2))::numeric   )/60   + ( (SUBSTRING(NEW.longlog::text,2,1))::numeric)    )::text)::numeric   ),  4)  
    END 
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION public.coordinate_centesimali_lon() OWNER TO utente;

--
-- Name: coordinate_centesimali_lon_scossec(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_centesimali_lon_scossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi numeric;
v1 numeric;
totale_centesimale numeric;
BEGIN
totale_centesimale = 0;

 IF ( LENGTH(NEW.lonmacs::text)=4 ) 
 then   v1 =  ( ( SUBSTRING(NEW.lonmacs::text,3,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.lonmacs::text,1,2)    )::numeric;
		totale_centesimale = round((gradi + v1),2);   
 END IF;
 
 IF ( LENGTH(NEW.lonmacs::text)=3 ) 
 then   v1 =  ( ( SUBSTRING(NEW.lonmacs::text,2,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.lonmacs::text,1,1)    )::numeric;
		totale_centesimale = round((gradi + v1),2);
 END IF;

 NEW.lon_centesimale = ( 
    
	            totale_centesimale::text
    
        );
        RETURN NEW;

END
$$;


ALTER FUNCTION public.coordinate_centesimali_lon_scossec() OWNER TO utente;

--
-- Name: coordinate_centesimali_scossec(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_centesimali_scossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi numeric;
v1 numeric;
totale_centesimale numeric;
BEGIN
 IF ( LENGTH(NEW.latmacs::text)=4 ) 
 then   v1 =  ( ( SUBSTRING(NEW.latmacs::text,3,2)    )::numeric    )/60;
        gradi = ( SUBSTRING(NEW.latmacs::text,1,2)    )::numeric;
		totale_centesimale = round((gradi + v1),2);
       NEW.lat_centesimale = ( 
    
	            totale_centesimale::text
    
        );
        RETURN NEW;
 END IF;


END
$$;


ALTER FUNCTION public.coordinate_centesimali_scossec() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lat(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_sessagesimali_lat() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lat_centesimale::text, '.', 1);
 decimali = split_part(NEW.lat_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 v3 = split_part(v2, '.', 1);
 IF ( LENGTH(v3)=1 ) 
 then  v3 = CONCAT('0',v3);
 END IF;
 v4 = split_part(v2, '.', 2);
 v4 = concat('0.',v4);
 v5 = round((v4::numeric * 60),2)::text;
 v6 =  split_part(v5, '.', 1);
 IF ( LENGTH(v6)=1 ) 
 then  v6 = CONCAT('0',v6);
 END IF;
 v7 =  split_part(v5, '.', 2);
  NEW.latloc = ( 
    
	             concat(gradi,v3,v6,v7)
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION public.coordinate_sessagesimali_lat() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lat_fscossec(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_sessagesimali_lat_fscossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lat_centesimale::text, '.', 1);
 decimali = split_part(NEW.lat_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 
 IF( v2::numeric = 0)
  then v3 = '00';	
 elseif (LENGTH(v2::text)=1) then
   v3 = concat('0',v2);
 elseif (LENGTH(split_part(v2, '.', 1)) = 2 )
 then  v3 = split_part(v2, '.', 1);
  elseif (LENGTH(split_part(v2, '.', 1)) = 1 )
 then  v3 = concat('0',split_part(v2, '.', 1));
	ELSE v3 = v2;
 END IF;
 

  NEW.latmacs = ( 
    
	             concat(gradi,v3)::numeric
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION public.coordinate_sessagesimali_lat_fscossec() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lon(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_sessagesimali_lon() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lon_centesimale::text, '.', 1);
 decimali = split_part(NEW.lon_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 v3 = split_part(v2, '.', 1);
 IF ( LENGTH(v3)=1 ) 
 then  v3 = CONCAT('0',v3);
 END IF;
 v4 = split_part(v2, '.', 2);
 v4 = concat('0.',v4);
 v5 = round((v4::numeric * 60),2)::text;
 v6 =  split_part(v5, '.', 1);
 IF ( LENGTH(v6)=1 ) 
 then  v6 = CONCAT('0',v6);
 END IF;
 v7 =  split_part(v5, '.', 2);
  NEW.longlog = ( 
    
	             concat(gradi,v3,v6,v7)
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION public.coordinate_sessagesimali_lon() OWNER TO utente;

--
-- Name: coordinate_sessagesimali_lon_fscossec(); Type: FUNCTION; Schema: public; Owner: utente
--

CREATE FUNCTION public.coordinate_sessagesimali_lon_fscossec() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
gradi text;
v1 text;
decimali text;
v2 text;
v3 text;
v4 text;
v5 text;
v6 text;
v7 text;
BEGIN
 gradi = split_part(NEW.lon_centesimale::text, '.', 1);
 decimali = split_part(NEW.lon_centesimale::text, '.', 2);
 v1 = concat('0.', decimali)::numeric;
 v2 = (v1::numeric * 60)::text;
 
 IF( v2::numeric = 0)
  then v3 = '00';	
 elseif (LENGTH(v2::text)=1) then
   v3 = concat('0',v2);
 elseif (LENGTH(split_part(v2, '.', 1)) = 2 )
 then  v3 = split_part(v2, '.', 1);
  elseif (LENGTH(split_part(v2, '.', 1)) = 1 )
 then  v3 = concat('0',split_part(v2, '.', 1));
	ELSE v3 = v2;
 END IF;
 

  NEW.lonmacs = ( 
    
	             concat(gradi,v3)::numeric
    
   );
    RETURN NEW;
END
$$;


ALTER FUNCTION public.coordinate_sessagesimali_lon_fscossec() OWNER TO utente;

--
-- Name: fnloc_geometry(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnloc_geometry() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
  NEW.geom = ST_SetSRID(ST_MakePoint(NEW.lon_centesimale , NEW.lat_centesimale), 4326);
  RETURN NEW;
END
$$;


ALTER FUNCTION public.fnloc_geometry() OWNER TO postgres;

--
-- Name: fscossec_geometry(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fscossec_geometry() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
  NEW.geom = ST_SetSRID(ST_MakePoint(NEW.lon_centesimale , NEW.lat_centesimale), 4326);
  RETURN NEW;
END
$$;


ALTER FUNCTION public.fscossec_geometry() OWNER TO postgres;

--
-- Name: genera_ordinam(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genera_ordinam() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
  NEW.ordinam = ( 
    CASE
        WHEN ( NEW.autore1 ) = '*' THEN NEW.titolo1   
        WHEN ( NEW.autore1 ) <> '*' THEN NEW.autore1  
    END 
   );
  RETURN NEW;
END
$$;


ALTER FUNCTION public.genera_ordinam() OWNER TO postgres;

SET default_tablespace = '';

--
-- Name: fnperiod_cfti; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fnperiod_cfti (
    id integer NOT NULL,
    nperiod_cfti character varying(25)
);


ALTER TABLE cfti5public.fnperiod_cfti OWNER TO utente;

--
-- Name: fnperiod; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fnperiod (
    id integer NOT NULL,
    nperiod character varying(6),
    datainiznp integer,
    datafinenp integer,
    codtotnperiod character varying(16),
    commentonperiod text,
    numcomm character varying(6),
    numbiblio character varying(12),
    numtb integer,
    livnp character varying(1),
    cric_np character varying(4),
    feffetti integer,
    ftipfen integer,
    areaepicentrale character varying,
    in_cfti boolean
);


ALTER TABLE public.fnperiod OWNER TO utente;

--
-- Name: fnperiod_cfti_view; Type: VIEW; Schema: cfti5public; Owner: utente
--

CREATE VIEW cfti5public.fnperiod_cfti_view AS
 SELECT fnperiod_cfti.nperiod_cfti,
    fnperiod.nperiod,
    fnperiod.id
   FROM public.fnperiod,
    cfti5public.fnperiod_cfti
  WHERE ((fnperiod_cfti.nperiod_cfti)::text = (fnperiod.nperiod)::text);


ALTER TABLE cfti5public.fnperiod_cfti_view OWNER TO utente;

--
-- Name: fscheda; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fscheda (
    id integer NOT NULL,
    codbib character varying(6),
    codric character varying(4),
    codmat character varying(8),
    autore1 text,
    ruolo character varying(40),
    rigeaut character varying(25),
    dalaut character varying(4),
    alaut character varying(4),
    titolo1 text,
    rigetit character varying(50),
    editaut integer,
    telrepe character varying(25),
    luogoed character varying(200),
    datauni character varying(4),
    testall1 integer,
    inizdis character varying(4),
    finedis character varying(4),
    complsn integer,
    statola character varying(2),
    classif character varying(4),
    nloca character varying(5),
    codmicr integer,
    fotiniz integer,
    codmicrenel integer,
    prota character varying(6),
    dataggscha character varying(25),
    oraggscha time without time zone,
    notea text,
    colloca character varying(65),
    tipodat integer,
    daltit character varying(4),
    altit character varying(4),
    dataun2 character varying(4),
    ordinam character varying(75),
    tipoa character varying(10),
    foto integer,
    grafici integer,
    tabelle integer,
    disegni integer,
    fscheda1 integer,
    fclassif integer
);


ALTER TABLE public.fscheda OWNER TO utente;

--
-- Name: fschedeb; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fschedeb (
    id integer NOT NULL,
    datab integer,
    dab integer,
    ab integer,
    usorar character varying(5),
    guerra integer,
    crisiec integer,
    tensoc integer,
    epidem integer,
    caresti integer,
    efflper integer,
    teorimp integer,
    noteb text,
    negativob integer,
    protb character varying(15),
    nperiob character varying(6),
    lungtb character varying(10),
    ncomp character varying(3),
    valb character varying(4),
    danni integer,
    codbtot character varying(30),
    codricb character varying(4),
    testob text,
    cbnptot character varying(12),
    fscheda integer,
    fscheda1 integer,
    feffetti integer,
    ftipfen integer,
    fnperiod integer,
    fvalfonte integer
);


ALTER TABLE public.fschedeb OWNER TO utente;

--
-- Name: fvalfonte; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fvalfonte (
    id integer NOT NULL,
    codval character varying(3),
    desval text,
    desvalingl text,
    noteval text,
    ordine_val numeric,
    cric_vb character varying(4)
);


ALTER TABLE public.fvalfonte OWNER TO utente;

--
-- Name: schedeb; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.schedeb AS
 SELECT fschedeb.id,
    fnperiod_cfti_view.nperiod,
    fscheda.codbib,
    fschedeb.valb,
    fvalfonte.desvalingl,
    concat('http://www.cftilab.it/file_repository/pdf_T/', fnperiod_cfti_view.nperiod, '-', fscheda.codbib, '_T.pdf') AS pdf_trascritto,
    concat('http://www.cftilab.it/file_repository/pdf_R/', fnperiod_cfti_view.nperiod, '-', fscheda.codbib, '_R.pdf') AS pdf_raster
   FROM (((public.fschedeb
     JOIN cfti5public.fnperiod_cfti_view ON ((fschedeb.fnperiod = fnperiod_cfti_view.id)))
     LEFT JOIN public.fvalfonte ON ((fschedeb.fvalfonte = fvalfonte.id)))
     LEFT JOIN public.fscheda ON ((fschedeb.fscheda = fscheda.id)))
  WITH NO DATA;


ALTER TABLE cfti5public.schedeb OWNER TO utente;

--
-- Name: schedea; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.schedea AS
 SELECT DISTINCT ON (fscheda.codbib) fscheda.id,
    fscheda.codbib,
    fscheda.autore1,
    fscheda.titolo1,
    fscheda.luogoed,
    fscheda.datauni,
    fscheda.dataun2,
    fscheda.ordinam
   FROM public.fscheda,
    cfti5public.schedeb
  WHERE ((fscheda.codbib)::text = (schedeb.codbib)::text)
  WITH NO DATA;


ALTER TABLE cfti5public.schedea OWNER TO utente;

--
-- Name: biblio_details; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.biblio_details AS
 SELECT schedea.codbib,
    schedea.autore1,
    schedea.titolo1,
    schedea.luogoed,
    schedea.datauni,
    schedea.dataun2,
    schedeb.desvalingl,
    schedeb.valb,
    schedeb.nperiod
   FROM (cfti5public.schedea
     JOIN cfti5public.schedeb ON (((schedea.codbib)::text = (schedeb.codbib)::text)))
  WITH NO DATA;


ALTER TABLE cfti5public.biblio_details OWNER TO utente;

--
-- Name: biblio_ee; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.biblio_ee (
    id integer NOT NULL,
    codbib character varying(25)
);


ALTER TABLE cfti5public.biblio_ee OWNER TO utente;

--
-- Name: codbib_list_ee; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.codbib_list_ee (
    id integer NOT NULL,
    codbib character varying(25)
);


ALTER TABLE cfti5public.codbib_list_ee OWNER TO utente;

--
-- Name: fcommenti; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fcommenti (
    id integer NOT NULL,
    codcomm character varying(20),
    testocomm text,
    nperiodcomm character varying(10),
    codfencomm character varying(3),
    cric_cm character varying(4),
    fnloc integer,
    ftabcatego integer,
    fnperiod integer,
    feffetti integer
);


ALTER TABLE public.fcommenti OWNER TO utente;

--
-- Name: ftabcatego; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.ftabcatego (
    id integer NOT NULL,
    codtab character varying(10),
    destab text,
    ordine numeric,
    desing text,
    cric_ca character varying(4)
);


ALTER TABLE public.ftabcatego OWNER TO utente;

--
-- Name: commgen; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.commgen AS
 SELECT fcommenti.id,
    fnperiod_cfti_view.nperiod_cfti,
    ftabcatego.codtab,
    fcommenti.testocomm
   FROM public.fcommenti,
    public.ftabcatego,
    cfti5public.fnperiod_cfti_view
  WHERE (((ftabcatego.codtab)::text <> 'D1'::text) AND ((ftabcatego.codtab)::text <> 'E1'::text) AND ((ftabcatego.codtab)::text <> 'A0'::text) AND ((ftabcatego.codtab)::text <> 'A1'::text) AND (fcommenti.fnperiod = fnperiod_cfti_view.id) AND (fcommenti.ftabcatego = ftabcatego.id))
  WITH NO DATA;


ALTER TABLE cfti5public.commgen OWNER TO utente;

--
-- Name: fnloc; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fnloc (
    id integer NOT NULL,
    nloc character varying(250),
    desloc character varying(250),
    latloc integer,
    longlog integer,
    quotloc character varying(250),
    numabit character varying(250),
    prov character varying(250),
    provlet character varying(250),
    regione character varying(250),
    comune character varying(250),
    frazion character varying(250),
    foglio character varying(250),
    tavolet character varying(250),
    risentimenti integer,
    siglastato character varying(250),
    desloc2 character varying(250),
    notesito text,
    cric_nl character varying(250),
    notenl text,
    lat_centesimale numeric,
    lon_centesimale numeric,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.fnloc OWNER TO utente;

--
-- Name: d1; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.d1 AS
 SELECT fcommenti.id,
    fnperiod_cfti_view.nperiod_cfti,
    ftabcatego.codtab,
    fcommenti.testocomm,
    fnloc.nloc
   FROM public.fcommenti,
    cfti5public.fnperiod_cfti_view,
    public.ftabcatego,
    public.fnloc
  WHERE (((ftabcatego.codtab)::text = 'D1'::text) AND (fcommenti.fnperiod = fnperiod_cfti_view.id) AND (fcommenti.ftabcatego = ftabcatego.id) AND (fcommenti.fnloc = fnloc.id))
  WITH NO DATA;


ALTER TABLE cfti5public.d1 OWNER TO utente;

--
-- Name: ee_tabella; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.ee_tabella (
    id integer NOT NULL,
    nterr character varying(25),
    nperiod character varying(25),
    nloc character varying(25),
    codice_eff character varying(25),
    commento text,
    risentimenti integer,
    maxint numeric
);


ALTER TABLE cfti5public.ee_tabella OWNER TO utente;

--
-- Name: fnloc_cfti_5; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fnloc_cfti_5 (
    id integer NOT NULL,
    nloc_6 character varying(250),
    nloc character varying(250),
    desloc text,
    provlet character varying(25),
    nazione character varying(250),
    postal_code character varying(25),
    quot_loc character varying(250),
    numabit character varying(250),
    prov character varying(25),
    regione character varying(250),
    comune character varying(250),
    frazione character varying(25),
    notesito text,
    lat_roma40 character varying(25),
    lon_roma40 character varying(25),
    istat character varying(250),
    istat_103 character varying(250),
    comune_nome text,
    capoluogo character varying(25),
    cap character varying(25),
    problemi text,
    istat_2011 character varying(250),
    comune_nome_2011 text,
    capoluogo_2011 character varying(25),
    in_cfti character varying(25),
    nloc_cfti character varying(250),
    desloc_cfti text,
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet_old character varying(25),
    cod_compl text,
    note_2018 text
);


ALTER TABLE cfti5public.fnloc_cfti_5 OWNER TO utente;

--
-- Name: ee; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.ee AS
 SELECT ee_tabella.id,
    ee_tabella.nterr,
    ee_tabella.nperiod,
    ee_tabella.nloc,
    ee_tabella.codice_eff,
    ee_tabella.commento,
    ee_tabella.risentimenti,
    ee_tabella.maxint,
    fnloc_cfti_5.nloc_cfti,
    fnloc_cfti_5.desloc_cfti,
    fnloc_cfti_5.lat_wgs84,
    fnloc_cfti_5.lon_wgs84,
    COALESCE(fnloc_cfti_5.notesito, '-'::text) AS notesito,
    fnloc_cfti_5.provlet,
    fnloc_cfti_5.nazione,
    fnloc_cfti_5.desloc
   FROM (cfti5public.ee_tabella
     LEFT JOIN cfti5public.fnloc_cfti_5 ON (((ee_tabella.nloc)::text = (fnloc_cfti_5.nloc)::text)))
  WITH NO DATA;


ALTER TABLE cfti5public.ee OWNER TO utente;

--
-- Name: ee_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.ee_med (
    id integer NOT NULL,
    codice_eff character varying(2),
    nterr character varying(25),
    nloc character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    desloc character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    nazione character varying(25),
    provlet character varying(25),
    notesito text
);


ALTER TABLE cfti5public.ee_med OWNER TO utente;

--
-- Name: fee_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fee_med (
    id integer NOT NULL,
    codice_eff character varying(2),
    nterr character varying(25),
    id_nterr integer,
    nloc character varying(25),
    id_nloc integer
);


ALTER TABLE cfti5public.fee_med OWNER TO utente;

--
-- Name: fee_med_total; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fee_med_total (
    id integer NOT NULL,
    nterr character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    codice_eff character varying(2),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    notesito text,
    provlet character varying(2),
    nazione character varying(25),
    nloc character varying(25),
    desloc character varying(250)
);


ALTER TABLE cfti5public.fee_med_total OWNER TO utente;

--
-- Name: fnloc_cfti; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fnloc_cfti (
    id integer NOT NULL,
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet_cfti character varying(250),
    nazione character varying(250),
    postal_code character varying(250),
    notesito text
);


ALTER TABLE cfti5public.fnloc_cfti OWNER TO utente;

--
-- Name: fpq_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fpq_med (
    id integer NOT NULL,
    intpqnum character varying(25),
    intpq character varying(25),
    nterr character varying(25),
    id_nterr integer,
    nloc character varying(25),
    id_nloc integer
);


ALTER TABLE cfti5public.fpq_med OWNER TO utente;

--
-- Name: fpq_med_total; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.fpq_med_total (
    id integer NOT NULL,
    nterr character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    intpqnum character varying(25),
    intpq character varying(25),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet character varying(25),
    notesito text,
    nloc character varying(25),
    desloc character varying(250),
    nazione character varying(25)
);


ALTER TABLE cfti5public.fpq_med_total OWNER TO utente;

--
-- Name: locind_ee; Type: VIEW; Schema: cfti5public; Owner: utente
--

CREATE VIEW cfti5public.locind_ee AS
 SELECT row_number() OVER (ORDER BY p.desloc_cfti) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.lat_wgs84,
    p.lon_wgs84,
    p.nloc,
    p.desloc,
    p.provlet,
    p.nazione,
    p.notesito,
    p.risentimenti,
    p.maxint,
    p.ee
   FROM ( SELECT DISTINCT ON (m.nloc_cfti) m.nloc_cfti,
            m.desloc_cfti,
            m.lat_wgs84,
            m.lon_wgs84,
            m.nloc,
            m.desloc,
            m.provlet,
            m.nazione,
            m.notesito,
            m.risentimenti,
            m.maxint,
            t.ee
           FROM (( SELECT ee.desloc_cfti,
                    ee.nloc_cfti,
                    count(ee.nloc_cfti) OVER (PARTITION BY ee.nloc_cfti) AS ee
                   FROM cfti5public.ee) t
             JOIN cfti5public.ee m ON (((m.nloc_cfti)::text = (t.nloc_cfti)::text)))) p;


ALTER TABLE cfti5public.locind_ee OWNER TO utente;

--
-- Name: fpiaquo; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fpiaquo (
    id integer NOT NULL,
    intpq character varying(10),
    deslocapq character varying(40),
    intpqnum numeric,
    codtotpq character varying(18),
    cric_pq character varying(4),
    tipo_pq character varying(1),
    dist_pqep integer,
    diff_ipqep numeric,
    flag_ee character varying(2),
    azimut_pqep character varying(7),
    n_morti character varying(7),
    n_feriti character varying(7),
    flag_mor character varying(2),
    flag_fer character varying(2),
    fnperiod integer,
    fscossec integer,
    fnloc integer,
    note_pq text
);


ALTER TABLE public.fpiaquo OWNER TO utente;

--
-- Name: fscossec; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fscossec (
    id integer NOT NULL,
    nterrs character varying(9),
    nperios integer,
    tipoevs character varying(1),
    longits character varying(4),
    oraorgs character varying(8),
    latitus character varying(4),
    dataors character varying(9),
    numstas integer,
    latmacs integer,
    lonmacs integer,
    npunpqs integer,
    profsts numeric,
    ios numeric,
    imaxs numeric,
    tios numeric,
    imins numeric,
    mes numeric,
    mls numeric,
    loceps character varying(30),
    statos character varying(3),
    regamms character varying(3),
    provams character varying(3),
    livrevs character varying(3),
    codigms character varying(9),
    codopes character varying(2),
    datares character varying(25),
    reliabs character varying(1),
    delta_io character varying(2),
    esoras character varying(1),
    eslats character varying(1),
    eslons character varying(1),
    riseqpr integer,
    promacs numeric,
    gameds numeric,
    tipoeqs character varying(1),
    mks integer,
    mps integer,
    mss integer,
    mbs integer,
    corpfgs character varying(3),
    dataors2 character varying(9),
    noterrs character varying(150),
    valsism character varying(2),
    rellocepics character varying(2),
    relmms character varying(2),
    azimiepic integer,
    rellocnum integer,
    datanum integer,
    dataunis character varying(8),
    data_sep character varying(10),
    ora_sep character varying(5),
    ios_old numeric,
    cric_sc character varying(4),
    npunpq_enel integer,
    nbiblio_enel integer,
    imax_enel integer,
    sito_enel character varying(3),
    err_mes numeric,
    fnperiod integer,
    lat_centesimale numeric,
    lon_centesimale numeric,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.fscossec OWNER TO utente;

--
-- Name: vpiaquo; Type: VIEW; Schema: public; Owner: utente
--

CREATE VIEW public.vpiaquo AS
 SELECT fpiaquo.id,
    fpiaquo.deslocapq AS "località",
    fpiaquo.intpq AS "intensità_romano",
    fpiaquo.intpqnum AS "intensità_arabo",
    fpiaquo.tipo_pq AS provenienza_pq,
    fpiaquo.codtotpq AS codice_totale_pq,
    fpiaquo.note_pq AS note,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc,
    fnloc.desloc,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod,
    fnperiod.datainiznp,
    fnperiod.datafinenp,
    ''::text AS "titolo_label_vscossec_Terremoto",
    fscossec.nterrs,
    fnloc.id AS id_fnloc,
    fnperiod.id AS id_fnperiod,
    fscossec.id AS id_fscossec
   FROM (((public.fpiaquo
     LEFT JOIN public.fnloc ON ((fpiaquo.fnloc = fnloc.id)))
     LEFT JOIN public.fnperiod ON ((fpiaquo.fnperiod = fnperiod.id)))
     LEFT JOIN public.fscossec ON ((fpiaquo.fscossec = fscossec.id)));


ALTER TABLE public.vpiaquo OWNER TO utente;

--
-- Name: pq; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.pq AS
 SELECT row_number() OVER (ORDER BY fnloc_cfti_5.desloc_cfti) AS id,
    vpiaquo."intensità_romano",
    vpiaquo."intensità_arabo",
    vpiaquo.nperiod,
    vpiaquo.nterrs,
    fnloc_cfti_5.nloc_cfti,
    fnloc_cfti_5.desloc_cfti,
    fnloc_cfti_5.lat_wgs84,
    fnloc_cfti_5.lon_wgs84,
    fnloc_cfti_5.provlet,
    fnloc_cfti_5.notesito,
    fnloc_cfti_5.nloc,
    fnloc_cfti_5.desloc,
    fnloc_cfti_5.nazione
   FROM ((public.vpiaquo
     JOIN cfti5public.fnperiod_cfti ON ((((vpiaquo.nperiod)::text = (fnperiod_cfti.nperiod_cfti)::text) AND ((vpiaquo."intensità_romano")::text <> 'EE'::text))))
     LEFT JOIN cfti5public.fnloc_cfti_5 ON (((vpiaquo.nloc)::text = (fnloc_cfti_5.nloc)::text)))
  WITH NO DATA;


ALTER TABLE cfti5public.pq OWNER TO utente;

--
-- Name: locind_pq; Type: VIEW; Schema: cfti5public; Owner: utente
--

CREATE VIEW cfti5public.locind_pq AS
 SELECT row_number() OVER (ORDER BY p.desloc_cfti) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.lat_wgs84,
    p.lon_wgs84,
    p.nloc,
    p.desloc,
    p.provlet,
    p.nazione,
    p.notesito,
    p.risentimenti,
    p.maxint
   FROM ( SELECT DISTINCT ON (m.nloc_cfti) m.nloc_cfti,
            m.desloc_cfti,
            m.lat_wgs84,
            m.lon_wgs84,
            m.nloc,
            m.desloc,
            m.provlet,
            m.nazione,
            m.notesito,
            t.risentimenti,
            t.maxint
           FROM (( SELECT pq."intensità_arabo",
                    pq.desloc_cfti,
                    pq.nloc_cfti,
                    count(pq.nloc_cfti) OVER (PARTITION BY pq.nloc_cfti) AS risentimenti,
                    max(pq."intensità_arabo") OVER (PARTITION BY pq.nloc_cfti) AS maxint
                   FROM cfti5public.pq) t
             JOIN cfti5public.pq m ON (((m.nloc_cfti)::text = (t.nloc_cfti)::text)))) p;


ALTER TABLE cfti5public.locind_pq OWNER TO utente;

--
-- Name: locind_ee_senza_pq; Type: VIEW; Schema: cfti5public; Owner: utente
--

CREATE VIEW cfti5public.locind_ee_senza_pq AS
 SELECT row_number() OVER (ORDER BY l.desloc_cfti) AS id,
    l.nloc_cfti,
    l.desloc_cfti,
    l.lat_wgs84,
    l.lon_wgs84,
    l.nloc,
    l.desloc,
    l.provlet,
    l.nazione,
    l.notesito,
    l.risentimenti,
    l.maxint,
    l.ee
   FROM (cfti5public.locind_ee l
     LEFT JOIN cfti5public.locind_pq r ON (((r.nloc_cfti)::text = (l.nloc_cfti)::text)))
  WHERE (r.nloc_cfti IS NULL);


ALTER TABLE cfti5public.locind_ee_senza_pq OWNER TO utente;

--
-- Name: locind_pq_con_ee; Type: VIEW; Schema: cfti5public; Owner: utente
--

CREATE VIEW cfti5public.locind_pq_con_ee AS
 SELECT row_number() OVER (ORDER BY p.desloc_cfti) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.lat_wgs84,
    p.lon_wgs84,
    p.nloc,
    p.desloc,
    p.provlet,
    p.nazione,
    p.notesito,
    p.risentimenti,
    p.maxint,
    p.ee
   FROM ( SELECT locind_pq.nloc_cfti,
            locind_pq.desloc_cfti,
            locind_pq.lat_wgs84,
            locind_pq.lon_wgs84,
            locind_pq.nloc,
            locind_pq.desloc,
            locind_pq.provlet,
            locind_pq.nazione,
            locind_pq.notesito,
            locind_pq.risentimenti,
            locind_pq.maxint,
            locind_ee.ee
           FROM (cfti5public.locind_pq
             LEFT JOIN cfti5public.locind_ee ON (((locind_pq.nloc_cfti)::text = (locind_ee.nloc_cfti)::text)))) p;


ALTER TABLE cfti5public.locind_pq_con_ee OWNER TO utente;

--
-- Name: notesito; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.notesito AS
 SELECT row_number() OVER (ORDER BY p.nloc) AS id,
    p.nloc_cfti,
    p.desloc_cfti,
    p.nloc,
    p.notesito
   FROM ( SELECT DISTINCT ON (m.nloc_cfti) m.nloc_cfti,
            m.desloc_cfti,
            m.nloc,
            m.notesito
           FROM cfti5public.fnloc_cfti_5 m
          WHERE (m.notesito <> ''::text)
          ORDER BY m.nloc_cfti, m.nloc) p
  WITH NO DATA;


ALTER TABLE cfti5public.notesito OWNER TO utente;

--
-- Name: locind; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.locind AS
 SELECT row_number() OVER (ORDER BY t.desloc_cfti) AS id,
    t.nloc_cfti,
    t.desloc_cfti,
    t.lat_wgs84,
    t.lon_wgs84,
    t.provlet,
    t.nazione,
    COALESCE(notesito.notesito, t.notesito) AS notesito,
    COALESCE(t.risentimenti, (0)::bigint) AS risentimenti,
    COALESCE(t.maxint, ('-2'::integer)::numeric) AS maxint,
    COALESCE(t.ee, (0)::bigint) AS ee
   FROM (( SELECT locind_pq_con_ee.nloc_cfti,
            locind_pq_con_ee.desloc_cfti,
            locind_pq_con_ee.lat_wgs84,
            locind_pq_con_ee.lon_wgs84,
            locind_pq_con_ee.nloc,
            locind_pq_con_ee.desloc,
            locind_pq_con_ee.provlet,
            locind_pq_con_ee.nazione,
            locind_pq_con_ee.notesito,
            locind_pq_con_ee.risentimenti,
            locind_pq_con_ee.maxint,
            locind_pq_con_ee.ee
           FROM cfti5public.locind_pq_con_ee
        UNION
         SELECT locind_ee_senza_pq.nloc_cfti,
            locind_ee_senza_pq.desloc_cfti,
            locind_ee_senza_pq.lat_wgs84,
            locind_ee_senza_pq.lon_wgs84,
            locind_ee_senza_pq.nloc,
            locind_ee_senza_pq.desloc,
            locind_ee_senza_pq.provlet,
            locind_ee_senza_pq.nazione,
            locind_ee_senza_pq.notesito,
            locind_ee_senza_pq.risentimenti,
            locind_ee_senza_pq.maxint,
            locind_ee_senza_pq.ee
           FROM cfti5public.locind_ee_senza_pq) t
     LEFT JOIN cfti5public.notesito ON (((t.nloc_cfti)::text = (notesito.nloc_cfti)::text)))
  WITH NO DATA;


ALTER TABLE cfti5public.locind OWNER TO utente;

--
-- Name: locind_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.locind_med (
    id integer NOT NULL,
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    nloc character varying(25),
    provlet character varying(25),
    notesito text,
    nazione character varying(25),
    risentimenti bigint,
    maxint text
);


ALTER TABLE cfti5public.locind_med OWNER TO utente;

--
-- Name: nloc_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.nloc_med (
    id integer NOT NULL,
    nloc6 character varying(25),
    seqnloc integer,
    nloc character varying(25),
    desloc character varying(250),
    provlet character varying(25),
    nazione character varying(25),
    postal_code character varying(25),
    quotloc character varying(25),
    numabit character varying(25),
    prov character varying(5),
    regione character varying(5),
    comune character varying(5),
    frazion character varying(25),
    notesito text,
    lat_roma40 character varying(25),
    lon_roma40 character varying(25),
    istat character varying(25),
    istat_103 character varying(25),
    comune_nome character varying(25),
    capoluogo character varying(25),
    cap character varying(25),
    problemi character varying(25),
    istat_2011 character varying(25),
    comune_nome_2011 character varying(25),
    capoluogo_2011 character varying(25),
    in_cfti character varying(2),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    provlet_old character varying(25)
);


ALTER TABLE cfti5public.nloc_med OWNER TO utente;

--
-- Name: nterrs; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.nterrs (
    id integer NOT NULL,
    cat character varying(25),
    nterr character varying(25),
    nperiod character varying(25),
    datanum character varying(25),
    data_label character varying(25),
    anno character varying(25),
    mese character varying(25),
    giorno character varying(25),
    data_incert character varying(25),
    anno2 character varying(25),
    mese2 character varying(25),
    giorno2 character varying(25),
    data_incert2 character varying(25),
    time_label character varying(25),
    ora character varying(25),
    minu character varying(25),
    sec character varying(25),
    time_incert character varying(25),
    lat character varying(25),
    lon character varying(25),
    rel character varying(25),
    io character varying(25),
    imax character varying(25),
    npun character varying(25),
    mm character varying(25),
    earthquakelocation text,
    country character varying(25),
    sigla character varying(25),
    ee_nt character varying(25),
    ee_np character varying(25),
    epicenter_type character varying(25),
    flagcomments character varying(25),
    flagfalseeq character varying(25),
    level character varying(25),
    new2018 text,
    comm2018 text,
    nota_novita text,
    nota_mm text,
    "time" character varying(25),
    npun_all character varying(25),
    npun_boxer character varying(25),
    ndata character varying(25)
);


ALTER TABLE cfti5public.nterrs OWNER TO utente;

--
-- Name: nterrs2; Type: VIEW; Schema: cfti5public; Owner: utente
--

CREATE VIEW cfti5public.nterrs2 AS
 SELECT fscossec.id,
    fscossec.nterrs,
    fnperiod_cfti_view.nperiod,
    fscossec.dataors,
    fscossec.oraorgs,
    fscossec.lat_centesimale,
    fscossec.lon_centesimale,
    fscossec.loceps,
    fscossec.npunpqs,
    fscossec.ios,
    fscossec.imaxs,
    fscossec.mks,
    fscossec.livrevs,
    fscossec.rellocepics,
    fscossec.statos
   FROM public.fscossec,
    cfti5public.fnperiod_cfti_view
  WHERE (fscossec.fnperiod = fnperiod_cfti_view.id);


ALTER TABLE cfti5public.nterrs2 OWNER TO utente;

--
-- Name: nterrs_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.nterrs_med (
    id integer NOT NULL,
    cat character varying(25),
    nterr character varying(25),
    nperiod character varying(25),
    datanum character varying(25),
    data_label character varying(250),
    anno character varying(25),
    mese character varying(25),
    giorno character varying(25),
    data_incert character varying(25),
    anno2 character varying(25),
    mese2 character varying(25),
    giorno2 character varying(25),
    data_incert2 character varying(25),
    time_label character varying(25),
    ora character varying(25),
    minu character varying(25),
    sec character varying(25),
    time_incert character varying(25),
    lat character varying(25),
    lon character varying(25),
    rel character varying(25),
    io character varying(25),
    imax character varying(25),
    npun character varying(25),
    mm character varying(25),
    earthquakelocation character varying(250),
    country character varying(25),
    sigla character varying(25),
    ee_nt character varying(2),
    ee_np character varying(2),
    epicenter_type character varying(250),
    flagcomments character varying(2),
    flagfalseeq character varying(2),
    level character varying(25)
);


ALTER TABLE cfti5public.nterrs_med OWNER TO utente;

--
-- Name: pq_med; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.pq_med (
    id integer NOT NULL,
    intpqnum character varying(25),
    intpq character varying(25),
    nterr character varying(25),
    nperiod character varying(25),
    nloc_cfti character varying(25),
    desloc_cfti character varying(250),
    lat_wgs84 character varying(25),
    lon_wgs84 character varying(25),
    nloc character varying(25),
    provlet character varying(25),
    notesito text,
    nazione character varying(25)
);


ALTER TABLE cfti5public.pq_med OWNER TO utente;

--
-- Name: quake_details; Type: MATERIALIZED VIEW; Schema: cfti5public; Owner: utente
--

CREATE MATERIALIZED VIEW cfti5public.quake_details AS
 SELECT nterrs.cat,
    nterrs.nterr,
    nterrs.nperiod,
    nterrs.datanum,
    nterrs.data_label,
    nterrs.anno,
    nterrs.mese,
    nterrs.giorno,
    nterrs.time_label,
    nterrs.ora,
    nterrs.minu,
    nterrs.sec,
    nterrs.lat,
    nterrs.lon,
    nterrs.rel,
    nterrs.level,
    nterrs.io,
    nterrs.imax,
    nterrs.npun,
    nterrs.mm,
    nterrs.earthquakelocation,
    nterrs.epicenter_type,
    nterrs.country,
    nterrs.ee_nt,
    nterrs.ee_np,
    nterrs.flagcomments,
    nterrs.flagfalseeq,
    pq."intensità_arabo",
    pq."intensità_romano",
    pq.nloc_cfti
   FROM (cfti5public.nterrs
     JOIN cfti5public.pq ON (((nterrs.nterr)::text = (pq.nterrs)::text)))
  WITH NO DATA;


ALTER TABLE cfti5public.quake_details OWNER TO utente;

--
-- Name: schedec; Type: TABLE; Schema: cfti5public; Owner: utente
--

CREATE TABLE cfti5public.schedec (
    id integer NOT NULL,
    codbib character varying(25),
    nloc character varying(25)
);


ALTER TABLE cfti5public.schedec OWNER TO utente;

--
-- Name: seq_loc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seq_loc (
    id integer NOT NULL,
    nperiod character varying(255),
    codicescossa character varying(255),
    nscosse character varying(255),
    durcalc character varying(255),
    durfonti character varying(255),
    intfonte character varying(255),
    imcs character varying(255),
    imcs_nuovo character varying(255),
    id_ character varying(255),
    nomeloc character varying(255),
    nome_trovato_utilizzato character varying(255),
    note text,
    nloc_cfti character varying(255),
    commentoloc text,
    commentoe1 text,
    codice_effetto_nuovo character varying(255),
    latloc double precision,
    lonloc double precision
);


ALTER TABLE public.seq_loc OWNER TO postgres;

--
-- Name: seq_loc; Type: VIEW; Schema: cfti5public; Owner: postgres
--

CREATE VIEW cfti5public.seq_loc AS
 SELECT seq_loc.id,
    seq_loc.nperiod,
    seq_loc.codicescossa,
    seq_loc.nscosse,
    seq_loc.durcalc,
    seq_loc.durfonti,
    seq_loc.intfonte,
    seq_loc.imcs,
    seq_loc.imcs_nuovo,
    seq_loc.id_,
    seq_loc.nomeloc,
    seq_loc.nome_trovato_utilizzato,
    seq_loc.note,
    seq_loc.nloc_cfti,
    seq_loc.commentoloc,
    seq_loc.commentoe1,
    seq_loc.codice_effetto_nuovo,
    seq_loc.latloc,
    seq_loc.lonloc
   FROM public.seq_loc;


ALTER TABLE cfti5public.seq_loc OWNER TO postgres;

--
-- Name: seq_scosse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seq_scosse (
    id integer NOT NULL,
    nperiod character varying(255),
    codicescossa character varying(255),
    nterr character varying(255),
    anno1 character varying(255),
    mese1 character varying(255),
    giorno1 character varying(255),
    anno2 character varying(255),
    mese2 character varying(255),
    giorno2 character varying(255),
    oraf character varying(255),
    oragmt1 character varying(255),
    oragmt2 character varying(255),
    timelabelgmt character varying(255),
    timelabelfonte character varying(255),
    io character varying(255),
    commentoscossa text
);


ALTER TABLE public.seq_scosse OWNER TO postgres;

--
-- Name: seq_scosse; Type: VIEW; Schema: cfti5public; Owner: postgres
--

CREATE VIEW cfti5public.seq_scosse AS
 SELECT seq_scosse.id,
    seq_scosse.nperiod,
    seq_scosse.codicescossa,
    seq_scosse.nterr,
    seq_scosse.anno1,
    seq_scosse.mese1,
    seq_scosse.giorno1,
    seq_scosse.anno2,
    seq_scosse.mese2,
    seq_scosse.giorno2,
    seq_scosse.oraf,
    seq_scosse.oragmt1,
    seq_scosse.oragmt2,
    seq_scosse.timelabelgmt,
    seq_scosse.timelabelfonte,
    seq_scosse.io,
    seq_scosse.commentoscossa
   FROM public.seq_scosse;


ALTER TABLE cfti5public.seq_scosse OWNER TO postgres;

--
-- Name: feffetti; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.feffetti (
    id integer NOT NULL,
    codeff character varying(4),
    deseff text,
    cric_ef character varying(4),
    noteeff text,
    codeff_new character varying(2),
    deseff_new text,
    desing_new text
);


ALTER TABLE public.feffetti OWNER TO utente;

--
-- Name: ftipfen; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.ftipfen (
    id integer NOT NULL,
    codtf character varying(4),
    destf text,
    notetf text
);


ALTER TABLE public.ftipfen OWNER TO utente;

--
-- Name: vnperiod; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vnperiod AS
 SELECT fnperiod.id,
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    fnperiod.livnp AS livello_approfondimento,
    fnperiod.numcomm AS numero_commenti,
    fnperiod.numbiblio AS numero_referenze,
    fnperiod.numtb AS numero_testi,
    fnperiod.areaepicentrale AS area_epicentrale,
    fnperiod.in_cfti,
        CASE
            WHEN (length((fnperiod.datainiznp)::text) < 4) THEN '0'::text
            WHEN (length((fnperiod.datafinenp)::text) < 4) THEN '0'::text
            WHEN (substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4)) = substr((fnperiod.datafinenp)::text, 1, (length((fnperiod.datafinenp)::text) - 4))) THEN substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4))
            WHEN (substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4)) <> substr((fnperiod.datafinenp)::text, 1, (length((fnperiod.datafinenp)::text) - 4))) THEN ((substr((fnperiod.datainiznp)::text, 1, (length((fnperiod.datainiznp)::text) - 4)) || '-'::text) || substr((fnperiod.datafinenp)::text, 1, (length((fnperiod.datafinenp)::text) - 4)))
            ELSE NULL::text
        END AS intervallo_anni,
    fnperiod.commentonperiod AS note,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    feffetti.id AS id_feffetti,
    ftipfen.id AS id_ftipfen
   FROM ((public.fnperiod
     LEFT JOIN public.ftipfen ON ((fnperiod.ftipfen = ftipfen.id)))
     LEFT JOIN public.feffetti ON ((fnperiod.feffetti = feffetti.id)));


ALTER TABLE public.vnperiod OWNER TO postgres;

--
-- Name: vnperiod_cfti; Type: VIEW; Schema: cfti5public; Owner: postgres
--

CREATE VIEW cfti5public.vnperiod_cfti AS
 SELECT vnperiod.id,
    vnperiod.codice_sequenza,
    vnperiod.inizio_sequenza,
    vnperiod.fine_sequenza,
    vnperiod.area_epicentrale,
    vnperiod.in_cfti,
    vnperiod.intervallo_anni
   FROM public.vnperiod
  WHERE (vnperiod.in_cfti = true);


ALTER TABLE cfti5public.vnperiod_cfti OWNER TO postgres;

--
-- Name: allegato; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.allegato (
    codiceallegato integer NOT NULL,
    tipoentita character varying(100) DEFAULT NULL::character varying,
    codiceentita character varying(255) DEFAULT NULL::character varying,
    descroggall character varying(250) DEFAULT NULL::character varying,
    autoreall character varying(250) DEFAULT NULL::character varying,
    versioneall character varying(250) DEFAULT NULL::character varying,
    lastdata timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    nomefileall character varying(250) NOT NULL
);


ALTER TABLE frontend.allegato OWNER TO postgres;

--
-- Name: allegato_codiceallegato_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.allegato_codiceallegato_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.allegato_codiceallegato_seq OWNER TO postgres;

--
-- Name: allegato_codiceallegato_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.allegato_codiceallegato_seq OWNED BY frontend.allegato.codiceallegato;


--
-- Name: api_console; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.api_console (
    id integer NOT NULL,
    ip_address character varying(20) DEFAULT ''::character varying NOT NULL,
    rw integer DEFAULT 0 NOT NULL,
    api_key character varying(100) DEFAULT ''::character varying NOT NULL,
    last_update timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE frontend.api_console OWNER TO postgres;

--
-- Name: api_console_id_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.api_console_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.api_console_id_seq OWNER TO postgres;

--
-- Name: api_console_id_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.api_console_id_seq OWNED BY frontend.api_console.id;


--
-- Name: button; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.button (
    id_button integer NOT NULL,
    id_table integer NOT NULL,
    definition text NOT NULL,
    button_type character varying(25) NOT NULL,
    background character varying(7),
    color character varying(7),
    button_name character varying(50) NOT NULL,
    last_data timestamp without time zone DEFAULT now() NOT NULL,
    id_utente integer NOT NULL,
    settings text
);


ALTER TABLE frontend.button OWNER TO postgres;

--
-- Name: button_id_button_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.button_id_button_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.button_id_button_seq OWNER TO postgres;

--
-- Name: button_id_button_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.button_id_button_seq OWNED BY frontend.button.id_button;


--
-- Name: cache_reg; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.cache_reg (
    id integer NOT NULL,
    obj bytea,
    last_update timestamp without time zone
);


ALTER TABLE frontend.cache_reg OWNER TO postgres;

--
-- Name: cache_reg_id_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.cache_reg_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.cache_reg_id_seq OWNER TO postgres;

--
-- Name: cache_reg_id_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.cache_reg_id_seq OWNED BY frontend.cache_reg.id;


--
-- Name: gruppo; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.gruppo (
    gid bigint NOT NULL,
    nome_gruppo character varying(50) NOT NULL,
    descrizione_gruppo text,
    data_gruppo timestamp without time zone DEFAULT now()
);


ALTER TABLE frontend.gruppo OWNER TO postgres;

--
-- Name: COLUMN gruppo.gid; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.gruppo.gid IS 'ID del gruppo';


--
-- Name: COLUMN gruppo.nome_gruppo; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.gruppo.nome_gruppo IS 'Nome del gruppo';


--
-- Name: link; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.link (
    codicelink integer NOT NULL,
    tipoentita character varying(100) DEFAULT NULL::character varying,
    codiceentita character varying(255) DEFAULT NULL::character varying,
    link character varying(250) DEFAULT NULL::character varying,
    descrizione character varying(250) DEFAULT NULL::character varying,
    lastdata timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE frontend.link OWNER TO postgres;

--
-- Name: link_codicelink_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.link_codicelink_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.link_codicelink_seq OWNER TO postgres;

--
-- Name: link_codicelink_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.link_codicelink_seq OWNED BY frontend.link.codicelink;


--
-- Name: log; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.log (
    id_log integer NOT NULL,
    op character varying(20) NOT NULL,
    uid bigint NOT NULL,
    gid bigint NOT NULL,
    data timestamp without time zone DEFAULT now(),
    tabella character varying(100) NOT NULL,
    id_record character varying(100) DEFAULT NULL::character varying,
    storico_pre text,
    storico_post text,
    fonte character(1) DEFAULT 'm'::bpchar NOT NULL,
    info_browser character varying(20) DEFAULT NULL::character varying
);


ALTER TABLE frontend.log OWNER TO postgres;

--
-- Name: log_id_log_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.log_id_log_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.log_id_log_seq OWNER TO postgres;

--
-- Name: log_id_log_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.log_id_log_seq OWNED BY frontend.log.id_log;


--
-- Name: recordlock; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.recordlock (
    tabella character varying(50) NOT NULL,
    colonna character varying(50) NOT NULL,
    id character varying(50) NOT NULL,
    tempo integer NOT NULL
);


ALTER TABLE frontend.recordlock OWNER TO postgres;

--
-- Name: registro_col; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_col (
    id_reg integer NOT NULL,
    id_table bigint,
    gid bigint,
    column_name character varying(255) DEFAULT NULL::character varying,
    ordinal_position smallint,
    column_default character varying(255) DEFAULT NULL::character varying,
    is_nullable character varying(3) DEFAULT NULL::character varying,
    column_type character varying(255) DEFAULT NULL::character varying,
    character_maximum_length integer,
    data_type character varying(255) DEFAULT NULL::character varying,
    extra character varying(200) DEFAULT NULL::character varying,
    in_tipo text,
    in_default text,
    in_visibile smallint DEFAULT 1,
    in_richiesto smallint DEFAULT 0,
    in_suggest smallint DEFAULT 0,
    in_table smallint DEFAULT 1,
    in_line smallint,
    in_ordine smallint DEFAULT 0,
    jstest text,
    commento character varying(255) DEFAULT NULL::character varying,
    alias_frontend character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE frontend.registro_col OWNER TO postgres;

--
-- Name: registro_col_id_reg_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_col_id_reg_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_col_id_reg_seq OWNER TO postgres;

--
-- Name: registro_col_id_reg_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_col_id_reg_seq OWNED BY frontend.registro_col.id_reg;


--
-- Name: registro_submask; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_submask (
    id_submask integer NOT NULL,
    id_table bigint NOT NULL,
    sub_select smallint DEFAULT 0,
    sub_insert smallint DEFAULT 0,
    sub_update smallint DEFAULT 0,
    sub_delete smallint DEFAULT 0,
    nome_tabella character varying(255) DEFAULT NULL::character varying,
    nome_frontend character varying(250) DEFAULT NULL::character varying,
    campo_pk_parent character varying(80) DEFAULT NULL::character varying,
    campo_fk_sub character varying(80) DEFAULT NULL::character varying,
    orderby_sub character varying(80) DEFAULT NULL::character varying,
    orderby_sub_sort character(4) DEFAULT 'ASC'::bpchar,
    data_modifica bigint,
    max_records smallint DEFAULT '10'::smallint,
    tipo_vista character varying(8) DEFAULT 'scheda'::character varying NOT NULL
);


ALTER TABLE frontend.registro_submask OWNER TO postgres;

--
-- Name: registro_submask_col; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_submask_col (
    id_reg_sub integer NOT NULL,
    id_submask bigint NOT NULL,
    column_name character varying(255) DEFAULT NULL::character varying,
    ordinal_position smallint,
    column_default character varying(255) DEFAULT NULL::character varying,
    is_nullable character varying(3) DEFAULT NULL::character varying,
    column_type character varying(255) DEFAULT NULL::character varying,
    character_maximum_length integer,
    data_type character varying(255) DEFAULT NULL::character varying,
    extra character varying(200) DEFAULT NULL::character varying,
    in_tipo text,
    in_default text,
    in_visibile smallint DEFAULT 1,
    in_richiesto smallint DEFAULT 0,
    commento character varying(255) DEFAULT NULL::character varying,
    alias_frontend character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE frontend.registro_submask_col OWNER TO postgres;

--
-- Name: registro_submask_col_id_reg_sub_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_submask_col_id_reg_sub_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_submask_col_id_reg_sub_seq OWNER TO postgres;

--
-- Name: registro_submask_col_id_reg_sub_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_submask_col_id_reg_sub_seq OWNED BY frontend.registro_submask_col.id_reg_sub;


--
-- Name: registro_submask_id_submask_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_submask_id_submask_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_submask_id_submask_seq OWNER TO postgres;

--
-- Name: registro_submask_id_submask_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_submask_id_submask_seq OWNED BY frontend.registro_submask.id_submask;


--
-- Name: registro_tab; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.registro_tab (
    id_table integer NOT NULL,
    gid bigint,
    visibile smallint DEFAULT 0,
    in_insert smallint DEFAULT 0,
    in_duplica smallint DEFAULT 0,
    in_update smallint DEFAULT 0,
    in_delete smallint DEFAULT 0,
    in_export smallint DEFAULT 0,
    in_import smallint DEFAULT 0,
    data_modifica integer DEFAULT 0,
    orderby character varying(255) DEFAULT NULL::character varying,
    table_name character varying(100) DEFAULT NULL::character varying,
    table_type character varying(20) DEFAULT 'BASE TABLE'::character varying,
    commento character varying(255) DEFAULT NULL::character varying,
    orderby_sort character varying(255) DEFAULT 'ASC'::character varying,
    permetti_allegati smallint DEFAULT 0,
    permetti_allegati_ins smallint DEFAULT 0,
    permetti_allegati_del smallint DEFAULT 0,
    permetti_link smallint DEFAULT 0,
    permetti_link_ins smallint DEFAULT 0,
    permetti_link_del smallint DEFAULT 0,
    view_pk character varying(60) DEFAULT NULL::character varying,
    fonte_al character varying(100) DEFAULT NULL::character varying,
    table_alias character varying(100) DEFAULT NULL::character varying,
    allow_filters smallint DEFAULT 0,
    default_view character varying(5) DEFAULT 'form'::character varying,
    default_filters text
);


ALTER TABLE frontend.registro_tab OWNER TO postgres;

--
-- Name: registro_tab_id_table_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.registro_tab_id_table_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.registro_tab_id_table_seq OWNER TO postgres;

--
-- Name: registro_tab_id_table_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.registro_tab_id_table_seq OWNED BY frontend.registro_tab.id_table;


--
-- Name: stat; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.stat (
    id_stat integer NOT NULL,
    nome_stat character varying(250) NOT NULL,
    desc_stat text,
    def_stat text,
    auth_stat smallint DEFAULT 3,
    tipo_graph character varying(8) DEFAULT 'barre'::character varying,
    data_stat timestamp without time zone DEFAULT now() NOT NULL,
    autore bigint NOT NULL,
    settings text,
    published smallint DEFAULT 0 NOT NULL
);


ALTER TABLE frontend.stat OWNER TO postgres;

--
-- Name: stat_id_stat_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.stat_id_stat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.stat_id_stat_seq OWNER TO postgres;

--
-- Name: stat_id_stat_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.stat_id_stat_seq OWNED BY frontend.stat.id_stat;


--
-- Name: utente; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.utente (
    id_utente integer NOT NULL,
    nick character varying(80) DEFAULT NULL::character varying,
    passwd character(32) DEFAULT NULL::bpchar,
    nome character varying(50) DEFAULT NULL::character varying,
    cognome character varying(50) DEFAULT NULL::character varying,
    email character varying(80) DEFAULT NULL::character varying,
    info text,
    data_ins date DEFAULT now(),
    gid bigint DEFAULT '0'::bigint NOT NULL,
    livello smallint DEFAULT '1'::smallint NOT NULL,
    recover_passwd character varying(32) DEFAULT NULL::character varying
);


ALTER TABLE frontend.utente OWNER TO postgres;

--
-- Name: utente_id_utente_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.utente_id_utente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.utente_id_utente_seq OWNER TO postgres;

--
-- Name: utente_id_utente_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.utente_id_utente_seq OWNED BY frontend.utente.id_utente;


--
-- Name: variabili; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.variabili (
    variabile character varying(32) NOT NULL,
    gid bigint DEFAULT 0 NOT NULL,
    valore character varying(255) DEFAULT NULL::character varying,
    descrizione character varying(255) DEFAULT NULL::character varying,
    tipo_var character varying(20) DEFAULT NULL::character varying,
    pubvar smallint DEFAULT 1 NOT NULL
);


ALTER TABLE frontend.variabili OWNER TO postgres;

--
-- Name: widget; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.widget (
    id_widget integer NOT NULL,
    id_table integer NOT NULL,
    widget_name character varying(255) DEFAULT ''::character varying NOT NULL,
    form_position character varying(11) DEFAULT '0'::character varying NOT NULL,
    widget_type character varying(100) DEFAULT ''::character varying NOT NULL,
    settings text NOT NULL
);


ALTER TABLE frontend.widget OWNER TO postgres;

--
-- Name: widget_id_widget_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.widget_id_widget_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.widget_id_widget_seq OWNER TO postgres;

--
-- Name: widget_id_widget_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.widget_id_widget_seq OWNED BY frontend.widget.id_widget;


--
-- Name: xml_rules; Type: TABLE; Schema: frontend; Owner: postgres
--

CREATE TABLE frontend.xml_rules (
    id_xml_rules integer NOT NULL,
    tabella character varying(50) NOT NULL,
    accesso character varying(20) DEFAULT 'RESTRICT'::character varying,
    accesso_gruppo character varying(100),
    autore integer,
    lastdata timestamp without time zone DEFAULT now(),
    xsl character varying(80),
    xslfo character varying(80),
    tipo_report character(1) DEFAULT 't'::bpchar,
    def_query text,
    nome_report character varying(255)
);


ALTER TABLE frontend.xml_rules OWNER TO postgres;

--
-- Name: COLUMN xml_rules.accesso; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.xml_rules.accesso IS 'RESTRICT,PUBLIC,FRONTEND,GROUP';


--
-- Name: COLUMN xml_rules.tipo_report; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.xml_rules.tipo_report IS 'Report basato su tabella (t) o su query (q)';


--
-- Name: COLUMN xml_rules.def_query; Type: COMMENT; Schema: frontend; Owner: postgres
--

COMMENT ON COLUMN frontend.xml_rules.def_query IS 'Query di definizione per i report di tipo "query"';


--
-- Name: xml_rules_id_xml_rules_seq; Type: SEQUENCE; Schema: frontend; Owner: postgres
--

CREATE SEQUENCE frontend.xml_rules_id_xml_rules_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend.xml_rules_id_xml_rules_seq OWNER TO postgres;

--
-- Name: xml_rules_id_xml_rules_seq; Type: SEQUENCE OWNED BY; Schema: frontend; Owner: postgres
--

ALTER SEQUENCE frontend.xml_rules_id_xml_rules_seq OWNED BY frontend.xml_rules.id_xml_rules;


--
-- Name: allegato; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.allegato (
    codiceallegato integer NOT NULL,
    tipoentita character varying(100) DEFAULT NULL::character varying,
    codiceentita character varying(255) DEFAULT NULL::character varying,
    descroggall character varying(250) DEFAULT NULL::character varying,
    autoreall character varying(250) DEFAULT NULL::character varying,
    versioneall character varying(250) DEFAULT NULL::character varying,
    lastdata timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    nomefileall character varying(250) NOT NULL
);


ALTER TABLE frontend2.allegato OWNER TO postgres;

--
-- Name: allegato_codiceallegato_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.allegato_codiceallegato_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.allegato_codiceallegato_seq OWNER TO postgres;

--
-- Name: allegato_codiceallegato_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.allegato_codiceallegato_seq OWNED BY frontend2.allegato.codiceallegato;


--
-- Name: button; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.button (
    id_button integer NOT NULL,
    id_table integer NOT NULL,
    definition text NOT NULL,
    button_type character varying(25) NOT NULL,
    background character varying(7),
    color character varying(7),
    button_name character varying(50) NOT NULL,
    last_data timestamp without time zone DEFAULT now() NOT NULL,
    id_utente integer NOT NULL,
    settings text
);


ALTER TABLE frontend2.button OWNER TO postgres;

--
-- Name: button_id_button_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.button_id_button_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.button_id_button_seq OWNER TO postgres;

--
-- Name: button_id_button_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.button_id_button_seq OWNED BY frontend2.button.id_button;


--
-- Name: cache_reg; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.cache_reg (
    id integer NOT NULL,
    obj bytea,
    last_update timestamp without time zone
);


ALTER TABLE frontend2.cache_reg OWNER TO postgres;

--
-- Name: cache_reg_id_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.cache_reg_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.cache_reg_id_seq OWNER TO postgres;

--
-- Name: cache_reg_id_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.cache_reg_id_seq OWNED BY frontend2.cache_reg.id;


--
-- Name: gruppo; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.gruppo (
    gid bigint NOT NULL,
    nome_gruppo character varying(50) NOT NULL,
    descrizione_gruppo text,
    data_gruppo timestamp without time zone DEFAULT now()
);


ALTER TABLE frontend2.gruppo OWNER TO postgres;

--
-- Name: COLUMN gruppo.gid; Type: COMMENT; Schema: frontend2; Owner: postgres
--

COMMENT ON COLUMN frontend2.gruppo.gid IS 'ID del gruppo';


--
-- Name: COLUMN gruppo.nome_gruppo; Type: COMMENT; Schema: frontend2; Owner: postgres
--

COMMENT ON COLUMN frontend2.gruppo.nome_gruppo IS 'Nome del gruppo';


--
-- Name: link; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.link (
    codicelink integer NOT NULL,
    tipoentita character varying(100) DEFAULT NULL::character varying,
    codiceentita character varying(255) DEFAULT NULL::character varying,
    link character varying(250) DEFAULT NULL::character varying,
    descrizione character varying(250) DEFAULT NULL::character varying,
    lastdata timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE frontend2.link OWNER TO postgres;

--
-- Name: link_codicelink_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.link_codicelink_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.link_codicelink_seq OWNER TO postgres;

--
-- Name: link_codicelink_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.link_codicelink_seq OWNED BY frontend2.link.codicelink;


--
-- Name: log; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.log (
    id_log integer NOT NULL,
    op character varying(20) NOT NULL,
    uid bigint NOT NULL,
    gid bigint NOT NULL,
    data timestamp without time zone DEFAULT now(),
    tabella character varying(100) NOT NULL,
    id_record character varying(100) DEFAULT NULL::character varying,
    storico_pre text,
    storico_post text,
    fonte character(1) DEFAULT 'm'::bpchar NOT NULL,
    info_browser character varying(20) DEFAULT NULL::character varying
);


ALTER TABLE frontend2.log OWNER TO postgres;

--
-- Name: log_id_log_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.log_id_log_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.log_id_log_seq OWNER TO postgres;

--
-- Name: log_id_log_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.log_id_log_seq OWNED BY frontend2.log.id_log;


--
-- Name: recordlock; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.recordlock (
    tabella character varying(50) NOT NULL,
    colonna character varying(50) NOT NULL,
    id character varying(50) NOT NULL,
    tempo integer NOT NULL
);


ALTER TABLE frontend2.recordlock OWNER TO postgres;

--
-- Name: registro_col; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.registro_col (
    id_reg integer NOT NULL,
    id_table bigint,
    gid bigint,
    column_name character varying(255) DEFAULT NULL::character varying,
    ordinal_position smallint,
    column_default character varying(255) DEFAULT NULL::character varying,
    is_nullable character varying(3) DEFAULT NULL::character varying,
    column_type character varying(255) DEFAULT NULL::character varying,
    character_maximum_length integer,
    data_type character varying(255) DEFAULT NULL::character varying,
    extra character varying(200) DEFAULT NULL::character varying,
    in_tipo text,
    in_default text,
    in_visibile smallint DEFAULT 1,
    in_richiesto smallint DEFAULT 0,
    in_suggest smallint DEFAULT 0,
    in_table smallint DEFAULT 1,
    in_line smallint,
    in_ordine smallint DEFAULT 0,
    jstest text,
    commento character varying(255) DEFAULT NULL::character varying,
    alias_frontend character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE frontend2.registro_col OWNER TO postgres;

--
-- Name: registro_col_id_reg_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.registro_col_id_reg_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.registro_col_id_reg_seq OWNER TO postgres;

--
-- Name: registro_col_id_reg_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.registro_col_id_reg_seq OWNED BY frontend2.registro_col.id_reg;


--
-- Name: registro_submask; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.registro_submask (
    id_submask integer NOT NULL,
    id_table bigint NOT NULL,
    sub_select smallint DEFAULT 0,
    sub_insert smallint DEFAULT 0,
    sub_update smallint DEFAULT 0,
    sub_delete smallint DEFAULT 0,
    nome_tabella character varying(255) DEFAULT NULL::character varying,
    nome_frontend character varying(250) DEFAULT NULL::character varying,
    campo_pk_parent character varying(80) DEFAULT NULL::character varying,
    campo_fk_sub character varying(80) DEFAULT NULL::character varying,
    orderby_sub character varying(80) DEFAULT NULL::character varying,
    orderby_sub_sort character(4) DEFAULT 'ASC'::bpchar,
    data_modifica bigint,
    max_records smallint DEFAULT '10'::smallint,
    tipo_vista character varying(8) DEFAULT 'scheda'::character varying NOT NULL
);


ALTER TABLE frontend2.registro_submask OWNER TO postgres;

--
-- Name: registro_submask_col; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.registro_submask_col (
    id_reg_sub integer NOT NULL,
    id_submask bigint NOT NULL,
    column_name character varying(255) DEFAULT NULL::character varying,
    ordinal_position smallint,
    column_default character varying(255) DEFAULT NULL::character varying,
    is_nullable character varying(3) DEFAULT NULL::character varying,
    column_type character varying(255) DEFAULT NULL::character varying,
    character_maximum_length integer,
    data_type character varying(255) DEFAULT NULL::character varying,
    extra character varying(200) DEFAULT NULL::character varying,
    in_tipo text,
    in_default text,
    in_visibile smallint DEFAULT 1,
    in_richiesto smallint DEFAULT 0,
    commento character varying(255) DEFAULT NULL::character varying,
    alias_frontend character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE frontend2.registro_submask_col OWNER TO postgres;

--
-- Name: registro_submask_col_id_reg_sub_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.registro_submask_col_id_reg_sub_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.registro_submask_col_id_reg_sub_seq OWNER TO postgres;

--
-- Name: registro_submask_col_id_reg_sub_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.registro_submask_col_id_reg_sub_seq OWNED BY frontend2.registro_submask_col.id_reg_sub;


--
-- Name: registro_submask_id_submask_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.registro_submask_id_submask_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.registro_submask_id_submask_seq OWNER TO postgres;

--
-- Name: registro_submask_id_submask_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.registro_submask_id_submask_seq OWNED BY frontend2.registro_submask.id_submask;


--
-- Name: registro_tab; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.registro_tab (
    id_table integer NOT NULL,
    gid bigint,
    visibile smallint DEFAULT 0,
    in_insert smallint DEFAULT 0,
    in_duplica smallint DEFAULT 0,
    in_update smallint DEFAULT 0,
    in_delete smallint DEFAULT 0,
    in_export smallint DEFAULT 0,
    in_import smallint DEFAULT 0,
    data_modifica integer DEFAULT 0,
    orderby character varying(255) DEFAULT NULL::character varying,
    table_name character varying(100) DEFAULT NULL::character varying,
    table_type character varying(20) DEFAULT 'BASE TABLE'::character varying,
    commento character varying(255) DEFAULT NULL::character varying,
    orderby_sort character varying(255) DEFAULT 'ASC'::character varying,
    permetti_allegati smallint DEFAULT 0,
    permetti_allegati_ins smallint DEFAULT 0,
    permetti_allegati_del smallint DEFAULT 0,
    permetti_link smallint DEFAULT 0,
    permetti_link_ins smallint DEFAULT 0,
    permetti_link_del smallint DEFAULT 0,
    view_pk character varying(60) DEFAULT NULL::character varying,
    fonte_al character varying(100) DEFAULT NULL::character varying,
    table_alias character varying(100) DEFAULT NULL::character varying,
    allow_filters smallint DEFAULT 0,
    default_view character varying(5) DEFAULT 'form'::character varying,
    default_filters text
);


ALTER TABLE frontend2.registro_tab OWNER TO postgres;

--
-- Name: registro_tab_id_table_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.registro_tab_id_table_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.registro_tab_id_table_seq OWNER TO postgres;

--
-- Name: registro_tab_id_table_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.registro_tab_id_table_seq OWNED BY frontend2.registro_tab.id_table;


--
-- Name: stat; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.stat (
    id_stat integer NOT NULL,
    nome_stat character varying(250) NOT NULL,
    desc_stat text,
    def_stat text,
    auth_stat smallint DEFAULT 3,
    tipo_graph character varying(8) DEFAULT 'barre'::character varying,
    data_stat timestamp without time zone DEFAULT now() NOT NULL,
    autore bigint NOT NULL,
    settings text,
    published smallint DEFAULT 0 NOT NULL
);


ALTER TABLE frontend2.stat OWNER TO postgres;

--
-- Name: stat_id_stat_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.stat_id_stat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.stat_id_stat_seq OWNER TO postgres;

--
-- Name: stat_id_stat_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.stat_id_stat_seq OWNED BY frontend2.stat.id_stat;


--
-- Name: utente; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.utente (
    id_utente integer NOT NULL,
    nick character varying(80) DEFAULT NULL::character varying,
    passwd character(32) DEFAULT NULL::bpchar,
    nome character varying(50) DEFAULT NULL::character varying,
    cognome character varying(50) DEFAULT NULL::character varying,
    email character varying(80) DEFAULT NULL::character varying,
    info text,
    data_ins date DEFAULT now(),
    gid bigint DEFAULT '0'::bigint NOT NULL,
    livello smallint DEFAULT '1'::smallint NOT NULL,
    recover_passwd character varying(32) DEFAULT NULL::character varying
);


ALTER TABLE frontend2.utente OWNER TO postgres;

--
-- Name: utente_id_utente_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.utente_id_utente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.utente_id_utente_seq OWNER TO postgres;

--
-- Name: utente_id_utente_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.utente_id_utente_seq OWNED BY frontend2.utente.id_utente;


--
-- Name: variabili; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.variabili (
    variabile character varying(32) NOT NULL,
    gid bigint DEFAULT 0 NOT NULL,
    valore character varying(255) DEFAULT NULL::character varying,
    descrizione character varying(255) DEFAULT NULL::character varying,
    tipo_var character varying(20) DEFAULT NULL::character varying,
    pubvar smallint DEFAULT 1 NOT NULL
);


ALTER TABLE frontend2.variabili OWNER TO postgres;

--
-- Name: widget; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.widget (
    id_widget integer NOT NULL,
    id_table integer NOT NULL,
    widget_name character varying(255) DEFAULT ''::character varying NOT NULL,
    form_position character varying(11) DEFAULT '0'::character varying NOT NULL,
    widget_type character varying(100) DEFAULT ''::character varying NOT NULL,
    settings text NOT NULL
);


ALTER TABLE frontend2.widget OWNER TO postgres;

--
-- Name: widget_id_widget_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.widget_id_widget_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.widget_id_widget_seq OWNER TO postgres;

--
-- Name: widget_id_widget_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.widget_id_widget_seq OWNED BY frontend2.widget.id_widget;


--
-- Name: xml_rules; Type: TABLE; Schema: frontend2; Owner: postgres
--

CREATE TABLE frontend2.xml_rules (
    id_xml_rules integer NOT NULL,
    tabella character varying(50) NOT NULL,
    accesso character varying(20) DEFAULT 'RESTRICT'::character varying,
    accesso_gruppo character varying(100),
    autore integer,
    lastdata timestamp without time zone DEFAULT now(),
    xsl character varying(80),
    xslfo character varying(80),
    tipo_report character(1) DEFAULT 't'::bpchar,
    def_query text,
    nome_report character varying(255)
);


ALTER TABLE frontend2.xml_rules OWNER TO postgres;

--
-- Name: COLUMN xml_rules.accesso; Type: COMMENT; Schema: frontend2; Owner: postgres
--

COMMENT ON COLUMN frontend2.xml_rules.accesso IS 'RESTRICT,PUBLIC,FRONTEND,GROUP';


--
-- Name: COLUMN xml_rules.tipo_report; Type: COMMENT; Schema: frontend2; Owner: postgres
--

COMMENT ON COLUMN frontend2.xml_rules.tipo_report IS 'Report basato su tabella (t) o su query (q)';


--
-- Name: COLUMN xml_rules.def_query; Type: COMMENT; Schema: frontend2; Owner: postgres
--

COMMENT ON COLUMN frontend2.xml_rules.def_query IS 'Query di definizione per i report di tipo "query"';


--
-- Name: xml_rules_id_xml_rules_seq; Type: SEQUENCE; Schema: frontend2; Owner: postgres
--

CREATE SEQUENCE frontend2.xml_rules_id_xml_rules_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frontend2.xml_rules_id_xml_rules_seq OWNER TO postgres;

--
-- Name: xml_rules_id_xml_rules_seq; Type: SEQUENCE OWNED BY; Schema: frontend2; Owner: postgres
--

ALTER SEQUENCE frontend2.xml_rules_id_xml_rules_seq OWNED BY frontend2.xml_rules.id_xml_rules;


--
-- Name: fclassif; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fclassif (
    id integer NOT NULL,
    codclass character varying(4) NOT NULL,
    desclass text
);


ALTER TABLE public.fclassif OWNER TO utente;

--
-- Name: fclassif_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fclassif ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fclassif_id_seq1
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fcodric; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fcodric (
    id integer NOT NULL,
    codrice character varying(4),
    desrice text
);


ALTER TABLE public.fcodric OWNER TO utente;

--
-- Name: fcod_ric_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fcodric ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fcod_ric_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fcommenti_id_seq; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fcommenti ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fcommenti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fdemo; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fdemo (
    id integer NOT NULL,
    nc character varying(3),
    attf character varying(1),
    daldem integer,
    aldem integer,
    attd character varying(1),
    demog numeric,
    id2 character varying(1),
    attid character varying(1),
    trend integer,
    notedem character varying(150),
    demcompl character varying(100),
    demnloc character varying(9),
    demfonti character varying(2),
    demtm character varying(4),
    demcb character varying(6),
    dempic character varying(2),
    ffontidemo integer,
    fnloc integer,
    fscheda integer
);


ALTER TABLE public.fdemo OWNER TO utente;

--
-- Name: fdemo_id_seq; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fdemo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fdemo_id_seq
    START WITH 3085
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fedifici; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fedifici (
    id integer NOT NULL,
    classed character varying(4),
    descrizedif text,
    codicedif character varying(13),
    noted text,
    cric_ed character varying(4),
    fnloc integer
);


ALTER TABLE public.fedifici OWNER TO utente;

--
-- Name: fedifici_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fedifici ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fedifici_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: feffetti_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.feffetti ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.feffetti_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ffontidemo; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.ffontidemo (
    id integer NOT NULL,
    codfdem character varying(4),
    desfdem character varying(250),
    notefdem character varying(250)
);


ALTER TABLE public.ffontidemo OWNER TO utente;

--
-- Name: ffontidemo_id_seq; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.ffontidemo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ffontidemo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ficon; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.ficon (
    id integer NOT NULL,
    codico character varying(17),
    didasico text,
    codtotico character varying(100),
    fotoinc character varying(100),
    noteico text,
    data_ic date,
    uti_ic character varying(4),
    psw_ic character varying(4),
    cric_ic character varying(4),
    fedifici integer,
    feffetti integer,
    fnloc integer,
    fnperiod integer,
    fscheda integer,
    fschedac integer,
    fschedeb integer,
    ftipfen integer
);


ALTER TABLE public.ficon OWNER TO utente;

--
-- Name: ficon_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.ficon ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ficon_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fnfiu; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fnfiu (
    id integer NOT NULL,
    nfiu character varying(5),
    nomfium character varying(22),
    tavfiu character varying(12),
    areafiu character varying(2),
    notefiu character varying(20)
);


ALTER TABLE public.fnfiu OWNER TO utente;

--
-- Name: fnfiu_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fnfiu ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fnfiu_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fnloc2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fnloc2 (
    id integer NOT NULL,
    nloc character varying(250),
    desloc character varying(250),
    latloc integer,
    longlog integer,
    quotloc character varying(250),
    numabit character varying(250),
    prov character varying(250),
    provlet character varying(250),
    regione character varying(250),
    comune character varying(250),
    frazion character varying(250),
    foglio character varying(250),
    tavolet character varying(250),
    latlocart integer,
    longlocart integer,
    risentimenti integer,
    flagrisent integer,
    siglastato character varying(250),
    desloc2 character varying(250),
    notesito text,
    data_nl character varying(25),
    uti_nl character varying(250),
    psw_nl character varying(250),
    cric_nl character varying(250),
    nsito character varying(250),
    dt_dal character varying(250),
    dt_al character varying(250),
    notenl text,
    titolo_label1 character varying(50),
    lat_centesimale numeric,
    lon_centesimale numeric,
    geometry public.geometry(Point,4326)
);


ALTER TABLE public.fnloc2 OWNER TO postgres;

--
-- Name: fnloc_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fnloc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fnloc_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fnperiod_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fnperiod ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fnperiod_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fpiaquo_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fpiaquo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fpiaquo_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fscheda1; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fscheda1 (
    id integer NOT NULL,
    coda1 character varying(7),
    desa1 text,
    inda1 text,
    capa1 character varying(5),
    loca1 character varying(30),
    prova1 character varying(2),
    tela1 character varying(23),
    notea1 text,
    cric_a1 character varying(4),
    fnloc integer
);


ALTER TABLE public.fscheda1 OWNER TO utente;

--
-- Name: fscheda1_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fscheda1 ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fscheda1_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fscheda_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fscheda ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fscheda_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedac; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fschedac (
    id integer NOT NULL,
    deslocac character varying(40),
    codctot character varying(36),
    cric_c character varying(4),
    fscheda integer,
    fschedeb integer,
    feffetti integer,
    ftipfen integer,
    fnperiod integer,
    fnloc integer,
    fnfiu integer
);


ALTER TABLE public.fschedac OWNER TO postgres;

--
-- Name: fschedac_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.fschedac ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fschedac_id_seq
    START WITH 8473
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedad; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fschedad (
    id integer NOT NULL,
    intensd numeric,
    testod text,
    codtotd character varying(40),
    flagintpq integer,
    fscossec integer,
    ftipfen integer,
    fschedeb integer,
    fschedac integer,
    fscheda integer,
    fpiaquo integer,
    fnperiod integer,
    fnloc integer,
    feffetti integer
);


ALTER TABLE public.fschedad OWNER TO utente;

--
-- Name: fschedad_id_seq; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fschedad ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fschedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedae; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.fschedae (
    id integer NOT NULL,
    codtote character varying(40),
    testoe text,
    cric_e character varying(4),
    ftipfen integer,
    feffetti integer,
    fschedeb integer,
    fschedac integer,
    fscheda integer,
    fnperiod integer,
    fnloc integer,
    fedifici integer
);


ALTER TABLE public.fschedae OWNER TO utente;

--
-- Name: fschedae_id_seq; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fschedae ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fschedae_id_seq
    START WITH 6808
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fschedeb_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fschedeb ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fschedeb_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fscossec2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fscossec2 (
    id integer,
    nterrs character varying(9),
    nperios integer,
    tipoevs character varying(1),
    longits character varying(4),
    oraorgs character varying(8),
    latitus character varying(4),
    dataors character varying(9),
    numstas integer,
    latmacs integer,
    lonmacs integer,
    npunpqs integer,
    profsts numeric,
    longmedian numeric,
    latmedian numeric,
    ios numeric,
    imaxs numeric,
    tios numeric,
    imins numeric,
    mes numeric,
    mls numeric,
    loceps character varying(30),
    statos character varying(3),
    regamms character varying(3),
    provams character varying(3),
    livrevs character varying(3),
    codigms character varying(9),
    codopes character varying(2),
    datares character varying(25),
    reliabs character varying(1),
    delta_io character varying(2),
    esoras character varying(1),
    eslats character varying(1),
    eslons character varying(1),
    seqsec integer,
    riseqpr integer,
    promacs numeric,
    gameds numeric,
    tipoeqs character varying(1),
    mks integer,
    mps integer,
    mss integer,
    mbs integer,
    corpfgs character varying(3),
    vuotosec character varying(5),
    latcart integer,
    longcart integer,
    sinoestraz character varying(1),
    dataors2 character varying(9),
    noterrs character varying(150),
    valsism character varying(2),
    rellocepics character varying(2),
    relmms character varying(2),
    azimiepic integer,
    rellocnum integer,
    datanum integer,
    dataunis character varying(8),
    data_sep character varying(10),
    ora_sep character varying(5),
    ios_old numeric,
    data_sc character varying(25),
    uti_sc character varying(4),
    psw_sc character varying(4),
    cric_sc character varying(4),
    seqcd integer,
    npunpq_enel integer,
    nbiblio_enel integer,
    imax_enel integer,
    sito_enel character varying(3),
    err_mes numeric,
    fnperiod integer,
    titolo_label1 character varying(30),
    titolo_label2 character varying(30),
    lat_centesimale numeric,
    lon_centesimale numeric,
    geometry public.geometry(Point,4326)
);


ALTER TABLE public.fscossec2 OWNER TO postgres;

--
-- Name: fscossec_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fscossec ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fscossec_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ftabcatego_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.ftabcatego ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ftabcatego_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ftipfen_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.ftipfen ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ftipfen_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fval_fonte_id_seq1; Type: SEQUENCE; Schema: public; Owner: utente
--

ALTER TABLE public.fvalfonte ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fval_fonte_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: test3; Type: TABLE; Schema: public; Owner: utente
--

CREATE TABLE public.test3 (
    id integer NOT NULL,
    test character varying
);


ALTER TABLE public.test3 OWNER TO utente;

--
-- Name: vclassif; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vclassif AS
 SELECT fclassif.id,
    fclassif.codclass AS codice_classificazione,
    fclassif.desclass AS classificazione
   FROM public.fclassif;


ALTER TABLE public.vclassif OWNER TO postgres;

--
-- Name: vcodric; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vcodric AS
 SELECT fcodric.id,
    fcodric.codrice AS codice_di_ricerca,
    fcodric.desrice AS ricerca
   FROM public.fcodric;


ALTER TABLE public.vcodric OWNER TO postgres;

--
-- Name: vcommenti; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vcommenti AS
 SELECT fcommenti.id,
    fcommenti.codcomm AS codice_totale_commento,
    fcommenti.testocomm AS testo_commento,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    fnloc.lat_centesimale,
    fnloc.lon_centesimale,
    fnloc.provlet AS provincia,
    fnloc.siglastato AS nazione,
    ''::text AS "titolo_label_vtabcatego_Cat_Commento",
    ftabcatego.codtab AS codice_categoria_commento,
    ftabcatego.destab AS categoria_commento,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    to_tsvector(fcommenti.testocomm) AS testocomm_tsv,
    fnperiod.id AS id_fnperiod,
    fnloc.id AS id_fnloc,
    ftabcatego.id AS id_ftabcatego,
    feffetti.id AS id_feffetti
   FROM ((((public.fcommenti
     LEFT JOIN public.fnperiod ON ((fcommenti.fnperiod = fnperiod.id)))
     LEFT JOIN public.ftabcatego ON ((fcommenti.ftabcatego = ftabcatego.id)))
     LEFT JOIN public.feffetti ON ((fcommenti.feffetti = feffetti.id)))
     LEFT JOIN public.fnloc ON ((fcommenti.fnloc = fnloc.id)));


ALTER TABLE public.vcommenti OWNER TO postgres;

--
-- Name: vdemo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vdemo AS
 SELECT fdemo.id,
    fdemo.nc,
    fdemo.attf,
    fdemo.daldem,
    fdemo.aldem,
    fdemo.attd,
    fdemo.demog,
    ''::text AS "titolo_label_vfontidemo_Fonte_Demografica",
    ffontidemo.codfdem AS codice_fonte_demografica,
    ffontidemo.desfdem AS fonte_demografica,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ffontidemo.id AS id_ffontidemo,
    fnloc.id AS id_fnloc,
    fscheda.id AS id_fscheda
   FROM (((public.fdemo
     LEFT JOIN public.ffontidemo ON ((fdemo.ffontidemo = ffontidemo.id)))
     LEFT JOIN public.fnloc ON ((fdemo.fnloc = fnloc.id)))
     LEFT JOIN public.fscheda ON ((fdemo.fscheda = fscheda.id)));


ALTER TABLE public.vdemo OWNER TO postgres;

--
-- Name: vedifici; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vedifici AS
 SELECT fedifici.id,
    fedifici.classed AS classificazione_edificio,
    fedifici.codicedif AS codice_edificio,
    fedifici.descrizedif AS nome_edificio,
    fedifici.noted AS note_edificio,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_localita,
    fnloc.desloc AS nome_localita,
    fnloc.numabit AS numero_abitanti,
    fnloc.provlet AS nome_provincia,
    fnloc.id AS id_fnloc
   FROM (public.fedifici
     LEFT JOIN public.fnloc ON ((fedifici.fnloc = fnloc.id)));


ALTER TABLE public.vedifici OWNER TO postgres;

--
-- Name: veffetti; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.veffetti AS
 SELECT feffetti.id,
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    feffetti.noteeff AS note
   FROM public.feffetti;


ALTER TABLE public.veffetti OWNER TO postgres;

--
-- Name: vfontidemo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vfontidemo AS
 SELECT ffontidemo.id,
    ffontidemo.codfdem AS codice_tipo_fonte_demografica,
    ffontidemo.desfdem AS tipo_fonte_demografica,
    ffontidemo.notefdem AS note_fonte_demografica
   FROM public.ffontidemo;


ALTER TABLE public.vfontidemo OWNER TO postgres;

--
-- Name: vicon; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vicon AS
 SELECT ficon.id,
    ficon.codico,
    ficon.didasico,
    ficon.codtotico,
    ficon.fotoinc,
    ficon.noteico,
    ''::text AS "titolo_label_vedifici_Edificio",
    fedifici.codicedif AS codice_edificio,
    fedifici.descrizedif AS edificio,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza_sismica,
    fnperiod.datainiznp AS data_inizio_sequenza,
    fnperiod.datafinenp AS data_fine_sequenza,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ''::text AS "titolo_label_vschedac_Scheda_C",
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    fedifici.id AS id_fedifici,
    feffetti.id AS id_feffetti,
    fnloc.id AS id_fnloc,
    fnperiod.id AS id_fnperiod,
    fscheda.id AS id_fscheda,
    fschedac.id AS id_fschedac,
    fschedeb.id AS id_fschedeb,
    ftipfen.id AS id_ftipfen
   FROM ((((((((public.ficon
     LEFT JOIN public.fedifici ON ((ficon.fedifici = fedifici.id)))
     LEFT JOIN public.feffetti ON ((ficon.feffetti = feffetti.id)))
     LEFT JOIN public.fnloc ON ((ficon.fnloc = fnloc.id)))
     LEFT JOIN public.fnperiod ON ((ficon.fnperiod = fnperiod.id)))
     LEFT JOIN public.fscheda ON ((ficon.fscheda = fscheda.id)))
     LEFT JOIN public.fschedac ON ((ficon.fschedac = fschedac.id)))
     LEFT JOIN public.fschedeb ON ((ficon.fschedeb = fschedeb.id)))
     LEFT JOIN public.ftipfen ON ((ficon.ftipfen = ftipfen.id)));


ALTER TABLE public.vicon OWNER TO postgres;

--
-- Name: vnfiu; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vnfiu AS
 SELECT fnfiu.id,
    fnfiu.nomfium AS nome_evidenza_geomorfologica,
    fnfiu.tavfiu AS tavoletta_igm,
    fnfiu.areafiu AS area,
    fnfiu.notefiu AS note
   FROM public.fnfiu;


ALTER TABLE public.vnfiu OWNER TO postgres;

--
-- Name: vnloc; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vnloc AS
 SELECT fnloc.id,
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    fnloc.latloc AS latitudine,
    fnloc.longlog AS longitudine,
    fnloc.lat_centesimale,
    fnloc.lon_centesimale,
    fnloc.quotloc AS quota,
    fnloc.numabit AS numero_abitanti_1971,
    fnloc.prov AS codice_provincia,
    fnloc.provlet AS provincia_nome,
    fnloc.regione AS codice_regione,
    fnloc.comune AS codice_comune,
    fnloc.frazion AS codice_frazione,
    fnloc.foglio AS foglio_igm,
    fnloc.tavolet AS tavoletta_igm,
    fnloc.risentimenti,
    fnloc.siglastato AS nazione,
    fnloc.desloc2 AS altro_nome_localita,
    fnloc.notesito AS note_storiche,
    fnloc.notenl AS note
   FROM public.fnloc;


ALTER TABLE public.vnloc OWNER TO postgres;

--
-- Name: vscheda; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vscheda AS
 SELECT fscheda.id,
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.editaut AS tipo_autore,
    fscheda.ruolo AS ruolo_autore,
    fscheda.dalaut AS ruolo_autore_da,
    fscheda.alaut AS ruolo_autore_a,
    fscheda.rigeaut AS riferimento_geografico,
    fscheda.titolo1 AS titolo,
    fscheda.daltit,
    fscheda.altit,
    fscheda.rigetit AS riferimento_geografico_titolo,
    fscheda.luogoed AS luogo_edizione,
    fscheda.datauni AS data_edizione_ms1,
    fscheda.dataun2 AS data_edizione_ms2,
    fscheda.codmicr AS codice_microfilm,
    fscheda.fotiniz AS fotogramma_iniziale,
    fscheda.codmicrenel AS codice_microfilm_enel,
    fscheda.prota,
    fscheda.notea,
    fscheda.colloca,
    fscheda.ordinam,
    fscheda.tipoa,
    ''::text AS "titolo_label_vscheda1_Sede_Ricerca",
    fscheda1.coda1 AS codice_sede_ricerca,
    fscheda1.desa1 AS sede_ricerca,
    ''::text AS "titolo_label_vclassif_Classificazione",
    fclassif.codclass AS codice_classificazione,
    fclassif.desclass AS classificazione,
    fscheda1.id AS id_fscheda1,
    fclassif.id AS id_fclassif
   FROM ((public.fscheda
     LEFT JOIN public.fscheda1 ON ((fscheda.fscheda1 = fscheda1.id)))
     LEFT JOIN public.fclassif ON ((fscheda.fclassif = fclassif.id)));


ALTER TABLE public.vscheda OWNER TO postgres;

--
-- Name: vscheda1; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vscheda1 AS
 SELECT fscheda1.id,
    fscheda1.coda1 AS codice_sede_ricerca,
    fscheda1.desa1 AS sede_ricerca,
    fscheda1.inda1 AS indirizzo,
    fscheda1.capa1 AS cap,
    fscheda1.loca1 AS localita,
    fscheda1.prova1 AS provincia,
    fscheda1.tela1 AS telefono,
    fscheda1.notea1 AS note,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    fnloc.id AS id_fnloc
   FROM (public.fscheda1
     LEFT JOIN public.fnloc ON ((fscheda1.fnloc = fnloc.id)));


ALTER TABLE public.vscheda1 OWNER TO postgres;

--
-- Name: vschedac; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vschedac AS
 SELECT fschedac.id,
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.datab AS data_evento_descritto,
    fschedeb.dab AS data_evento_descritto_dal,
    fschedeb.ab AS data_evento_descritto_al,
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS descrizione_effetto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS codice_toponimo,
    fnloc.desloc AS nome_toponimo,
    ''::text AS "titolo_label_vnfiu_Fiume",
    fnfiu.nomfium AS nome_fiume,
    fnfiu.tavfiu AS tav_fiume,
    fnfiu.areafiu AS area_fiume,
    fschedeb.id AS id_fschedeb,
    feffetti.id AS id_feffetti,
    ftipfen.id AS id_ftipfen,
    fnperiod.id AS id_fnperiod,
    fnloc.id AS id_fnloc,
    fnfiu.id AS id_fnfiu
   FROM (((((((public.fschedac
     LEFT JOIN public.fscheda ON ((fschedac.fscheda = fscheda.id)))
     LEFT JOIN public.fschedeb ON ((fschedac.fschedeb = fschedeb.id)))
     LEFT JOIN public.feffetti ON ((fschedac.feffetti = feffetti.id)))
     LEFT JOIN public.ftipfen ON ((fschedac.ftipfen = ftipfen.id)))
     LEFT JOIN public.fnperiod ON ((fschedac.fnperiod = fnperiod.id)))
     LEFT JOIN public.fnloc ON ((fschedac.fnloc = fnloc.id)))
     LEFT JOIN public.fnfiu ON ((fschedac.fnfiu = fnfiu.id)));


ALTER TABLE public.vschedac OWNER TO postgres;

--
-- Name: vschedad; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vschedad AS
 SELECT fschedad.id,
    fschedad.intensd,
    fschedad.codtotd,
    fschedad.flagintpq,
    fschedad.testod,
    ''::text AS "titolo_label_vscossec_Terremoto",
    fscossec.nterrs,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_vschedac_Scheda_C",
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza_sismica,
    fnperiod.datainiznp AS data_inizio_sequenza,
    fnperiod.datafinenp AS data_fine_sequenza,
    ''::text AS "titolo_label_vpiaquo_Piano_Quotato",
    fpiaquo.deslocapq AS "des_località",
    fpiaquo.intpq AS "intensità_arabo",
    fpiaquo.intpqnum AS "intensità_romano",
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    fscossec.id AS id_fscossec,
    ftipfen.id AS id_ftipfen,
    fschedeb.id AS id_fschedeb,
    fschedac.id AS id_fschedac,
    fscheda.id AS id_fscheda,
    fnperiod.id AS id_fnperiod,
    fpiaquo.id AS id_fpiaquo,
    fnloc.id AS id_fnloc,
    feffetti.id AS id_feffetti
   FROM (((((((((public.fschedad
     LEFT JOIN public.fscossec ON ((fschedad.fscossec = fscossec.id)))
     LEFT JOIN public.ftipfen ON ((fschedad.ftipfen = ftipfen.id)))
     LEFT JOIN public.fschedeb ON ((fschedad.fschedeb = fschedeb.id)))
     LEFT JOIN public.fschedac ON ((fschedad.fschedac = fschedac.id)))
     LEFT JOIN public.fscheda ON ((fschedad.fscheda = fscheda.id)))
     LEFT JOIN public.fnperiod ON ((fschedad.fnperiod = fnperiod.id)))
     LEFT JOIN public.fpiaquo ON ((fschedad.fpiaquo = fpiaquo.id)))
     LEFT JOIN public.fnloc ON ((fschedad.fnloc = fnloc.id)))
     LEFT JOIN public.feffetti ON ((fschedad.feffetti = feffetti.id)));


ALTER TABLE public.vschedad OWNER TO postgres;

--
-- Name: vschedae; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vschedae AS
 SELECT fschedae.id,
    fschedae.codtote,
    fschedae.cric_e,
    fschedae.testoe,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS effetto,
    ''::text AS "titolo_label_vschedeb_Scheda_B",
    fschedeb.testob AS testo_trascritto,
    ''::text AS "titolo_label_vschedac_Scheda_C",
    fschedac.deslocac AS "descrizione_località",
    fschedac.codctot AS codice_totale,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.codric AS codice_ricerca,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza_sismica,
    fnperiod.datainiznp AS data_inizio_sequenza,
    fnperiod.datafinenp AS data_fine_sequenza,
    ''::text AS "titolo_label_vnloc_Località",
    fnloc.nloc AS "codice_località",
    fnloc.desloc AS "località",
    ''::text AS "titolo_label_vedifici_Edificio",
    fedifici.codicedif AS codice_edificio,
    fedifici.descrizedif AS edificio,
    fedifici.id AS id_fedifici,
    feffetti.id AS id_feffetti,
    fnloc.id AS id_fnloc,
    fnperiod.id AS id_fnperiod,
    fscheda.id AS id_fscheda,
    fschedac.id AS id_fschedac,
    fschedeb.id AS id_fschedeb,
    ftipfen.id AS id_ftipfen
   FROM ((((((((public.fschedae
     LEFT JOIN public.ftipfen ON ((fschedae.ftipfen = ftipfen.id)))
     LEFT JOIN public.feffetti ON ((fschedae.feffetti = feffetti.id)))
     LEFT JOIN public.fschedeb ON ((fschedae.fschedeb = fschedeb.id)))
     LEFT JOIN public.fschedac ON ((fschedae.fschedac = fschedac.id)))
     LEFT JOIN public.fscheda ON ((fschedae.fscheda = fscheda.id)))
     LEFT JOIN public.fnperiod ON ((fschedae.fnperiod = fnperiod.id)))
     LEFT JOIN public.fnloc ON ((fschedae.fnloc = fnloc.id)))
     LEFT JOIN public.fedifici ON ((fschedae.fedifici = fedifici.id)));


ALTER TABLE public.vschedae OWNER TO postgres;

--
-- Name: vschedeb; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vschedeb AS
 SELECT fschedeb.id,
    fschedeb.datab AS data_evento_descritto_dal,
    fschedeb.dab AS data_evento_descritto,
    fschedeb.ab AS data_evento_descritto_al,
    fschedeb.testob AS testo_trascritto,
    fschedeb.noteb AS note,
    ''::text AS "titolo_label_vscheda_Bibliografia",
    fscheda.codbib AS codice_bibliografico,
    fscheda.autore1 AS autore,
    fscheda.titolo1 AS titolo,
    fscheda.datauni AS data_edizione_ms1,
    fscheda.dataun2 AS data_edizione_ms2,
    fscheda.luogoed AS luogo_di_edizione,
    ''::text AS "titolo_label_vscheda1_Sede_Ricerca",
    fscheda1.coda1 AS codice_sede_ricerca,
    fscheda1.desa1 AS sede_ricerca,
    ''::text AS "titolo_label_veffetti_Effetto",
    feffetti.codeff AS codice_effetto,
    feffetti.deseff AS descrizione_effetto,
    ''::text AS "titolo_label_vtipfen_Fenomeno",
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    ''::text AS "titolo_label_vvalfonte_Valore_Del_Testo",
    fvalfonte.codval AS codice_valore,
    fvalfonte.desval AS valore,
    concat('http://www.cftilab.it/file_repository/pdf_T/', fnperiod.nperiod, '-', fscheda.codbib, '_T.pdf') AS pdf_trascritto,
    concat('http://www.cftilab.it/file_repository/pdf_R/', fnperiod.nperiod, '-', fscheda.codbib, '_R.pdf') AS pdf_raster,
    to_tsvector(fschedeb.testob) AS testob_tsv,
    fscheda.id AS id_fscheda,
    fscheda1.id AS id_fscheda1,
    feffetti.id AS id_feffetti,
    ftipfen.id AS id_ftipfen,
    fnperiod.id AS id_fnperiod,
    fvalfonte.id AS id_fvalfonte
   FROM ((((((public.fschedeb
     LEFT JOIN public.fscheda ON ((fschedeb.fscheda = fscheda.id)))
     LEFT JOIN public.fscheda1 ON ((fschedeb.fscheda1 = fscheda1.id)))
     LEFT JOIN public.feffetti ON ((fschedeb.feffetti = feffetti.id)))
     LEFT JOIN public.ftipfen ON ((fschedeb.ftipfen = ftipfen.id)))
     LEFT JOIN public.fnperiod ON ((fschedeb.fnperiod = fnperiod.id)))
     LEFT JOIN public.fvalfonte ON ((fschedeb.fvalfonte = fvalfonte.id)));


ALTER TABLE public.vschedeb OWNER TO postgres;

--
-- Name: vscossec; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vscossec AS
 SELECT fscossec.id,
    fscossec.nterrs AS numero_terremoto,
    fscossec.dataors AS data_origine,
    fscossec.oraorgs AS orario_origine,
    fscossec.latitus AS lat_epic_strum,
    fscossec.longits AS lon_epic_strum,
    fscossec.latmacs AS lat_epic_macro,
    fscossec.lonmacs AS lon_epic_macro,
    fscossec.loceps AS zona_epicentrale,
    fscossec.npunpqs AS n_punti_pq,
    fscossec.profsts AS "profondità",
    fscossec.ios AS io,
    fscossec.imaxs AS imax,
    fscossec.lat_centesimale,
    fscossec.lon_centesimale,
    ''::text AS "titolo_label_vnperiod_Sequenza_Sismica",
    fnperiod.nperiod AS codice_sequenza,
    fnperiod.datainiznp AS inizio_sequenza,
    fnperiod.datafinenp AS fine_sequenza,
    fnperiod.id AS id_fnperiod
   FROM (public.fscossec
     LEFT JOIN public.fnperiod ON ((fscossec.fnperiod = fnperiod.id)));


ALTER TABLE public.vscossec OWNER TO postgres;

--
-- Name: vtabcatego; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vtabcatego AS
 SELECT ftabcatego.id,
    ftabcatego.codtab AS codice_categoria_commento,
    ftabcatego.destab AS categoria_commento,
    ftabcatego.ordine AS ordinamento,
    ftabcatego.desing AS categoria_commento_inglese
   FROM public.ftabcatego;


ALTER TABLE public.vtabcatego OWNER TO postgres;

--
-- Name: vtipfen; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vtipfen AS
 SELECT ftipfen.id,
    ftipfen.codtf AS codice_fenomeno,
    ftipfen.destf AS fenomeno,
    ftipfen.notetf AS note
   FROM public.ftipfen;


ALTER TABLE public.vtipfen OWNER TO postgres;

--
-- Name: vvalfonte; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vvalfonte AS
 SELECT fvalfonte.id,
    fvalfonte.codval AS codice_valore,
    fvalfonte.desval AS valore,
    fvalfonte.noteval AS note,
    fvalfonte.desvalingl AS denominazione_inglese,
    fvalfonte.ordine_val AS ordinamento
   FROM public.fvalfonte;


ALTER TABLE public.vvalfonte OWNER TO postgres;

--
-- Name: allegato codiceallegato; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.allegato ALTER COLUMN codiceallegato SET DEFAULT nextval('frontend.allegato_codiceallegato_seq'::regclass);


--
-- Name: api_console id; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.api_console ALTER COLUMN id SET DEFAULT nextval('frontend.api_console_id_seq'::regclass);


--
-- Name: button id_button; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.button ALTER COLUMN id_button SET DEFAULT nextval('frontend.button_id_button_seq'::regclass);


--
-- Name: cache_reg id; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.cache_reg ALTER COLUMN id SET DEFAULT nextval('frontend.cache_reg_id_seq'::regclass);


--
-- Name: link codicelink; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.link ALTER COLUMN codicelink SET DEFAULT nextval('frontend.link_codicelink_seq'::regclass);


--
-- Name: log id_log; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.log ALTER COLUMN id_log SET DEFAULT nextval('frontend.log_id_log_seq'::regclass);


--
-- Name: registro_col id_reg; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_col ALTER COLUMN id_reg SET DEFAULT nextval('frontend.registro_col_id_reg_seq'::regclass);


--
-- Name: registro_submask id_submask; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask ALTER COLUMN id_submask SET DEFAULT nextval('frontend.registro_submask_id_submask_seq'::regclass);


--
-- Name: registro_submask_col id_reg_sub; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask_col ALTER COLUMN id_reg_sub SET DEFAULT nextval('frontend.registro_submask_col_id_reg_sub_seq'::regclass);


--
-- Name: registro_tab id_table; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_tab ALTER COLUMN id_table SET DEFAULT nextval('frontend.registro_tab_id_table_seq'::regclass);


--
-- Name: stat id_stat; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.stat ALTER COLUMN id_stat SET DEFAULT nextval('frontend.stat_id_stat_seq'::regclass);


--
-- Name: utente id_utente; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.utente ALTER COLUMN id_utente SET DEFAULT nextval('frontend.utente_id_utente_seq'::regclass);


--
-- Name: widget id_widget; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.widget ALTER COLUMN id_widget SET DEFAULT nextval('frontend.widget_id_widget_seq'::regclass);


--
-- Name: xml_rules id_xml_rules; Type: DEFAULT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.xml_rules ALTER COLUMN id_xml_rules SET DEFAULT nextval('frontend.xml_rules_id_xml_rules_seq'::regclass);


--
-- Name: allegato codiceallegato; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.allegato ALTER COLUMN codiceallegato SET DEFAULT nextval('frontend2.allegato_codiceallegato_seq'::regclass);


--
-- Name: button id_button; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.button ALTER COLUMN id_button SET DEFAULT nextval('frontend2.button_id_button_seq'::regclass);


--
-- Name: cache_reg id; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.cache_reg ALTER COLUMN id SET DEFAULT nextval('frontend2.cache_reg_id_seq'::regclass);


--
-- Name: link codicelink; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.link ALTER COLUMN codicelink SET DEFAULT nextval('frontend2.link_codicelink_seq'::regclass);


--
-- Name: log id_log; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.log ALTER COLUMN id_log SET DEFAULT nextval('frontend2.log_id_log_seq'::regclass);


--
-- Name: registro_col id_reg; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_col ALTER COLUMN id_reg SET DEFAULT nextval('frontend2.registro_col_id_reg_seq'::regclass);


--
-- Name: registro_submask id_submask; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_submask ALTER COLUMN id_submask SET DEFAULT nextval('frontend2.registro_submask_id_submask_seq'::regclass);


--
-- Name: registro_submask_col id_reg_sub; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_submask_col ALTER COLUMN id_reg_sub SET DEFAULT nextval('frontend2.registro_submask_col_id_reg_sub_seq'::regclass);


--
-- Name: registro_tab id_table; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_tab ALTER COLUMN id_table SET DEFAULT nextval('frontend2.registro_tab_id_table_seq'::regclass);


--
-- Name: stat id_stat; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.stat ALTER COLUMN id_stat SET DEFAULT nextval('frontend2.stat_id_stat_seq'::regclass);


--
-- Name: utente id_utente; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.utente ALTER COLUMN id_utente SET DEFAULT nextval('frontend2.utente_id_utente_seq'::regclass);


--
-- Name: widget id_widget; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.widget ALTER COLUMN id_widget SET DEFAULT nextval('frontend2.widget_id_widget_seq'::regclass);


--
-- Name: xml_rules id_xml_rules; Type: DEFAULT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.xml_rules ALTER COLUMN id_xml_rules SET DEFAULT nextval('frontend2.xml_rules_id_xml_rules_seq'::regclass);


--
-- Name: biblio_ee biblio_ee_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.biblio_ee
    ADD CONSTRAINT biblio_ee_pkey PRIMARY KEY (id);


--
-- Name: codbib_list_ee codbib_list_ee_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.codbib_list_ee
    ADD CONSTRAINT codbib_list_ee_pkey PRIMARY KEY (id);


--
-- Name: ee_med ee_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.ee_med
    ADD CONSTRAINT ee_med_pkey PRIMARY KEY (id);


--
-- Name: ee_tabella ee_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.ee_tabella
    ADD CONSTRAINT ee_pkey PRIMARY KEY (id);


--
-- Name: fee_med fee_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fee_med
    ADD CONSTRAINT fee_med_pkey PRIMARY KEY (id);


--
-- Name: fee_med_total fee_med_total_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fee_med_total
    ADD CONSTRAINT fee_med_total_pkey PRIMARY KEY (id);


--
-- Name: fnloc_cfti_5 fnloc_cfti_5_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fnloc_cfti_5
    ADD CONSTRAINT fnloc_cfti_5_pkey PRIMARY KEY (id);


--
-- Name: fnloc_cfti fnloc_cfti_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fnloc_cfti
    ADD CONSTRAINT fnloc_cfti_pkey PRIMARY KEY (id);


--
-- Name: fnperiod_cfti fnperiod_cfti_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fnperiod_cfti
    ADD CONSTRAINT fnperiod_cfti_pkey PRIMARY KEY (id);


--
-- Name: fpq_med fpq_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fpq_med
    ADD CONSTRAINT fpq_med_pkey PRIMARY KEY (id);


--
-- Name: fpq_med_total fpq_med_total_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.fpq_med_total
    ADD CONSTRAINT fpq_med_total_pkey PRIMARY KEY (id);


--
-- Name: locind_med locind_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.locind_med
    ADD CONSTRAINT locind_med_pkey PRIMARY KEY (id);


--
-- Name: nloc_med nloc_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.nloc_med
    ADD CONSTRAINT nloc_med_pkey PRIMARY KEY (id);


--
-- Name: nterrs_med nterrs_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.nterrs_med
    ADD CONSTRAINT nterrs_med_pkey PRIMARY KEY (id);


--
-- Name: nterrs nterrs_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.nterrs
    ADD CONSTRAINT nterrs_pkey PRIMARY KEY (id);


--
-- Name: pq_med pq_med_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.pq_med
    ADD CONSTRAINT pq_med_pkey PRIMARY KEY (id);


--
-- Name: schedec schedec_pkey; Type: CONSTRAINT; Schema: cfti5public; Owner: utente
--

ALTER TABLE ONLY cfti5public.schedec
    ADD CONSTRAINT schedec_pkey PRIMARY KEY (id);


--
-- Name: api_console api_console_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.api_console
    ADD CONSTRAINT api_console_pkey PRIMARY KEY (id);


--
-- Name: button button_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.button
    ADD CONSTRAINT button_pkey PRIMARY KEY (id_button);


--
-- Name: cache_reg cache_reg_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.cache_reg
    ADD CONSTRAINT cache_reg_pkey PRIMARY KEY (id);


--
-- Name: allegato pk_allegato; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.allegato
    ADD CONSTRAINT pk_allegato PRIMARY KEY (codiceallegato);


--
-- Name: gruppo pk_gruppo; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.gruppo
    ADD CONSTRAINT pk_gruppo PRIMARY KEY (gid);


--
-- Name: registro_tab pk_id_table; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_tab
    ADD CONSTRAINT pk_id_table PRIMARY KEY (id_table);


--
-- Name: link pk_link; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.link
    ADD CONSTRAINT pk_link PRIMARY KEY (codicelink);


--
-- Name: log pk_log; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.log
    ADD CONSTRAINT pk_log PRIMARY KEY (id_log);


--
-- Name: recordlock pk_recordlock; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.recordlock
    ADD CONSTRAINT pk_recordlock PRIMARY KEY (tabella, colonna, id);


--
-- Name: registro_col pk_registro_col; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_col
    ADD CONSTRAINT pk_registro_col PRIMARY KEY (id_reg);


--
-- Name: registro_submask pk_registro_submask; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask
    ADD CONSTRAINT pk_registro_submask PRIMARY KEY (id_submask);


--
-- Name: registro_submask_col pk_registro_submask_col; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask_col
    ADD CONSTRAINT pk_registro_submask_col PRIMARY KEY (id_reg_sub);


--
-- Name: stat pk_stat; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.stat
    ADD CONSTRAINT pk_stat PRIMARY KEY (id_stat);


--
-- Name: utente pk_utente; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.utente
    ADD CONSTRAINT pk_utente PRIMARY KEY (id_utente);


--
-- Name: variabili pk_variabili; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.variabili
    ADD CONSTRAINT pk_variabili PRIMARY KEY (variabile, gid);


--
-- Name: xml_rules pk_xml_rules; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.xml_rules
    ADD CONSTRAINT pk_xml_rules PRIMARY KEY (id_xml_rules);


--
-- Name: api_console u_apy_key; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.api_console
    ADD CONSTRAINT u_apy_key UNIQUE (api_key);


--
-- Name: gruppo u_gruppo_nome_gruppo; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.gruppo
    ADD CONSTRAINT u_gruppo_nome_gruppo UNIQUE (nome_gruppo);


--
-- Name: widget widget_pkey; Type: CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.widget
    ADD CONSTRAINT widget_pkey PRIMARY KEY (id_widget);


--
-- Name: button button_pkey; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.button
    ADD CONSTRAINT button_pkey PRIMARY KEY (id_button);


--
-- Name: cache_reg cache_reg_pkey; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.cache_reg
    ADD CONSTRAINT cache_reg_pkey PRIMARY KEY (id);


--
-- Name: allegato pk_allegato; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.allegato
    ADD CONSTRAINT pk_allegato PRIMARY KEY (codiceallegato);


--
-- Name: gruppo pk_gruppo; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.gruppo
    ADD CONSTRAINT pk_gruppo PRIMARY KEY (gid);


--
-- Name: registro_tab pk_id_table; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_tab
    ADD CONSTRAINT pk_id_table PRIMARY KEY (id_table);


--
-- Name: link pk_link; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.link
    ADD CONSTRAINT pk_link PRIMARY KEY (codicelink);


--
-- Name: log pk_log; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.log
    ADD CONSTRAINT pk_log PRIMARY KEY (id_log);


--
-- Name: recordlock pk_recordlock; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.recordlock
    ADD CONSTRAINT pk_recordlock PRIMARY KEY (tabella, colonna, id);


--
-- Name: registro_col pk_registro_col; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_col
    ADD CONSTRAINT pk_registro_col PRIMARY KEY (id_reg);


--
-- Name: registro_submask pk_registro_submask; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_submask
    ADD CONSTRAINT pk_registro_submask PRIMARY KEY (id_submask);


--
-- Name: registro_submask_col pk_registro_submask_col; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_submask_col
    ADD CONSTRAINT pk_registro_submask_col PRIMARY KEY (id_reg_sub);


--
-- Name: stat pk_stat; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.stat
    ADD CONSTRAINT pk_stat PRIMARY KEY (id_stat);


--
-- Name: utente pk_utente; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.utente
    ADD CONSTRAINT pk_utente PRIMARY KEY (id_utente);


--
-- Name: variabili pk_variabili; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.variabili
    ADD CONSTRAINT pk_variabili PRIMARY KEY (variabile, gid);


--
-- Name: xml_rules pk_xml_rules; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.xml_rules
    ADD CONSTRAINT pk_xml_rules PRIMARY KEY (id_xml_rules);


--
-- Name: gruppo u_gruppo_nome_gruppo; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.gruppo
    ADD CONSTRAINT u_gruppo_nome_gruppo UNIQUE (nome_gruppo);


--
-- Name: widget widget_pkey; Type: CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.widget
    ADD CONSTRAINT widget_pkey PRIMARY KEY (id_widget);


--
-- Name: fcommenti codice_totale_commento_unique; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcommenti
    ADD CONSTRAINT codice_totale_commento_unique UNIQUE (fnperiod, ftabcatego, fnloc, feffetti);


--
-- Name: fclassif fclassif_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fclassif
    ADD CONSTRAINT fclassif_pkey PRIMARY KEY (id);


--
-- Name: fcodric fcod_ric_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcodric
    ADD CONSTRAINT fcod_ric_pkey PRIMARY KEY (id);


--
-- Name: fcommenti fcommenti_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcommenti
    ADD CONSTRAINT fcommenti_pkey PRIMARY KEY (id);


--
-- Name: fdemo fdemo_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fdemo
    ADD CONSTRAINT fdemo_pkey PRIMARY KEY (id);


--
-- Name: fedifici fedifici_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fedifici
    ADD CONSTRAINT fedifici_pkey PRIMARY KEY (id);


--
-- Name: feffetti feffetti_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.feffetti
    ADD CONSTRAINT feffetti_pkey PRIMARY KEY (id);


--
-- Name: ffontidemo ffontidemo_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ffontidemo
    ADD CONSTRAINT ffontidemo_pkey PRIMARY KEY (id);


--
-- Name: ficon ficon_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_pkey PRIMARY KEY (id);


--
-- Name: fnfiu fnfiu_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fnfiu
    ADD CONSTRAINT fnfiu_pkey PRIMARY KEY (id);


--
-- Name: fnloc2 fnloc2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fnloc2
    ADD CONSTRAINT fnloc2_pkey PRIMARY KEY (id);


--
-- Name: fnloc fnloc_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fnloc
    ADD CONSTRAINT fnloc_pkey PRIMARY KEY (id);


--
-- Name: fnperiod fnperiod_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fnperiod
    ADD CONSTRAINT fnperiod_pkey PRIMARY KEY (id);


--
-- Name: fpiaquo fpiaquo_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fpiaquo
    ADD CONSTRAINT fpiaquo_pkey PRIMARY KEY (id);


--
-- Name: fscheda1 fscheda1_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscheda1
    ADD CONSTRAINT fscheda1_pkey PRIMARY KEY (id);


--
-- Name: fscheda fscheda_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscheda
    ADD CONSTRAINT fscheda_pkey PRIMARY KEY (id);


--
-- Name: fschedac fschedac_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fschedac
    ADD CONSTRAINT fschedac_pkey PRIMARY KEY (id);


--
-- Name: fschedad fschedad_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_pkey PRIMARY KEY (id);


--
-- Name: fschedae fschedae_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_pkey PRIMARY KEY (id);


--
-- Name: fschedeb fschedeb_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_pkey PRIMARY KEY (id);


--
-- Name: fscossec fscossec_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscossec
    ADD CONSTRAINT fscossec_pkey PRIMARY KEY (id);


--
-- Name: ftabcatego ftabcatego_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ftabcatego
    ADD CONSTRAINT ftabcatego_pkey PRIMARY KEY (id);


--
-- Name: ftipfen ftipfen_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ftipfen
    ADD CONSTRAINT ftipfen_pkey PRIMARY KEY (id);


--
-- Name: fvalfonte fval_fonte_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fvalfonte
    ADD CONSTRAINT fval_fonte_pkey PRIMARY KEY (id);


--
-- Name: seq_loc seq_loc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seq_loc
    ADD CONSTRAINT seq_loc_pkey PRIMARY KEY (id);


--
-- Name: seq_scosse seq_scosse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seq_scosse
    ADD CONSTRAINT seq_scosse_pkey PRIMARY KEY (id);


--
-- Name: test3 test3_pkey; Type: CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.test3
    ADD CONSTRAINT test3_pkey PRIMARY KEY (id);


--
-- Name: idx_biblio_details_codbib_nperiod; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_biblio_details_codbib_nperiod ON cfti5public.biblio_details USING btree (codbib, nperiod);


--
-- Name: idx_commgen_nperiod_cfti; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_commgen_nperiod_cfti ON cfti5public.commgen USING btree (nperiod_cfti);


--
-- Name: idx_d1_nperiod_cfti; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_d1_nperiod_cfti ON cfti5public.d1 USING btree (nperiod_cfti);


--
-- Name: idx_pq_med_nterrs; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_pq_med_nterrs ON cfti5public.pq_med USING btree (nterr);


--
-- Name: idx_pq_nterr; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_pq_nterr ON cfti5public.pq USING btree (nterrs);


--
-- Name: idx_quake_details_nloc_cfti; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_quake_details_nloc_cfti ON cfti5public.quake_details USING btree (nloc_cfti);


--
-- Name: idx_schedea_codbib; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_schedea_codbib ON cfti5public.schedea USING btree (codbib);


--
-- Name: idx_schedeb_nperiod; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_schedeb_nperiod ON cfti5public.schedeb USING btree (nperiod);


--
-- Name: idx_schedec_nloc; Type: INDEX; Schema: cfti5public; Owner: utente
--

CREATE INDEX idx_schedec_nloc ON cfti5public.schedec USING btree (nloc);


--
-- Name: i_autore_stat; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_autore_stat ON frontend.stat USING btree (autore);


--
-- Name: i_button_id_table; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_button_id_table ON frontend.button USING btree (id_table);


--
-- Name: i_gid; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_gid ON frontend.registro_tab USING btree (gid);


--
-- Name: i_id_submask; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_id_submask ON frontend.registro_submask_col USING btree (id_submask);


--
-- Name: i_id_table; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_id_table ON frontend.registro_col USING btree (id_table);


--
-- Name: i_registro_col_gid; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_registro_col_gid ON frontend.registro_col USING btree (gid);


--
-- Name: i_table_name; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_table_name ON frontend.registro_tab USING btree (table_name);


--
-- Name: i_utente_gid; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_utente_gid ON frontend.utente USING btree (gid);


--
-- Name: i_variabile; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_variabile ON frontend.variabili USING btree (variabile);


--
-- Name: i_widget_id_table; Type: INDEX; Schema: frontend; Owner: postgres
--

CREATE INDEX i_widget_id_table ON frontend.widget USING btree (id_table);


--
-- Name: i_autore_stat; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_autore_stat ON frontend2.stat USING btree (autore);


--
-- Name: i_button_id_table; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_button_id_table ON frontend2.button USING btree (id_table);


--
-- Name: i_gid; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_gid ON frontend2.registro_tab USING btree (gid);


--
-- Name: i_id_submask; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_id_submask ON frontend2.registro_submask_col USING btree (id_submask);


--
-- Name: i_id_table; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_id_table ON frontend2.registro_col USING btree (id_table);


--
-- Name: i_registro_col_gid; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_registro_col_gid ON frontend2.registro_col USING btree (gid);


--
-- Name: i_table_name; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_table_name ON frontend2.registro_tab USING btree (table_name);


--
-- Name: i_utente_gid; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_utente_gid ON frontend2.utente USING btree (gid);


--
-- Name: i_variabile; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_variabile ON frontend2.variabili USING btree (variabile);


--
-- Name: i_widget_id_table; Type: INDEX; Schema: frontend2; Owner: postgres
--

CREATE INDEX i_widget_id_table ON frontend2.widget USING btree (id_table);


--
-- Name: fnloc2_wkb_geometry_geometry_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fnloc2_wkb_geometry_geometry_idx ON public.fnloc2 USING gist (geometry);


--
-- Name: fscossec2_wkb_geometry_geometry_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fscossec2_wkb_geometry_geometry_idx ON public.fscossec2 USING gist (geometry);


--
-- Name: vcommenti vcommenti_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vcommenti_insert AS
    ON INSERT TO public.vcommenti DO INSTEAD  INSERT INTO public.fcommenti (testocomm)
  VALUES (new.testo_commento);


--
-- Name: vcommenti vcommenti_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vcommenti_update AS
    ON UPDATE TO public.vcommenti DO INSTEAD  UPDATE public.fcommenti SET testocomm = new.testo_commento
  WHERE (fcommenti.id = old.id);


--
-- Name: vdemo vdemo_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vdemo_insert AS
    ON INSERT TO public.vdemo DO INSTEAD  INSERT INTO public.fdemo (nc, attf, daldem, aldem, attd, demog)
  VALUES (new.nc, new.attf, new.daldem, new.aldem, new.attd, new.demog);


--
-- Name: vdemo vdemo_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vdemo_update AS
    ON UPDATE TO public.vdemo DO INSTEAD  UPDATE public.fdemo SET nc = new.nc, attf = new.attf, daldem = new.daldem, aldem = new.aldem, attd = new.attd, demog = new.demog
  WHERE (fdemo.id = old.id);


--
-- Name: vedifici vedifici_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vedifici_insert AS
    ON INSERT TO public.vedifici DO INSTEAD  INSERT INTO public.fedifici (classed, codicedif, descrizedif, noted)
  VALUES (new.classificazione_edificio, new.codice_edificio, new.nome_edificio, new.note_edificio);


--
-- Name: vedifici vedifici_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vedifici_update AS
    ON UPDATE TO public.vedifici DO INSTEAD  UPDATE public.fedifici SET classed = new.classificazione_edificio, codicedif = new.codice_edificio, descrizedif = new.nome_edificio, noted = new.note_edificio
  WHERE (fedifici.id = old.id);


--
-- Name: vicon vicon_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vicon_insert AS
    ON INSERT TO public.vicon DO INSTEAD  INSERT INTO public.ficon (codico, didasico, codtotico, fotoinc, noteico)
  VALUES (new.codico, new.didasico, new.codtotico, new.fotoinc, new.noteico);


--
-- Name: vicon vicon_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vicon_update AS
    ON UPDATE TO public.vicon DO INSTEAD  UPDATE public.ficon SET codico = new.codico, didasico = new.didasico, codtotico = new.codtotico, fotoinc = new.fotoinc, noteico = new.noteico
  WHERE (ficon.id = old.id);


--
-- Name: vscheda1 vscheda1_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vscheda1_insert AS
    ON INSERT TO public.vscheda1 DO INSTEAD  INSERT INTO public.fscheda1 (coda1, desa1, inda1, capa1, loca1, prova1, tela1, notea1)
  VALUES (new.codice_sede_ricerca, new.sede_ricerca, new.indirizzo, new.cap, new.localita, new.provincia, new.telefono, new.note);


--
-- Name: vscheda1 vscheda1_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vscheda1_update AS
    ON UPDATE TO public.vscheda1 DO INSTEAD  UPDATE public.fscheda1 SET coda1 = new.codice_sede_ricerca, desa1 = new.sede_ricerca, inda1 = new.indirizzo, capa1 = new.cap, loca1 = new.localita, prova1 = new.provincia, tela1 = new.telefono, notea1 = new.note
  WHERE (fscheda1.id = old.id);


--
-- Name: vscheda vscheda_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vscheda_insert AS
    ON INSERT TO public.vscheda DO INSTEAD  INSERT INTO public.fscheda (codbib, codric, autore1, editaut, titolo1, ruolo, dalaut, alaut, daltit, altit, rigeaut, rigetit, luogoed, datauni, dataun2, fotiniz, codmicrenel, codmicr, prota, colloca, tipoa, ordinam, notea)
  VALUES (new.codice_bibliografico, new.codice_ricerca, new.autore, new.tipo_autore, new.titolo, new.ruolo_autore, new.ruolo_autore_da, new.ruolo_autore_a, new.daltit, new.altit, new.riferimento_geografico, new.riferimento_geografico_titolo, new.luogo_edizione, new.data_edizione_ms1, new.data_edizione_ms2, new.fotogramma_iniziale, new.codice_microfilm_enel, new.codice_microfilm, new.prota, new.colloca, new.tipoa, new.ordinam, new.notea);


--
-- Name: vscheda vscheda_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vscheda_update AS
    ON UPDATE TO public.vscheda DO INSTEAD  UPDATE public.fscheda SET codbib = new.codice_bibliografico, codric = new.codice_ricerca, autore1 = new.autore, editaut = new.tipo_autore, titolo1 = new.titolo, ruolo = new.ruolo_autore, dalaut = new.ruolo_autore_da, alaut = new.ruolo_autore_a, daltit = new.daltit, altit = new.altit, rigeaut = new.riferimento_geografico, rigetit = new.riferimento_geografico_titolo, luogoed = new.luogo_edizione, datauni = new.data_edizione_ms1, dataun2 = new.data_edizione_ms2, fotiniz = new.fotogramma_iniziale, codmicrenel = new.codice_microfilm_enel, codmicr = new.codice_microfilm, prota = new.prota, colloca = new.colloca, tipoa = new.tipoa, ordinam = new.ordinam, notea = new.notea
  WHERE (fscheda.id = old.id);


--
-- Name: vschedad vschedad_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vschedad_insert AS
    ON INSERT TO public.vschedad DO INSTEAD  INSERT INTO public.fschedad (intensd, codtotd, flagintpq, testod)
  VALUES (new.intensd, new.codtotd, new.flagintpq, new.testod);


--
-- Name: vschedad vschedad_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vschedad_update AS
    ON UPDATE TO public.vschedad DO INSTEAD  UPDATE public.fschedad SET intensd = new.intensd, codtotd = new.codtotd, flagintpq = new.flagintpq, testod = new.testod
  WHERE (fschedad.id = old.id);


--
-- Name: vschedeb vschedeb_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vschedeb_insert AS
    ON INSERT TO public.vschedeb DO INSTEAD  INSERT INTO public.fschedeb (testob, noteb, dab, ab, datab)
  VALUES (new.testo_trascritto, new.note, new.data_evento_descritto, new.data_evento_descritto_al, new.data_evento_descritto_dal);


--
-- Name: vschedeb vschedeb_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vschedeb_update AS
    ON UPDATE TO public.vschedeb DO INSTEAD  UPDATE public.fschedeb SET testob = new.testo_trascritto, noteb = new.note, dab = new.data_evento_descritto, ab = new.data_evento_descritto_al, datab = new.data_evento_descritto_dal
  WHERE (fschedeb.id = old.id);


--
-- Name: vscossec vscossec_insert; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vscossec_insert AS
    ON INSERT TO public.vscossec DO INSTEAD  INSERT INTO public.fscossec (nterrs, dataors, oraorgs, latitus, longits, latmacs, lonmacs, loceps, npunpqs, profsts, ios, imaxs, lat_centesimale, lon_centesimale)
  VALUES (new.numero_terremoto, new.data_origine, new.orario_origine, new.lat_epic_strum, new.lon_epic_strum, new.lat_epic_macro, new.lon_epic_macro, new.zona_epicentrale, new.n_punti_pq, new."profondità", new.io, new.imax, new.lat_centesimale, new.lon_centesimale);


--
-- Name: vscossec vscossec_update; Type: RULE; Schema: public; Owner: postgres
--

CREATE RULE vscossec_update AS
    ON UPDATE TO public.vscossec DO INSTEAD  UPDATE public.fscossec SET nterrs = new.numero_terremoto, dataors = new.data_origine, oraorgs = new.orario_origine, latitus = new.lat_epic_strum, longits = new.lon_epic_strum, latmacs = new.lat_epic_macro, lonmacs = new.lon_epic_macro, loceps = new.zona_epicentrale, npunpqs = new.n_punti_pq, profsts = new."profondità", ios = new.io, imaxs = new.imax, lat_centesimale = new.lat_centesimale, lon_centesimale = new.lon_centesimale
  WHERE (fscossec.id = old.id);


--
-- Name: fnloc lat_centesimale; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER lat_centesimale BEFORE INSERT OR UPDATE OF latloc ON public.fnloc FOR EACH ROW EXECUTE PROCEDURE public.coordinate_centesimali();


--
-- Name: fscossec lat_centesimale; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER lat_centesimale BEFORE INSERT OR UPDATE OF latmacs ON public.fscossec FOR EACH ROW EXECUTE PROCEDURE public.coordinate_centesimali_scossec();


--
-- Name: fnloc lon_centesimale; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER lon_centesimale BEFORE INSERT OR UPDATE OF longlog ON public.fnloc FOR EACH ROW EXECUTE PROCEDURE public.coordinate_centesimali_lon();


--
-- Name: fscossec lon_centesimale; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER lon_centesimale BEFORE INSERT OR UPDATE OF lonmacs ON public.fscossec FOR EACH ROW EXECUTE PROCEDURE public.coordinate_centesimali_lon_scossec();


--
-- Name: fscheda ordinam; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER ordinam BEFORE INSERT OR UPDATE OF autore1, titolo1 ON public.fscheda FOR EACH ROW EXECUTE PROCEDURE public.genera_ordinam();


--
-- Name: fnloc z_campo_geometry; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER z_campo_geometry BEFORE INSERT OR UPDATE OF latloc, longlog ON public.fnloc FOR EACH ROW EXECUTE PROCEDURE public.fnloc_geometry();


--
-- Name: fscossec z_campo_geometry; Type: TRIGGER; Schema: public; Owner: utente
--

CREATE TRIGGER z_campo_geometry BEFORE INSERT OR UPDATE OF latmacs, lonmacs ON public.fscossec FOR EACH ROW EXECUTE PROCEDURE public.fscossec_geometry();


--
-- Name: button fk_button_id_table; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.button
    ADD CONSTRAINT fk_button_id_table FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registro_col fk_registro_col_1; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_col
    ADD CONSTRAINT fk_registro_col_1 FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON DELETE CASCADE;


--
-- Name: registro_submask fk_registro_submask; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask
    ADD CONSTRAINT fk_registro_submask FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON DELETE CASCADE;


--
-- Name: registro_submask_col fk_registro_submask_col; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_submask_col
    ADD CONSTRAINT fk_registro_submask_col FOREIGN KEY (id_submask) REFERENCES frontend.registro_submask(id_submask) ON DELETE CASCADE;


--
-- Name: utente fk_utente; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.utente
    ADD CONSTRAINT fk_utente FOREIGN KEY (gid) REFERENCES frontend.gruppo(gid) ON UPDATE CASCADE;


--
-- Name: widget fk_widget_id_table; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.widget
    ADD CONSTRAINT fk_widget_id_table FOREIGN KEY (id_table) REFERENCES frontend.registro_tab(id_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registro_tab gid; Type: FK CONSTRAINT; Schema: frontend; Owner: postgres
--

ALTER TABLE ONLY frontend.registro_tab
    ADD CONSTRAINT gid FOREIGN KEY (gid) REFERENCES frontend.gruppo(gid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: button fk_button_id_table; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.button
    ADD CONSTRAINT fk_button_id_table FOREIGN KEY (id_table) REFERENCES frontend2.registro_tab(id_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registro_col fk_registro_col_1; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_col
    ADD CONSTRAINT fk_registro_col_1 FOREIGN KEY (id_table) REFERENCES frontend2.registro_tab(id_table) ON DELETE CASCADE;


--
-- Name: registro_submask fk_registro_submask; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_submask
    ADD CONSTRAINT fk_registro_submask FOREIGN KEY (id_table) REFERENCES frontend2.registro_tab(id_table) ON DELETE CASCADE;


--
-- Name: registro_submask_col fk_registro_submask_col; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_submask_col
    ADD CONSTRAINT fk_registro_submask_col FOREIGN KEY (id_submask) REFERENCES frontend2.registro_submask(id_submask) ON DELETE CASCADE;


--
-- Name: utente fk_utente; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.utente
    ADD CONSTRAINT fk_utente FOREIGN KEY (gid) REFERENCES frontend2.gruppo(gid) ON UPDATE CASCADE;


--
-- Name: widget fk_widget_id_table; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.widget
    ADD CONSTRAINT fk_widget_id_table FOREIGN KEY (id_table) REFERENCES frontend2.registro_tab(id_table) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registro_tab gid; Type: FK CONSTRAINT; Schema: frontend2; Owner: postgres
--

ALTER TABLE ONLY frontend2.registro_tab
    ADD CONSTRAINT gid FOREIGN KEY (gid) REFERENCES frontend2.gruppo(gid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fcommenti fcommenti_feffetti_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcommenti
    ADD CONSTRAINT fcommenti_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES public.feffetti(id);


--
-- Name: fcommenti fcommenti_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcommenti
    ADD CONSTRAINT fcommenti_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: fcommenti fcommenti_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcommenti
    ADD CONSTRAINT fcommenti_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: fcommenti fcommenti_ftabcatego_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fcommenti
    ADD CONSTRAINT fcommenti_ftabcatego_fkey FOREIGN KEY (ftabcatego) REFERENCES public.ftabcatego(id);


--
-- Name: fdemo fdemo_ffontidemo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fdemo
    ADD CONSTRAINT fdemo_ffontidemo_fkey FOREIGN KEY (ffontidemo) REFERENCES public.ffontidemo(id);


--
-- Name: fdemo fdemo_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fdemo
    ADD CONSTRAINT fdemo_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: fdemo fdemo_fscheda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fdemo
    ADD CONSTRAINT fdemo_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES public.fscheda(id);


--
-- Name: fedifici fedifici_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fedifici
    ADD CONSTRAINT fedifici_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: ficon ficon_fedifici_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_fedifici_fkey FOREIGN KEY (fedifici) REFERENCES public.fedifici(id);


--
-- Name: ficon ficon_feffetti_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES public.feffetti(id);


--
-- Name: ficon ficon_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: ficon ficon_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: ficon ficon_fscheda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES public.fscheda(id);


--
-- Name: ficon ficon_fschedeb_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_fschedeb_fkey FOREIGN KEY (fschedeb) REFERENCES public.fschedeb(id);


--
-- Name: ficon ficon_ftipfen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.ficon
    ADD CONSTRAINT ficon_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES public.ftipfen(id);


--
-- Name: fnperiod fnperiod_feffetti_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fnperiod
    ADD CONSTRAINT fnperiod_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES public.feffetti(id);


--
-- Name: fnperiod fnperiod_ftipfen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fnperiod
    ADD CONSTRAINT fnperiod_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES public.ftipfen(id);


--
-- Name: fpiaquo fpiaquo_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fpiaquo
    ADD CONSTRAINT fpiaquo_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: fpiaquo fpiaquo_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fpiaquo
    ADD CONSTRAINT fpiaquo_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: fpiaquo fpiaquo_fscossec_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fpiaquo
    ADD CONSTRAINT fpiaquo_fscossec_fkey FOREIGN KEY (fscossec) REFERENCES public.fscossec(id);


--
-- Name: fscheda1 fscheda1_id_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscheda1
    ADD CONSTRAINT fscheda1_id_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: fscheda fscheda_fclassif_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscheda
    ADD CONSTRAINT fscheda_fclassif_fkey FOREIGN KEY (fclassif) REFERENCES public.fclassif(id);


--
-- Name: fscheda fscheda_fscheda1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscheda
    ADD CONSTRAINT fscheda_fscheda1_fkey FOREIGN KEY (fscheda1) REFERENCES public.fscheda1(id);


--
-- Name: fschedad fschedad_feffetti_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES public.feffetti(id);


--
-- Name: fschedad fschedad_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: fschedad fschedad_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: fschedad fschedad_fpiaquo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_fpiaquo_fkey FOREIGN KEY (fpiaquo) REFERENCES public.fpiaquo(id);


--
-- Name: fschedad fschedad_fscheda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES public.fscheda(id);


--
-- Name: fschedad fschedad_fschedeb_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_fschedeb_fkey FOREIGN KEY (fschedeb) REFERENCES public.fschedeb(id);


--
-- Name: fschedad fschedad_fscossec_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_fscossec_fkey FOREIGN KEY (fscossec) REFERENCES public.fscossec(id);


--
-- Name: fschedad fschedad_ftipfen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedad
    ADD CONSTRAINT fschedad_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES public.ftipfen(id);


--
-- Name: fschedae fschedae_fedifici_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_fedifici_fkey FOREIGN KEY (fedifici) REFERENCES public.fedifici(id);


--
-- Name: fschedae fschedae_feffetti_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES public.feffetti(id);


--
-- Name: fschedae fschedae_fnloc_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_fnloc_fkey FOREIGN KEY (fnloc) REFERENCES public.fnloc(id);


--
-- Name: fschedae fschedae_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: fschedae fschedae_fscheda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES public.fscheda(id);


--
-- Name: fschedae fschedae_fschedeb_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_fschedeb_fkey FOREIGN KEY (fschedeb) REFERENCES public.fschedeb(id);


--
-- Name: fschedae fschedae_ftipfen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedae
    ADD CONSTRAINT fschedae_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES public.ftipfen(id);


--
-- Name: fschedeb fschedeb_feffetti_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_feffetti_fkey FOREIGN KEY (feffetti) REFERENCES public.feffetti(id);


--
-- Name: fschedeb fschedeb_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: fschedeb fschedeb_fscheda1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_fscheda1_fkey FOREIGN KEY (fscheda1) REFERENCES public.fscheda1(id);


--
-- Name: fschedeb fschedeb_fscheda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_fscheda_fkey FOREIGN KEY (fscheda) REFERENCES public.fscheda(id);


--
-- Name: fschedeb fschedeb_ftipfen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_ftipfen_fkey FOREIGN KEY (ftipfen) REFERENCES public.ftipfen(id);


--
-- Name: fschedeb fschedeb_fvalfonte_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fschedeb
    ADD CONSTRAINT fschedeb_fvalfonte_fkey FOREIGN KEY (fvalfonte) REFERENCES public.fvalfonte(id);


--
-- Name: fscossec fscossec_fnperiod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: utente
--

ALTER TABLE ONLY public.fscossec
    ADD CONSTRAINT fscossec_fnperiod_fkey FOREIGN KEY (fnperiod) REFERENCES public.fnperiod(id);


--
-- Name: SCHEMA pg_catalog; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA pg_catalog TO utente;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: utente
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO utente;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: FUNCTION raster_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_in(cstring) TO utente;


--
-- Name: FUNCTION raster_out(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_out(public.raster) TO utente;


--
-- Name: FUNCTION box2d_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d_in(cstring) TO utente;


--
-- Name: FUNCTION box2d_out(public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d_out(public.box2d) TO utente;


--
-- Name: FUNCTION box2df_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2df_in(cstring) TO utente;


--
-- Name: FUNCTION box2df_out(public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2df_out(public.box2df) TO utente;


--
-- Name: FUNCTION box3d_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d_in(cstring) TO utente;


--
-- Name: FUNCTION box3d_out(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d_out(public.box3d) TO utente;


--
-- Name: FUNCTION geography_analyze(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_analyze(internal) TO utente;


--
-- Name: FUNCTION geography_in(cstring, oid, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_in(cstring, oid, integer) TO utente;


--
-- Name: FUNCTION geography_out(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_out(public.geography) TO utente;


--
-- Name: FUNCTION geography_recv(internal, oid, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_recv(internal, oid, integer) TO utente;


--
-- Name: FUNCTION geography_send(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_send(public.geography) TO utente;


--
-- Name: FUNCTION geography_typmod_in(cstring[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_typmod_in(cstring[]) TO utente;


--
-- Name: FUNCTION geography_typmod_out(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_typmod_out(integer) TO utente;


--
-- Name: FUNCTION geometry_analyze(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_analyze(internal) TO utente;


--
-- Name: FUNCTION geometry_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_in(cstring) TO utente;


--
-- Name: FUNCTION geometry_out(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_out(public.geometry) TO utente;


--
-- Name: FUNCTION geometry_recv(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_recv(internal) TO utente;


--
-- Name: FUNCTION geometry_send(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_send(public.geometry) TO utente;


--
-- Name: FUNCTION geometry_typmod_in(cstring[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_typmod_in(cstring[]) TO utente;


--
-- Name: FUNCTION geometry_typmod_out(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_typmod_out(integer) TO utente;


--
-- Name: FUNCTION gidx_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gidx_in(cstring) TO utente;


--
-- Name: FUNCTION gidx_out(public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gidx_out(public.gidx) TO utente;


--
-- Name: FUNCTION spheroid_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.spheroid_in(cstring) TO utente;


--
-- Name: FUNCTION spheroid_out(public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.spheroid_out(public.spheroid) TO utente;


--
-- Name: FUNCTION box3d(public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d(public.box2d) TO utente;


--
-- Name: FUNCTION geometry(public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.box2d) TO utente;


--
-- Name: FUNCTION box(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box(public.box3d) TO utente;


--
-- Name: FUNCTION box2d(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d(public.box3d) TO utente;


--
-- Name: FUNCTION geometry(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.box3d) TO utente;


--
-- Name: FUNCTION geography(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography(bytea) TO utente;


--
-- Name: FUNCTION geometry(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(bytea) TO utente;


--
-- Name: FUNCTION bytea(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.bytea(public.geography) TO utente;


--
-- Name: FUNCTION geography(public.geography, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography(public.geography, integer, boolean) TO utente;


--
-- Name: FUNCTION geometry(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.geography) TO utente;


--
-- Name: FUNCTION box(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box(public.geometry) TO utente;


--
-- Name: FUNCTION box2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d(public.geometry) TO utente;


--
-- Name: FUNCTION box3d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d(public.geometry) TO utente;


--
-- Name: FUNCTION bytea(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.bytea(public.geometry) TO utente;


--
-- Name: FUNCTION geography(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography(public.geometry) TO utente;


--
-- Name: FUNCTION geometry(public.geometry, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.geometry, integer, boolean) TO utente;


--
-- Name: FUNCTION path(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.path(public.geometry) TO utente;


--
-- Name: FUNCTION point(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.point(public.geometry) TO utente;


--
-- Name: FUNCTION polygon(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.polygon(public.geometry) TO utente;


--
-- Name: FUNCTION text(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.text(public.geometry) TO utente;


--
-- Name: FUNCTION geometry(path); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(path) TO utente;


--
-- Name: FUNCTION geometry(point); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(point) TO utente;


--
-- Name: FUNCTION geometry(polygon); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(polygon) TO utente;


--
-- Name: FUNCTION box3d(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d(public.raster) TO utente;


--
-- Name: FUNCTION bytea(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.bytea(public.raster) TO utente;


--
-- Name: FUNCTION st_convexhull(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_convexhull(public.raster) TO utente;


--
-- Name: FUNCTION geometry(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(text) TO utente;


--
-- Name: FUNCTION __st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.__st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer) TO utente;


--
-- Name: FUNCTION _add_raster_constraint(cn name, sql text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint(cn name, sql text) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_overview_constraint(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_overview_constraint(ovschema name, ovtable name, ovcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint(rastschema name, rasttable name, cn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint(rastschema name, rasttable name, cn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name) TO utente;


--
-- Name: FUNCTION _overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer) TO utente;


--
-- Name: FUNCTION _postgis_deprecate(oldname text, newname text, version text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_deprecate(oldname text, newname text, version text) TO utente;


--
-- Name: FUNCTION _postgis_index_extent(tbl regclass, col text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_index_extent(tbl regclass, col text) TO utente;


--
-- Name: FUNCTION _postgis_join_selectivity(regclass, text, regclass, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_join_selectivity(regclass, text, regclass, text, text) TO utente;


--
-- Name: FUNCTION _postgis_pgsql_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_pgsql_version() TO utente;


--
-- Name: FUNCTION _postgis_scripts_pgsql_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_scripts_pgsql_version() TO utente;


--
-- Name: FUNCTION _postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text) TO utente;


--
-- Name: FUNCTION _postgis_stats(tbl regclass, att_name text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_stats(tbl regclass, att_name text, text) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name) TO utente;


--
-- Name: FUNCTION _raster_constraint_nodata_values(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_nodata_values(rast public.raster) TO utente;


--
-- Name: FUNCTION _raster_constraint_out_db(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_out_db(rast public.raster) TO utente;


--
-- Name: FUNCTION _raster_constraint_pixel_types(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_pixel_types(rast public.raster) TO utente;


--
-- Name: FUNCTION _st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_3dintersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_asgeojson(integer, public.geography, integer, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geography, integer, integer) TO utente;


--
-- Name: FUNCTION _st_asgeojson(integer, public.geometry, integer, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geometry, integer, integer) TO utente;


--
-- Name: FUNCTION _st_asgml(integer, public.geography, integer, integer, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgml(integer, public.geography, integer, integer, text, text) TO utente;


--
-- Name: FUNCTION _st_asgml(integer, public.geometry, integer, integer, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgml(integer, public.geometry, integer, integer, text, text) TO utente;


--
-- Name: FUNCTION _st_askml(integer, public.geography, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_askml(integer, public.geography, integer, text) TO utente;


--
-- Name: FUNCTION _st_askml(integer, public.geometry, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_askml(integer, public.geometry, integer, text) TO utente;


--
-- Name: FUNCTION _st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION _st_asx3d(integer, public.geometry, integer, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asx3d(integer, public.geometry, integer, integer, text) TO utente;


--
-- Name: FUNCTION _st_bestsrid(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_bestsrid(public.geography) TO utente;


--
-- Name: FUNCTION _st_bestsrid(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_bestsrid(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION _st_buffer(public.geometry, double precision, cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_buffer(public.geometry, double precision, cstring) TO utente;


--
-- Name: FUNCTION _st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO utente;


--
-- Name: FUNCTION _st_colormap(rast public.raster, nband integer, colormap text, method text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_colormap(rast public.raster, nband integer, colormap text, method text) TO utente;


--
-- Name: FUNCTION _st_concavehull(param_inputgeom public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_concavehull(param_inputgeom public.geometry) TO utente;


--
-- Name: FUNCTION _st_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_contains(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_containsproperly(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_convertarray4ma(value double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_convertarray4ma(value double precision[]) TO utente;


--
-- Name: FUNCTION _st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_countagg_finalfn(agg public.agg_count); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_finalfn(agg public.agg_count) TO utente;


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_coveredby(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_coveredby(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_covers(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_covers(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION _st_covers(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_covers(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_crosses(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_crosses(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION _st_distance(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distance(public.geography, public.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_distancetree(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION _st_distancetree(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, boolean) TO utente;


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION _st_dwithin(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithin(public.geography, public.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION _st_dwithinuncached(public.geography, public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision) TO utente;


--
-- Name: FUNCTION _st_dwithinuncached(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION _st_equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_equals(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_expand(public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_expand(public.geography, double precision) TO utente;


--
-- Name: FUNCTION _st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer) TO utente;


--
-- Name: FUNCTION _st_geomfromgml(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_geomfromgml(text, integer) TO utente;


--
-- Name: FUNCTION _st_grayscale4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_grayscale4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_intersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_intersects(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_intersects(geom public.geometry, rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_intersects(geom public.geometry, rast public.raster, nband integer) TO utente;


--
-- Name: FUNCTION _st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_longestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_longestline(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_maxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION _st_orderingequals(geometrya public.geometry, geometryb public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO utente;


--
-- Name: FUNCTION _st_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_overlaps(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION _st_pointoutside(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_pointoutside(public.geography) TO utente;


--
-- Name: FUNCTION _st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION _st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION _st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO utente;


--
-- Name: FUNCTION _st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO utente;


--
-- Name: FUNCTION _st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_samealignment_finalfn(agg public.agg_samealignment); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_samealignment_finalfn(agg public.agg_samealignment) TO utente;


--
-- Name: FUNCTION _st_samealignment_transfn(agg public.agg_samealignment, rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_samealignment_transfn(agg public.agg_samealignment, rast public.raster) TO utente;


--
-- Name: FUNCTION _st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION _st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION _st_summarystats_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_finalfn(internal) TO utente;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, boolean, double precision) TO utente;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean) TO utente;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean, double precision) TO utente;


--
-- Name: FUNCTION _st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION _st_touches(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_touches(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION _st_union_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_finalfn(internal) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, text) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, public.unionarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, public.unionarg[]) TO utente;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer, text) TO utente;


--
-- Name: FUNCTION _st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO utente;


--
-- Name: FUNCTION _st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean) TO utente;


--
-- Name: FUNCTION _st_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_within(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION _st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION _st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO utente;


--
-- Name: FUNCTION _updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO utente;


--
-- Name: FUNCTION addauth(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addauth(text) TO utente;


--
-- Name: FUNCTION addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO utente;


--
-- Name: FUNCTION addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO utente;


--
-- Name: FUNCTION addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean) TO utente;


--
-- Name: FUNCTION addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer) TO utente;


--
-- Name: FUNCTION addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION box3dtobox(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3dtobox(public.box3d) TO utente;


--
-- Name: FUNCTION checkauth(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.checkauth(text, text) TO utente;


--
-- Name: FUNCTION checkauth(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.checkauth(text, text, text) TO utente;


--
-- Name: FUNCTION checkauthtrigger(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.checkauthtrigger() TO utente;


--
-- Name: FUNCTION contains_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.box2df) TO utente;


--
-- Name: FUNCTION contains_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.geometry) TO utente;


--
-- Name: FUNCTION contains_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.contains_2d(public.geometry, public.box2df) TO utente;


--
-- Name: FUNCTION disablelongtransactions(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.disablelongtransactions() TO utente;


--
-- Name: FUNCTION dropgeometrycolumn(table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(table_name character varying, column_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrytable(table_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrytable(table_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrytable(schema_name character varying, table_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrytable(schema_name character varying, table_name character varying) TO utente;


--
-- Name: FUNCTION dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying) TO utente;


--
-- Name: FUNCTION dropoverviewconstraints(ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovtable name, ovcolumn name) TO utente;


--
-- Name: FUNCTION dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO utente;


--
-- Name: FUNCTION enablelongtransactions(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.enablelongtransactions() TO utente;


--
-- Name: FUNCTION equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.equals(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION find_srid(character varying, character varying, character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.find_srid(character varying, character varying, character varying) TO utente;


--
-- Name: FUNCTION fnloc_geometry(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.fnloc_geometry() TO utente;


--
-- Name: FUNCTION fscossec_geometry(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.fscossec_geometry() TO utente;


--
-- Name: FUNCTION geog_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geog_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geography_cmp(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_cmp(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_distance_knn(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_distance_knn(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_eq(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_eq(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_ge(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_ge(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_gist_compress(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_compress(internal) TO utente;


--
-- Name: FUNCTION geography_gist_consistent(internal, public.geography, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_consistent(internal, public.geography, integer) TO utente;


--
-- Name: FUNCTION geography_gist_decompress(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_decompress(internal) TO utente;


--
-- Name: FUNCTION geography_gist_distance(internal, public.geography, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_distance(internal, public.geography, integer) TO utente;


--
-- Name: FUNCTION geography_gist_penalty(internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_penalty(internal, internal, internal) TO utente;


--
-- Name: FUNCTION geography_gist_picksplit(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_picksplit(internal, internal) TO utente;


--
-- Name: FUNCTION geography_gist_same(public.box2d, public.box2d, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_same(public.box2d, public.box2d, internal) TO utente;


--
-- Name: FUNCTION geography_gist_union(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_union(bytea, internal) TO utente;


--
-- Name: FUNCTION geography_gt(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gt(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_le(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_le(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_lt(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_lt(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geography_overlaps(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_overlaps(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION geom2d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geom2d_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geom3d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geom3d_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geom4d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geom4d_brin_inclusion_add_value(internal, internal, internal, internal) TO utente;


--
-- Name: FUNCTION geometry_above(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_above(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_below(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_below(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_cmp(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_cmp(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_contained_3d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_contained_3d(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_contained_by_raster(public.geometry, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_contained_by_raster(public.geometry, public.raster) TO utente;


--
-- Name: FUNCTION geometry_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_contains(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_contains_3d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_contains_3d(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_box(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_box(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_centroid_nd(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_centroid_nd(public.geometry, public.geometry) TO utente;


--
-- Name: FUNCTION geometry_distance_cpa(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_cpa(public.geometry, public.geometry) TO utente;


--
-- Name: FUNCTION geometry_eq(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_eq(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_ge(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_ge(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_gist_compress_2d(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_compress_2d(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_compress_nd(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_compress_nd(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_consistent_2d(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_consistent_2d(internal, public.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_consistent_nd(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_consistent_nd(internal, public.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_decompress_2d(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_decompress_2d(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_decompress_nd(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_decompress_nd(internal) TO utente;


--
-- Name: FUNCTION geometry_gist_distance_2d(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_distance_2d(internal, public.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_distance_nd(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_distance_nd(internal, public.geometry, integer) TO utente;


--
-- Name: FUNCTION geometry_gist_penalty_2d(internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_penalty_2d(internal, internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_penalty_nd(internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_penalty_nd(internal, internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_picksplit_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_picksplit_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_picksplit_nd(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_picksplit_nd(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_same_nd(public.geometry, public.geometry, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_same_nd(public.geometry, public.geometry, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_union_2d(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_union_2d(bytea, internal) TO utente;


--
-- Name: FUNCTION geometry_gist_union_nd(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_union_nd(bytea, internal) TO utente;


--
-- Name: FUNCTION geometry_gt(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gt(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_hash(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_hash(public.geometry) TO utente;


--
-- Name: FUNCTION geometry_le(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_le(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_left(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_left(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_lt(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_lt(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overabove(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overabove(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overbelow(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overbelow(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overlaps(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overlaps_3d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overlaps_3d(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overlaps_nd(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overlaps_nd(public.geometry, public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overleft(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overleft(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_overright(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overright(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_raster_contain(public.geometry, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_raster_contain(public.geometry, public.raster) TO utente;


--
-- Name: FUNCTION geometry_raster_overlap(public.geometry, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_raster_overlap(public.geometry, public.raster) TO utente;


--
-- Name: FUNCTION geometry_right(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_right(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_same(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_same(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_same_3d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_same_3d(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometry_spgist_choose_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_choose_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_choose_3d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_choose_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_compress_2d(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_compress_2d(internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_compress_3d(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_compress_3d(internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_config_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_config_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_config_3d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_config_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_inner_consistent_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_inner_consistent_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_inner_consistent_3d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_inner_consistent_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_leaf_consistent_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_leaf_consistent_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_leaf_consistent_3d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_leaf_consistent_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_picksplit_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_picksplit_2d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_spgist_picksplit_3d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_spgist_picksplit_3d(internal, internal) TO utente;


--
-- Name: FUNCTION geometry_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_within(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION geometrytype(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometrytype(public.geography) TO utente;


--
-- Name: FUNCTION geometrytype(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometrytype(public.geometry) TO utente;


--
-- Name: FUNCTION geomfromewkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geomfromewkb(bytea) TO utente;


--
-- Name: FUNCTION geomfromewkt(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geomfromewkt(text) TO utente;


--
-- Name: FUNCTION get_proj4_from_srid(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.get_proj4_from_srid(integer) TO utente;


--
-- Name: FUNCTION gettransactionid(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gettransactionid() TO utente;


--
-- Name: FUNCTION gserialized_gist_joinsel_2d(internal, oid, internal, smallint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_2d(internal, oid, internal, smallint) TO utente;


--
-- Name: FUNCTION gserialized_gist_joinsel_nd(internal, oid, internal, smallint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_nd(internal, oid, internal, smallint) TO utente;


--
-- Name: FUNCTION gserialized_gist_sel_2d(internal, oid, internal, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_sel_2d(internal, oid, internal, integer) TO utente;


--
-- Name: FUNCTION gserialized_gist_sel_nd(internal, oid, internal, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_sel_nd(internal, oid, internal, integer) TO utente;


--
-- Name: FUNCTION is_contained_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.box2df) TO utente;


--
-- Name: FUNCTION is_contained_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.geometry) TO utente;


--
-- Name: FUNCTION is_contained_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.geometry, public.box2df) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, text) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, timestamp without time zone) TO utente;


--
-- Name: FUNCTION lockrow(text, text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, text, timestamp without time zone) TO utente;


--
-- Name: FUNCTION longtransactionsenabled(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.longtransactionsenabled() TO utente;


--
-- Name: FUNCTION overlaps_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.box2df) TO utente;


--
-- Name: FUNCTION overlaps_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.geometry) TO utente;


--
-- Name: FUNCTION overlaps_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.geometry, public.box2df) TO utente;


--
-- Name: FUNCTION overlaps_geog(public.geography, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.geography, public.gidx) TO utente;


--
-- Name: FUNCTION overlaps_geog(public.gidx, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.geography) TO utente;


--
-- Name: FUNCTION overlaps_geog(public.gidx, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.gidx) TO utente;


--
-- Name: FUNCTION overlaps_nd(public.geometry, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.geometry, public.gidx) TO utente;


--
-- Name: FUNCTION overlaps_nd(public.gidx, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.geometry) TO utente;


--
-- Name: FUNCTION overlaps_nd(public.gidx, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.gidx) TO utente;


--
-- Name: FUNCTION pgis_asgeobuf_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asgeobuf_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_asgeobuf_transfn(internal, anyelement); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asgeobuf_transfn(internal, anyelement) TO utente;


--
-- Name: FUNCTION pgis_asgeobuf_transfn(internal, anyelement, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asgeobuf_transfn(internal, anyelement, text) TO utente;


--
-- Name: FUNCTION pgis_asmvt_combinefn(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_combinefn(internal, internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_deserialfn(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_deserialfn(bytea, internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_serialfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_serialfn(internal) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement, text) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement, text, integer) TO utente;


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement, text, integer, text) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(internal, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(internal, public.geometry) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(internal, public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(internal, public.geometry, double precision) TO utente;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(internal, public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(internal, public.geometry, double precision, integer) TO utente;


--
-- Name: FUNCTION pgis_geometry_clusterintersecting_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_clusterintersecting_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_clusterwithin_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_clusterwithin_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_collect_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_collect_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_makeline_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_makeline_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_polygonize_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_polygonize_finalfn(internal) TO utente;


--
-- Name: FUNCTION pgis_geometry_union_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_union_finalfn(internal) TO utente;


--
-- Name: FUNCTION populate_geometry_columns(use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.populate_geometry_columns(use_typmod boolean) TO utente;


--
-- Name: FUNCTION populate_geometry_columns(tbl_oid oid, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.populate_geometry_columns(tbl_oid oid, use_typmod boolean) TO utente;


--
-- Name: FUNCTION postgis_addbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_addbbox(public.geometry) TO utente;


--
-- Name: FUNCTION postgis_cache_bbox(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_cache_bbox() TO utente;


--
-- Name: FUNCTION postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text) TO utente;


--
-- Name: FUNCTION postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text) TO utente;


--
-- Name: FUNCTION postgis_constraint_type(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_constraint_type(geomschema text, geomtable text, geomcolumn text) TO utente;


--
-- Name: FUNCTION postgis_dropbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_dropbbox(public.geometry) TO utente;


--
-- Name: FUNCTION postgis_extensions_upgrade(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_extensions_upgrade() TO utente;


--
-- Name: FUNCTION postgis_full_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_full_version() TO utente;


--
-- Name: FUNCTION postgis_gdal_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_gdal_version() TO utente;


--
-- Name: FUNCTION postgis_geos_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_geos_version() TO utente;


--
-- Name: FUNCTION postgis_getbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_getbbox(public.geometry) TO utente;


--
-- Name: FUNCTION postgis_hasbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_hasbbox(public.geometry) TO utente;


--
-- Name: FUNCTION postgis_lib_build_date(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_lib_build_date() TO utente;


--
-- Name: FUNCTION postgis_lib_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_lib_version() TO utente;


--
-- Name: FUNCTION postgis_libjson_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_libjson_version() TO utente;


--
-- Name: FUNCTION postgis_liblwgeom_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_liblwgeom_version() TO utente;


--
-- Name: FUNCTION postgis_libprotobuf_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_libprotobuf_version() TO utente;


--
-- Name: FUNCTION postgis_libxml_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_libxml_version() TO utente;


--
-- Name: FUNCTION postgis_noop(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_noop(public.geometry) TO utente;


--
-- Name: FUNCTION postgis_noop(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_noop(public.raster) TO utente;


--
-- Name: FUNCTION postgis_proj_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_proj_version() TO utente;


--
-- Name: FUNCTION postgis_raster_lib_build_date(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_raster_lib_build_date() TO utente;


--
-- Name: FUNCTION postgis_raster_lib_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_raster_lib_version() TO utente;


--
-- Name: FUNCTION postgis_raster_scripts_installed(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_raster_scripts_installed() TO utente;


--
-- Name: FUNCTION postgis_scripts_build_date(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_scripts_build_date() TO utente;


--
-- Name: FUNCTION postgis_scripts_installed(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_scripts_installed() TO utente;


--
-- Name: FUNCTION postgis_scripts_released(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_scripts_released() TO utente;


--
-- Name: FUNCTION postgis_svn_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_svn_version() TO utente;


--
-- Name: FUNCTION postgis_transform_geometry(public.geometry, text, text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_transform_geometry(public.geometry, text, text, integer) TO utente;


--
-- Name: FUNCTION postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean) TO utente;


--
-- Name: FUNCTION postgis_typmod_dims(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_typmod_dims(integer) TO utente;


--
-- Name: FUNCTION postgis_typmod_srid(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_typmod_srid(integer) TO utente;


--
-- Name: FUNCTION postgis_typmod_type(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_typmod_type(integer) TO utente;


--
-- Name: FUNCTION postgis_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_version() TO utente;


--
-- Name: FUNCTION raster_above(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_above(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_below(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_below(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_contain(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_contain(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_contained(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_contained(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_contained_by_geometry(public.raster, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_contained_by_geometry(public.raster, public.geometry) TO utente;


--
-- Name: FUNCTION raster_eq(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_eq(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_geometry_contain(public.raster, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_geometry_contain(public.raster, public.geometry) TO utente;


--
-- Name: FUNCTION raster_geometry_overlap(public.raster, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_geometry_overlap(public.raster, public.geometry) TO utente;


--
-- Name: FUNCTION raster_hash(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_hash(public.raster) TO utente;


--
-- Name: FUNCTION raster_left(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_left(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_overabove(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overabove(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_overbelow(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overbelow(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_overlap(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overlap(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_overleft(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overleft(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_overright(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overright(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_right(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_right(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION raster_same(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_same(public.raster, public.raster) TO utente;


--
-- Name: FUNCTION st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_3ddistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3ddistance(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_3dintersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_3dlength(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dlength(public.geometry) TO utente;


--
-- Name: FUNCTION st_3dlength_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dlength_spheroid(public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_3dlongestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dlongestline(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_3dmakebox(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dmakebox(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_3dperimeter(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dperimeter(public.geometry) TO utente;


--
-- Name: FUNCTION st_3dshortestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dshortestline(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_addband(rast public.raster, addbandargset public.addbandarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, addbandargset public.addbandarg[]) TO utente;


--
-- Name: FUNCTION st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer) TO utente;


--
-- Name: FUNCTION st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer) TO utente;


--
-- Name: FUNCTION st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_addmeasure(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addmeasure(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_addpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_addpoint(geom1 public.geometry, geom2 public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_angle(line1 public.geometry, line2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_angle(line1 public.geometry, line2 public.geometry) TO utente;


--
-- Name: FUNCTION st_angle(pt1 public.geometry, pt2 public.geometry, pt3 public.geometry, pt4 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_angle(pt1 public.geometry, pt2 public.geometry, pt3 public.geometry, pt4 public.geometry) TO utente;


--
-- Name: FUNCTION st_approxcount(rast public.raster, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rast public.raster, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO utente;


--
-- Name: FUNCTION st_area(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area(text) TO utente;


--
-- Name: FUNCTION st_area(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area(public.geometry) TO utente;


--
-- Name: FUNCTION st_area(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area(geog public.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_area2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area2d(public.geometry) TO utente;


--
-- Name: FUNCTION st_asbinary(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geography) TO utente;


--
-- Name: FUNCTION st_asbinary(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geometry) TO utente;


--
-- Name: FUNCTION st_asbinary(public.geography, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geography, text) TO utente;


--
-- Name: FUNCTION st_asbinary(public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geometry, text) TO utente;


--
-- Name: FUNCTION st_asbinary(public.raster, outasin boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.raster, outasin boolean) TO utente;


--
-- Name: FUNCTION st_asencodedpolyline(geom public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asencodedpolyline(geom public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_asewkb(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkb(public.geometry) TO utente;


--
-- Name: FUNCTION st_asewkb(public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkb(public.geometry, text) TO utente;


--
-- Name: FUNCTION st_asewkt(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkt(text) TO utente;


--
-- Name: FUNCTION st_asewkt(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkt(public.geography) TO utente;


--
-- Name: FUNCTION st_asewkt(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkt(public.geometry) TO utente;


--
-- Name: FUNCTION st_asgdalraster(rast public.raster, format text, options text[], srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgdalraster(rast public.raster, format text, options text[], srid integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(text) TO utente;


--
-- Name: FUNCTION st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(text) TO utente;


--
-- Name: FUNCTION st_asgml(geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(geog public.geography, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgml(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(geom public.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text) TO utente;


--
-- Name: FUNCTION st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text) TO utente;


--
-- Name: FUNCTION st_ashexewkb(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry) TO utente;


--
-- Name: FUNCTION st_ashexewkb(public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry, text) TO utente;


--
-- Name: FUNCTION st_ashexwkb(public.raster, outasin boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ashexwkb(public.raster, outasin boolean) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, options text[]) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nbands integer[], options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], options text[]) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nbands integer[], quality integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], quality integer) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nband integer, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, options text[]) TO utente;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nband integer, quality integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, quality integer) TO utente;


--
-- Name: FUNCTION st_askml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(text) TO utente;


--
-- Name: FUNCTION st_askml(geog public.geography, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(geog public.geography, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_askml(geom public.geometry, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(geom public.geometry, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text) TO utente;


--
-- Name: FUNCTION st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text) TO utente;


--
-- Name: FUNCTION st_aslatlontext(geom public.geometry, tmpl text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aslatlontext(geom public.geometry, tmpl text) TO utente;


--
-- Name: FUNCTION st_asmvtgeom(geom public.geometry, bounds public.box2d, extent integer, buffer integer, clip_geom boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asmvtgeom(geom public.geometry, bounds public.box2d, extent integer, buffer integer, clip_geom boolean) TO utente;


--
-- Name: FUNCTION st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_aspng(rast public.raster, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, options text[]) TO utente;


--
-- Name: FUNCTION st_aspng(rast public.raster, nbands integer[], options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], options text[]) TO utente;


--
-- Name: FUNCTION st_aspng(rast public.raster, nbands integer[], compression integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], compression integer) TO utente;


--
-- Name: FUNCTION st_aspng(rast public.raster, nband integer, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, options text[]) TO utente;


--
-- Name: FUNCTION st_aspng(rast public.raster, nband integer, compression integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, compression integer) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO utente;


--
-- Name: FUNCTION st_assvg(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_assvg(text) TO utente;


--
-- Name: FUNCTION st_assvg(geog public.geography, rel integer, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_assvg(geog public.geography, rel integer, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer) TO utente;


--
-- Name: FUNCTION st_astext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(text) TO utente;


--
-- Name: FUNCTION st_astext(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(public.geography) TO utente;


--
-- Name: FUNCTION st_astext(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(public.geometry) TO utente;


--
-- Name: FUNCTION st_astext(public.geography, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(public.geography, integer) TO utente;


--
-- Name: FUNCTION st_astext(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast public.raster, options text[], srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, options text[], srid integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast public.raster, compression text, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, compression text, srid integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast public.raster, nbands integer[], options text[], srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], options text[], srid integer) TO utente;


--
-- Name: FUNCTION st_astiff(rast public.raster, nbands integer[], compression text, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], compression text, srid integer) TO utente;


--
-- Name: FUNCTION st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO utente;


--
-- Name: FUNCTION st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO utente;


--
-- Name: FUNCTION st_aswkb(public.raster, outasin boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aswkb(public.raster, outasin boolean) TO utente;


--
-- Name: FUNCTION st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer) TO utente;


--
-- Name: FUNCTION st_azimuth(geog1 public.geography, geog2 public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_azimuth(geog1 public.geography, geog2 public.geography) TO utente;


--
-- Name: FUNCTION st_azimuth(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_azimuth(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_band(rast public.raster, nbands integer[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands integer[]) TO utente;


--
-- Name: FUNCTION st_band(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_band(rast public.raster, nbands text, delimiter character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands text, delimiter character) TO utente;


--
-- Name: FUNCTION st_bandfilesize(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandfilesize(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandfiletimestamp(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandfiletimestamp(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandisnodata(rast public.raster, forcechecking boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, forcechecking boolean) TO utente;


--
-- Name: FUNCTION st_bandisnodata(rast public.raster, band integer, forcechecking boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, band integer, forcechecking boolean) TO utente;


--
-- Name: FUNCTION st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint) TO utente;


--
-- Name: FUNCTION st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text, OUT outdbbandnum integer, OUT filesize bigint, OUT filetimestamp bigint) TO utente;


--
-- Name: FUNCTION st_bandnodatavalue(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandnodatavalue(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandpath(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandpath(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bandpixeltype(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandpixeltype(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_bdmpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bdmpolyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_bdpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bdpolyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_boundary(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_boundary(public.geometry) TO utente;


--
-- Name: FUNCTION st_boundingdiagonal(geom public.geometry, fits boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_boundingdiagonal(geom public.geometry, fits boolean) TO utente;


--
-- Name: FUNCTION st_box2dfromgeohash(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_box2dfromgeohash(text, integer) TO utente;


--
-- Name: FUNCTION st_buffer(text, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision) TO utente;


--
-- Name: FUNCTION st_buffer(public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision) TO utente;


--
-- Name: FUNCTION st_buffer(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_buffer(text, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision, integer) TO utente;


--
-- Name: FUNCTION st_buffer(text, double precision, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision, text) TO utente;


--
-- Name: FUNCTION st_buffer(public.geography, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, integer) TO utente;


--
-- Name: FUNCTION st_buffer(public.geography, double precision, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, text) TO utente;


--
-- Name: FUNCTION st_buffer(public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, integer) TO utente;


--
-- Name: FUNCTION st_buffer(public.geometry, double precision, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, text) TO utente;


--
-- Name: FUNCTION st_buildarea(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buildarea(public.geometry) TO utente;


--
-- Name: FUNCTION st_centroid(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_centroid(text) TO utente;


--
-- Name: FUNCTION st_centroid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_centroid(public.geometry) TO utente;


--
-- Name: FUNCTION st_centroid(public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_centroid(public.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_chaikinsmoothing(public.geometry, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_chaikinsmoothing(public.geometry, integer, boolean) TO utente;


--
-- Name: FUNCTION st_cleangeometry(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_cleangeometry(public.geometry) TO utente;


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO utente;


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean) TO utente;


--
-- Name: FUNCTION st_clipbybox2d(geom public.geometry, box public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clipbybox2d(geom public.geometry, box public.box2d) TO utente;


--
-- Name: FUNCTION st_closestpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_closestpoint(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_closestpointofapproach(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_closestpointofapproach(public.geometry, public.geometry) TO utente;


--
-- Name: FUNCTION st_clusterdbscan(public.geometry, eps double precision, minpoints integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterdbscan(public.geometry, eps double precision, minpoints integer) TO utente;


--
-- Name: FUNCTION st_clusterintersecting(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry[]) TO utente;


--
-- Name: FUNCTION st_clusterkmeans(geom public.geometry, k integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterkmeans(geom public.geometry, k integer) TO utente;


--
-- Name: FUNCTION st_clusterwithin(public.geometry[], double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry[], double precision) TO utente;


--
-- Name: FUNCTION st_collect(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collect(public.geometry[]) TO utente;


--
-- Name: FUNCTION st_collect(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collect(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_collectionextract(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collectionextract(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_collectionhomogenize(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collectionhomogenize(public.geometry) TO utente;


--
-- Name: FUNCTION st_colormap(rast public.raster, colormap text, method text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, colormap text, method text) TO utente;


--
-- Name: FUNCTION st_colormap(rast public.raster, nband integer, colormap text, method text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, nband integer, colormap text, method text) TO utente;


--
-- Name: FUNCTION st_combine_bbox(public.box2d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combine_bbox(public.box2d, public.geometry) TO utente;


--
-- Name: FUNCTION st_combine_bbox(public.box3d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combine_bbox(public.box3d, public.geometry) TO utente;


--
-- Name: FUNCTION st_combinebbox(public.box2d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box2d, public.geometry) TO utente;


--
-- Name: FUNCTION st_combinebbox(public.box3d, public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.box3d) TO utente;


--
-- Name: FUNCTION st_combinebbox(public.box3d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.geometry) TO utente;


--
-- Name: FUNCTION st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean) TO utente;


--
-- Name: FUNCTION st_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_contains(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_contains(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_containsproperly(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_containsproperly(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_convexhull(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_convexhull(public.geometry) TO utente;


--
-- Name: FUNCTION st_coorddim(geometry public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coorddim(geometry public.geometry) TO utente;


--
-- Name: FUNCTION st_count(rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rast public.raster, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_count(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rast public.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_coveredby(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(text, text) TO utente;


--
-- Name: FUNCTION st_coveredby(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION st_coveredby(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_coveredby(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_covers(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(text, text) TO utente;


--
-- Name: FUNCTION st_covers(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION st_covers(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_covers(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_cpawithin(public.geometry, public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_cpawithin(public.geometry, public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_createoverview(tab regclass, col name, factor integer, algo text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_createoverview(tab regclass, col name, factor integer, algo text) TO utente;


--
-- Name: FUNCTION st_crosses(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_crosses(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_curvetoline(geom public.geometry, tol double precision, toltype integer, flags integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_curvetoline(geom public.geometry, tol double precision, toltype integer, flags integer) TO utente;


--
-- Name: FUNCTION st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer) TO utente;


--
-- Name: FUNCTION st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision) TO utente;


--
-- Name: FUNCTION st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION st_difference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_difference(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_dimension(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dimension(public.geometry) TO utente;


--
-- Name: FUNCTION st_disjoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_disjoint(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_disjoint(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_distance(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(text, text) TO utente;


--
-- Name: FUNCTION st_distance(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION st_distance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_distance(public.geography, public.geography, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography, boolean) TO utente;


--
-- Name: FUNCTION st_distance_sphere(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance_sphere(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_distancecpa(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distancecpa(public.geometry, public.geometry) TO utente;


--
-- Name: FUNCTION st_distancesphere(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distancesphere(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_dump(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dump(public.geometry) TO utente;


--
-- Name: FUNCTION st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_dumppoints(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumppoints(public.geometry) TO utente;


--
-- Name: FUNCTION st_dumprings(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumprings(public.geometry) TO utente;


--
-- Name: FUNCTION st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]) TO utente;


--
-- Name: FUNCTION st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_dwithin(text, text, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(text, text, double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(public.geography, public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision) TO utente;


--
-- Name: FUNCTION st_dwithin(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision, boolean) TO utente;


--
-- Name: FUNCTION st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO utente;


--
-- Name: FUNCTION st_endpoint(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_endpoint(public.geometry) TO utente;


--
-- Name: FUNCTION st_envelope(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_envelope(public.geometry) TO utente;


--
-- Name: FUNCTION st_envelope(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_envelope(public.raster) TO utente;


--
-- Name: FUNCTION st_equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_equals(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_estimated_extent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimated_extent(text, text) TO utente;


--
-- Name: FUNCTION st_estimated_extent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimated_extent(text, text, text) TO utente;


--
-- Name: FUNCTION st_estimatedextent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text) TO utente;


--
-- Name: FUNCTION st_estimatedextent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text) TO utente;


--
-- Name: FUNCTION st_estimatedextent(text, text, text, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text, boolean) TO utente;


--
-- Name: FUNCTION st_expand(public.box2d, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(public.box2d, double precision) TO utente;


--
-- Name: FUNCTION st_expand(public.box3d, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(public.box3d, double precision) TO utente;


--
-- Name: FUNCTION st_expand(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_expand(box public.box2d, dx double precision, dy double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(box public.box2d, dx double precision, dy double precision) TO utente;


--
-- Name: FUNCTION st_expand(box public.box3d, dx double precision, dy double precision, dz double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(box public.box3d, dx double precision, dy double precision, dz double precision) TO utente;


--
-- Name: FUNCTION st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision) TO utente;


--
-- Name: FUNCTION st_exteriorring(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_exteriorring(public.geometry) TO utente;


--
-- Name: FUNCTION st_filterbym(public.geometry, double precision, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_filterbym(public.geometry, double precision, double precision, boolean) TO utente;


--
-- Name: FUNCTION st_find_extent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_find_extent(text, text) TO utente;


--
-- Name: FUNCTION st_find_extent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_find_extent(text, text, text) TO utente;


--
-- Name: FUNCTION st_findextent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_findextent(text, text) TO utente;


--
-- Name: FUNCTION st_findextent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_findextent(text, text, text) TO utente;


--
-- Name: FUNCTION st_flipcoordinates(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_flipcoordinates(public.geometry) TO utente;


--
-- Name: FUNCTION st_force2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force2d(public.geometry) TO utente;


--
-- Name: FUNCTION st_force3d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force3d(public.geometry) TO utente;


--
-- Name: FUNCTION st_force3dm(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force3dm(public.geometry) TO utente;


--
-- Name: FUNCTION st_force3dz(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force3dz(public.geometry) TO utente;


--
-- Name: FUNCTION st_force4d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force4d(public.geometry) TO utente;


--
-- Name: FUNCTION st_force_2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_2d(public.geometry) TO utente;


--
-- Name: FUNCTION st_force_3d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_3d(public.geometry) TO utente;


--
-- Name: FUNCTION st_force_3dm(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_3dm(public.geometry) TO utente;


--
-- Name: FUNCTION st_force_3dz(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_3dz(public.geometry) TO utente;


--
-- Name: FUNCTION st_force_4d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_4d(public.geometry) TO utente;


--
-- Name: FUNCTION st_force_collection(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_collection(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcecollection(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcecollection(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcecurve(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcecurve(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcepolygonccw(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcepolygonccw(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcepolygoncw(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcepolygoncw(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcerhr(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcerhr(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcesfs(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry) TO utente;


--
-- Name: FUNCTION st_forcesfs(public.geometry, version text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry, version text) TO utente;


--
-- Name: FUNCTION st_frechetdistance(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_frechetdistance(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_fromgdalraster(gdaldata bytea, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_fromgdalraster(gdaldata bytea, srid integer) TO utente;


--
-- Name: FUNCTION st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT can_read boolean, OUT can_write boolean, OUT create_options text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT can_read boolean, OUT can_write boolean, OUT create_options text) TO utente;


--
-- Name: FUNCTION st_generatepoints(area public.geometry, npoints numeric); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_generatepoints(area public.geometry, npoints numeric) TO utente;


--
-- Name: FUNCTION st_geogfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geogfromtext(text) TO utente;


--
-- Name: FUNCTION st_geogfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geogfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geographyfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geographyfromtext(text) TO utente;


--
-- Name: FUNCTION st_geohash(geog public.geography, maxchars integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geohash(geog public.geography, maxchars integer) TO utente;


--
-- Name: FUNCTION st_geohash(geom public.geometry, maxchars integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geohash(geom public.geometry, maxchars integer) TO utente;


--
-- Name: FUNCTION st_geomcollfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromtext(text) TO utente;


--
-- Name: FUNCTION st_geomcollfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_geomcollfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomcollfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean) TO utente;


--
-- Name: FUNCTION st_geometryfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometryfromtext(text) TO utente;


--
-- Name: FUNCTION st_geometryfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometryfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_geometryn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometryn(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_geometrytype(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometrytype(public.geometry) TO utente;


--
-- Name: FUNCTION st_geomfromewkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromewkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomfromewkt(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromewkt(text) TO utente;


--
-- Name: FUNCTION st_geomfromgeohash(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgeohash(text, integer) TO utente;


--
-- Name: FUNCTION st_geomfromgeojson(json); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgeojson(json) TO utente;


--
-- Name: FUNCTION st_geomfromgeojson(jsonb); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgeojson(jsonb) TO utente;


--
-- Name: FUNCTION st_geomfromgeojson(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgeojson(text) TO utente;


--
-- Name: FUNCTION st_geomfromgml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgml(text) TO utente;


--
-- Name: FUNCTION st_geomfromgml(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgml(text, integer) TO utente;


--
-- Name: FUNCTION st_geomfromkml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromkml(text) TO utente;


--
-- Name: FUNCTION st_geomfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromtext(text) TO utente;


--
-- Name: FUNCTION st_geomfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_geomfromtwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromtwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_geomfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_georeference(rast public.raster, format text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_georeference(rast public.raster, format text) TO utente;


--
-- Name: FUNCTION st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision) TO utente;


--
-- Name: FUNCTION st_gmltosql(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_gmltosql(text) TO utente;


--
-- Name: FUNCTION st_gmltosql(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_gmltosql(text, integer) TO utente;


--
-- Name: FUNCTION st_grayscale(rastbandargset public.rastbandarg[], extenttype text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_grayscale(rastbandargset public.rastbandarg[], extenttype text) TO utente;


--
-- Name: FUNCTION st_grayscale(rast public.raster, redband integer, greenband integer, blueband integer, extenttype text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_grayscale(rast public.raster, redband integer, greenband integer, blueband integer, extenttype text) TO utente;


--
-- Name: FUNCTION st_hasarc(geometry public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hasarc(geometry public.geometry) TO utente;


--
-- Name: FUNCTION st_hasnoband(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hasnoband(rast public.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_height(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_height(public.raster) TO utente;


--
-- Name: FUNCTION st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_interiorringn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_interiorringn(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_interpolatepoint(line public.geometry, point public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_interpolatepoint(line public.geometry, point public.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(text, text) TO utente;


--
-- Name: FUNCTION st_intersection(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION st_intersection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(rast public.raster, geomin public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, geomin public.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(geomin public.geometry, rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(geomin public.geometry, rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_intersection(rast public.raster, band integer, geomin public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, band integer, geomin public.geometry) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]) TO utente;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_intersects(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(text, text) TO utente;


--
-- Name: FUNCTION st_intersects(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(public.geography, public.geography) TO utente;


--
-- Name: FUNCTION st_intersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_intersects(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_intersects(geom public.geometry, rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(geom public.geometry, rast public.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_intersects(rast public.raster, nband integer, geom public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, nband integer, geom public.geometry) TO utente;


--
-- Name: FUNCTION st_intersects(rast public.raster, geom public.geometry, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, geom public.geometry, nband integer) TO utente;


--
-- Name: FUNCTION st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_isclosed(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isclosed(public.geometry) TO utente;


--
-- Name: FUNCTION st_iscollection(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_iscollection(public.geometry) TO utente;


--
-- Name: FUNCTION st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer) TO utente;


--
-- Name: FUNCTION st_isempty(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isempty(public.geometry) TO utente;


--
-- Name: FUNCTION st_isempty(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isempty(rast public.raster) TO utente;


--
-- Name: FUNCTION st_ispolygonccw(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ispolygonccw(public.geometry) TO utente;


--
-- Name: FUNCTION st_ispolygoncw(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ispolygoncw(public.geometry) TO utente;


--
-- Name: FUNCTION st_isring(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isring(public.geometry) TO utente;


--
-- Name: FUNCTION st_issimple(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_issimple(public.geometry) TO utente;


--
-- Name: FUNCTION st_isvalid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalid(public.geometry) TO utente;


--
-- Name: FUNCTION st_isvalid(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalid(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_isvaliddetail(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry) TO utente;


--
-- Name: FUNCTION st_isvaliddetail(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_isvalidreason(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry) TO utente;


--
-- Name: FUNCTION st_isvalidreason(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_isvalidtrajectory(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalidtrajectory(public.geometry) TO utente;


--
-- Name: FUNCTION st_length(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length(text) TO utente;


--
-- Name: FUNCTION st_length(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length(public.geometry) TO utente;


--
-- Name: FUNCTION st_length(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length(geog public.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_length2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length2d(public.geometry) TO utente;


--
-- Name: FUNCTION st_length2d_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length2d_spheroid(public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_length2dspheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length2dspheroid(public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_length_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length_spheroid(public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_lengthspheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_lengthspheroid(public.geometry, public.spheroid) TO utente;


--
-- Name: FUNCTION st_line_interpolate_point(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_line_interpolate_point(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_line_locate_point(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_line_locate_point(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_line_substring(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_line_substring(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_linefromencodedpolyline(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromencodedpolyline(text, integer) TO utente;


--
-- Name: FUNCTION st_linefrommultipoint(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefrommultipoint(public.geometry) TO utente;


--
-- Name: FUNCTION st_linefromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromtext(text) TO utente;


--
-- Name: FUNCTION st_linefromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_linefromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_linefromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_lineinterpolatepoint(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_lineinterpolatepoint(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_lineinterpolatepoints(public.geometry, double precision, repeat boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_lineinterpolatepoints(public.geometry, double precision, repeat boolean) TO utente;


--
-- Name: FUNCTION st_linelocatepoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linelocatepoint(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_linemerge(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linemerge(public.geometry) TO utente;


--
-- Name: FUNCTION st_linestringfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_linestringfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_linesubstring(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linesubstring(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_linetocurve(geometry public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linetocurve(geometry public.geometry) TO utente;


--
-- Name: FUNCTION st_locate_along_measure(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locate_along_measure(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_locate_between_measures(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locate_between_measures(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision) TO utente;


--
-- Name: FUNCTION st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision) TO utente;


--
-- Name: FUNCTION st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision) TO utente;


--
-- Name: FUNCTION st_longestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_longestline(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_m(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_m(public.geometry) TO utente;


--
-- Name: FUNCTION st_makebox2d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makebox2d(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_makeemptycoverage(tilewidth integer, tileheight integer, width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptycoverage(tilewidth integer, tileheight integer, width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO utente;


--
-- Name: FUNCTION st_makeemptyraster(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(rast public.raster) TO utente;


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision) TO utente;


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO utente;


--
-- Name: FUNCTION st_makeenvelope(double precision, double precision, double precision, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeenvelope(double precision, double precision, double precision, double precision, integer) TO utente;


--
-- Name: FUNCTION st_makeline(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeline(public.geometry[]) TO utente;


--
-- Name: FUNCTION st_makeline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeline(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_makepoint(double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepointm(double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepointm(double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_makepolygon(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry) TO utente;


--
-- Name: FUNCTION st_makepolygon(public.geometry, public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry, public.geometry[]) TO utente;


--
-- Name: FUNCTION st_makevalid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makevalid(public.geometry) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_maxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_mem_size(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mem_size(public.geometry) TO utente;


--
-- Name: FUNCTION st_memsize(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memsize(public.geometry) TO utente;


--
-- Name: FUNCTION st_memsize(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memsize(public.raster) TO utente;


--
-- Name: FUNCTION st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer) TO utente;


--
-- Name: FUNCTION st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_minconvexhull(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minconvexhull(rast public.raster, nband integer) TO utente;


--
-- Name: FUNCTION st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer) TO utente;


--
-- Name: FUNCTION st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision) TO utente;


--
-- Name: FUNCTION st_minimumclearance(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumclearance(public.geometry) TO utente;


--
-- Name: FUNCTION st_minimumclearanceline(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumclearanceline(public.geometry) TO utente;


--
-- Name: FUNCTION st_minpossiblevalue(pixeltype text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minpossiblevalue(pixeltype text) TO utente;


--
-- Name: FUNCTION st_mlinefromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromtext(text) TO utente;


--
-- Name: FUNCTION st_mlinefromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_mlinefromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_mlinefromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_mpointfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromtext(text) TO utente;


--
-- Name: FUNCTION st_mpointfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_mpointfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_mpointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_mpolyfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromtext(text) TO utente;


--
-- Name: FUNCTION st_mpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_mpolyfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_mpolyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_multi(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multi(public.geometry) TO utente;


--
-- Name: FUNCTION st_multilinefromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multilinefromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_multilinestringfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text) TO utente;


--
-- Name: FUNCTION st_multilinestringfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_multipointfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipointfromtext(text) TO utente;


--
-- Name: FUNCTION st_multipointfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_multipointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_multipolyfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_multipolyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_multipolygonfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text) TO utente;


--
-- Name: FUNCTION st_multipolygonfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_ndims(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ndims(public.geometry) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_node(g public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_node(g public.geometry) TO utente;


--
-- Name: FUNCTION st_normalize(geom public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_normalize(geom public.geometry) TO utente;


--
-- Name: FUNCTION st_notsamealignmentreason(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_notsamealignmentreason(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_npoints(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_npoints(public.geometry) TO utente;


--
-- Name: FUNCTION st_nrings(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nrings(public.geometry) TO utente;


--
-- Name: FUNCTION st_numbands(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numbands(public.raster) TO utente;


--
-- Name: FUNCTION st_numgeometries(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numgeometries(public.geometry) TO utente;


--
-- Name: FUNCTION st_numinteriorring(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numinteriorring(public.geometry) TO utente;


--
-- Name: FUNCTION st_numinteriorrings(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numinteriorrings(public.geometry) TO utente;


--
-- Name: FUNCTION st_numpatches(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numpatches(public.geometry) TO utente;


--
-- Name: FUNCTION st_numpoints(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numpoints(public.geometry) TO utente;


--
-- Name: FUNCTION st_offsetcurve(line public.geometry, distance double precision, params text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_offsetcurve(line public.geometry, distance double precision, params text) TO utente;


--
-- Name: FUNCTION st_orderingequals(geometrya public.geometry, geometryb public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO utente;


--
-- Name: FUNCTION st_orientedenvelope(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_orientedenvelope(public.geometry) TO utente;


--
-- Name: FUNCTION st_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_overlaps(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_overlaps(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_patchn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_patchn(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_perimeter(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_perimeter(public.geometry) TO utente;


--
-- Name: FUNCTION st_perimeter(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_perimeter(geog public.geography, use_spheroid boolean) TO utente;


--
-- Name: FUNCTION st_perimeter2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_perimeter2d(public.geometry) TO utente;


--
-- Name: FUNCTION st_pixelascentroid(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelascentroid(rast public.raster, x integer, y integer) TO utente;


--
-- Name: FUNCTION st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspoint(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspoint(rast public.raster, x integer, y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspolygon(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspolygon(rast public.raster, x integer, y integer) TO utente;


--
-- Name: FUNCTION st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelheight(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelheight(public.raster) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO utente;


--
-- Name: FUNCTION st_pixelwidth(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelwidth(public.raster) TO utente;


--
-- Name: FUNCTION st_point(double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_point(double precision, double precision) TO utente;


--
-- Name: FUNCTION st_point_inside_circle(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_point_inside_circle(public.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_pointfromgeohash(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromgeohash(text, integer) TO utente;


--
-- Name: FUNCTION st_pointfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromtext(text) TO utente;


--
-- Name: FUNCTION st_pointfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_pointfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_pointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_pointinsidecircle(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointinsidecircle(public.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_pointn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointn(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_pointonsurface(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointonsurface(public.geometry) TO utente;


--
-- Name: FUNCTION st_points(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_points(public.geometry) TO utente;


--
-- Name: FUNCTION st_polyfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromtext(text) TO utente;


--
-- Name: FUNCTION st_polyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_polyfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_polyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_polygon(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygon(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_polygon(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygon(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_polygonfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromtext(text) TO utente;


--
-- Name: FUNCTION st_polygonfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromtext(text, integer) TO utente;


--
-- Name: FUNCTION st_polygonfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_polygonfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea, integer) TO utente;


--
-- Name: FUNCTION st_polygonize(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonize(public.geometry[]) TO utente;


--
-- Name: FUNCTION st_project(geog public.geography, distance double precision, azimuth double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_project(geog public.geography, distance double precision, azimuth double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO utente;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision) TO utente;


--
-- Name: FUNCTION st_quantizecoordinates(g public.geometry, prec_x integer, prec_y integer, prec_z integer, prec_m integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantizecoordinates(g public.geometry, prec_x integer, prec_y integer, prec_z integer, prec_m integer) TO utente;


--
-- Name: FUNCTION st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordx(rast public.raster, xr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordx(rast public.raster, xr integer, yr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer, yr integer) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordy(rast public.raster, yr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, yr integer) TO utente;


--
-- Name: FUNCTION st_rastertoworldcoordy(rast public.raster, xr integer, yr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, xr integer, yr integer) TO utente;


--
-- Name: FUNCTION st_rastfromhexwkb(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastfromhexwkb(text) TO utente;


--
-- Name: FUNCTION st_rastfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastfromwkb(bytea) TO utente;


--
-- Name: FUNCTION st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO utente;


--
-- Name: FUNCTION st_reclass(rast public.raster, reclassexpr text, pixeltype text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, reclassexpr text, pixeltype text) TO utente;


--
-- Name: FUNCTION st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, text) TO utente;


--
-- Name: FUNCTION st_relatematch(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relatematch(text, text) TO utente;


--
-- Name: FUNCTION st_removepoint(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_removepoint(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_removerepeatedpoints(geom public.geometry, tolerance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_removerepeatedpoints(geom public.geometry, tolerance double precision) TO utente;


--
-- Name: FUNCTION st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean) TO utente;


--
-- Name: FUNCTION st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text) TO utente;


--
-- Name: FUNCTION st_reverse(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reverse(public.geometry) TO utente;


--
-- Name: FUNCTION st_rotate(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotate(public.geometry, double precision, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, public.geometry) TO utente;


--
-- Name: FUNCTION st_rotate(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_rotatex(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotatex(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotatey(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotatey(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotatez(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotatez(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_rotation(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotation(public.raster) TO utente;


--
-- Name: FUNCTION st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_samealignment(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_samealignment(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision) TO utente;


--
-- Name: FUNCTION st_scale(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, public.geometry) TO utente;


--
-- Name: FUNCTION st_scale(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_scale(public.geometry, public.geometry, origin public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, public.geometry, origin public.geometry) TO utente;


--
-- Name: FUNCTION st_scale(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_scalex(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scalex(public.raster) TO utente;


--
-- Name: FUNCTION st_scaley(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scaley(public.raster) TO utente;


--
-- Name: FUNCTION st_segmentize(geog public.geography, max_segment_length double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_segmentize(geog public.geography, max_segment_length double precision) TO utente;


--
-- Name: FUNCTION st_segmentize(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_segmentize(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_setbandindex(rast public.raster, band integer, outdbindex integer, force boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandindex(rast public.raster, band integer, outdbindex integer, force boolean) TO utente;


--
-- Name: FUNCTION st_setbandisnodata(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandisnodata(rast public.raster, band integer) TO utente;


--
-- Name: FUNCTION st_setbandnodatavalue(rast public.raster, nodatavalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, nodatavalue double precision) TO utente;


--
-- Name: FUNCTION st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean) TO utente;


--
-- Name: FUNCTION st_setbandpath(rast public.raster, band integer, outdbpath text, outdbindex integer, force boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandpath(rast public.raster, band integer, outdbpath text, outdbindex integer, force boolean) TO utente;


--
-- Name: FUNCTION st_seteffectivearea(public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_seteffectivearea(public.geometry, double precision, integer) TO utente;


--
-- Name: FUNCTION st_setgeoreference(rast public.raster, georef text, format text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, georef text, format text) TO utente;


--
-- Name: FUNCTION st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision) TO utente;


--
-- Name: FUNCTION st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision) TO utente;


--
-- Name: FUNCTION st_setpoint(public.geometry, integer, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setpoint(public.geometry, integer, public.geometry) TO utente;


--
-- Name: FUNCTION st_setrotation(rast public.raster, rotation double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setrotation(rast public.raster, rotation double precision) TO utente;


--
-- Name: FUNCTION st_setscale(rast public.raster, scale double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scale double precision) TO utente;


--
-- Name: FUNCTION st_setscale(rast public.raster, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scalex double precision, scaley double precision) TO utente;


--
-- Name: FUNCTION st_setskew(rast public.raster, skew double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skew double precision) TO utente;


--
-- Name: FUNCTION st_setskew(rast public.raster, skewx double precision, skewy double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skewx double precision, skewy double precision) TO utente;


--
-- Name: FUNCTION st_setsrid(geog public.geography, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setsrid(geog public.geography, srid integer) TO utente;


--
-- Name: FUNCTION st_setsrid(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setsrid(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_setsrid(rast public.raster, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setsrid(rast public.raster, srid integer) TO utente;


--
-- Name: FUNCTION st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast public.raster, geom public.geometry, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, geom public.geometry, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast public.raster, x integer, y integer, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, x integer, y integer, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision) TO utente;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO utente;


--
-- Name: FUNCTION st_sharedpaths(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_sharedpaths(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_shift_longitude(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_shift_longitude(public.geometry) TO utente;


--
-- Name: FUNCTION st_shiftlongitude(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_shiftlongitude(public.geometry) TO utente;


--
-- Name: FUNCTION st_shortestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_shortestline(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_simplify(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_simplify(public.geometry, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision, boolean) TO utente;


--
-- Name: FUNCTION st_simplifypreservetopology(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplifypreservetopology(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_simplifyvw(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplifyvw(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_skewx(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_skewx(public.raster) TO utente;


--
-- Name: FUNCTION st_skewy(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_skewy(public.raster) TO utente;


--
-- Name: FUNCTION st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_snap(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snap(geom1 public.geometry, geom2 public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO utente;


--
-- Name: FUNCTION st_split(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_split(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_srid(geog public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_srid(geog public.geography) TO utente;


--
-- Name: FUNCTION st_srid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_srid(public.geometry) TO utente;


--
-- Name: FUNCTION st_srid(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_srid(public.raster) TO utente;


--
-- Name: FUNCTION st_startpoint(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_startpoint(public.geometry) TO utente;


--
-- Name: FUNCTION st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_subdivide(geom public.geometry, maxvertices integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_subdivide(geom public.geometry, maxvertices integer) TO utente;


--
-- Name: FUNCTION st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO utente;


--
-- Name: FUNCTION st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO utente;


--
-- Name: FUNCTION st_summary(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summary(public.geography) TO utente;


--
-- Name: FUNCTION st_summary(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summary(public.geometry) TO utente;


--
-- Name: FUNCTION st_summary(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summary(rast public.raster) TO utente;


--
-- Name: FUNCTION st_summarystats(rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_swapordinates(geom public.geometry, ords cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_swapordinates(geom public.geometry, ords cstring) TO utente;


--
-- Name: FUNCTION st_symdifference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_symdifference(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_symmetricdifference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_symmetricdifference(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision) TO utente;


--
-- Name: FUNCTION st_touches(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_touches(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_touches(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_transform(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(public.geometry, integer) TO utente;


--
-- Name: FUNCTION st_transform(geom public.geometry, to_proj text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, to_proj text) TO utente;


--
-- Name: FUNCTION st_transform(geom public.geometry, from_proj text, to_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_srid integer) TO utente;


--
-- Name: FUNCTION st_transform(geom public.geometry, from_proj text, to_proj text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_proj text) TO utente;


--
-- Name: FUNCTION st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO utente;


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO utente;


--
-- Name: FUNCTION st_translate(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_translate(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_transscale(public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transscale(public.geometry, double precision, double precision, double precision, double precision) TO utente;


--
-- Name: FUNCTION st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO utente;


--
-- Name: FUNCTION st_unaryunion(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_unaryunion(public.geometry) TO utente;


--
-- Name: FUNCTION st_union(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.geometry[]) TO utente;


--
-- Name: FUNCTION st_union(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_upperleftx(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_upperleftx(public.raster) TO utente;


--
-- Name: FUNCTION st_upperlefty(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_upperlefty(public.raster) TO utente;


--
-- Name: FUNCTION st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean) TO utente;


--
-- Name: FUNCTION st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rast public.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO utente;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO utente;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO utente;


--
-- Name: FUNCTION st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO utente;


--
-- Name: FUNCTION st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO utente;


--
-- Name: FUNCTION st_width(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_width(public.raster) TO utente;


--
-- Name: FUNCTION st_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_within(geom1 public.geometry, geom2 public.geometry) TO utente;


--
-- Name: FUNCTION st_within(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, rast2 public.raster) TO utente;


--
-- Name: FUNCTION st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO utente;


--
-- Name: FUNCTION st_wkbtosql(wkb bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_wkbtosql(wkb bytea) TO utente;


--
-- Name: FUNCTION st_wkttosql(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_wkttosql(text) TO utente;


--
-- Name: FUNCTION st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer) TO utente;


--
-- Name: FUNCTION st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, xw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, pt public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, pt public.geometry) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, yw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, yw double precision) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, pt public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, pt public.geometry) TO utente;


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision) TO utente;


--
-- Name: FUNCTION st_wrapx(geom public.geometry, wrap double precision, move double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_wrapx(geom public.geometry, wrap double precision, move double precision) TO utente;


--
-- Name: FUNCTION st_x(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_x(public.geometry) TO utente;


--
-- Name: FUNCTION st_xmax(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_xmax(public.box3d) TO utente;


--
-- Name: FUNCTION st_xmin(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_xmin(public.box3d) TO utente;


--
-- Name: FUNCTION st_y(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_y(public.geometry) TO utente;


--
-- Name: FUNCTION st_ymax(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ymax(public.box3d) TO utente;


--
-- Name: FUNCTION st_ymin(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ymin(public.box3d) TO utente;


--
-- Name: FUNCTION st_z(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_z(public.geometry) TO utente;


--
-- Name: FUNCTION st_zmax(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_zmax(public.box3d) TO utente;


--
-- Name: FUNCTION st_zmflag(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_zmflag(public.geometry) TO utente;


--
-- Name: FUNCTION st_zmin(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_zmin(public.box3d) TO utente;


--
-- Name: FUNCTION unlockrows(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.unlockrows(text) TO utente;


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, integer) TO utente;


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, character varying, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, character varying, integer) TO utente;


--
-- Name: FUNCTION updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer) TO utente;


--
-- Name: FUNCTION updaterastersrid(table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updaterastersrid(table_name name, column_name name, new_srid integer) TO utente;


--
-- Name: FUNCTION updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO utente;


--
-- Name: FUNCTION st_3dextent(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dextent(public.geometry) TO utente;


--
-- Name: FUNCTION st_accum(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_accum(public.geometry) TO utente;


--
-- Name: FUNCTION st_asgeobuf(anyelement); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeobuf(anyelement) TO utente;


--
-- Name: FUNCTION st_asgeobuf(anyelement, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeobuf(anyelement, text) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement, text) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement, text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement, text, integer) TO utente;


--
-- Name: FUNCTION st_asmvt(anyelement, text, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement, text, integer, text) TO utente;


--
-- Name: FUNCTION st_clusterintersecting(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry) TO utente;


--
-- Name: FUNCTION st_clusterwithin(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry, double precision) TO utente;


--
-- Name: FUNCTION st_collect(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collect(public.geometry) TO utente;


--
-- Name: FUNCTION st_countagg(public.raster, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, boolean) TO utente;


--
-- Name: FUNCTION st_countagg(public.raster, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean) TO utente;


--
-- Name: FUNCTION st_countagg(public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean, double precision) TO utente;


--
-- Name: FUNCTION st_extent(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_extent(public.geometry) TO utente;


--
-- Name: FUNCTION st_makeline(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeline(public.geometry) TO utente;


--
-- Name: FUNCTION st_memcollect(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memcollect(public.geometry) TO utente;


--
-- Name: FUNCTION st_memunion(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memunion(public.geometry) TO utente;


--
-- Name: FUNCTION st_polygonize(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonize(public.geometry) TO utente;


--
-- Name: FUNCTION st_samealignment(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_samealignment(public.raster) TO utente;


--
-- Name: FUNCTION st_summarystatsagg(public.raster, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, boolean, double precision) TO utente;


--
-- Name: FUNCTION st_summarystatsagg(public.raster, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean) TO utente;


--
-- Name: FUNCTION st_summarystatsagg(public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean, double precision) TO utente;


--
-- Name: FUNCTION st_union(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.geometry) TO utente;


--
-- Name: FUNCTION st_union(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster) TO utente;


--
-- Name: FUNCTION st_union(public.raster, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, integer) TO utente;


--
-- Name: FUNCTION st_union(public.raster, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, text) TO utente;


--
-- Name: FUNCTION st_union(public.raster, public.unionarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, public.unionarg[]) TO utente;


--
-- Name: FUNCTION st_union(public.raster, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, integer, text) TO utente;


--
-- Name: TABLE fnloc2; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.fnloc2 TO utente;


--
-- Name: TABLE fschedac; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.fschedac TO utente;


--
-- Name: SEQUENCE fschedac_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.fschedac_id_seq TO utente;


--
-- Name: TABLE fscossec2; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.fscossec2 TO utente;


--
-- Name: TABLE geography_columns; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.geography_columns TO utente;


--
-- Name: TABLE geometry_columns; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.geometry_columns TO utente;


--
-- Name: TABLE raster_columns; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.raster_columns TO utente;


--
-- Name: TABLE raster_overviews; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.raster_overviews TO utente;


--
-- Name: TABLE spatial_ref_sys; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.spatial_ref_sys TO utente;


--
-- Name: TABLE vclassif; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vclassif TO utente;


--
-- Name: TABLE vcodric; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vcodric TO utente;


--
-- Name: TABLE vcommenti; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vcommenti TO utente;


--
-- Name: TABLE vdemo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vdemo TO utente;


--
-- Name: TABLE vedifici; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vedifici TO utente;


--
-- Name: TABLE veffetti; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.veffetti TO utente;


--
-- Name: TABLE vfontidemo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vfontidemo TO utente;


--
-- Name: TABLE vicon; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vicon TO utente;


--
-- Name: TABLE vnfiu; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vnfiu TO utente;


--
-- Name: TABLE vnloc; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vnloc TO utente;


--
-- Name: TABLE vscheda; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vscheda TO utente;


--
-- Name: TABLE vscheda1; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vscheda1 TO utente;


--
-- Name: TABLE vschedac; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vschedac TO utente;


--
-- Name: TABLE vschedad; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vschedad TO utente;


--
-- Name: TABLE vschedae; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vschedae TO utente;


--
-- Name: TABLE vschedeb; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vschedeb TO utente;


--
-- Name: TABLE vscossec; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vscossec TO utente;


--
-- Name: TABLE vtabcatego; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vtabcatego TO utente;


--
-- Name: TABLE vtipfen; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vtipfen TO utente;


--
-- Name: TABLE vvalfonte; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.vvalfonte TO utente;


--
-- PostgreSQL database dump complete
--

